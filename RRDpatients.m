%% RRD patients
%% Patient 4
p4sigfilenames=['ARTS LOPES - Copy\4\recordings\20160222_082058.signals'; 'ARTS LOPES - Copy\4\recordings\20160224_091707.signals'; 'ARTS LOPES - Copy\4\recordings\20160226_150856.signals'; 'ARTS LOPES - Copy\4\recordings\20160302_163809.signals';  
'ARTS LOPES - Copy\4\recordings\20160222_082406.signals'; 'ARTS LOPES - Copy\4\recordings\20160224_091953.signals'; 'ARTS LOPES - Copy\4\recordings\20160301_134755.signals'; 'ARTS LOPES - Copy\4\recordings\20160304_103031.signals';  
'ARTS LOPES - Copy\4\recordings\20160222_082525.signals'; 'ARTS LOPES - Copy\4\recordings\20160224_163330.signals'; 'ARTS LOPES - Copy\4\recordings\20160301_140339.signals'; 'ARTS LOPES - Copy\4\recordings\20160304_104420.signals'; 
'ARTS LOPES - Copy\4\recordings\20160222_082918.signals'; 'ARTS LOPES - Copy\4\recordings\20160224_164212.signals'; 'ARTS LOPES - Copy\4\recordings\20160302_161300.signals'; 'ARTS LOPES - Copy\4\recordings\20160308_113131.signals';  
'ARTS LOPES - Copy\4\recordings\20160223_113449.signals'; 'ARTS LOPES - Copy\4\recordings\20160224_165043.signals'; 'ARTS LOPES - Copy\4\recordings\20160302_162810.signals'; 'ARTS LOPES - Copy\4\recordings\20160308_113733.signals';  
'ARTS LOPES - Copy\4\recordings\20160223_113600.signals'; 'ARTS LOPES - Copy\4\recordings\20160224_165550.signals'; 'ARTS LOPES - Copy\4\recordings\20160302_163304.signals';  
'ARTS LOPES - Copy\4\recordings\20160223_114017.signals'; 'ARTS LOPES - Copy\4\recordings\20160226_150045.signals'; 'ARTS LOPES - Copy\4\recordings\20160302_163354.signals'];  
p4sigfilenames = cellstr(p4sigfilenames);  

p4paramfilenames=['ARTS LOPES - Copy\4\recordings\20160222_082058.params'; 'ARTS LOPES - Copy\4\recordings\20160224_091707.params'; 'ARTS LOPES - Copy\4\recordings\20160226_150856.params'; 'ARTS LOPES - Copy\4\recordings\20160302_163809.params';  
'ARTS LOPES - Copy\4\recordings\20160222_082406.params'; 'ARTS LOPES - Copy\4\recordings\20160224_091953.params'; 'ARTS LOPES - Copy\4\recordings\20160301_134755.params'; 'ARTS LOPES - Copy\4\recordings\20160304_103031.params';  
'ARTS LOPES - Copy\4\recordings\20160222_082525.params'; 'ARTS LOPES - Copy\4\recordings\20160224_163330.params'; 'ARTS LOPES - Copy\4\recordings\20160301_140339.params'; 'ARTS LOPES - Copy\4\recordings\20160304_104420.params';  
'ARTS LOPES - Copy\4\recordings\20160222_082918.params'; 'ARTS LOPES - Copy\4\recordings\20160224_164212.params'; 'ARTS LOPES - Copy\4\recordings\20160302_161300.params'; 'ARTS LOPES - Copy\4\recordings\20160308_113131.params';  
'ARTS LOPES - Copy\4\recordings\20160223_113449.params'; 'ARTS LOPES - Copy\4\recordings\20160224_165043.params'; 'ARTS LOPES - Copy\4\recordings\20160302_162810.params'; 'ARTS LOPES - Copy\4\recordings\20160308_113733.params';  
'ARTS LOPES - Copy\4\recordings\20160223_113600.params'; 'ARTS LOPES - Copy\4\recordings\20160224_165550.params'; 'ARTS LOPES - Copy\4\recordings\20160302_163304.params';  
'ARTS LOPES - Copy\4\recordings\20160223_114017.params'; 'ARTS LOPES - Copy\4\recordings\20160226_150045.params'; 'ARTS LOPES - Copy\4\recordings\20160302_163354.params']; 
p4paramfilenames = cellstr(p4paramfilenames); 

for i=1:length(p4sigfilenames)
    [D4(i),P4(i)]=readSignalsFile(p4sigfilenames{i,1},p4paramfilenames{i,1});
end

D4=OrderStructDate(D4);
P4=OrderStructDate(P4);

% % Calculate BWS (Body weight support)
% for i=1:length(D4)
%     BWS= (P4(i).data.Patient_Mass_kg-D4(i).data.m_cont_treadmillData_Fy)./P4(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P4(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P4(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D4(i).data.m_cont_stepCounter);
% end
% 
%    avBWS=avBWS';
%    avGGF=avGGF';
%    avSpeed=avSpeed';
%    steps=steps';

clear p4sigfilenames p4paramfilenames
 
% %join 5,6,7
% data5.BWS=vertcat(((P4(5).data.Patient_Mass_kg-D4(5).data.m_cont_treadmillData_Fy)./P4(5).data.Patient_Mass_kg.*100),((P4(6).data.Patient_Mass_kg-D4(6).data.m_cont_treadmillData_Fy)./P4(6).data.Patient_Mass_kg.*100),((P4(7).data.Patient_Mass_kg-D4(7).data.m_cont_treadmillData_Fy)./P4(7).data.Patient_Mass_kg.*100));
% meanBWS5=mean(data5.BWS);
% data5.steps=max(D4(5).data.m_cont_stepCounter)+max(D4(6).data.m_cont_stepCounter)+max(D4(7).data.m_cont_stepCounter);
% data5.speed=vertcat(P4(5).data.General_WalkingVelocity_m_s,P4(6).data.General_WalkingVelocity_m_s,P4(7).data.General_WalkingVelocity_m_s);
% meanspeed5=mean(data5.speed);
% data5.GGF=vertcat(P4(5).data.General_GuidanceForce_pct,(P4(6).data.General_GuidanceForce_pct),(P4(7).data.General_GuidanceForce_pct));
% meanGGF5=mean(data5.GGF);
% data5.duration=P4(5).duration+P4(6).duration+P4(7).duration;
% 
% %join 8,10,11,12,13
% data8.BWS=vertcat(((P4(8).data.Patient_Mass_kg-D4(8).data.m_cont_treadmillData_Fy)./P4(8).data.Patient_Mass_kg.*100),((P4(10).data.Patient_Mass_kg-D4(10).data.m_cont_treadmillData_Fy)./P4(10).data.Patient_Mass_kg.*100),((P4(11).data.Patient_Mass_kg-D4(11).data.m_cont_treadmillData_Fy)./P4(11).data.Patient_Mass_kg.*100),((P4(12).data.Patient_Mass_kg-D4(12).data.m_cont_treadmillData_Fy)./P4(12).data.Patient_Mass_kg.*100),((P4(13).data.Patient_Mass_kg-D4(13).data.m_cont_treadmillData_Fy)./P4(13).data.Patient_Mass_kg.*100));
% meanBWS8=mean(data8.BWS);
% data8.steps=max(D4(8).data.m_cont_stepCounter)+max(D4(10).data.m_cont_stepCounter)+max(D4(11).data.m_cont_stepCounter)+max(D4(12).data.m_cont_stepCounter)+max(D4(13).data.m_cont_stepCounter);
% data8.speed=vertcat(P4(8).data.General_WalkingVelocity_m_s,P4(10).data.General_WalkingVelocity_m_s,P4(11).data.General_WalkingVelocity_m_s,P4(12).data.General_WalkingVelocity_m_s,P4(13).data.General_WalkingVelocity_m_s);
% meanspeed8=mean(data8.speed);
% data8.GGF=vertcat(P4(8).data.General_GuidanceForce_pct,(P4(10).data.General_GuidanceForce_pct),(P4(11).data.General_GuidanceForce_pct),(P4(12).data.General_GuidanceForce_pct),(P4(13).data.General_GuidanceForce_pct));
% meanGGF8=mean(data8.GGF);
% data8.duration=P4(8).duration+P4(10).duration+P4(11).duration+P4(12).duration+P4(13).duration;
% 
% %join 14,15
% data14.BWS=vertcat(((P4(14).data.Patient_Mass_kg-D4(14).data.m_cont_treadmillData_Fy)./P4(14).data.Patient_Mass_kg.*100),((P4(15).data.Patient_Mass_kg-D4(15).data.m_cont_treadmillData_Fy)./P4(15).data.Patient_Mass_kg.*100));
% meanBWS14=mean(data14.BWS);
% data14.steps=max(D4(14).data.m_cont_stepCounter)+max(D4(15).data.m_cont_stepCounter);
% data14.speed=vertcat(P4(14).data.General_WalkingVelocity_m_s,P4(15).data.General_WalkingVelocity_m_s);
% meanspeed14=mean(data14.speed);
% data14.GGF=vertcat(P4(14).data.General_GuidanceForce_pct,(P4(15).data.General_GuidanceForce_pct));
% meanGGF14=mean(data14.GGF);
% data14.duration=P4(14).duration+P4(15).duration;
% 
% %join 16,17
% data16.BWS=vertcat(((P4(16).data.Patient_Mass_kg-D4(16).data.m_cont_treadmillData_Fy)./P4(16).data.Patient_Mass_kg.*100),((P4(17).data.Patient_Mass_kg-D4(17).data.m_cont_treadmillData_Fy)./P4(17).data.Patient_Mass_kg.*100));
% meanBWS16=mean(data16.BWS);
% data16.steps=max(D4(16).data.m_cont_stepCounter)+max(D4(17).data.m_cont_stepCounter);
% data16.speed=vertcat(P4(16).data.General_WalkingVelocity_m_s,P4(17).data.General_WalkingVelocity_m_s);
% meanspeed16=mean(data16.speed);
% data16.GGF=vertcat(P4(16).data.General_GuidanceForce_pct,(P4(17).data.General_GuidanceForce_pct));
% meanGGF16=mean(data16.GGF);
% data16.duration=P4(16).duration+P4(17).duration;
% 
% %join 18,19,20
% data18.BWS=vertcat(((P4(18).data.Patient_Mass_kg-D4(18).data.m_cont_treadmillData_Fy)./P4(18).data.Patient_Mass_kg.*100),((P4(19).data.Patient_Mass_kg-D4(19).data.m_cont_treadmillData_Fy)./P4(19).data.Patient_Mass_kg.*100),((P4(20).data.Patient_Mass_kg-D4(20).data.m_cont_treadmillData_Fy)./P4(20).data.Patient_Mass_kg.*100));
% meanBWS18=mean(data18.BWS);
% data18.steps=max(D4(18).data.m_cont_stepCounter)+max(D4(19).data.m_cont_stepCounter)+max(D4(20).data.m_cont_stepCounter);
% data18.speed=vertcat(P4(18).data.General_WalkingVelocity_m_s,P4(19).data.General_WalkingVelocity_m_s,P4(20).data.General_WalkingVelocity_m_s);
% meanspeed18=mean(data18.speed);
% data18.GGF=vertcat(P4(18).data.General_GuidanceForce_pct,(P4(19).data.General_GuidanceForce_pct),(P4(20).data.General_GuidanceForce_pct));
% meanGGF18=mean(data18.GGF);
% data18.duration=P4(18).duration+P4(19).duration+P4(20).duration;
% 
% %join 23,24
% data23.BWS=vertcat(((P4(23).data.Patient_Mass_kg-D4(23).data.m_cont_treadmillData_Fy)./P4(23).data.Patient_Mass_kg.*100),((P4(24).data.Patient_Mass_kg-D4(24).data.m_cont_treadmillData_Fy)./P4(24).data.Patient_Mass_kg.*100));
% meanBWS23=mean(data23.BWS);
% data23.steps=max(D4(23).data.m_cont_stepCounter)+max(D4(24).data.m_cont_stepCounter);
% data23.speed=vertcat(P4(23).data.General_WalkingVelocity_m_s,P4(24).data.General_WalkingVelocity_m_s);
% meanspeed23=mean(data23.speed);
% data23.GGF=vertcat(P4(23).data.General_GuidanceForce_pct,(P4(24).data.General_GuidanceForce_pct));
% meanGGF23=mean(data23.GGF);
% data23.duration=P4(23).duration+P4(24).duration;
% 
% %join 25,26
% data25.BWS=vertcat(((P4(25).data.Patient_Mass_kg-D4(25).data.m_cont_treadmillData_Fy)./P4(25).data.Patient_Mass_kg.*100),((P4(26).data.Patient_Mass_kg-D4(26).data.m_cont_treadmillData_Fy)./P4(26).data.Patient_Mass_kg.*100));
% meanBWS25=mean(data25.BWS);
% data25.steps=max(D4(25).data.m_cont_stepCounter)+max(D4(26).data.m_cont_stepCounter);
% data25.speed=vertcat(P4(25).data.General_WalkingVelocity_m_s,P4(26).data.General_WalkingVelocity_m_s);
% meanspeed25=mean(data25.speed);
% data25.GGF=vertcat(P4(25).data.General_GuidanceForce_pct,(P4(26).data.General_GuidanceForce_pct));
% meanGGF25=mean(data25.GGF);
% data25.duration=P4(25).duration+P4(26).duration;
 
%% Patient 10

p10sigfilenames=['ARTS LOPES - Copy\10\recordings\20160613_082814.signals'; 'ARTS LOPES - Copy\10\recordings\20160620_092031.signals'; 'ARTS LOPES - Copy\10\recordings\20160627_145209.signals'; 'ARTS LOPES - Copy\10\recordings\20160711_090459.signals';  
'ARTS LOPES - Copy\10\recordings\20160615_104004.signals';  'ARTS LOPES - Copy\10\recordings\20160622_130638.signals';  'ARTS LOPES - Copy\10\recordings\20160629_130459.signals';  'ARTS LOPES - Copy\10\recordings\20160713_130806.signals'; 
'ARTS LOPES - Copy\10\recordings\20160615_104746.signals';  'ARTS LOPES - Copy\10\recordings\20160622_130703.signals';  'ARTS LOPES - Copy\10\recordings\20160629_131857.signals';  'ARTS LOPES - Copy\10\recordings\20160713_131110.signals';  
'ARTS LOPES - Copy\10\recordings\20160617_145812.signals';  'ARTS LOPES - Copy\10\recordings\20160622_131855.signals';  'ARTS LOPES - Copy\10\recordings\20160701_151026.signals';  'ARTS LOPES - Copy\10\recordings\20160715_140614.signals';  
'ARTS LOPES - Copy\10\recordings\20160617_145838.signals';  'ARTS LOPES - Copy\10\recordings\20160624_150725.signals';  'ARTS LOPES - Copy\10\recordings\20160704_091002.signals';  'ARTS LOPES - Copy\10\recordings\20160715_140948.signals';  
'ARTS LOPES - Copy\10\recordings\20160617_150832.signals';  'ARTS LOPES - Copy\10\recordings\20160624_151638.signals';  'ARTS LOPES - Copy\10\recordings\20160704_091416.signals';  'ARTS LOPES - Copy\10\recordings\20160718_100847.signals';  
'ARTS LOPES - Copy\10\recordings\20160617_151747.signals';  'ARTS LOPES - Copy\10\recordings\20160624_152145.signals';  'ARTS LOPES - Copy\10\recordings\20160706_130008.signals';  'ARTS LOPES - Copy\10\recordings\20160720_163354.signals';  
'ARTS LOPES - Copy\10\recordings\20160620_091701.signals';  'ARTS LOPES - Copy\10\recordings\20160627_143858.signals';  'ARTS LOPES - Copy\10\recordings\20160708_103811.signals'];  
p10sigfilenames = cellstr(p10sigfilenames);  

p10paramfilenames=['ARTS LOPES - Copy\10\recordings\20160613_082814.params';  'ARTS LOPES - Copy\10\recordings\20160620_092031.params';  'ARTS LOPES - Copy\10\recordings\20160627_145209.params';  'ARTS LOPES - Copy\10\recordings\20160711_090459.params';  
'ARTS LOPES - Copy\10\recordings\20160615_104004.params';  'ARTS LOPES - Copy\10\recordings\20160622_130638.params';  'ARTS LOPES - Copy\10\recordings\20160629_130459.params';  'ARTS LOPES - Copy\10\recordings\20160713_130806.params';  
'ARTS LOPES - Copy\10\recordings\20160615_104746.params';  'ARTS LOPES - Copy\10\recordings\20160622_130703.params';  'ARTS LOPES - Copy\10\recordings\20160629_131857.params';  'ARTS LOPES - Copy\10\recordings\20160713_131110.params';  
'ARTS LOPES - Copy\10\recordings\20160617_145812.params';  'ARTS LOPES - Copy\10\recordings\20160622_131855.params';  'ARTS LOPES - Copy\10\recordings\20160701_151026.params';  'ARTS LOPES - Copy\10\recordings\20160715_140614.params';  
'ARTS LOPES - Copy\10\recordings\20160617_145838.params';  'ARTS LOPES - Copy\10\recordings\20160624_150725.params';  'ARTS LOPES - Copy\10\recordings\20160704_091002.params';  'ARTS LOPES - Copy\10\recordings\20160715_140948.params';  
'ARTS LOPES - Copy\10\recordings\20160617_150832.params';  'ARTS LOPES - Copy\10\recordings\20160624_151638.params';  'ARTS LOPES - Copy\10\recordings\20160704_091416.params';  'ARTS LOPES - Copy\10\recordings\20160718_100847.params';  
'ARTS LOPES - Copy\10\recordings\20160617_151747.params';  'ARTS LOPES - Copy\10\recordings\20160624_152145.params';  'ARTS LOPES - Copy\10\recordings\20160706_130008.params';  'ARTS LOPES - Copy\10\recordings\20160720_163354.params';  
'ARTS LOPES - Copy\10\recordings\20160620_091701.params';  'ARTS LOPES - Copy\10\recordings\20160627_143858.params';  'ARTS LOPES - Copy\10\recordings\20160708_103811.params']; 
p10paramfilenames = cellstr(p10paramfilenames); 

for i=1:length(p10sigfilenames)
    [D10(i),P10(i)]=readSignalsFile(p10sigfilenames{i,1},p10paramfilenames{i,1});
end

D10=OrderStructDate(D10);
P10=OrderStructDate(P10);

% for i=1:length(D10)
%     BWS= (P10(i).data.Patient_Mass_kg-D10(i).data.m_cont_treadmillData_Fy)./P10(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P10(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P10(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D10(i).data.m_cont_stepCounter);
% end
% 
%     avBWS=avBWS';
%     avGGF=avGGF';
%     avSpeed=avSpeed';
%     steps=steps';

clear p10sigfilenames p10paramfilenames

%join 2,3
% data2.BWS=vertcat(((P10(2).data.Patient_Mass_kg-D10(2).data.m_cont_treadmillData_Fy)./P10(2).data.Patient_Mass_kg.*100),((P10(3).data.Patient_Mass_kg-D10(3).data.m_cont_treadmillData_Fy)./P10(3).data.Patient_Mass_kg.*100));
% meanBWS2=mean(data2.BWS);
% data2.steps=max(D10(2).data.m_cont_stepCounter)+max(D10(13).data.m_cont_stepCounter);
% data2.speed=vertcat(P10(2).data.General_WalkingVelocity_m_s,P10(3).data.General_WalkingVelocity_m_s);
% meanspeed2=mean(data2.speed);
% data2.GGF=vertcat(P10(2).data.General_GuidanceForce_pct,(P10(3).data.General_GuidanceForce_pct));
% meanGGF2=mean(data2.GGF);
% data2.duration=P10(2).duration+P10(3).duration;
% 
% %join 5,6,7
% data5.BWS=vertcat(((P10(5).data.Patient_Mass_kg-D10(5).data.m_cont_treadmillData_Fy)./P10(5).data.Patient_Mass_kg.*100),((P10(6).data.Patient_Mass_kg-D10(6).data.m_cont_treadmillData_Fy)./P10(6).data.Patient_Mass_kg.*100),((P10(7).data.Patient_Mass_kg-D10(7).data.m_cont_treadmillData_Fy)./P10(7).data.Patient_Mass_kg.*100));
% meanBWS5=mean(data5.BWS);
% data5.steps=max(D10(5).data.m_cont_stepCounter)+max(D10(6).data.m_cont_stepCounter)+max(D10(7).data.m_cont_stepCounter);
% data5.speed=vertcat(P10(5).data.General_WalkingVelocity_m_s,P10(6).data.General_WalkingVelocity_m_s,P10(7).data.General_WalkingVelocity_m_s);
% meanspeed5=mean(data5.speed);
% data5.GGF=vertcat(P10(5).data.General_GuidanceForce_pct,(P10(6).data.General_GuidanceForce_pct),(P10(7).data.General_GuidanceForce_pct));
% meanGGF5=mean(data5.GGF);
% data5.duration=P10(5).duration+P10(6).duration+P10(7).duration;
% 
% %join 8,9
% data8.BWS=vertcat(((P10(8).data.Patient_Mass_kg-D10(8).data.m_cont_treadmillData_Fy)./P10(8).data.Patient_Mass_kg.*100),((P10(9).data.Patient_Mass_kg-D10(9).data.m_cont_treadmillData_Fy)./P10(9).data.Patient_Mass_kg.*100));
% meanBWS8=mean(data8.BWS);
% data8.steps=max(D10(8).data.m_cont_stepCounter)+max(D10(9).data.m_cont_stepCounter);
% data8.speed=vertcat(P10(8).data.General_WalkingVelocity_m_s,P10(9).data.General_WalkingVelocity_m_s);
% meanspeed8=mean(data8.speed);
% data8.GGF=vertcat(P10(8).data.General_GuidanceForce_pct,(P10(9).data.General_GuidanceForce_pct));
% meanGGF8=mean(data8.GGF);
% data8.duration=P10(8).duration+P10(9).duration;
% 
% 
% %join 11,12
% data11.BWS=vertcat(((P10(11).data.Patient_Mass_kg-D10(11).data.m_cont_treadmillData_Fy)./P10(11).data.Patient_Mass_kg.*100),((P10(12).data.Patient_Mass_kg-D10(12).data.m_cont_treadmillData_Fy)./P10(12).data.Patient_Mass_kg.*100));
% meanBWS11=mean(data11.BWS);
% data11.steps=max(D10(11).data.m_cont_stepCounter)+max(D10(12).data.m_cont_stepCounter);
% data11.speed=vertcat(P10(11).data.General_WalkingVelocity_m_s,P10(12).data.General_WalkingVelocity_m_s);
% meanspeed11=mean(data11.speed);
% data11.GGF=vertcat(P10(11).data.General_GuidanceForce_pct,(P10(12).data.General_GuidanceForce_pct));
% meanGGF11=mean(data11.GGF);
% data11.duration=P10(11).duration+P10(12).duration;
% 
% %join 13,14,15
% data13.BWS=vertcat(((P10(13).data.Patient_Mass_kg-D10(13).data.m_cont_treadmillData_Fy)./P10(13).data.Patient_Mass_kg.*100),((P10(14).data.Patient_Mass_kg-D10(14).data.m_cont_treadmillData_Fy)./P10(14).data.Patient_Mass_kg.*100),((P10(15).data.Patient_Mass_kg-D10(15).data.m_cont_treadmillData_Fy)./P10(15).data.Patient_Mass_kg.*100));
% meanBWS13=mean(data13.BWS);
% data13.steps=max(D10(13).data.m_cont_stepCounter)+max(D10(14).data.m_cont_stepCounter)+max(D10(15).data.m_cont_stepCounter);
% data13.speed=vertcat(P10(13).data.General_WalkingVelocity_m_s,P10(14).data.General_WalkingVelocity_m_s,P10(15).data.General_WalkingVelocity_m_s);
% meanspeed13=mean(data13.speed);
% data13.GGF=vertcat(P10(13).data.General_GuidanceForce_pct,(P10(14).data.General_GuidanceForce_pct),(P10(15).data.General_GuidanceForce_pct));
% meanGGF13=mean(data13.GGF);
% data13.duration=P10(13).duration+P10(14).duration+P10(15).duration;
% 
% %join 16,17
% data16.BWS=vertcat(((P10(16).data.Patient_Mass_kg-D10(16).data.m_cont_treadmillData_Fy)./P10(16).data.Patient_Mass_kg.*100),((P10(17).data.Patient_Mass_kg-D10(17).data.m_cont_treadmillData_Fy)./P10(17).data.Patient_Mass_kg.*100));
% meanBWS16=mean(data16.BWS);
% data16.steps=max(D10(16).data.m_cont_stepCounter)+max(D10(17).data.m_cont_stepCounter);
% data16.speed=vertcat(P10(16).data.General_WalkingVelocity_m_s,P10(17).data.General_WalkingVelocity_m_s);
% meanspeed16=mean(data16.speed);
% data16.GGF=vertcat(P10(16).data.General_GuidanceForce_pct,(P10(17).data.General_GuidanceForce_pct));
% meanGGF16=mean(data16.GGF);
% data16.duration=P10(16).duration+P10(17).duration;
% 
% 
% %join 18,19
% data18.BWS=vertcat(((P10(18).data.Patient_Mass_kg-D10(18).data.m_cont_treadmillData_Fy)./P10(18).data.Patient_Mass_kg.*100),((P10(19).data.Patient_Mass_kg-D10(19).data.m_cont_treadmillData_Fy)./P10(19).data.Patient_Mass_kg.*100));
% meanBWS18=mean(data18.BWS);
% data18.steps=max(D10(18).data.m_cont_stepCounter)+max(D10(19).data.m_cont_stepCounter);
% data18.speed=vertcat(P10(18).data.General_WalkingVelocity_m_s,P10(19).data.General_WalkingVelocity_m_s);
% meanspeed18=mean(data18.speed);
% data18.GGF=vertcat(P10(18).data.General_GuidanceForce_pct,(P10(19).data.General_GuidanceForce_pct));
% meanGGF18=mean(data18.GGF);
% data18.duration=P10(18).duration+P10(19).duration;
% 
% %join 21,22
% data21.BWS=vertcat(((P10(21).data.Patient_Mass_kg-D10(21).data.m_cont_treadmillData_Fy)./P10(21).data.Patient_Mass_kg.*100),((P10(22).data.Patient_Mass_kg-D10(22).data.m_cont_treadmillData_Fy)./P10(22).data.Patient_Mass_kg.*100));
% meanBWS21=mean(data21.BWS);
% data21.steps=max(D10(21).data.m_cont_stepCounter)+max(D10(22).data.m_cont_stepCounter);
% data21.speed=vertcat(P10(21).data.General_WalkingVelocity_m_s,P10(22).data.General_WalkingVelocity_m_s);
% meanspeed21=mean(data21.speed);
% data21.GGF=vertcat(P10(21).data.General_GuidanceForce_pct,(P10(22).data.General_GuidanceForce_pct));
% meanGGF21=mean(data21.GGF);
% data21.duration=P10(21).duration+P10(22).duration;
% 
% 
% %join 26,27
% data26.BWS=vertcat(((P10(26).data.Patient_Mass_kg-D10(26).data.m_cont_treadmillData_Fy)./P10(26).data.Patient_Mass_kg.*100),((P10(27).data.Patient_Mass_kg-D10(27).data.m_cont_treadmillData_Fy)./P10(27).data.Patient_Mass_kg.*100));
% meanBWS26=mean(data26.BWS);
% data26.steps=max(D10(26).data.m_cont_stepCounter)+max(D10(27).data.m_cont_stepCounter);
% data26.speed=vertcat(P10(26).data.General_WalkingVelocity_m_s,P10(27).data.General_WalkingVelocity_m_s);
% meanspeed26=mean(data26.speed);
% data26.GGF=vertcat(P10(26).data.General_GuidanceForce_pct,(P10(27).data.General_GuidanceForce_pct));
% meanGGF26=mean(data26.GGF);
% data26.duration=P10(26).duration+P10(27).duration;
% 
% 
% %join 28,29
% data28.BWS=vertcat(((P10(28).data.Patient_Mass_kg-D10(28).data.m_cont_treadmillData_Fy)./P10(28).data.Patient_Mass_kg.*100),((P10(29).data.Patient_Mass_kg-D10(29).data.m_cont_treadmillData_Fy)./P10(29).data.Patient_Mass_kg.*100));
% meanBWS28=mean(data28.BWS);
% data28.steps=max(D10(28).data.m_cont_stepCounter)+max(D10(29).data.m_cont_stepCounter);
% data28.speed=vertcat(P10(28).data.General_WalkingVelocity_m_s,P10(29).data.General_WalkingVelocity_m_s);
% meanspeed28=mean(data28.speed);
% data28.GGF=vertcat(P10(28).data.General_GuidanceForce_pct,(P10(29).data.General_GuidanceForce_pct));
% meanGGF28=mean(data28.GGF);
% data28.duration=P10(28).duration+P10(29).duration;


%% Patient 22

p22sigfilenames=['ARTS LOPES - Copy\22\recordings\20160919_152637.signals';  'ARTS LOPES - Copy\22\recordings\20160922_152238.signals';  'ARTS LOPES - Copy\22\recordings\20161013_135032.signals';  'ARTS LOPES - Copy\22\recordings\20161101_140930.signals';  
'ARTS LOPES - Copy\22\recordings\20160919_152924.signals';  'ARTS LOPES - Copy\22\recordings\20160922_152632.signals';  'ARTS LOPES - Copy\22\recordings\20161013_155223.signals';  'ARTS LOPES - Copy\22\recordings\20161116_130859.signals';  
'ARTS LOPES - Copy\22\recordings\20160921_140913.signals';  'ARTS LOPES - Copy\22\recordings\20160928_140632.signals';  'ARTS LOPES - Copy\22\recordings\20161017_141112.signals';  'ARTS LOPES - Copy\22\recordings\20161118_102029.signals';  
'ARTS LOPES - Copy\22\recordings\20160921_141417.signals';  'ARTS LOPES - Copy\22\recordings\20160928_141159.signals';  'ARTS LOPES - Copy\22\recordings\20161019_141344.signals';  'ARTS LOPES - Copy\22\recordings\20161118_102145.signals';  
'ARTS LOPES - Copy\22\recordings\20160922_151425.signals';  'ARTS LOPES - Copy\22\recordings\20160929_134028.signals';  'ARTS LOPES - Copy\22\recordings\20161024_141846.signals';  'ARTS LOPES - Copy\22\recordings\20161128_140240.signals';  
'ARTS LOPES - Copy\22\recordings\20160922_151453.signals';  'ARTS LOPES - Copy\22\recordings\20160929_134100.signals';  'ARTS LOPES - Copy\22\recordings\20161024_142453.signals';  'ARTS LOPES - Copy\22\recordings\20161128_140941.signals';  
'ARTS LOPES - Copy\22\recordings\20160922_151856.signals';  'ARTS LOPES - Copy\22\recordings\20161006_134338.signals';  'ARTS LOPES - Copy\22\recordings\20161026_140720.signals';  
'ARTS LOPES - Copy\22\recordings\20160922_152148.signals';  'ARTS LOPES - Copy\22\recordings\20161013_135004.signals';  'ARTS LOPES - Copy\22\recordings\20161026_140830.signals'];  
p22sigfilenames = cellstr(p22sigfilenames);  

p22paramfilenames=['ARTS LOPES - Copy\22\recordings\20160919_152637.params';  'ARTS LOPES - Copy\22\recordings\20160922_152238.params';  'ARTS LOPES - Copy\22\recordings\20161013_135032.params';  'ARTS LOPES - Copy\22\recordings\20161101_140930.params';  
'ARTS LOPES - Copy\22\recordings\20160919_152924.params';  'ARTS LOPES - Copy\22\recordings\20160922_152632.params';  'ARTS LOPES - Copy\22\recordings\20161013_155223.params';  'ARTS LOPES - Copy\22\recordings\20161116_130859.params';  
'ARTS LOPES - Copy\22\recordings\20160921_140913.params';  'ARTS LOPES - Copy\22\recordings\20160928_140632.params';  'ARTS LOPES - Copy\22\recordings\20161017_141112.params';  'ARTS LOPES - Copy\22\recordings\20161118_102029.params';  
'ARTS LOPES - Copy\22\recordings\20160921_141417.params';  'ARTS LOPES - Copy\22\recordings\20160928_141159.params';  'ARTS LOPES - Copy\22\recordings\20161019_141344.params';  'ARTS LOPES - Copy\22\recordings\20161118_102145.params';  
'ARTS LOPES - Copy\22\recordings\20160922_151425.params';  'ARTS LOPES - Copy\22\recordings\20160929_134028.params';  'ARTS LOPES - Copy\22\recordings\20161024_141846.params';  'ARTS LOPES - Copy\22\recordings\20161128_140240.params';  
'ARTS LOPES - Copy\22\recordings\20160922_151453.params';  'ARTS LOPES - Copy\22\recordings\20160929_134100.params';  'ARTS LOPES - Copy\22\recordings\20161024_142453.params';  'ARTS LOPES - Copy\22\recordings\20161128_140941.params';  
'ARTS LOPES - Copy\22\recordings\20160922_151856.params';  'ARTS LOPES - Copy\22\recordings\20161006_134338.params';  'ARTS LOPES - Copy\22\recordings\20161026_140720.params';  
'ARTS LOPES - Copy\22\recordings\20160922_152148.params';  'ARTS LOPES - Copy\22\recordings\20161013_135004.params';  'ARTS LOPES - Copy\22\recordings\20161026_140830.params']; 
p22paramfilenames = cellstr(p22paramfilenames); 

for i=1:26
    [D22(i),P22(i)]=readSignalsFile(p22sigfilenames{i,1},p22paramfilenames{i,1});
end

for i=27:29
    [D22(i),P22(i)]=readSignalsFile(p22sigfilenames{i+1,1},p22paramfilenames{i+1,1});
end

D22=OrderStructDate(D22);
P22=OrderStructDate(P22);

% for i=1:26
%     BWS= (P22(i).data.Patient_Mass_kg-D22(i).data.m_cont_treadmillData_Fy)./P22(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P22(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P22(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D22(i).data.r_cont_stepCounter);
% end
% 
%     avBWS(27)=0;
%     avGGF(27)=0;
%     avSpeed(27)=0;
%     steps(27)=0;
% 
%     for i=28:length(D22)
%     BWS= (P22(i).data.Patient_Mass_kg-D22(i).data.m_cont_treadmillData_Fy)./P22(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P22(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P22(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D22(i).data.m_cont_stepCounter);
% end
%     
%     avBWS=avBWS';
%     avGGF=avGGF';
%     avSpeed=avSpeed';
%     steps=steps';

    clear p22sigfilenames p22paramfilenames
    
% %join 3,4
% data3.BWS=vertcat(((P22(3).data.Patient_Mass_kg-D22(3).data.m_cont_treadmillData_Fy)./P22(3).data.Patient_Mass_kg.*100),((P22(4).data.Patient_Mass_kg-D22(4).data.m_cont_treadmillData_Fy)./P22(4).data.Patient_Mass_kg.*100));
% meanBWS3=mean(data3.BWS);
% data3.steps=max(D22(3).data.m_cont_stepCounter)+max(D22(4).data.m_cont_stepCounter);
% data3.speed=vertcat(P22(3).data.General_WalkingVelocity_m_s,P22(4).data.General_WalkingVelocity_m_s);
% meanspeed3=mean(data3.speed);
% data3.GGF=vertcat(P22(3).data.General_GuidanceForce_pct,(P22(4).data.General_GuidanceForce_pct));
% meanGGF3=mean(data3.GGF);
% data3.duration=P22(3).duration+P22(4).duration;
% 
% %join 11,12
% data11.BWS=vertcat(((P22(11).data.Patient_Mass_kg-D22(11).data.m_cont_treadmillData_Fy)./P22(11).data.Patient_Mass_kg.*100),((P22(12).data.Patient_Mass_kg-D22(12).data.m_cont_treadmillData_Fy)./P22(12).data.Patient_Mass_kg.*100));
% meanBWS11=mean(data11.BWS);
% data11.steps=max(D22(11).data.m_cont_stepCounter)+max(D22(12).data.m_cont_stepCounter);
% data11.speed=vertcat(P22(11).data.General_WalkingVelocity_m_s,P22(12).data.General_WalkingVelocity_m_s);
% meanspeed11=mean(data11.speed);
% data11.GGF=vertcat(P22(11).data.General_GuidanceForce_pct,(P22(12).data.General_GuidanceForce_pct));
% meanGGF11=mean(data11.GGF);
% data11.duration=P22(11).duration+P22(12).duration;
% 
% %join 21,22
% data21.BWS=vertcat(((P22(21).data.Patient_Mass_kg-D22(21).data.m_cont_treadmillData_Fy)./P22(21).data.Patient_Mass_kg.*100),((P22(22).data.Patient_Mass_kg-D22(22).data.m_cont_treadmillData_Fy)./P22(22).data.Patient_Mass_kg.*100));
% meanBWS21=mean(data21.BWS);
% data21.steps=max(D22(21).data.m_cont_stepCounter)+max(D22(22).data.m_cont_stepCounter);
% data21.speed=vertcat(P22(21).data.General_WalkingVelocity_m_s,P22(22).data.General_WalkingVelocity_m_s);
% meanspeed21=mean(data21.speed);
% data21.GGF=vertcat(P22(21).data.General_GuidanceForce_pct,(P22(22).data.General_GuidanceForce_pct));
% meanGGF21=mean(data21.GGF);
% data21.duration=P22(21).duration+P22(22).duration;
% 
% %join 26,27
% data26.BWS=vertcat(((P22(26).data.Patient_Mass_kg-D22(26).data.m_cont_treadmillData_Fy)./P22(26).data.Patient_Mass_kg.*100),((P22(27).data.Patient_Mass_kg-D22(27).data.m_cont_treadmillData_Fy)./P22(27).data.Patient_Mass_kg.*100));
% meanBWS26=mean(data26.BWS);
% data26.steps=max(D22(26).data.m_cont_stepCounter)+max(D22(27).data.m_cont_stepCounter);
% data26.speed=vertcat(P22(26).data.General_WalkingVelocity_m_s,P22(27).data.General_WalkingVelocity_m_s);
% meanspeed26=mean(data26.speed);
% data26.GGF=vertcat(P22(26).data.General_GuidanceForce_pct,(P22(27).data.General_GuidanceForce_pct));
% meanGGF26=mean(data26.GGF);
% data26.duration=P22(26).duration+P22(27).duration;
% 
% %join 28,29
% data28.BWS=vertcat(((P22(28).data.Patient_Mass_kg-D22(28).data.m_cont_treadmillData_Fy)./P22(28).data.Patient_Mass_kg.*100),((P22(29).data.Patient_Mass_kg-D22(29).data.m_cont_treadmillData_Fy)./P22(29).data.Patient_Mass_kg.*100));
% meanBWS28=mean(data28.BWS);
% data28.steps=max(D22(28).data.m_cont_stepCounter)+max(D22(29).data.m_cont_stepCounter);
% data28.speed=vertcat(P22(28).data.General_WalkingVelocity_m_s,P22(29).data.General_WalkingVelocity_m_s);
% meanspeed28=mean(data28.speed);
% data28.GGF=vertcat(P22(28).data.General_GuidanceForce_pct,(P22(29).data.General_GuidanceForce_pct));
% meanGGF28=mean(data28.GGF);
% data28.duration=P22(28).duration+P22(29).duration;
    
%% Patient 30

p30sigfilenames=['ARTS LOPES - Copy\30\recordings\20161115_143123.signals';  'ARTS LOPES - Copy\30\recordings\20161118_130745.signals';  'ARTS LOPES - Copy\30\recordings\20161130_142814.signals';  'ARTS LOPES - Copy\30\recordings\20161221_140706.signals';  
'ARTS LOPES - Copy\30\recordings\20161115_143404.signals';  'ARTS LOPES - Copy\30\recordings\20161123_141403.signals';  'ARTS LOPES - Copy\30\recordings\20161202_140418.signals';  
'ARTS LOPES - Copy\30\recordings\20161115_144223.signals';  'ARTS LOPES - Copy\30\recordings\20161123_143631.signals';  'ARTS LOPES - Copy\30\recordings\20161214_141926.signals';  
'ARTS LOPES - Copy\30\recordings\20161116_110150.signals';  'ARTS LOPES - Copy\30\recordings\20161125_140245.signals';  'ARTS LOPES - Copy\30\recordings\20161216_135900.signals';  
'ARTS LOPES - Copy\30\recordings\20161116_111539.signals';  'ARTS LOPES - Copy\30\recordings\20161130_140312.signals';  'ARTS LOPES - Copy\30\recordings\20161216_141045.signals'];  
p30sigfilenames = cellstr(p30sigfilenames);  

p30paramfilenames=['ARTS LOPES - Copy\30\recordings\20161115_143123.params';  'ARTS LOPES - Copy\30\recordings\20161118_130745.params';  'ARTS LOPES - Copy\30\recordings\20161130_142814.params';  'ARTS LOPES - Copy\30\recordings\20161221_140706.params';  
'ARTS LOPES - Copy\30\recordings\20161115_143304.params';  'ARTS LOPES - Copy\30\recordings\20161118_130932.params';  'ARTS LOPES - Copy\30\recordings\20161202_140203.params';  'ARTS LOPES - Copy\30\recordings\20161223_140233.params';  
'ARTS LOPES - Copy\30\recordings\20161115_143404.params';  'ARTS LOPES - Copy\30\recordings\20161123_141403.params';  'ARTS LOPES - Copy\30\recordings\20161202_140418.params';  
'ARTS LOPES - Copy\30\recordings\20161115_144223.params';  'ARTS LOPES - Copy\30\recordings\20161123_143631.params';  'ARTS LOPES - Copy\30\recordings\20161214_141926.params';  
'ARTS LOPES - Copy\30\recordings\20161116_110150.params';  'ARTS LOPES - Copy\30\recordings\20161125_140245.params';  'ARTS LOPES - Copy\30\recordings\20161216_135900.params';  
'ARTS LOPES - Copy\30\recordings\20161116_111539.params';  'ARTS LOPES - Copy\30\recordings\20161130_140312.params';  'ARTS LOPES - Copy\30\recordings\20161216_141045.params']; 
p30paramfilenames = cellstr(p30paramfilenames); 

for i=1:length(p30sigfilenames)
    [D30(i),P30(i)]=readSignalsFile(p30sigfilenames{i,1},p30paramfilenames{i,1});
end

D30=OrderStructDate(D30);
P30=OrderStructDate(P30);

% Calculate BWS (Body weight support)
% for i=1:length(D30)
%     BWS= (P30(i).data.Patient_Mass_kg-D30(i).data.m_cont_treadmillData_Fy)./P30(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P30(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P30(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D30(i).data.m_cont_stepCounter);
% end
% 
%     avBWS=avBWS';
%     avGGF=avGGF';
%     avSpeed=avSpeed';
%     steps=steps';
    
clear p30sigfilenames p30paramfilenames

% join 2,3
% data2.BWS=vertcat(((P30(2).data.Patient_Mass_kg-D30(2).data.m_cont_treadmillData_Fy)./P30(2).data.Patient_Mass_kg.*100),((P30(3).data.Patient_Mass_kg-D30(3).data.m_cont_treadmillData_Fy)./P30(3).data.Patient_Mass_kg.*100));
% meanBWS2=mean(data2.BWS);
% data2.steps=max(D30(2).data.m_cont_stepCounter)+max(D30(3).data.m_cont_stepCounter);
% data2.speed=vertcat(P30(2).data.General_WalkingVelocity_m_s,P30(3).data.General_WalkingVelocity_m_s);
% meanspeed2=mean(data2.speed);
% data2.GGF=vertcat(P30(2).data.General_GuidanceForce_pct,(P30(3).data.General_GuidanceForce_pct));
% meanGGF2=mean(data2.GGF);
% data2.duration=P30(2).duration+P30(3).duration;
% 
% join 4,5
% data4.BWS=vertcat(((P30(4).data.Patient_Mass_kg-D30(4).data.m_cont_treadmillData_Fy)./P30(4).data.Patient_Mass_kg.*100),((P30(5).data.Patient_Mass_kg-D30(5).data.m_cont_treadmillData_Fy)./P30(5).data.Patient_Mass_kg.*100));
% meanBWS4=mean(data4.BWS);
% data4.steps=max(D30(4).data.m_cont_stepCounter)+max(D30(5).data.m_cont_stepCounter);
% data4.speed=vertcat(P30(4).data.General_WalkingVelocity_m_s,P30(5).data.General_WalkingVelocity_m_s);
% meanspeed4=mean(data4.speed);
% data4.GGF=vertcat(P30(4).data.General_GuidanceForce_pct,(P30(5).data.General_GuidanceForce_pct));
% meanGGF4=mean(data4.GGF);
% data4.duration=P30(4).duration+P30(5).duration;
% 
% join 7,8
% data7.BWS=vertcat(((P30(7).data.Patient_Mass_kg-D30(7).data.m_cont_treadmillData_Fy)./P30(7).data.Patient_Mass_kg.*100),((P30(8).data.Patient_Mass_kg-D30(8).data.m_cont_treadmillData_Fy)./P30(8).data.Patient_Mass_kg.*100));
% meanBWS7=mean(data7.BWS);
% data7.steps=max(D30(7).data.m_cont_stepCounter)+max(D30(8).data.m_cont_stepCounter);
% data7.speed=vertcat(P30(7).data.General_WalkingVelocity_m_s,P30(8).data.General_WalkingVelocity_m_s);
% meanspeed7=mean(data7.speed);
% data7.GGF=vertcat(P30(7).data.General_GuidanceForce_pct,(P30(8).data.General_GuidanceForce_pct));
% meanGGF7=mean(data7.GGF);
% data7.duration=P30(7).duration+P30(8).duration;
% 
% join 10,11
% data10.BWS=vertcat(((P30(10).data.Patient_Mass_kg-D30(10).data.m_cont_treadmillData_Fy)./P30(10).data.Patient_Mass_kg.*100),((P30(11).data.Patient_Mass_kg-D30(11).data.m_cont_treadmillData_Fy)./P30(11).data.Patient_Mass_kg.*100));
% meanBWS10=mean(data10.BWS);
% data10.steps=max(D30(10).data.m_cont_stepCounter)+max(D30(11).data.m_cont_stepCounter);
% data10.speed=vertcat(P30(10).data.General_WalkingVelocity_m_s,P30(11).data.General_WalkingVelocity_m_s);
% meanspeed10=mean(data10.speed);
% data10.GGF=vertcat(P30(10).data.General_GuidanceForce_pct,(P30(11).data.General_GuidanceForce_pct));
% meanGGF10=mean(data10.GGF);
% data10.duration=P30(10).duration+P30(11).duration
% 
% join 14,15
% data14.BWS=vertcat(((P30(14).data.Patient_Mass_kg-D30(14).data.m_cont_treadmillData_Fy)./P30(14).data.Patient_Mass_kg.*140),((P30(15).data.Patient_Mass_kg-D30(15).data.m_cont_treadmillData_Fy)./P30(15).data.Patient_Mass_kg.*140));
% meanBWS14=mean(data14.BWS);
% data14.steps=max(D30(14).data.m_cont_stepCounter)+max(D30(15).data.m_cont_stepCounter);
% data14.speed=vertcat(P30(14).data.General_WalkingVelocity_m_s,P30(15).data.General_WalkingVelocity_m_s);
% meanspeed14=mean(data14.speed);
% data14.GGF=vertcat(P30(14).data.General_GuidanceForce_pct,(P30(15).data.General_GuidanceForce_pct));
% meanGGF14=mean(data14.GGF);
% data14.duration=P30(14).duration+P30(15).duration;

%% Patient 105

p105sigfilenames=['ARTS LOPES - Copy\105\recordings\20170320_081330.signals';  'ARTS LOPES - Copy\105\recordings\20170331_135944.signals';  'ARTS LOPES - Copy\105\recordings\20170407_134305.signals';  'ARTS LOPES - Copy\105\recordings\20170503_101327.signals';  
'ARTS LOPES - Copy\105\recordings\20170322_150339.signals';  'ARTS LOPES - Copy\105\recordings\20170331_152644.signals';  'ARTS LOPES - Copy\105\recordings\20170407_134628.signals';  'ARTS LOPES - Copy\105\recordings\20170508_081305.signals';  
'ARTS LOPES - Copy\105\recordings\20170322_151124.signals';  'ARTS LOPES - Copy\105\recordings\20170403_075418.signals';  'ARTS LOPES - Copy\105\recordings\20170410_111649.signals';  'ARTS LOPES - Copy\105\recordings\20170508_081409.signals';  
'ARTS LOPES - Copy\105\recordings\20170322_151819.signals';  'ARTS LOPES - Copy\105\recordings\20170405_133925.signals';  'ARTS LOPES - Copy\105\recordings\20170412_133418.signals';  'ARTS LOPES - Copy\105\recordings\20170516_094706.signals';  
'ARTS LOPES - Copy\105\recordings\20170324_140123.signals';  'ARTS LOPES - Copy\105\recordings\20170405_134352.signals';  'ARTS LOPES - Copy\105\recordings\20170419_134622.signals';  
'ARTS LOPES - Copy\105\recordings\20170324_140906.signals';  'ARTS LOPES - Copy\105\recordings\20170407_133504.signals';  'ARTS LOPES - Copy\105\recordings\20170425_104414.signals';  
'ARTS LOPES - Copy\105\recordings\20170329_133304.signals';  'ARTS LOPES - Copy\105\recordings\20170407_133758.signals';  'ARTS LOPES - Copy\105\recordings\20170426_153942.signals';  
'ARTS LOPES - Copy\105\recordings\20170329_133858.signals';  'ARTS LOPES - Copy\105\recordings\20170407_134045.signals';  'ARTS LOPES - Copy\105\recordings\20170503_100437.signals'];  
p105sigfilenames = cellstr(p105sigfilenames);  

p105paramfilenames=['ARTS LOPES - Copy\105\recordings\20170320_081330.params';  'ARTS LOPES - Copy\105\recordings\20170331_135944.params';  'ARTS LOPES - Copy\105\recordings\20170407_134305.params';  'ARTS LOPES - Copy\105\recordings\20170503_101327.params';  
'ARTS LOPES - Copy\105\recordings\20170322_150339.params';  'ARTS LOPES - Copy\105\recordings\20170331_152644.params';  'ARTS LOPES - Copy\105\recordings\20170407_134628.params';  'ARTS LOPES - Copy\105\recordings\20170508_081305.params';  
'ARTS LOPES - Copy\105\recordings\20170322_151124.params';  'ARTS LOPES - Copy\105\recordings\20170403_075418.params';  'ARTS LOPES - Copy\105\recordings\20170410_111649.params';  'ARTS LOPES - Copy\105\recordings\20170508_081409.params';  
'ARTS LOPES - Copy\105\recordings\20170322_151819.params';  'ARTS LOPES - Copy\105\recordings\20170405_133925.params';  'ARTS LOPES - Copy\105\recordings\20170412_133418.params';  'ARTS LOPES - Copy\105\recordings\20170516_094706.params';  
'ARTS LOPES - Copy\105\recordings\20170324_140123.params';  'ARTS LOPES - Copy\105\recordings\20170405_134352.params';  'ARTS LOPES - Copy\105\recordings\20170419_134622.params';  
'ARTS LOPES - Copy\105\recordings\20170324_140906.params';  'ARTS LOPES - Copy\105\recordings\20170407_133504.params';  'ARTS LOPES - Copy\105\recordings\20170425_104414.params';  
'ARTS LOPES - Copy\105\recordings\20170329_133304.params';  'ARTS LOPES - Copy\105\recordings\20170407_133758.params';  'ARTS LOPES - Copy\105\recordings\20170426_153942.params';  
'ARTS LOPES - Copy\105\recordings\20170329_133858.params';  'ARTS LOPES - Copy\105\recordings\20170407_134045.params';  'ARTS LOPES - Copy\105\recordings\20170503_100437.params']; 
p105paramfilenames = cellstr(p105paramfilenames); 

for i=1:length(p105sigfilenames)
    [D105(i),P105(i)]=readSignalsFile(p105sigfilenames{i,1},p105paramfilenames{i,1});
end

D105=OrderStructDate(D105);
P105=OrderStructDate(P105);

% Calculate BWS (Body weight support)
% for i=1:length(D105)
%     BWS= (P105(i).data.Patient_Mass_kg-D105(i).data.m_cont_treadmillData_Fy)./P105(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P105(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P105(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D105(i).data.m_cont_stepCounter);
% end
% 
%     avBWS=avBWS';
%     avGGF=avGGF';
%     avSpeed=avSpeed';
%     steps=steps';

clear p105sigfilenames p105paramfilenames
   
%join 2,3,4
% data2.BWS=vertcat(((P105(2).data.Patient_Mass_kg-D105(2).data.m_cont_treadmillData_Fy)./P105(2).data.Patient_Mass_kg.*100),((P105(3).data.Patient_Mass_kg-D105(3).data.m_cont_treadmillData_Fy)./P105(3).data.Patient_Mass_kg.*100),((P105(4).data.Patient_Mass_kg-D105(4).data.m_cont_treadmillData_Fy)./P105(4).data.Patient_Mass_kg.*100));
% meanBWS2=mean(data2.BWS);
% data2.steps=max(D105(2).data.m_cont_stepCounter)+max(D105(3).data.m_cont_stepCounter)+max(D105(4).data.m_cont_stepCounter);
% data2.speed=vertcat(P105(2).data.General_WalkingVelocity_m_s,P105(3).data.General_WalkingVelocity_m_s,P105(4).data.General_WalkingVelocity_m_s);
% meanspeed2=mean(data2.speed);
% data2.GGF=vertcat(P105(2).data.General_GuidanceForce_pct,(P105(3).data.General_GuidanceForce_pct),(P105(4).data.General_GuidanceForce_pct));
% meanGGF2=mean(data2.GGF);
% data2.duration=P105(2).duration+P105(3).duration+P105(4).duration;
% 
% %join 5,6
% data5.BWS=vertcat(((P105(5).data.Patient_Mass_kg-D105(5).data.m_cont_treadmillData_Fy)./P105(5).data.Patient_Mass_kg.*100),((P105(6).data.Patient_Mass_kg-D105(6).data.m_cont_treadmillData_Fy)./P105(6).data.Patient_Mass_kg.*100));
% meanBWS5=mean(data5.BWS);
% data5.steps=max(D105(5).data.m_cont_stepCounter)+max(D105(6).data.m_cont_stepCounter);
% data5.speed=vertcat(P105(5).data.General_WalkingVelocity_m_s,P105(6).data.General_WalkingVelocity_m_s);
% meanspeed5=mean(data5.speed);
% data5.GGF=vertcat(P105(5).data.General_GuidanceForce_pct,(P105(6).data.General_GuidanceForce_pct));
% meanGGF5=mean(data5.GGF);
% data5.duration=P105(5).duration+P105(6).duration;
% 
% %join 9,10
% data9.BWS=vertcat(((P105(9).data.Patient_Mass_kg-D105(9).data.m_cont_treadmillData_Fy)./P105(9).data.Patient_Mass_kg.*100),((P105(10).data.Patient_Mass_kg-D105(10).data.m_cont_treadmillData_Fy)./P105(10).data.Patient_Mass_kg.*100));
% meanBWS9=mean(data9.BWS);
% data9.steps=max(D105(9).data.m_cont_stepCounter)+max(D105(10).data.m_cont_stepCounter);
% data9.speed=vertcat(P105(9).data.General_WalkingVelocity_m_s,P105(10).data.General_WalkingVelocity_m_s);
% meanspeed9=mean(data9.speed);
% data9.GGF=vertcat(P105(9).data.General_GuidanceForce_pct,(P105(10).data.General_GuidanceForce_pct));
% meanGGF9=mean(data9.GGF);
% data9.duration=P105(9).duration+P105(10).duration;
% 
% %join 12,13
% data12.BWS=vertcat(((P105(12).data.Patient_Mass_kg-D105(12).data.m_cont_treadmillData_Fy)./P105(12).data.Patient_Mass_kg.*100),((P105(13).data.Patient_Mass_kg-D105(13).data.m_cont_treadmillData_Fy)./P105(13).data.Patient_Mass_kg.*100));
% meanBWS12=mean(data12.BWS);
% data12.steps=max(D105(12).data.m_cont_stepCounter)+max(D105(13).data.m_cont_stepCounter);
% data12.speed=vertcat(P105(12).data.General_WalkingVelocity_m_s,P105(13).data.General_WalkingVelocity_m_s);
% meanspeed12=mean(data12.speed);
% data12.GGF=vertcat(P105(12).data.General_GuidanceForce_pct,(P105(13).data.General_GuidanceForce_pct));
% meanGGF12=mean(data12.GGF);
% data12.duration=P105(12).duration+P105(13).duration;
% 
% %join 14,15,16,17,18
% data14.BWS=vertcat(((P105(14).data.Patient_Mass_kg-D105(14).data.m_cont_treadmillData_Fy)./P105(14).data.Patient_Mass_kg.*100),((P105(15).data.Patient_Mass_kg-D105(15).data.m_cont_treadmillData_Fy)./P105(15).data.Patient_Mass_kg.*100),((P105(16).data.Patient_Mass_kg-D105(16).data.m_cont_treadmillData_Fy)./P105(16).data.Patient_Mass_kg.*100),((P105(17).data.Patient_Mass_kg-D105(17).data.m_cont_treadmillData_Fy)./P105(17).data.Patient_Mass_kg.*100),((P105(18).data.Patient_Mass_kg-D105(18).data.m_cont_treadmillData_Fy)./P105(18).data.Patient_Mass_kg.*100));
% meanBWS14=mean(data14.BWS);
% data14.steps=max(D105(14).data.m_cont_stepCounter)+max(D105(15).data.m_cont_stepCounter)+max(D105(16).data.m_cont_stepCounter)+max(D105(17).data.m_cont_stepCounter)+max(D105(18).data.m_cont_stepCounter);
% data14.speed=vertcat(P105(14).data.General_WalkingVelocity_m_s,P105(15).data.General_WalkingVelocity_m_s,P105(16).data.General_WalkingVelocity_m_s,P105(17).data.General_WalkingVelocity_m_s,P105(18).data.General_WalkingVelocity_m_s);
% meanspeed14=mean(data14.speed);
% data14.GGF=vertcat(P105(14).data.General_GuidanceForce_pct,(P105(15).data.General_GuidanceForce_pct),(P105(16).data.General_GuidanceForce_pct),(P105(17).data.General_GuidanceForce_pct),(P105(18).data.General_GuidanceForce_pct));
% meanGGF14=mean(data14.GGF);
% data14.duration=P105(14).duration+P105(15).duration+P105(16).duration+P105(17).duration+P105(18).duration;
% 
% %join 26,27
% data26.BWS=vertcat(((P105(26).data.Patient_Mass_kg-D105(26).data.m_cont_treadmillData_Fy)./P105(26).data.Patient_Mass_kg.*100),((P105(27).data.Patient_Mass_kg-D105(27).data.m_cont_treadmillData_Fy)./P105(27).data.Patient_Mass_kg.*100));
% meanBWS26=mean(data26.BWS);
% data26.steps=max(D105(26).data.m_cont_stepCounter)+max(D105(27).data.m_cont_stepCounter);
% data26.speed=vertcat(P105(26).data.General_WalkingVelocity_m_s,P105(27).data.General_WalkingVelocity_m_s);
% meanspeed26=mean(data26.speed);
% data26.GGF=vertcat(P105(26).data.General_GuidanceForce_pct,(P105(27).data.General_GuidanceForce_pct));
% meanGGF26=mean(data26.GGF);
% data26.duration=P105(26).duration+P105(27).duration;
% 
% %join 7,8
% data7.BWS=vertcat(((P105(7).data.Patient_Mass_kg-D105(7).data.m_cont_treadmillData_Fy)./P105(7).data.Patient_Mass_kg.*100),((P105(8).data.Patient_Mass_kg-D105(8).data.m_cont_treadmillData_Fy)./P105(8).data.Patient_Mass_kg.*100));
% meanBWS7=mean(data7.BWS)
% data7.steps=max(D105(7).data.m_cont_stepCounter)+max(D105(8).data.m_cont_stepCounter);
% data7.speed=vertcat(P105(7).data.General_WalkingVelocity_m_s,P105(8).data.General_WalkingVelocity_m_s);
% meanspeed7=mean(data7.speed)
% data7.GGF=vertcat(P105(7).data.General_GuidanceForce_pct,(P105(8).data.General_GuidanceForce_pct));
% meanGGF7=mean(data7.GGF)
% data7.duration=P105(7).duration+P105(8).duration;

%% Patient 108

p108sigfilenames=['ARTS LOPES - Copy\108\recordings\20170411_131502.signals';  'ARTS LOPES - Copy\108\recordings\20170421_133409.signals';  'ARTS LOPES - Copy\108\recordings\20170501_130553.signals';  'ARTS LOPES - Copy\108\recordings\20170515_091116.signals';  
'ARTS LOPES - Copy\108\recordings\20170411_131926.signals';  'ARTS LOPES - Copy\108\recordings\20170421_134502.signals';  'ARTS LOPES - Copy\108\recordings\20170501_131823.signals';  'ARTS LOPES - Copy\108\recordings\20170515_091150.signals';  
'ARTS LOPES - Copy\108\recordings\20170413_161129.signals';  'ARTS LOPES - Copy\108\recordings\20170421_135028.signals';  'ARTS LOPES - Copy\108\recordings\20170503_143400.signals';  'ARTS LOPES - Copy\108\recordings\20170515_130447.signals';  
'ARTS LOPES - Copy\108\recordings\20170413_162052.signals';  'ARTS LOPES - Copy\108\recordings\20170424_133808.signals';  'ARTS LOPES - Copy\108\recordings\20170503_143829.signals';  'ARTS LOPES - Copy\108\recordings\20170522_130624.signals';  
'ARTS LOPES - Copy\108\recordings\20170420_110940.signals';  'ARTS LOPES - Copy\108\recordings\20170424_134910.signals';  'ARTS LOPES - Copy\108\recordings\20170508_163602.signals';  
'ARTS LOPES - Copy\108\recordings\20170420_111153.signals';  'ARTS LOPES - Copy\108\recordings\20170426_154659.signals';  'ARTS LOPES - Copy\108\recordings\20170510_114242.signals';  
'ARTS LOPES - Copy\108\recordings\20170420_111655.signals';  'ARTS LOPES - Copy\108\recordings\20170426_154852.signals';  'ARTS LOPES - Copy\108\recordings\20170512_133402.signals';  
'ARTS LOPES - Copy\108\recordings\20170420_112108.signals';  'ARTS LOPES - Copy\108\recordings\20170426_160147.signals';  'ARTS LOPES - Copy\108\recordings\20170515_090716.signals'];  
p108sigfilenames = cellstr(p108sigfilenames);  

p108paramfilenames=['ARTS LOPES - Copy\108\recordings\20170411_131502.params';  'ARTS LOPES - Copy\108\recordings\20170421_133409.params';  'ARTS LOPES - Copy\108\recordings\20170501_130553.params';  'ARTS LOPES - Copy\108\recordings\20170515_091116.params';  
'ARTS LOPES - Copy\108\recordings\20170411_131926.params';  'ARTS LOPES - Copy\108\recordings\20170421_134502.params';  'ARTS LOPES - Copy\108\recordings\20170501_131823.params';  'ARTS LOPES - Copy\108\recordings\20170515_091150.params';  
'ARTS LOPES - Copy\108\recordings\20170413_161129.params';  'ARTS LOPES - Copy\108\recordings\20170421_135028.params';  'ARTS LOPES - Copy\108\recordings\20170503_143400.params';  'ARTS LOPES - Copy\108\recordings\20170515_130447.params';  
'ARTS LOPES - Copy\108\recordings\20170413_162052.params';  'ARTS LOPES - Copy\108\recordings\20170424_133808.params';  'ARTS LOPES - Copy\108\recordings\20170503_143829.params';  'ARTS LOPES - Copy\108\recordings\20170522_130624.params';  
'ARTS LOPES - Copy\108\recordings\20170420_110940.params';  'ARTS LOPES - Copy\108\recordings\20170424_134910.params';  'ARTS LOPES - Copy\108\recordings\20170508_163602.params';  
'ARTS LOPES - Copy\108\recordings\20170420_111153.params';  'ARTS LOPES - Copy\108\recordings\20170426_154659.params';  'ARTS LOPES - Copy\108\recordings\20170510_114242.params';  
'ARTS LOPES - Copy\108\recordings\20170420_111655.params';  'ARTS LOPES - Copy\108\recordings\20170426_154852.params';  'ARTS LOPES - Copy\108\recordings\20170512_133402.params';  
'ARTS LOPES - Copy\108\recordings\20170420_112108.params';  'ARTS LOPES - Copy\108\recordings\20170426_160147.params';  'ARTS LOPES - Copy\108\recordings\20170515_090716.params']; 
p108paramfilenames = cellstr(p108paramfilenames); 

for i=1:length(p108sigfilenames)
    [D108(i),P108(i)]=readSignalsFile(p108sigfilenames{i,1},p108paramfilenames{i,1});
end

D108=OrderStructDate(D108);
P108=OrderStructDate(P108);

%Calculate BWS (Body weight support)
% for i=1:length(D108)
%     BWS= (P108(i).data.Patient_Mass_kg-D108(i).data.m_cont_treadmillData_Fy)./P108(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P108(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P108(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D108(i).data.m_cont_stepCounter);
% end
% 
%     avBWS=avBWS';
%     avGGF=avGGF';
%     avSpeed=avSpeed';
%     steps=steps';

clear p108sigfilenames p108paramfilenames

% %join 1,2 
% data1.BWS=vertcat(((P108(1).data.Patient_Mass_kg-D108(1).data.m_cont_treadmillData_Fy)./P108(1).data.Patient_Mass_kg.*100),((P108(2).data.Patient_Mass_kg-D108(2).data.m_cont_treadmillData_Fy)./P108(2).data.Patient_Mass_kg.*100));
% meanBWS1=mean(data1.BWS);
% data1.steps=max(D108(1).data.m_cont_stepCounter)+max(D108(2).data.m_cont_stepCounter);
% data1.speed=vertcat(P108(1).data.General_WalkingVelocity_m_s,P108(2).data.General_WalkingVelocity_m_s);
% meanspeed1=mean(data1.speed);
% data1.GGF=vertcat(P108(1).data.General_GuidanceForce_pct,(P108(2).data.General_GuidanceForce_pct));
% meanGGF1=mean(data1.GGF);
% data1.duration=P108(1).duration+P108(2).duration;
% 
% %join 3,4
% data3.BWS=vertcat(((P108(3).data.Patient_Mass_kg-D108(3).data.m_cont_treadmillData_Fy)./P108(3).data.Patient_Mass_kg.*100),((P108(4).data.Patient_Mass_kg-D108(4).data.m_cont_treadmillData_Fy)./P108(4).data.Patient_Mass_kg.*100));
% meanBWS3=mean(data3.BWS);
% data3.steps=max(D108(3).data.m_cont_stepCounter)+max(D108(4).data.m_cont_stepCounter);
% data3.speed=vertcat(P108(3).data.General_WalkingVelocity_m_s,P108(4).data.General_WalkingVelocity_m_s);
% meanspeed3=mean(data3.speed);
% data3.GGF=vertcat(P108(3).data.General_GuidanceForce_pct,(P108(4).data.General_GuidanceForce_pct));
% meanGGF3=mean(data3.GGF);
% data3.duration=P108(3).duration+P108(4).duration;
% 
% %join 5,6,7,8
% data5.BWS=vertcat(((P108(5).data.Patient_Mass_kg-D108(5).data.m_cont_treadmillData_Fy)./P108(5).data.Patient_Mass_kg.*100),((P108(6).data.Patient_Mass_kg-D108(6).data.m_cont_treadmillData_Fy)./P108(6).data.Patient_Mass_kg.*100),((P108(7).data.Patient_Mass_kg-D108(7).data.m_cont_treadmillData_Fy)./P108(7).data.Patient_Mass_kg.*100),((P108(8).data.Patient_Mass_kg-D108(8).data.m_cont_treadmillData_Fy)./P108(8).data.Patient_Mass_kg.*100));
% meanBWS5=mean(data5.BWS);
% data5.steps=max(D108(5).data.m_cont_stepCounter)+max(D108(6).data.m_cont_stepCounter)+max(D108(7).data.m_cont_stepCounter)+max(D108(8).data.m_cont_stepCounter);
% data5.speed=vertcat(P108(5).data.General_WalkingVelocity_m_s,P108(6).data.General_WalkingVelocity_m_s,P108(7).data.General_WalkingVelocity_m_s,P108(8).data.General_WalkingVelocity_m_s);
% meanspeed5=mean(data5.speed);
% data5.GGF=vertcat(P108(5).data.General_GuidanceForce_pct,(P108(6).data.General_GuidanceForce_pct),(P108(7).data.General_GuidanceForce_pct),(P108(8).data.General_GuidanceForce_pct));
% meanGGF5=mean(data5.GGF);
% data5.duration=P108(5).duration+P108(6).duration+P108(7).duration+P108(8).duration;
% 
% 
% %join 9,10,11
% data9.BWS=vertcat(((P108(9).data.Patient_Mass_kg-D108(9).data.m_cont_treadmillData_Fy)./P108(9).data.Patient_Mass_kg.*100),((P108(10).data.Patient_Mass_kg-D108(10).data.m_cont_treadmillData_Fy)./P108(10).data.Patient_Mass_kg.*100),((P108(11).data.Patient_Mass_kg-D108(11).data.m_cont_treadmillData_Fy)./P108(11).data.Patient_Mass_kg.*100));
% meanBWS9=mean(data9.BWS);
% data9.steps=max(D108(9).data.m_cont_stepCounter)+max(D108(10).data.m_cont_stepCounter)+max(D108(11).data.m_cont_stepCounter);
% data9.speed=vertcat(P108(9).data.General_WalkingVelocity_m_s,P108(10).data.General_WalkingVelocity_m_s,P108(11).data.General_WalkingVelocity_m_s);
% meanspeed9=mean(data9.speed);
% data9.GGF=vertcat(P108(9).data.General_GuidanceForce_pct,(P108(10).data.General_GuidanceForce_pct),(P108(11).data.General_GuidanceForce_pct));
% meanGGF9=mean(data9.GGF);
% data9.duration=P108(9).duration+P108(10).duration+P108(11).duration;
% 
% %join 12,13
% data12.BWS=vertcat(((P108(12).data.Patient_Mass_kg-D108(12).data.m_cont_treadmillData_Fy)./P108(12).data.Patient_Mass_kg.*100),((P108(13).data.Patient_Mass_kg-D108(13).data.m_cont_treadmillData_Fy)./P108(13).data.Patient_Mass_kg.*100));
% meanBWS12=mean(data12.BWS);
% data12.steps=max(D108(12).data.m_cont_stepCounter)+max(D108(13).data.m_cont_stepCounter);
% data12.speed=vertcat(P108(12).data.General_WalkingVelocity_m_s,P108(13).data.General_WalkingVelocity_m_s);
% meanspeed12=mean(data12.speed);
% data12.GGF=vertcat(P108(12).data.General_GuidanceForce_pct,(P108(13).data.General_GuidanceForce_pct));
% meanGGF12=mean(data12.GGF);
% data12.duration=P108(12).duration+P108(13).duration;
% 
% %join15,16
% data15.BWS=vertcat(((P108(15).data.Patient_Mass_kg-D108(15).data.m_cont_treadmillData_Fy)./P108(15).data.Patient_Mass_kg.*100),((P108(16).data.Patient_Mass_kg-D108(16).data.m_cont_treadmillData_Fy)./P108(16).data.Patient_Mass_kg.*100));
% meanBWS15=mean(data15.BWS);
% data15.steps=max(D108(15).data.m_cont_stepCounter)+max(D108(16).data.m_cont_stepCounter);
% data15.speed=vertcat(P108(15).data.General_WalkingVelocity_m_s,P108(16).data.General_WalkingVelocity_m_s);
% meanspeed15=mean(data15.speed);
% data15.GGF=vertcat(P108(15).data.General_GuidanceForce_pct,(P108(16).data.General_GuidanceForce_pct));
% meanGGF15=mean(data15.GGF);
% data15.duration=P108(15).duration+P108(16).duration;
% 
% %join 17,18
% data17.BWS=vertcat(((P108(17).data.Patient_Mass_kg-D108(17).data.m_cont_treadmillData_Fy)./P108(17).data.Patient_Mass_kg.*100),((P108(18).data.Patient_Mass_kg-D108(18).data.m_cont_treadmillData_Fy)./P108(18).data.Patient_Mass_kg.*100));
% meanBWS17=mean(data17.BWS);
% data17.steps=max(D108(17).data.m_cont_stepCounter)+max(D108(18).data.m_cont_stepCounter);
% data17.speed=vertcat(P108(17).data.General_WalkingVelocity_m_s,P108(18).data.General_WalkingVelocity_m_s);
% meanspeed17=mean(data17.speed);
% data17.GGF=vertcat(P108(17).data.General_GuidanceForce_pct,(P108(18).data.General_GuidanceForce_pct));
% meanGGF17=mean(data17.GGF);
% data17.duration=P108(17).duration+P108(18).duration;
% 
% %join 19 20 21
% data19.BWS=vertcat(((P108(19).data.Patient_Mass_kg-D108(19).data.m_cont_treadmillData_Fy)./P108(19).data.Patient_Mass_kg.*100),((P108(20).data.Patient_Mass_kg-D108(20).data.m_cont_treadmillData_Fy)./P108(20).data.Patient_Mass_kg.*100),((P108(21).data.Patient_Mass_kg-D108(21).data.m_cont_treadmillData_Fy)./P108(21).data.Patient_Mass_kg.*100));
% meanBWS19=mean(data19.BWS);
% data19.steps=max(D108(19).data.m_cont_stepCounter)+max(D108(20).data.m_cont_stepCounter)+max(D108(21).data.m_cont_stepCounter);
% data19.speed=vertcat(P108(19).data.General_WalkingVelocity_m_s,P108(20).data.General_WalkingVelocity_m_s,P108(21).data.General_WalkingVelocity_m_s);
% meanspeed19=mean(data19.speed);
% data19.GGF=vertcat(P108(19).data.General_GuidanceForce_pct,(P108(20).data.General_GuidanceForce_pct),(P108(21).data.General_GuidanceForce_pct));
% meanGGF19=mean(data19.GGF);
% data19.duration=P108(19).duration+P108(20).duration+P108(21).duration;


%% Patient 110
p110sigfilenames=[ 'ARTS LOPES - Copy\110\recordings\20170424_144230.signals'; 'ARTS LOPES - Copy\110\recordings\20170424_144536.signals'; 'ARTS LOPES - Copy\110\recordings\20170424_144633.signals'; 'ARTS LOPES - Copy\110\recordings\20170424_145638.signals'; 'ARTS LOPES - Copy\110\recordings\20170501_150425.signals'; 'ARTS LOPES - Copy\110\recordings\20170501_151403.signals'; 'ARTS LOPES - Copy\110\recordings\20170503_153321.signals';...
'ARTS LOPES - Copy\110\recordings\20170503_154522.signals'; 'ARTS LOPES - Copy\110\recordings\20170503_154551.signals'; 'ARTS LOPES - Copy\110\recordings\20170510_161151.signals'; 'ARTS LOPES - Copy\110\recordings\20170510_161246.signals'; 'ARTS LOPES - Copy\110\recordings\20170510_161347.signals'; 'ARTS LOPES - Copy\110\recordings\20170510_161553.signals'; 'ARTS LOPES - Copy\110\recordings\20170510_162132.signals'; 'ARTS LOPES - Copy\110\recordings\20170510_162201.signals';...
'ARTS LOPES - Copy\110\recordings\20170510_162809.signals'; 'ARTS LOPES - Copy\110\recordings\20170511_153741.signals'; 'ARTS LOPES - Copy\110\recordings\20170511_155015.signals'; 'ARTS LOPES - Copy\110\recordings\20170512_150834.signals'; 'ARTS LOPES - Copy\110\recordings\20170512_152524.signals'; 'ARTS LOPES - Copy\110\recordings\20170515_163629.signals';...
'ARTS LOPES - Copy\110\recordings\20170515_164757.signals'; 'ARTS LOPES - Copy\110\recordings\20170518_103356.signals'; 'ARTS LOPES - Copy\110\recordings\20170518_103535.signals'; 'ARTS LOPES - Copy\110\recordings\20170518_103712.signals'; 'ARTS LOPES - Copy\110\recordings\20170518_104720.signals';... 
'ARTS LOPES - Copy\110\recordings\20170518_154514.signals'; 'ARTS LOPES - Copy\110\recordings\20170518_154801.signals'; 'ARTS LOPES - Copy\110\recordings\20170607_114724.signals'; 'ARTS LOPES - Copy\110\recordings\20170608_153816.signals'; 'ARTS LOPES - Copy\110\recordings\20170612_164029.signals'; 'ARTS LOPES - Copy\110\recordings\20170616_132517.signals';...
'ARTS LOPES - Copy\110\recordings\20170616_132609.signals'; 'ARTS LOPES - Copy\110\recordings\20170616_151153.signals'; 'ARTS LOPES - Copy\110\recordings\20170616_152356.signals'; 'ARTS LOPES - Copy\110\recordings\20170619_161314.signals'];
p110sigfilenames= cellstr(p110sigfilenames);  

%dir *.params*
p110paramfilenames=[ 'ARTS LOPES - Copy\110\recordings\20170424_144230.params'; 'ARTS LOPES - Copy\110\recordings\20170424_144536.params'; 'ARTS LOPES - Copy\110\recordings\20170424_144633.params'; 'ARTS LOPES - Copy\110\recordings\20170424_145638.params'; 'ARTS LOPES - Copy\110\recordings\20170501_150425.params'; 'ARTS LOPES - Copy\110\recordings\20170501_151403.params'; 'ARTS LOPES - Copy\110\recordings\20170503_153321.params';...
'ARTS LOPES - Copy\110\recordings\20170503_154522.params'; 'ARTS LOPES - Copy\110\recordings\20170503_154551.params'; 'ARTS LOPES - Copy\110\recordings\20170510_161151.params'; 'ARTS LOPES - Copy\110\recordings\20170510_161246.params'; 'ARTS LOPES - Copy\110\recordings\20170510_161347.params'; 'ARTS LOPES - Copy\110\recordings\20170510_161553.params'; 'ARTS LOPES - Copy\110\recordings\20170510_162132.params'; 'ARTS LOPES - Copy\110\recordings\20170510_162201.params';...
'ARTS LOPES - Copy\110\recordings\20170510_162809.params'; 'ARTS LOPES - Copy\110\recordings\20170511_153741.params'; 'ARTS LOPES - Copy\110\recordings\20170511_155015.params'; 'ARTS LOPES - Copy\110\recordings\20170512_150834.params'; 'ARTS LOPES - Copy\110\recordings\20170512_152524.params'; 'ARTS LOPES - Copy\110\recordings\20170515_163629.params';...
'ARTS LOPES - Copy\110\recordings\20170515_164757.params'; 'ARTS LOPES - Copy\110\recordings\20170518_103356.params'; 'ARTS LOPES - Copy\110\recordings\20170518_103535.params'; 'ARTS LOPES - Copy\110\recordings\20170518_103712.params'; 'ARTS LOPES - Copy\110\recordings\20170518_104720.params';... 
'ARTS LOPES - Copy\110\recordings\20170518_154514.params'; 'ARTS LOPES - Copy\110\recordings\20170518_154801.params'; 'ARTS LOPES - Copy\110\recordings\20170607_114724.params'; 'ARTS LOPES - Copy\110\recordings\20170608_153816.params'; 'ARTS LOPES - Copy\110\recordings\20170612_164029.params'; 'ARTS LOPES - Copy\110\recordings\20170616_132517.params';...
'ARTS LOPES - Copy\110\recordings\20170616_132609.params'; 'ARTS LOPES - Copy\110\recordings\20170616_151153.params'; 'ARTS LOPES - Copy\110\recordings\20170616_152356.params'; 'ARTS LOPES - Copy\110\recordings\20170619_161314.params'];
p110paramfilenames = cellstr(p110paramfilenames); 

%there is a file with missing info(25)(20170518_103712.signals)
for i=1:24
    [D110(i),P110(i)]=readSignalsFile(p110sigfilenames{i,1},p110paramfilenames{i,1});
end

for i=25:length(p110sigfilenames)-1
    [D110(i),P110(i)]=readSignalsFile(p110sigfilenames{i+1,1},p110paramfilenames{i+1,1});
end
    

D110=OrderStructDate(D110);
P110=OrderStructDate(P110);

% Calculate BWS (Body weight support)
% for i=1:length(D108)
%     BWS= (P108(i).data.Patient_Mass_kg-D108(i).data.m_cont_treadmillData_Fy)./P108(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(P108(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(P108(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(D108(i).data.m_cont_stepCounter);
% end
% 
%     avBWS=avBWS';
%     avGGF=avGGF';
%     avSpeed=avSpeed';
%     steps=steps';

clear p110sigfilenames p110paramfilenames

% %join 1,3,4
% data1.BWS=vertcat(((P110(1).data.Patient_Mass_kg-D110(1).data.m_cont_treadmillData_Fy)./P110(1).data.Patient_Mass_kg.*100),((P110(3).data.Patient_Mass_kg-D110(3).data.m_cont_treadmillData_Fy)./P110(3).data.Patient_Mass_kg.*100),((P110(4).data.Patient_Mass_kg-D110(4).data.m_cont_treadmillData_Fy)./P110(4).data.Patient_Mass_kg.*100));
% meanBWS1=mean(data1.BWS);
% data1.steps=max(D110(1).data.m_cont_stepCounter)+max(D110(3).data.m_cont_stepCounter)+max(D110(4).data.m_cont_stepCounter);
% data1.speed=vertcat(P110(1).data.General_WalkingVelocity_m_s,P110(3).data.General_WalkingVelocity_m_s,P110(4).data.General_WalkingVelocity_m_s);
% meanspeed1=mean(data1.speed);
% data1.GGF=vertcat(P110(1).data.General_GuidanceForce_pct,(P110(3).data.General_GuidanceForce_pct),(P110(4).data.General_GuidanceForce_pct));
% meanGGF1=mean(data1.GGF);
% data1.duration=P110(1).duration+P110(3).duration+P110(4).duration;
% 
% %join 5,6 
% data5.BWS=vertcat(((P110(5).data.Patient_Mass_kg-D110(5).data.m_cont_treadmillData_Fy)./P110(5).data.Patient_Mass_kg.*100),((P110(6).data.Patient_Mass_kg-D110(6).data.m_cont_treadmillData_Fy)./P110(6).data.Patient_Mass_kg.*100));
% meanBWS5=mean(data5.BWS);
% data5.steps=max(D110(5).data.m_cont_stepCounter)+max(D110(6).data.m_cont_stepCounter);
% data5.speed=vertcat(P110(5).data.General_WalkingVelocity_m_s,P110(6).data.General_WalkingVelocity_m_s);
% meanspeed5=mean(data5.speed);
% data5.GGF=vertcat(P110(5).data.General_GuidanceForce_pct,(P110(6).data.General_GuidanceForce_pct));
% meanGGF5=mean(data5.GGF);
% data5.duration=P110(5).duration+P110(6).duration;
% 
% %join 7,9
% data7.BWS=vertcat(((P110(7).data.Patient_Mass_kg-D110(7).data.m_cont_treadmillData_Fy)./P110(7).data.Patient_Mass_kg.*100),((P110(9).data.Patient_Mass_kg-D110(9).data.m_cont_treadmillData_Fy)./P110(9).data.Patient_Mass_kg.*100));
% meanBWS7=mean(data7.BWS);
% data7.steps=max(D110(7).data.m_cont_stepCounter)+max(D110(9).data.m_cont_stepCounter);
% data7.speed=vertcat(P110(7).data.General_WalkingVelocity_m_s,P110(9).data.General_WalkingVelocity_m_s);
% meanspeed7=mean(data7.speed);
% data7.GGF=vertcat(P110(7).data.General_GuidanceForce_pct,(P110(9).data.General_GuidanceForce_pct));
% meanGGF7=mean(data7.GGF);
% data7.duration=P110(7).duration+P110(9).duration;
% 
% %join 12,13,15,16
% data12.BWS=vertcat(((P110(12).data.Patient_Mass_kg-D110(12).data.m_cont_treadmillData_Fy)./P110(12).data.Patient_Mass_kg.*100),((P110(13).data.Patient_Mass_kg-D110(13).data.m_cont_treadmillData_Fy)./P110(13).data.Patient_Mass_kg.*100),((P110(15).data.Patient_Mass_kg-D110(15).data.m_cont_treadmillData_Fy)./P110(15).data.Patient_Mass_kg.*100),((P110(16).data.Patient_Mass_kg-D110(16).data.m_cont_treadmillData_Fy)./P110(16).data.Patient_Mass_kg.*100));
% meanBWS12=mean(data12.BWS);
% data12.steps=max(D110(12).data.m_cont_stepCounter)+max(D110(13).data.m_cont_stepCounter)+max(D110(15).data.m_cont_stepCounter)+max(D110(16).data.m_cont_stepCounter);
% data12.speed=vertcat(P110(12).data.General_WalkingVelocity_m_s,P110(13).data.General_WalkingVelocity_m_s,P110(15).data.General_WalkingVelocity_m_s,P110(16).data.General_WalkingVelocity_m_s);
% meanspeed12=mean(data12.speed);
% data12.GGF=vertcat(P110(12).data.General_GuidanceForce_pct,(P110(13).data.General_GuidanceForce_pct),(P110(15).data.General_GuidanceForce_pct),(P110(16).data.General_GuidanceForce_pct));
% meanGGF12=mean(data12.GGF);
% data12.duration=P110(12).duration+P110(13).duration+P110(15).duration+P110(16).duration;
% 
% %join 17,18
% data17.BWS=vertcat(((P110(17).data.Patient_Mass_kg-D110(17).data.m_cont_treadmillData_Fy)./P110(17).data.Patient_Mass_kg.*100),((P110(18).data.Patient_Mass_kg-D110(18).data.m_cont_treadmillData_Fy)./P110(18).data.Patient_Mass_kg.*100));
% meanBWS17=mean(data17.BWS);
% data17.steps=max(D110(17).data.m_cont_stepCounter)+max(D110(18).data.m_cont_stepCounter);
% data17.speed=vertcat(P110(17).data.General_WalkingVelocity_m_s,P110(18).data.General_WalkingVelocity_m_s);
% meanspeed17=mean(data17.speed);
% data17.GGF=vertcat(P110(17).data.General_GuidanceForce_pct,(P110(18).data.General_GuidanceForce_pct));
% meanGGF17=mean(data17.GGF);
% data17.duration=P110(17).duration+P110(18).duration;
% 
% %join 19,20
% data19.BWS=vertcat(((P110(19).data.Patient_Mass_kg-D110(19).data.m_cont_treadmillData_Fy)./P110(19).data.Patient_Mass_kg.*100),((P110(20).data.Patient_Mass_kg-D110(20).data.m_cont_treadmillData_Fy)./P110(20).data.Patient_Mass_kg.*100));
% meanBWS19=mean(data19.BWS);
% data19.steps=max(D110(19).data.m_cont_stepCounter)+max(D110(20).data.m_cont_stepCounter);
% data19.speed=vertcat(P110(19).data.General_WalkingVelocity_m_s,P110(20).data.General_WalkingVelocity_m_s);
% meanspeed19=mean(data19.speed);
% data19.GGF=vertcat(P110(19).data.General_GuidanceForce_pct,(P110(20).data.General_GuidanceForce_pct));
% meanGGF19=mean(data19.GGF);
% data19.duration=P110(19).duration+P110(20).duration;
% 
% %join 21,22
% data21.BWS=vertcat(((P110(21).data.Patient_Mass_kg-D110(21).data.m_cont_treadmillData_Fy)./P110(21).data.Patient_Mass_kg.*100),((P110(22).data.Patient_Mass_kg-D110(22).data.m_cont_treadmillData_Fy)./P110(22).data.Patient_Mass_kg.*100));
% meanBWS21=mean(data21.BWS);
% data21.steps=max(D110(21).data.m_cont_stepCounter)+max(D110(22).data.m_cont_stepCounter);
% data21.speed=vertcat(P110(21).data.General_WalkingVelocity_m_s,P110(22).data.General_WalkingVelocity_m_s);
% meanspeed21=mean(data21.speed);
% data21.GGF=vertcat(P110(21).data.General_GuidanceForce_pct,(P110(22).data.General_GuidanceForce_pct));
% meanGGF21=mean(data21.GGF);
% data21.duration=P110(21).duration+P110(22).duration;
% 
% %join 34,35
% data34.BWS=vertcat(((P110(34).data.Patient_Mass_kg-D110(34).data.m_cont_treadmillData_Fy)./P110(34).data.Patient_Mass_kg.*100),((P110(35).data.Patient_Mass_kg-D110(35).data.m_cont_treadmillData_Fy)./P110(35).data.Patient_Mass_kg.*100));
% meanBWS34=mean(data34.BWS);
% data34.steps=max(D110(34).data.m_cont_stepCounter)+max(D110(35).data.m_cont_stepCounter);
% data34.speed=vertcat(P110(34).data.General_WalkingVelocity_m_s,P110(35).data.General_WalkingVelocity_m_s);
% meanspeed34=mean(data34.speed);
% data34.GGF=vertcat(P110(34).data.General_GuidanceForce_pct,(P110(35).data.General_GuidanceForce_pct));
% meanGGF34=mean(data34.GGF);
% data34.duration=P110(34).duration+P110(35).duration;
%% P112
% p112sigfilenames=['ARTS LOPES - Copy\112\recordings\20170523_151046.signals';  'ARTS LOPES - Copy\112\recordings\20170619_133901.signals';  'ARTS LOPES - Copy\112\recordings\20170629_110754.signals';  'ARTS LOPES - Copy\112\recordings\20170717_133813.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170523_151426.signals';  'ARTS LOPES - Copy\112\recordings\20170619_135108.signals';  'ARTS LOPES - Copy\112\recordings\20170629_112134.signals';  'ARTS LOPES - Copy\112\recordings\20170717_134855.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170523_152203.signals';  'ARTS LOPES - Copy\112\recordings\20170622_154135.signals';  'ARTS LOPES - Copy\112\recordings\20170703_134306.signals';  'ARTS LOPES - Copy\112\recordings\20170718_133305.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170608_160700.signals';  'ARTS LOPES - Copy\112\recordings\20170622_154532.signals';  'ARTS LOPES - Copy\112\recordings\20170703_135522.signals';  'ARTS LOPES - Copy\112\recordings\20170718_134528.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170608_161542.signals';  'ARTS LOPES - Copy\112\recordings\20170622_155531.signals';  'ARTS LOPES - Copy\112\recordings\20170704_111354.signals';  'ARTS LOPES - Copy\112\recordings\20170720_111449.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170612_141055.signals';  'ARTS LOPES - Copy\112\recordings\20170626_133629.signals';  'ARTS LOPES - Copy\112\recordings\20170704_133805.signals';  'ARTS LOPES - Copy\112\recordings\20170720_112513.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170612_142150.signals';  'ARTS LOPES - Copy\112\recordings\20170626_134734.signals';  'ARTS LOPES - Copy\112\recordings\20170704_135205.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170613_134214.signals';  'ARTS LOPES - Copy\112\recordings\20170627_133759.signals';  'ARTS LOPES - Copy\112\recordings\20170711_133750.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170613_135312.signals';  'ARTS LOPES - Copy\112\recordings\20170627_134234.signals';  'ARTS LOPES - Copy\112\recordings\20170711_134633.signals';  
% 'ARTS LOPES - Copy\112\recordings\20170619_133541.signals';  'ARTS LOPES - Copy\112\recordings\20170627_135218.signals';  'ARTS LOPES - Copy\112\recordings\20170717_133644.signals'];  
% p112sigfilenames = cellstr(p112sigfilenames);  
% 
% p112paramfilenames=['ARTS LOPES - Copy\112\recordings\20170523_151046.params';  'ARTS LOPES - Copy\112\recordings\20170619_133901.params';  'ARTS LOPES - Copy\112\recordings\20170629_110754.params';  'ARTS LOPES - Copy\112\recordings\20170717_133813.params';  
% 'ARTS LOPES - Copy\112\recordings\20170523_151426.params';  'ARTS LOPES - Copy\112\recordings\20170619_135108.params';  'ARTS LOPES - Copy\112\recordings\20170629_112134.params';  'ARTS LOPES - Copy\112\recordings\20170717_134855.params';  
% 'ARTS LOPES - Copy\112\recordings\20170523_152203.params';  'ARTS LOPES - Copy\112\recordings\20170622_154135.params';  'ARTS LOPES - Copy\112\recordings\20170703_134306.params';  'ARTS LOPES - Copy\112\recordings\20170718_133305.params';  
% 'ARTS LOPES - Copy\112\recordings\20170608_160700.params';  'ARTS LOPES - Copy\112\recordings\20170622_154532.params';  'ARTS LOPES - Copy\112\recordings\20170703_135522.params';  'ARTS LOPES - Copy\112\recordings\20170718_134528.params';  
% 'ARTS LOPES - Copy\112\recordings\20170608_161542.params';  'ARTS LOPES - Copy\112\recordings\20170622_155531.params';  'ARTS LOPES - Copy\112\recordings\20170704_111354.params';  'ARTS LOPES - Copy\112\recordings\20170720_111449.params';  
% 'ARTS LOPES - Copy\112\recordings\20170612_141055.params';  'ARTS LOPES - Copy\112\recordings\20170626_133629.params';  'ARTS LOPES - Copy\112\recordings\20170704_133805.params';  'ARTS LOPES - Copy\112\recordings\20170720_112513.params';  
% 'ARTS LOPES - Copy\112\recordings\20170612_142150.params';  'ARTS LOPES - Copy\112\recordings\20170626_134734.params';  'ARTS LOPES - Copy\112\recordings\20170704_135205.params';  
% 'ARTS LOPES - Copy\112\recordings\20170613_134214.params';  'ARTS LOPES - Copy\112\recordings\20170627_133759.params';  'ARTS LOPES - Copy\112\recordings\20170711_133750.params';  
% 'ARTS LOPES - Copy\112\recordings\20170613_135312.params';  'ARTS LOPES - Copy\112\recordings\20170627_134234.params';  'ARTS LOPES - Copy\112\recordings\20170711_134633.params';  
% 'ARTS LOPES - Copy\112\recordings\20170619_133541.params';  'ARTS LOPES - Copy\112\recordings\20170627_135218.params';  'ARTS LOPES - Copy\112\recordings\20170717_133644.params']; 
% p112paramfilenames = cellstr(p112paramfilenames); 
% 
% for i=1:length(p112sigfilenames)
%     [D112(i),P112(i)]=readSignalsFile(p112sigfilenames{i,1},p112paramfilenames{i,1});
% end
% 
% D112=OrderStructDate(D112);
% P112=OrderStructDate(P112);
% 
% %% P114
% p114sigfilenames=['ARTS LOPES - Copy\114\recordings\20180522_093846.signals';  'ARTS LOPES - Copy\114\recordings\20180522_094903.signals';  'ARTS LOPES - Copy\114\recordings\20180529_150941.signals';  'ARTS LOPES - Copy\114\recordings\20180530_082809.signals';  
% 'ARTS LOPES - Copy\114\recordings\20180522_094149.signals';  'ARTS LOPES - Copy\114\recordings\20180522_095044.signals';  'ARTS LOPES - Copy\114\recordings\20180529_151307.signals';  'ARTS LOPES - Copy\114\recordings\20180530_083236.signals';  
% 'ARTS LOPES - Copy\114\recordings\20180522_094419.signals';  'ARTS LOPES - Copy\114\recordings\20180529_150837.signals';  'ARTS LOPES - Copy\114\recordings\20180529_151800.signals'];  
% p114sigfilenames = cellstr(p114sigfilenames);  
% 
% p114paramfilenames=['ARTS LOPES - Copy\114\recordings\20180522_093846.params';  'ARTS LOPES - Copy\114\recordings\20180522_094903.params';  'ARTS LOPES - Copy\114\recordings\20180529_150941.params';  'ARTS LOPES - Copy\114\recordings\20180530_082809.params';  
% 'ARTS LOPES - Copy\114\recordings\20180522_094149.params';  'ARTS LOPES - Copy\114\recordings\20180522_095044.params';  'ARTS LOPES - Copy\114\recordings\20180529_151307.params';  'ARTS LOPES - Copy\114\recordings\20180530_083236.params';  
% 'ARTS LOPES - Copy\114\recordings\20180522_094419.params';  'ARTS LOPES - Copy\114\recordings\20180529_150837.params';  'ARTS LOPES - Copy\114\recordings\20180529_151800.params']; 
% p114paramfilenames = cellstr(p114paramfilenames); 
% 
% for i=1:length(p114sigfilenames)
%     [D114(i),P114(i)]=readSignalsFile(p114sigfilenames{i,1},p114paramfilenames{i,1});
% end
% 
% D114=OrderStructDate(D114);
% P114=OrderStructDate(P114);