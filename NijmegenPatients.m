%% Nijmegen Patients
%dir *.signals*
%dir *.params*
%% S02
pS02sigfilenames=['NijmegenPatientFiles\ARTS02\recordings\20160315_135059.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160324_170437.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160406_093850.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160420_093332.signals';  
'NijmegenPatientFiles\ARTS02\recordings\20160316_102914.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160329_132336.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160407_103045.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160421_134047.signals';  
'NijmegenPatientFiles\ARTS02\recordings\20160316_105258.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160330_132949.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160412_132858.signals';  
'NijmegenPatientFiles\ARTS02\recordings\20160317_170007.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160331_170113.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160413_092547.signals';  
'NijmegenPatientFiles\ARTS02\recordings\20160322_143406.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160405_143226.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160414_134222.signals';  
'NijmegenPatientFiles\ARTS02\recordings\20160323_093021.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160406_093431.signals'; 'NijmegenPatientFiles\ARTS02\recordings\20160419_133145.signals'];  
pS02sigfilenames = cellstr(pS02sigfilenames);  

pS02paramfilenames=['NijmegenPatientFiles\ARTS02\recordings\20160315_135059.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160324_170437.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160406_093850.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160420_093332.params';  
'NijmegenPatientFiles\ARTS02\recordings\20160316_102914.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160329_132336.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160407_103045.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160421_134047.params';  
'NijmegenPatientFiles\ARTS02\recordings\20160316_105258.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160330_132949.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160412_132858.params';  
'NijmegenPatientFiles\ARTS02\recordings\20160317_170007.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160331_170113.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160413_092547.params';  
'NijmegenPatientFiles\ARTS02\recordings\20160322_143406.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160405_143226.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160414_134222.params';  
'NijmegenPatientFiles\ARTS02\recordings\20160323_093021.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160406_093431.params'; 'NijmegenPatientFiles\ARTS02\recordings\20160419_133145.params']; 
pS02paramfilenames = cellstr(pS02paramfilenames); 

for i=1:length(pS02sigfilenames)
    [DS02(i),PS02(i)]=readSignalsFile(pS02sigfilenames{i,1},pS02paramfilenames{i,1});
end

%Order Structs according to date
DS02=OrderStructDate(DS02);
PS02=OrderStructDate(PS02);
% 
% % Calculate BWS (Body weight support)
% for i=1:length(DS02)
%     BWS= (PS02(i).data.Patient_Mass_kg-DS02(i).data.m_cont_treadmillData_Fy)./PS02(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(PS02(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(PS02(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(DS02(i).data.m_cont_stepCounter);
% end
% 
% avBWS=avBWS';
% avGGF=avGGF';
% avSpeed=avSpeed';
% steps=steps';

clear pS02sigfilenames pS02paramfilenames

%join 2&3
% data16.BWS=vertcat(((PS02(2).data.Patient_Mass_kg-DS02(2).data.m_cont_treadmillData_Fy)./PS02(2).data.Patient_Mass_kg.*100),((PS02(3).data.Patient_Mass_kg-DS02(3).data.m_cont_treadmillData_Fy)./PS02(3).data.Patient_Mass_kg.*100));
% meanBWS16=mean(data16.BWS);
% data16.steps=max(DS02(2).data.m_cont_stepCounter)+max(DS02(3).data.m_cont_stepCounter);
% data16.speed=vertcat(PS02(2).data.General_WalkingVelocity_m_s,PS02(3).data.General_WalkingVelocity_m_s);
% meanspeed16=mean(data16.speed);
% data16.GGF=vertcat(PS02(2).data.General_GuidanceForce_pct,(PS02(3).data.General_GuidanceForce_pct))
% meanGGF16=mean(data16.GGF);
% data16.duration=PS02(2).duration+PS02(3).duration;

%join 13&14
% data07.BWS=vertcat(((PS02(13).data.Patient_Mass_kg-DS02(13).data.m_cont_treadmillData_Fy)./PS02(13).data.Patient_Mass_kg.*100),((PS02(14).data.Patient_Mass_kg-DS02(14).data.m_cont_treadmillData_Fy)./PS02(14).data.Patient_Mass_kg.*100));
% meanBWS07=mean(data07.BWS);
% data07.steps=max(DS02(13).data.m_cont_stepCounter)+max(DS02(14).data.m_cont_stepCounter);
% data07.speed=vertcat(PS02(13).data.General_WalkingVelocity_m_s,PS02(14).data.General_WalkingVelocity_m_s);
% meanspeed07=mean(data07.speed);
% data07.GGF=vertcat(PS02(13).data.General_GuidanceForce_pct,(PS02(14).data.General_GuidanceForce_pct))
% meanGGF07=mean(data07.GGF);
% data07.duration=PS02(13).duration+PS02(14).duration;

%% S03
pS03sigfilenames=['NijmegenPatientFiles\ARTS03\recordings\20160518_084954.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160601_092621.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160608_164320.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160620_102749.signals';  
'NijmegenPatientFiles\ARTS03\recordings\20160520_082730.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160601_092951.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160610_121714.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160620_103429.signals';  
'NijmegenPatientFiles\ARTS03\recordings\20160524_162823.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160601_095121.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160613_103449.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160622_111050.signals';  
'NijmegenPatientFiles\ARTS03\recordings\20160525_082834.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160603_102851.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160615_162652.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160627_102759.signals';  
'NijmegenPatientFiles\ARTS03\recordings\20160527_083735.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160603_103651.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160617_102317.signals';  
'NijmegenPatientFiles\ARTS03\recordings\20160530_132704.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160606_103157.signals'; 'NijmegenPatientFiles\ARTS03\recordings\20160620_102635.signals'];  
pS03sigfilenames = cellstr(pS03sigfilenames);  

pS03paramfilenames=['NijmegenPatientFiles\ARTS03\recordings\20160518_084954.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160601_092621.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160608_164320.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160620_102749.params';  
'NijmegenPatientFiles\ARTS03\recordings\20160520_082730.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160601_092951.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160610_121714.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160620_103429.params';  
'NijmegenPatientFiles\ARTS03\recordings\20160524_162823.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160601_095121.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160613_103449.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160622_111050.params';  
'NijmegenPatientFiles\ARTS03\recordings\20160525_082834.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160603_102851.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160615_162652.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160627_102759.params';  
'NijmegenPatientFiles\ARTS03\recordings\20160527_083735.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160603_103651.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160617_102317.params';  
'NijmegenPatientFiles\ARTS03\recordings\20160530_132704.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160606_103157.params'; 'NijmegenPatientFiles\ARTS03\recordings\20160620_102635.params']; 
pS03paramfilenames = cellstr(pS03paramfilenames); 

for i=1:length(pS03sigfilenames)
    [DS03(i),PS03(i)]=readSignalsFile(pS03sigfilenames{i,1},pS03paramfilenames{i,1});
end

%Order Structs according to date
DS03=OrderStructDate(DS03);
PS03=OrderStructDate(PS03);

% Calculate BWS (Body weight support)
% for i=1:length(DS03)
%     BWS=(PS03(i).data.Patient_Mass_kg-DS03(i).data.m_cont_treadmillData_Fy)./PS03(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(PS03(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(PS03(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(DS03(i).data.m_cont_stepCounter);
% end
% 
% avBWS=avBWS';
% avGGF=avGGF';
% avSpeed=avSpeed';
% steps=steps';

clear pS03sigfilenames pS03paramfilenames

%join 7,8,9
% data7.BWS=vertcat(((PS03(7).data.Patient_Mass_kg-DS03(7).data.m_cont_treadmillData_Fy)./PS03(7).data.Patient_Mass_kg.*100),((PS03(8).data.Patient_Mass_kg-DS03(8).data.m_cont_treadmillData_Fy)./PS03(8).data.Patient_Mass_kg.*100),((PS03(9).data.Patient_Mass_kg-DS03(9).data.m_cont_treadmillData_Fy)./PS03(9).data.Patient_Mass_kg.*100));
% meanBWS7=mean(data7.BWS);
% data7.steps=max(DS03(7).data.m_cont_stepCounter)+max(DS03(8).data.m_cont_stepCounter)+max(DS03(9).data.m_cont_stepCounter);
% data7.speed=vertcat(PS03(7).data.General_WalkingVelocity_m_s,PS03(8).data.General_WalkingVelocity_m_s,PS03(9).data.General_WalkingVelocity_m_s);
% meanspeed7=mean(data7.speed);
% data7.GGF=vertcat(PS03(7).data.General_GuidanceForce_pct,(PS03(8).data.General_GuidanceForce_pct),(PS03(9).data.General_GuidanceForce_pct));
% meanGGF7=mean(data7.GGF);
% data7.duration=PS03(7).duration+PS03(8).duration+PS03(9).duration;

%join 18,19
% data18.BWS=vertcat(((PS03(18).data.Patient_Mass_kg-DS03(18).data.m_cont_treadmillData_Fy)./PS03(18).data.Patient_Mass_kg.*100),((PS03(19).data.Patient_Mass_kg-DS03(19).data.m_cont_treadmillData_Fy)./PS03(19).data.Patient_Mass_kg.*100));
% meanBWS18=mean(data18.BWS);
% data18.steps=max(DS03(18).data.m_cont_stepCounter)+max(DS03(19).data.m_cont_stepCounter);
% data18.speed=vertcat(PS03(18).data.General_WalkingVelocity_m_s,PS03(19).data.General_WalkingVelocity_m_s);
% meanspeed18=mean(data18.speed);
% data18.GGF=vertcat(PS03(18).data.General_GuidanceForce_pct,(PS03(19).data.General_GuidanceForce_pct));
% meanGGF18=mean(data18.GGF);
% data18.duration=PS03(18).duration+PS03(19).duration;


%% S04
pS04sigfilenames=['NijmegenPatientFiles\ARTS04\recordings\20160831_091548.signals'; 'NijmegenPatientFiles\ARTS04\recordings\20160831_091702.signals'; 'NijmegenPatientFiles\ARTS04\recordings\20160831_092514.signals'];  
pS04sigfilenames = cellstr(pS04sigfilenames);  

pS04paramfilenames=['NijmegenPatientFiles\ARTS04\recordings\20160831_091548.params'; 'NijmegenPatientFiles\ARTS04\recordings\20160831_091702.params'; 'NijmegenPatientFiles\ARTS04\recordings\20160831_092514.params']; 
pS04paramfilenames = cellstr(pS04paramfilenames); 

for i=1:length(pS04sigfilenames)
    [DS04(i),PS04(i)]=readSignalsFile(pS04sigfilenames{i,1},pS04paramfilenames{i,1});
end

%Order Structs according to date
DS04=OrderStructDate(DS04);
PS04=OrderStructDate(PS04);
% 
% % Calculate BWS (Body weight support)
% for i=1:length(DS04)
%     BWS= (PS04(i).data.Patient_Mass_kg-DS04(i).data.m_cont_treadmillData_Fy)./PS04(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(PS04(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(PS04(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(DS04(i).data.m_cont_stepCounter);
% end
% 
% avBWS=avBWS';
% avGGF=avGGF';
% avSpeed=avSpeed';
% steps=steps';
% 
clear pS04sigfilenames pS04paramfilenames

%% S05
pS05sigfilenames=['NijmegenPatientFiles\ARTS05\recordings\20160920_165558.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20160928_161412.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161005_160741.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_163956.signals';  
'NijmegenPatientFiles\ARTS05\recordings\20160920_170022.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20160930_112331.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161005_161047.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_164053.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160921_143413.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20160930_112633.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161011_165448.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_164821.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160921_143750.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161004_105805.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_160120.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_164859.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160921_143939.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161004_111536.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_161943.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_165302.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160922_103551.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161004_112137.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_162125.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161026_160742.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160927_105736.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161004_112415.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_162500.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161026_162128.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160927_110500.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161004_112730.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_162622.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161026_162335.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160928_160911.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161005_160141.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_163633.signals';
'NijmegenPatientFiles\ARTS05\recordings\20160928_161126.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161005_160418.signals'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_163848.signals'];  
pS05sigfilenames = cellstr(pS05sigfilenames);  

pS05paramfilenames=['NijmegenPatientFiles\ARTS05\recordings\20160920_165558.params'; 'NijmegenPatientFiles\ARTS05\recordings\20160928_161412.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161005_160741.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_163956.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160920_170022.params';  'NijmegenPatientFiles\ARTS05\recordings\20160930_112331.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161005_161047.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_164053.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160921_143413.params';  'NijmegenPatientFiles\ARTS05\recordings\20160930_112633.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161011_165448.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_164821.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160921_143750.params';  'NijmegenPatientFiles\ARTS05\recordings\20161004_105805.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_160120.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_164859.params'; 
'NijmegenPatientFiles\ARTS05\recordings\20160921_143939.params';  'NijmegenPatientFiles\ARTS05\recordings\20161004_111536.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_161943.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_165302.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160922_103551.params';  'NijmegenPatientFiles\ARTS05\recordings\20161004_112137.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_162125.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161026_160742.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160927_105736.params';  'NijmegenPatientFiles\ARTS05\recordings\20161004_112415.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161012_162500.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161026_162128.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160927_110500.params';  'NijmegenPatientFiles\ARTS05\recordings\20161004_112730.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_162622.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161026_162335.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160928_160911.params';  'NijmegenPatientFiles\ARTS05\recordings\20161005_160141.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_163633.params';  
'NijmegenPatientFiles\ARTS05\recordings\20160928_161126.params';  'NijmegenPatientFiles\ARTS05\recordings\20161005_160418.params'; 'NijmegenPatientFiles\ARTS05\recordings\20161025_163848.params']; 
pS05paramfilenames = cellstr(pS05paramfilenames); 

for i=1:length(pS05sigfilenames)
    [DS05(i),PS05(i)]=readSignalsFile(pS05sigfilenames{i,1},pS05paramfilenames{i,1});
end

%Order Structs according to date
DS05=OrderStructDate(DS05);
PS05=OrderStructDate(PS05);

% Calculate BWS (Body weight support)
% for i=1:length(DS05)
%     BWS= (PS05(i).data.Patient_Mass_kg-DS05(i).data.m_cont_treadmillData_Fy)./PS05(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(PS05(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(PS05(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(DS05(i).data.m_cont_stepCounter);
% end
% 
% avBWS=avBWS';
% avGGF=avGGF';
% avSpeed=avSpeed';
% steps=steps';

clear pS05sigfilenames pS05paramfilenames

% %join 1,2
% data1.BWS=vertcat(((PS05(1).data.Patient_Mass_kg-DS05(1).data.m_cont_treadmillData_Fy)./PS05(1).data.Patient_Mass_kg.*100),((PS05(2).data.Patient_Mass_kg-DS05(2).data.m_cont_treadmillData_Fy)./PS05(2).data.Patient_Mass_kg.*100));
% meanBWS1=mean(data1.BWS);
% data1.steps=max(DS05(1).data.m_cont_stepCounter)+max(DS05(2).data.m_cont_stepCounter);
% data1.speed=vertcat(PS05(1).data.General_WalkingVelocity_m_s,PS05(2).data.General_WalkingVelocity_m_s);
% meanspeed1=mean(data1.speed);
% data1.GGF=vertcat(PS05(1).data.General_GuidanceForce_pct,(PS05(2).data.General_GuidanceForce_pct));
% meanGGF1=mean(data1.GGF);
% data1.duration=PS05(1).duration+PS05(2).duration;
% 
% %join 3,4,5
% data3.BWS=vertcat(((PS05(3).data.Patient_Mass_kg-DS05(3).data.m_cont_treadmillData_Fy)./PS05(3).data.Patient_Mass_kg.*100),((PS05(4).data.Patient_Mass_kg-DS05(4).data.m_cont_treadmillData_Fy)./PS05(4).data.Patient_Mass_kg.*100),((PS05(5).data.Patient_Mass_kg-DS05(5).data.m_cont_treadmillData_Fy)./PS05(5).data.Patient_Mass_kg.*100));
% meanBWS3=mean(data3.BWS);
% data3.steps=max(DS05(3).data.m_cont_stepCounter)+max(DS05(4).data.m_cont_stepCounter)+max(DS05(5).data.m_cont_stepCounter);
% data3.speed=vertcat(PS05(3).data.General_WalkingVelocity_m_s,PS05(4).data.General_WalkingVelocity_m_s,PS05(5).data.General_WalkingVelocity_m_s);
% meanspeed3=mean(data3.speed);
% data3.GGF=vertcat(PS05(3).data.General_GuidanceForce_pct,(PS05(4).data.General_GuidanceForce_pct),(PS05(5).data.General_GuidanceForce_pct));
% meanGGF3=mean(data3.GGF);
% data3.duration=PS05(3).duration+PS05(4).duration+PS05(5).duration;
% 
% %join 7,8
% data7.BWS=vertcat(((PS05(7).data.Patient_Mass_kg-DS05(7).data.m_cont_treadmillData_Fy)./PS05(7).data.Patient_Mass_kg.*100),((PS05(8).data.Patient_Mass_kg-DS05(8).data.m_cont_treadmillData_Fy)./PS05(8).data.Patient_Mass_kg.*100));
% meanBWS7=mean(data7.BWS);
% data7.steps=max(DS05(7).data.m_cont_stepCounter)+max(DS05(8).data.m_cont_stepCounter);
% data7.speed=vertcat(PS05(7).data.General_WalkingVelocity_m_s,PS05(8).data.General_WalkingVelocity_m_s);
% meanspeed7=mean(data7.speed);
% data7.GGF=vertcat(PS05(7).data.General_GuidanceForce_pct,(PS05(8).data.General_GuidanceForce_pct));
% meanGGF7=mean(data7.GGF);
% data7.duration=PS05(7).duration+PS05(8).duration;
% 
% %join 9,10,11
% data9.BWS=vertcat(((PS05(9).data.Patient_Mass_kg-DS05(9).data.m_cont_treadmillData_Fy)./PS05(9).data.Patient_Mass_kg.*100),((PS05(10).data.Patient_Mass_kg-DS05(10).data.m_cont_treadmillData_Fy)./PS05(10).data.Patient_Mass_kg.*100),((PS05(11).data.Patient_Mass_kg-DS05(11).data.m_cont_treadmillData_Fy)./PS05(11).data.Patient_Mass_kg.*100));
% meanBWS9=mean(data9.BWS);
% data9.steps=max(DS05(9).data.m_cont_stepCounter)+max(DS05(10).data.m_cont_stepCounter);
% data9.speed=vertcat(PS05(9).data.General_WalkingVelocity_m_s,PS05(10).data.General_WalkingVelocity_m_s,PS05(11).data.General_WalkingVelocity_m_s);
% meanspeed9=mean(data9.speed);
% data9.GGF=vertcat(PS05(9).data.General_GuidanceForce_pct,(PS05(10).data.General_GuidanceForce_pct),(PS05(11).data.General_GuidanceForce_pct));
% meanGGF9=mean(data9.GGF);
% data9.duration=PS05(9).duration+PS05(10).duration+PS05(11).duration;
% 
% %join 12,13
% data12.BWS=vertcat(((PS05(12).data.Patient_Mass_kg-DS05(12).data.m_cont_treadmillData_Fy)./PS05(12).data.Patient_Mass_kg.*100),((PS05(13).data.Patient_Mass_kg-DS05(13).data.m_cont_treadmillData_Fy)./PS05(13).data.Patient_Mass_kg.*100));
% meanBWS12=mean(data12.BWS);
% data12.steps=max(DS05(12).data.m_cont_stepCounter)+max(DS05(13).data.m_cont_stepCounter);
% data12.speed=vertcat(PS05(12).data.General_WalkingVelocity_m_s,PS05(13).data.General_WalkingVelocity_m_s);
% meanspeed12=mean(data12.speed);
% data12.GGF=vertcat(PS05(12).data.General_GuidanceForce_pct,(PS05(13).data.General_GuidanceForce_pct));
% meanGGF12=mean(data12.GGF);
% data12.duration=PS05(12).duration+PS05(13).duration;
% 
% %join 14-15 16 17 18
% data15.BWS=vertcat(((PS05(14).data.Patient_Mass_kg-DS05(14).data.m_cont_treadmillData_Fy)./PS05(14).data.Patient_Mass_kg.*100),((PS05(15).data.Patient_Mass_kg-DS05(15).data.m_cont_treadmillData_Fy)./PS05(15).data.Patient_Mass_kg.*100),((PS05(16).data.Patient_Mass_kg-DS05(16).data.m_cont_treadmillData_Fy)./PS05(16).data.Patient_Mass_kg.*100),((PS05(17).data.Patient_Mass_kg-DS05(17).data.m_cont_treadmillData_Fy)./PS05(17).data.Patient_Mass_kg.*100),((PS05(18).data.Patient_Mass_kg-DS05(18).data.m_cont_treadmillData_Fy)./PS05(18).data.Patient_Mass_kg.*100));
% meanBWS15=mean(data15.BWS);
% data15.steps=max(DS05(14).data.m_cont_stepCounter)+max(DS05(15).data.m_cont_stepCounter)+max(DS05(16).data.m_cont_stepCounter)+max(DS05(17).data.m_cont_stepCounter)+max(DS05(18).data.m_cont_stepCounter);
% data15.speed=vertcat(PS05(14).data.General_WalkingVelocity_m_s,PS05(16).data.General_WalkingVelocity_m_s,PS05(15).data.General_WalkingVelocity_m_s,PS05(16).data.General_WalkingVelocity_m_s,PS05(17).data.General_WalkingVelocity_m_s,PS05(18).data.General_WalkingVelocity_m_s);
% meanspeed15=mean(data15.speed);
% data15.GGF=vertcat(PS05(14).data.General_GuidanceForce_pct,PS05(15).data.General_GuidanceForce_pct,(PS05(16).data.General_GuidanceForce_pct),(PS05(17).data.General_GuidanceForce_pct),(PS05(18).data.General_GuidanceForce_pct));
% meanGGF15=mean(data15.GGF);
% data15.duration=PS05(14).duration+PS05(15).duration+PS05(16).duration+PS05(17).duration+PS05(18).duration;
% 
% % 19,20 21 22
% data19.BWS=vertcat(((PS05(19).data.Patient_Mass_kg-DS05(19).data.m_cont_treadmillData_Fy)./PS05(19).data.Patient_Mass_kg.*100),((PS05(20).data.Patient_Mass_kg-DS05(20).data.m_cont_treadmillData_Fy)./PS05(20).data.Patient_Mass_kg.*100),((PS05(21).data.Patient_Mass_kg-DS05(21).data.m_cont_treadmillData_Fy)./PS05(21).data.Patient_Mass_kg.*100),((PS05(22).data.Patient_Mass_kg-DS05(22).data.m_cont_treadmillData_Fy)./PS05(22).data.Patient_Mass_kg.*100));
% meanBWS19=mean(data19.BWS);
% data19.steps=max(DS05(19).data.m_cont_stepCounter)+max(DS05(20).data.m_cont_stepCounter)+max(DS05(21).data.m_cont_stepCounter)+max(DS05(22).data.m_cont_stepCounter);
% data19.speed=vertcat(PS05(19).data.General_WalkingVelocity_m_s,PS05(20).data.General_WalkingVelocity_m_s,PS05(21).data.General_WalkingVelocity_m_s,PS05(22).data.General_WalkingVelocity_m_s);
% meanspeed19=mean(data19.speed);
% data19.GGF=vertcat(PS05(19).data.General_GuidanceForce_pct,(PS05(20).data.General_GuidanceForce_pct),(PS05(21).data.General_GuidanceForce_pct),(PS05(22).data.General_GuidanceForce_pct));
% meanGGF19=mean(data19.GGF);
% data19.duration=PS05(19).duration+PS05(20).duration+PS05(21).duration+PS05(22).duration;
% 
% %24,26,27
% data24.BWS=vertcat(((PS05(24).data.Patient_Mass_kg-DS05(24).data.m_cont_treadmillData_Fy)./PS05(24).data.Patient_Mass_kg.*100),((PS05(26).data.Patient_Mass_kg-DS05(26).data.m_cont_treadmillData_Fy)./PS05(26).data.Patient_Mass_kg.*100),((PS05(27).data.Patient_Mass_kg-DS05(27).data.m_cont_treadmillData_Fy)./PS05(27).data.Patient_Mass_kg.*100));
% meanBWS24=mean(data24.BWS);
% data24.steps=max(DS05(24).data.m_cont_stepCounter)+max(DS05(26).data.m_cont_stepCounter)+max(DS05(27).data.m_cont_stepCounter);
% data24.speed=vertcat(PS05(24).data.General_WalkingVelocity_m_s,PS05(26).data.General_WalkingVelocity_m_s,PS05(27).data.General_WalkingVelocity_m_s);
% meanspeed24=mean(data24.speed);
% data24.GGF=vertcat(PS05(24).data.General_GuidanceForce_pct,(PS05(26).data.General_GuidanceForce_pct),(PS05(27).data.General_GuidanceForce_pct));
% meanGGF24=mean(data24.GGF);
% data24.duration=PS05(24).duration+PS05(26).duration+PS05(27).duration;
% 
% %31,32,33
% data31.BWS=vertcat(((PS05(31).data.Patient_Mass_kg-DS05(31).data.m_cont_treadmillData_Fy)./PS05(31).data.Patient_Mass_kg.*100),((PS05(32).data.Patient_Mass_kg-DS05(32).data.m_cont_treadmillData_Fy)./PS05(32).data.Patient_Mass_kg.*100),((PS05(33).data.Patient_Mass_kg-DS05(33).data.m_cont_treadmillData_Fy)./PS05(33).data.Patient_Mass_kg.*100));
% meanBWS31=mean(data31.BWS);
% data31.steps=max(DS05(31).data.m_cont_stepCounter)+max(DS05(32).data.m_cont_stepCounter)+max(DS05(33).data.m_cont_stepCounter);
% data31.speed=vertcat(PS05(31).data.General_WalkingVelocity_m_s,PS05(32).data.General_WalkingVelocity_m_s,PS05(33).data.General_WalkingVelocity_m_s);
% meanspeed31=mean(data31.speed);
% data31.GGF=vertcat(PS05(31).data.General_GuidanceForce_pct,(PS05(32).data.General_GuidanceForce_pct),(PS05(33).data.General_GuidanceForce_pct));
% meanGGF31=mean(data31.GGF);
% data31.duration=PS05(31).duration+PS05(32).duration+PS05(33).duration;
% 
% %36,38
% data36.BWS=vertcat(((PS05(36).data.Patient_Mass_kg-DS05(36).data.m_cont_treadmillData_Fy)./PS05(36).data.Patient_Mass_kg.*100),((PS05(38).data.Patient_Mass_kg-DS05(38).data.m_cont_treadmillData_Fy)./PS05(38).data.Patient_Mass_kg.*100));
% meanBWS36=mean(data36.BWS);
% data36.steps=max(DS05(36).data.m_cont_stepCounter)+max(DS05(38).data.m_cont_stepCounter);
% data36.speed=vertcat(PS05(36).data.General_WalkingVelocity_m_s,PS05(38).data.General_WalkingVelocity_m_s);
% meanspeed36=mean(data36.speed);
% data36.GGF=vertcat(PS05(36).data.General_GuidanceForce_pct,(PS05(38).data.General_GuidanceForce_pct));
% meanGGF36=mean(data36.GGF);
% data36.duration=PS05(36).duration+PS05(38).duration;

%% S06
pS06sigfilenames=['NijmegenPatientFiles\ARTS06\recordings\20161010_154420.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161019_140412.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161028_111343.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161121_102836.signals';  
'NijmegenPatientFiles\ARTS06\recordings\20161010_154644.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161021_093036.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161031_103343.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161124_110419.signals';
'NijmegenPatientFiles\ARTS06\recordings\20161010_154947.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161021_094054.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161031_104552.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161125_093749.signals';
'NijmegenPatientFiles\ARTS06\recordings\20161012_140738.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161024_102329.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161031_105225.signals';
'NijmegenPatientFiles\ARTS06\recordings\20161014_094054.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161026_092928.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161102_133014.signals';  
'NijmegenPatientFiles\ARTS06\recordings\20161017_103114.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161026_093935.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161103_102334.signals';  
'NijmegenPatientFiles\ARTS06\recordings\20161017_104747.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161028_110225.signals'; 'NijmegenPatientFiles\ARTS06\recordings\20161117_112913.signals'];  
pS06sigfilenames = cellstr(pS06sigfilenames);  

pS06paramfilenames=['NijmegenPatientFiles\ARTS06\recordings\20161010_154420.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161019_140412.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161028_111343.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161121_102836.params';  
'NijmegenPatientFiles\ARTS06\recordings\20161010_154644.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161021_093036.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161031_103343.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161124_110419.params';  
'NijmegenPatientFiles\ARTS06\recordings\20161010_154947.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161021_094054.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161031_104552.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161125_093749.params';  
'NijmegenPatientFiles\ARTS06\recordings\20161012_140738.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161024_102329.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161031_105225.params';  
'NijmegenPatientFiles\ARTS06\recordings\20161014_094054.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161026_092928.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161102_133014.params';  
'NijmegenPatientFiles\ARTS06\recordings\20161017_103114.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161026_093935.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161103_102334.params';  
'NijmegenPatientFiles\ARTS06\recordings\20161017_104747.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161028_110225.params'; 'NijmegenPatientFiles\ARTS06\recordings\20161117_112913.params']; 
pS06paramfilenames = cellstr(pS06paramfilenames); 

for i=1:length(pS06sigfilenames)
    [DS06(i),PS06(i)]=readSignalsFile(pS06sigfilenames{i,1},pS06paramfilenames{i,1});
end

%Order Structs according to date
DS06=OrderStructDate(DS06);
PS06=OrderStructDate(PS06);

% Calculate BWS (Body weight support)
% for i=1:length(DS06)
%     BWS= (PS06(i).data.Patient_Mass_kg-DS06(i).data.m_cont_treadmillData_Fy)./PS06(i).data.Patient_Mass_kg.*100;
%     avBWS(i)=mean(BWS);
%     avGGF(i)=mean(PS06(i).data.General_GuidanceForce_pct);
%     avSpeed(i)=mean(PS06(i).data.General_WalkingVelocity_m_s);
%     steps(i)=max(DS06(i).data.m_cont_stepCounter);
% end
% 
% avBWS=avBWS';
% avGGF=avGGF';
% avSpeed=avSpeed';
% steps=steps';

clear pS06sigfilenames pS06paramfilenames

% %join 1,2,3
% data1.BWS=vertcat(((PS06(1).data.Patient_Mass_kg-DS06(1).data.m_cont_treadmillData_Fy)./PS06(1).data.Patient_Mass_kg.*100),((PS06(2).data.Patient_Mass_kg-DS06(2).data.m_cont_treadmillData_Fy)./PS06(2).data.Patient_Mass_kg.*100),((PS06(3).data.Patient_Mass_kg-DS06(3).data.m_cont_treadmillData_Fy)./PS06(3).data.Patient_Mass_kg.*100));
% meanBWS1=mean(data1.BWS);
% data1.steps=max(DS06(1).data.m_cont_stepCounter)+max(DS06(2).data.m_cont_stepCounter)+max(DS06(3).data.m_cont_stepCounter);
% data1.speed=vertcat(PS06(1).data.General_WalkingVelocity_m_s,PS06(2).data.General_WalkingVelocity_m_s,PS06(3).data.General_WalkingVelocity_m_s);
% meanspeed1=mean(data1.speed);
% data1.GGF=vertcat(PS06(1).data.General_GuidanceForce_pct,(PS06(2).data.General_GuidanceForce_pct),(PS06(3).data.General_GuidanceForce_pct));
% meanGGF1=mean(data1.GGF);
% data1.duration=PS06(1).duration+PS06(2).duration+PS06(3).duration;
% 
% %join 6,7
% data6.BWS=vertcat(((PS06(6).data.Patient_Mass_kg-DS06(6).data.m_cont_treadmillData_Fy)./PS06(6).data.Patient_Mass_kg.*100),((PS06(7).data.Patient_Mass_kg-DS06(7).data.m_cont_treadmillData_Fy)./PS06(7).data.Patient_Mass_kg.*100));
% meanBWS6=mean(data6.BWS);
% data6.steps=max(DS06(6).data.m_cont_stepCounter)+max(DS06(7).data.m_cont_stepCounter);
% data6.speed=vertcat(PS06(6).data.General_WalkingVelocity_m_s,PS06(7).data.General_WalkingVelocity_m_s);
% meanspeed6=mean(data6.speed);
% data6.GGF=vertcat(PS06(6).data.General_GuidanceForce_pct,(PS06(7).data.General_GuidanceForce_pct));
% meanGGF6=mean(data6.GGF);
% data6.duration=PS06(6).duration+PS06(7).duration;
% 
% %join 9,10
% data9.BWS=vertcat(((PS06(9).data.Patient_Mass_kg-DS06(9).data.m_cont_treadmillData_Fy)./PS06(9).data.Patient_Mass_kg.*100),((PS06(10).data.Patient_Mass_kg-DS06(10).data.m_cont_treadmillData_Fy)./PS06(10).data.Patient_Mass_kg.*100));
% meanBWS9=mean(data9.BWS);
% data9.steps=max(DS06(9).data.m_cont_stepCounter)+max(DS06(10).data.m_cont_stepCounter);
% data9.speed=vertcat(PS06(9).data.General_WalkingVelocity_m_s,PS06(10).data.General_WalkingVelocity_m_s);
% meanspeed9=mean(data9.speed);
% data9.GGF=vertcat(PS06(9).data.General_GuidanceForce_pct,(PS06(10).data.General_GuidanceForce_pct));
% meanGGF9=mean(data9.GGF);
% data9.duration=PS06(9).duration+PS06(10).duration;
% 
% %join 12,13
% data12.BWS=vertcat(((PS06(12).data.Patient_Mass_kg-DS06(12).data.m_cont_treadmillData_Fy)./PS06(12).data.Patient_Mass_kg.*100),((PS06(13).data.Patient_Mass_kg-DS06(13).data.m_cont_treadmillData_Fy)./PS06(13).data.Patient_Mass_kg.*100));
% meanBWS12=mean(data12.BWS);
% data12.steps=max(DS06(12).data.m_cont_stepCounter)+max(DS06(13).data.m_cont_stepCounter);
% data12.speed=vertcat(PS06(12).data.General_WalkingVelocity_m_s,PS06(13).data.General_WalkingVelocity_m_s);
% meanspeed12=mean(data12.speed);
% data12.GGF=vertcat(PS06(12).data.General_GuidanceForce_pct,(PS06(13).data.General_GuidanceForce_pct));
% meanGGF12=mean(data12.GGF);
% data12.duration=PS06(12).duration+PS06(13).duration;
% 
% %join 14,15
% data14.BWS=vertcat(((PS06(14).data.Patient_Mass_kg-DS06(14).data.m_cont_treadmillData_Fy)./PS06(14).data.Patient_Mass_kg.*100),((PS06(15).data.Patient_Mass_kg-DS06(15).data.m_cont_treadmillData_Fy)./PS06(15).data.Patient_Mass_kg.*100));
% meanBWS14=mean(data14.BWS);
% data14.steps=max(DS06(14).data.m_cont_stepCounter)+max(DS06(15).data.m_cont_stepCounter);
% data14.speed=vertcat(PS06(14).data.General_WalkingVelocity_m_s,PS06(15).data.General_WalkingVelocity_m_s);
% meanspeed14=mean(data14.speed);
% data14.GGF=vertcat(PS06(14).data.General_GuidanceForce_pct,(PS06(15).data.General_GuidanceForce_pct));
% meanGGF14=mean(data14.GGF);
% data14.duration=PS06(14).duration+PS06(15).duration;


%% test data Utwente (me)
%[Dtest,Ptest]=readSignalsFile('LopesTestData\20180921_094945.signals','LopesTestData\20180921_094945.params');

%% S07
pS07sigfilenames=['NijmegenPatientFiles\ARTS07\20190820_101311.signals';  'NijmegenPatientFiles\ARTS07\20190827_103700.signals';  'NijmegenPatientFiles\ARTS07\20190829_102853.signals';  
'NijmegenPatientFiles\ARTS07\20190820_101530.signals';  'NijmegenPatientFiles\ARTS07\20190827_103852.signals';  'NijmegenPatientFiles\ARTS07\20190829_103620.signals';  
'NijmegenPatientFiles\ARTS07\20190822_104734.signals';  'NijmegenPatientFiles\ARTS07\20190827_104406.signals';  'NijmegenPatientFiles\ARTS07\20190829_104109.signals';  
'NijmegenPatientFiles\ARTS07\20190822_110459.signals';  'NijmegenPatientFiles\ARTS07\20190827_105627.signals';  'NijmegenPatientFiles\ARTS07\20190829_110824.signals';  
'NijmegenPatientFiles\ARTS07\20190822_111106.signals';  'NijmegenPatientFiles\ARTS07\20190827_110057.signals';  'NijmegenPatientFiles\ARTS07\20190829_111358.signals';  
'NijmegenPatientFiles\ARTS07\20190822_111638.signals';  'NijmegenPatientFiles\ARTS07\20190827_110400.signals'];  
pS07sigfilenames = cellstr(pS07sigfilenames);  

pS07paramfilenames=['NijmegenPatientFiles\ARTS07\20190820_101311.params';  'NijmegenPatientFiles\ARTS07\20190822_111106.params';  'NijmegenPatientFiles\ARTS07\20190827_104406.params';  'NijmegenPatientFiles\ARTS07\20190829_102853.params'; 'NijmegenPatientFiles\ARTS07\20190829_111358.params';  
'NijmegenPatientFiles\ARTS07\20190820_101530.params';  'NijmegenPatientFiles\ARTS07\20190822_111638.params';  'NijmegenPatientFiles\ARTS07\20190827_105627.params';  'NijmegenPatientFiles\ARTS07\20190829_103620.params';  
'NijmegenPatientFiles\ARTS07\20190822_104734.params';  'NijmegenPatientFiles\ARTS07\20190827_103700.params';  'NijmegenPatientFiles\ARTS07\20190827_110057.params';  'NijmegenPatientFiles\ARTS07\20190829_104109.params';  
'NijmegenPatientFiles\ARTS07\20190822_110459.params';  'NijmegenPatientFiles\ARTS07\20190827_103852.params';  'NijmegenPatientFiles\ARTS07\20190827_110400.params';  'NijmegenPatientFiles\ARTS07\20190829_110824.params']; 
pS07paramfilenames = cellstr(pS07paramfilenames); 

for i=1:length(pS07sigfilenames)
    [DS07(i),PS07(i)]=readSignalsFile(pS07sigfilenames{i,1},pS07paramfilenames{i,1});
end

%Order Structs according to date
DS07=OrderStructDate(DS07);
PS07=OrderStructDate(PS07);

clear pS07sigfilenames pS07paramfilenames
% 
%% S08

pS08sigfilenames=['NijmegenPatientFiles\ARTS08\20190822_114225.signals';  'NijmegenPatientFiles\ARTS08\20190822_120700.signals';  'NijmegenPatientFiles\ARTS08\20190827_142521.signals';  'NijmegenPatientFiles\ARTS08\20190829_113638.signals';  'NijmegenPatientFiles\ARTS08\20190829_120115.signals';  
'NijmegenPatientFiles\ARTS08\20190822_114942.signals';  'NijmegenPatientFiles\ARTS08\20190827_135812.signals';  'NijmegenPatientFiles\ARTS08\20190827_144306.signals';  'NijmegenPatientFiles\ARTS08\20190829_114615.signals';  'NijmegenPatientFiles\ARTS08\20190829_121043.signals';  
'NijmegenPatientFiles\ARTS08\20190822_115857.signals';  'NijmegenPatientFiles\ARTS08\20190827_141550.signals';  'NijmegenPatientFiles\ARTS08\20190829_112908.signals';  'NijmegenPatientFiles\ARTS08\20190829_115455.signals'];  
pS08sigfilenames = cellstr(pS08sigfilenames);  

pS08paramfilenames=['NijmegenPatientFiles\ARTS08\20190822_114225.params';  'NijmegenPatientFiles\ARTS08\20190822_120700.params';  'NijmegenPatientFiles\ARTS08\20190827_142521.params';  'NijmegenPatientFiles\ARTS08\20190829_113638.params';  'NijmegenPatientFiles\ARTS08\20190829_120115.params';  
'NijmegenPatientFiles\ARTS08\20190822_114942.params';  'NijmegenPatientFiles\ARTS08\20190827_135812.params';  'NijmegenPatientFiles\ARTS08\20190827_144306.params';  'NijmegenPatientFiles\ARTS08\20190829_114615.params';  'NijmegenPatientFiles\ARTS08\20190829_121043.params';  
'NijmegenPatientFiles\ARTS08\20190822_115857.params';  'NijmegenPatientFiles\ARTS08\20190827_141550.params';  'NijmegenPatientFiles\ARTS08\20190829_112908.params';  'NijmegenPatientFiles\ARTS08\20190829_115455.params']; 
pS08paramfilenames = cellstr(pS08paramfilenames); 

for i=1:length(pS08sigfilenames)
    [DS08(i),PS08(i)]=readSignalsFile(pS08sigfilenames{i,1},pS08paramfilenames{i,1});
end

%Order Structs according to date
DS08=OrderStructDate(DS08);
PS08=OrderStructDate(PS08);

clear pS08sigfilenames pS08paramfilenames