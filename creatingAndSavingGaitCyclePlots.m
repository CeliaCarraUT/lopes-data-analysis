%% code for generating all the gait cycle plots
%calculate power and velocity and store into 'data'and 'rawdata' to check

%remove INVALID TRIALS from all patients
%load the R/L paretic structures
%load('FINALpareticRLstructs.mat')
load('RLstructs.mat')

center=input('Introduce the name of the participants center (SMK or RRD): ','s');

[DS02,~]=removeInvalidTrials(DS02,PS02);
[DS03,~]=removeInvalidTrials(DS03,PS03);
[DS04,~]=removeInvalidTrials(DS04,PS04);
[DS05,~]=removeInvalidTrials(DS05,PS05);
[DS06,~]=removeInvalidTrials(DS06,PS06);
[DS07,~]=removeInvalidTrials(DS07,PS07);
[DS08,~]=removeInvalidTrials(DS08,PS08);
% [D4,~]=removeInvalidTrials(D4,P4);
% [D10,~]=removeInvalidTrials(D10,P10);
% [D22,~]=removeInvalidTrials(D22,P22);
% [D30,~]=removeInvalidTrials(D30,P30);
% [D105,~]=removeInvalidTrials(D105,P105);
% [D108,~]=removeInvalidTrials(D108,P108);
% [D110,~]=removeInvalidTrials(D110,P110);

clear PS02 PS03 PS04 PS05 PS06
%clear P4 P10 P22 P30 P105 P108 P110

Patients={'DS02';'DS03';'DS04';'DS05';'DS06'}; %SMK
Patients2={'D4';'D10';'D22';'D30';'D105';'D108';'D110'}; %RRD

for p=1:length(Patients) %change taking into account patient set
    DataSet=eval(Patients{p,1});  %change taking into account patient set
    DataSet=addResultFieldStruct(DataSet);
    
    for i=1:length(DataSet) 
        DataSet(i)=calculateWorkSteps(DataSet(i));
%         DataSet(i).data.Work_PX=DataSet(i).Results.Work_PX;
%         DataSet(i).map{end+1}='Work_PX';   
%         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_PX;
%         
%         DataSet(i).data.Work_PZ=DataSet(i).Results.Work_PZ;
%         DataSet(i).map{end+1}='Work_PZ';   
%         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_PZ;
%         
%         DataSet(i).data.Work_LHF=DataSet(i).Results.Work_LHF;
%         DataSet(i).map{end+1}='Work_LHF';   
%         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_LHF;
%         
%         DataSet(i).data.Work_LHA=DataSet(i).Results.Work_LHA;
%         DataSet(i).map{end+1}='Work_LHA';   
%         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_LHA;
%         
%         DataSet(i).data.Work_LKF=DataSet(i).Results.Work_LKF;
%         DataSet(i).map{end+1}='Work_LKF';   
%         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_LKF;
%         
%         DataSet(i).data.Work_RHF=DataSet(i).Results.Work_RHF;
%         DataSet(i).map{end+1}='Work_RHF';   
%         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_RHF;
%         
%         DataSet(i).data.Work_RHA=DataSet(i).Results.Work_RHA;
%         DataSet(i).map{end+1}='Work_RHA';   
%         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_RHA;
%         
%         DataSet(i).data.Work_RKF=DataSet(i).Results.Work_RKF;
% %         DataSet(i).map{end+1}='Work_RKF';   
% %         DataSet(i).rawdata(:,end+1)=DataSet(i).data.Work_RKF;
        
        DataSet(i).data.TotalPower_PX=DataSet(i).data.m_cont_jointForcesMeasured_PX.*gradient(DataSet(i).data.m_cont_jointAngles_PX,0.01);
        DataSet(i).map{end+1}='TotalPower_PX';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_PX;
        
        DataSet(i).data.TotalPower_PZ=DataSet(i).data.m_cont_jointForcesMeasured_PZ.*gradient(DataSet(i).data.m_cont_jointAngles_PZ,0.01);
        DataSet(i).map{end+1}='TotalPower_PZ';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_PZ;
        
        DataSet(i).data.TotalPower_LHF=DataSet(i).data.m_cont_jointForcesMeasured_LHF.*gradient(DataSet(i).data.m_cont_jointAngles_LHF,0.01);
        DataSet(i).map{end+1}='TotalPower_LHF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_LHF;
        
        DataSet(i).data.TotalPower_LHA=DataSet(i).data.m_cont_jointForcesMeasured_LHA.*gradient(DataSet(i).data.m_cont_jointAngles_LHA,0.01);
        DataSet(i).map{end+1}='TotalPower_LHA';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_LHA;
        
        DataSet(i).data.TotalPower_LKF=DataSet(i).data.m_cont_jointForcesMeasured_LKF.*gradient(DataSet(i).data.m_cont_jointAngles_LKF,0.01);
        DataSet(i).map{end+1}='TotalPower_LKF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_LKF;
        
        DataSet(i).data.TotalPower_RHF=DataSet(i).data.m_cont_jointForcesMeasured_RHF.*gradient(DataSet(i).data.m_cont_jointAngles_RHF,0.01);
        DataSet(i).map{end+1}='TotalPower_RHF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_RHF;
        
        DataSet(i).data.TotalPower_RHA=DataSet(i).data.m_cont_jointForcesMeasured_RHA.*gradient(DataSet(i).data.m_cont_jointAngles_RHA,0.01);
        DataSet(i).map{end+1}='TotalPower_RHA';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_RHA;
        
        DataSet(i).data.TotalPower_RKF=DataSet(i).data.m_cont_jointForcesMeasured_RKF.*gradient(DataSet(i).data.m_cont_jointAngles_RKF,0.01);
        DataSet(i).map{end+1}='TotalPower_RKF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.TotalPower_RKF;
        
        %joint velocity
        DataSet(i).data.JointVelocity_PX=gradient(DataSet(i).data.m_cont_jointAngles_PX,0.01);
        DataSet(i).map{end+1}='JointVelocity_PX';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_PX;
        
        DataSet(i).data.JointVelocity_PZ=gradient(DataSet(i).data.m_cont_jointAngles_PZ,0.01);
        DataSet(i).map{end+1}='JointVelocity_PZ';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_PZ;
        
        DataSet(i).data.JointVelocity_LHF=gradient(DataSet(i).data.m_cont_jointAngles_LHF,0.01);
        DataSet(i).map{end+1}='JointVelocity_LHF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_LHF;
        
        DataSet(i).data.JointVelocity_LHA=gradient(DataSet(i).data.m_cont_jointAngles_LHA,0.01);
        DataSet(i).map{end+1}='JointVelocity_LHA';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_LHA;
        
        DataSet(i).data.JointVelocity_LKF=gradient(DataSet(i).data.m_cont_jointAngles_LKF,0.01);
        DataSet(i).map{end+1}='JointVelocity_LKF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_LKF;
        
        DataSet(i).data.JointVelocity_RHF=gradient(DataSet(i).data.m_cont_jointAngles_RHF,0.01);
        DataSet(i).map{end+1}='JointVelocity_RHF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_RHF;
        
        DataSet(i).data.JointVelocity_RHA=gradient(DataSet(i).data.m_cont_jointAngles_RHA,0.01);
        DataSet(i).map{end+1}='JointVelocity_RHA';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_RHA;
        
        DataSet(i).data.JointVelocity_RKF=gradient(DataSet(i).data.m_cont_jointAngles_RKF,0.01);
        DataSet(i).map{end+1}='JointVelocity_RKF';   
        DataSet(i).rawdata(:,end+1)=DataSet(i).data.JointVelocity_RKF;
    end
    assignin('base',Patients{p,1},DataSet);
    clear DataSet
end

%% Only joint angles, assistance level and interaction forces (one example: subject-DS02, trial-11 , parameter-RightstepHeightGF)

parameters=fieldnames(RpareticVAR);
for k=1:length(parameters)
    if strcmp(parameters{k,1},'WeightShift_GuidanceForce_pct')==1 || strcmp(parameters{k,1},'RightStepHeight_GuidanceForce_pct')==1 || strcmp(parameters{k,1},'RightStabilityStance_GuidanceForce_pct')==1 || strcmp(parameters{k,1},'RightStepLength_GuidanceForce_pct')==1 ...
            || strcmp(parameters{k,1},'RightPrepositioning_GuidanceForce_pct')==1

        %1st check that the matrix contains the values of the patients from the specific center first
        if strcmp(center,'SMK')==1
            RpareticVAR.(parameters{k,1})=sortrows(RpareticVAR.(parameters{k,1}),11);
        elseif strcmp(center,'RRD')==1
            %change order of the matrix for RRD patients (so it runs through the loop)
            RpareticVAR.(parameters{k,1})=sortrows(RpareticVAR.(parameters{k,1}),-11);
        end

        %check the trial number and select the adequate dataset to slipt into steps
        for m=1:size(RpareticVAR.(parameters{k,1}),1)
            if strcmp(center,'SMK')==1
                if RpareticVAR.(parameters{k,1})(m,11)==1
                    subject=1;
                     DataSet=DS02;
                    SignalsdataSteps=splitIntoSteps(DS02(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==2
                    subject=2;
                     DataSet=DS03;
                    SignalsdataSteps=splitIntoSteps(DS03(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==3
                    subject=3;
                     DataSet=DS04;
                    SignalsdataSteps=splitIntoSteps(DS04(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==4
                    subject=4;
                     DataSet=DS05;
                    SignalsdataSteps=splitIntoSteps(DS05(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==5
                    subject=5;
                     DataSet=DS06;
                    SignalsdataSteps=splitIntoSteps(DS06(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==6
                    subject=7;
                     DataSet=DS07;
                    SignalsdataSteps=splitIntoSteps(DS07(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==7
                    subject=7;
                     DataSet=DS08;
                    SignalsdataSteps=splitIntoSteps(DS08(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                else
                    break
                end
            elseif strcmp(center,'RRD')==1 %check the RRD patients datasets
                 if RpareticVAR.(parameters{k,1})(m,11)==8
                    subject=8;
                    DataSet=D4;
                    SignalsdataSteps=splitIntoSteps(D4(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==9
                    subject=9;
                    DataSet=D10;
                    SignalsdataSteps=splitIntoSteps(D10(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==10
                    subject=10;
                    DataSet=D22;
                    SignalsdataSteps=splitIntoSteps(D22(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==11
                    subject=11;
                    DataSet=D30;
                    SignalsdataSteps=splitIntoSteps(D30(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==12
                    subject=12;
                    DataSet=D105;
                    SignalsdataSteps=splitIntoSteps(D105(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==13
                    subject=13;
                    DataSet=D108;
                    SignalsdataSteps=splitIntoSteps(D108(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif RpareticVAR.(parameters{k,1})(m,11)==14
                    subject=14;
                    DataSet=D110;
                    SignalsdataSteps=splitIntoSteps(D110(RpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                 else
                    break
                end
            end
            
            step2=RpareticVAR.(parameters{k,1})(m,14); %select step form the loop
            %change the title depending on the inputs
            trial=RpareticVAR.(parameters{k,1})(m,13);
            GGF=floor(RpareticVAR.(parameters{k,1})(m,12));
            initalParam=RpareticVAR.(parameters{k,1})(m,1);
            finalParam=RpareticVAR.(parameters{k,1})(m,2);
                
            if strcmp(parameters{k,1},'WeightShift_GuidanceForce_pct')==1 
                parameter='Weight Shift GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                indexes=[2 4 7];
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\WeightShift\';
            elseif strcmp(parameters{k,1},'RightStepHeight_GuidanceForce_pct')==1
                parameter='Right Step Height GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                indexes=8;
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\RightStepHeight\';
            elseif strcmp(parameters{k,1},'RightStabilityStance_GuidanceForce_pct')==1
                parameter='Right Stability GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                indexes=8;
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\RightStability\';
            elseif strcmp(parameters{k,1},'RightStepLength_GuidanceForce_pct')==1
                parameter='Right Step Length GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                indexes=6;
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\RightStepLength\';
            elseif strcmp(parameters{k,1},'RightPrepositioning_GuidanceForce_pct')==1
                parameter='Right Prepositioning GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                indexes=[6 8];
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\RightPrepositioning\';
            end
               
                %select the reference step
                jointsRef.PX=mean(SignalsdataSteps.data.r_cont_jointAngles_PX(:,step2-10:step2+10),2);
                jointsRef.PZ=mean(SignalsdataSteps.data.r_cont_jointAngles_PZ(:,step2-10:step2+10),2);
                jointsRef.LHF=mean(SignalsdataSteps.data.r_cont_jointAngles_LHF(:,step2-10:step2+10),2);
                jointsRef.LHA=mean(SignalsdataSteps.data.r_cont_jointAngles_LHA(:,step2-10:step2+10),2);
                jointsRef.LKF=mean(SignalsdataSteps.data.r_cont_jointAngles_LKF(:,step2-10:step2+10),2);
                jointsRef.RHF=mean(SignalsdataSteps.data.r_cont_jointAngles_RHF(:,step2-10:step2+10),2);
                jointsRef.RHA=mean(SignalsdataSteps.data.r_cont_jointAngles_RHA(:,step2-10:step2+10),2);
                jointsRef.RKF=mean(SignalsdataSteps.data.r_cont_jointAngles_RKF(:,step2-10:step2+10),2);

                %calculate joint ANGLES before and after for every joint
                %pelvis
                VarBefore.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2-10:step2-1),2);
                VarAfter.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2+1:step2+10),2);
                VarBefore.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2-10:step2-1),2);
                VarAfter.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2+1:step2+10),2);
                %left
                VarBefore.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2-10:step2-1),2);
                VarAfter.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2+1:step2+10),2);
                VarBefore.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2-10:step2-1),2);
                VarAfter.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2+1:step2+10),2);
                VarBefore.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2-10:step2-1),2);
                VarAfter.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2+1:step2+10),2);
                %Right
                VarBefore.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2-10:step2-1),2);
                VarAfter.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2+1:step2+10),2);
                VarBefore.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2-10:step2-1),2);
                VarAfter.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2+1:step2+10),2);
                VarBefore.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2-10:step2-1),2);
                VarAfter.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2+1:step2+10),2);

                %interaction FORCES before and after
                %pelvis
                ForcesBefore.PX=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PX(:,step2-10:step2-1),2);
                ForcesAfter.PX=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PX(:,step2+1:step2+10),2);
                ForcesBefore.PZ=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PZ(:,step2-10:step2-1),2);
                ForcesAfter.PZ=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PZ(:,step2+1:step2+10),2);
                %left
                ForcesBefore.LHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHF(:,step2-10:step2-1),2);
                ForcesAfter.LHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHF(:,step2+1:step2+10),2);
                ForcesBefore.LHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHA(:,step2-10:step2-1),2);
                ForcesAfter.LHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHA(:,step2+1:step2+10),2);
                ForcesBefore.LKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LKF(:,step2-10:step2-1),2);
                ForcesAfter.LKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LKF(:,step2+1:step2+10),2);
                %Right
                ForcesBefore.RHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHF(:,step2-10:step2-1),2);
                ForcesAfter.RHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHF(:,step2+1:step2+10),2);
                ForcesBefore.RHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHA(:,step2-10:step2-1),2);
                ForcesAfter.RHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHA(:,step2+1:step2+10),2);
                ForcesBefore.RKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RKF(:,step2-10:step2-1),2);
                ForcesAfter.RKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RKF(:,step2+1:step2+10),2);

                %Joint Stiffness Before and After
                %pelvis
                StiffBefore.PX=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PX(:,step2-10:step2-1),2);
                StiffAfter.PX=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PX(:,step2+1:step2+10),2);
                StiffBefore.PZ=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PZ(:,step2-10:step2-1),2);
                StiffAfter.PZ=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PZ(:,step2+1:step2+10),2);
                %left
                StiffBefore.LHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHF(:,step2-10:step2-1),2);
                StiffAfter.LHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHF(:,step2+1:step2+10),2);
                StiffBefore.LHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHA(:,step2-10:step2-1),2);
                StiffAfter.LHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHA(:,step2+1:step2+10),2);
                StiffBefore.LKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LKF(:,step2-10:step2-1),2);
                StiffAfter.LKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LKF(:,step2+1:step2+10),2);
                %right
                StiffBefore.RHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHF(:,step2-10:step2-1),2);
                StiffAfter.RHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHF(:,step2+1:step2+10),2);
                StiffBefore.RHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHA(:,step2-10:step2-1),2);
                StiffAfter.RHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHA(:,step2+1:step2+10),2);
                StiffBefore.RKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RKF(:,step2-10:step2-1),2);
                StiffAfter.RKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RKF(:,step2+1:step2+10),2);
                
                %Joint speed
                %pelvis
                SpeedBefore.PX=mean(SignalsdataSteps.data.JointVelocity_PX(:,step2-10:step2-1),2);
                SpeedAfter.PX=mean(SignalsdataSteps.data.JointVelocity_PX(:,step2+1:step2+10),2);
                SpeedBefore.PZ=mean(SignalsdataSteps.data.JointVelocity_PZ(:,step2-10:step2-1),2);
                SpeedAfter.PZ=mean(SignalsdataSteps.data.JointVelocity_PZ(:,step2+1:step2+10),2);
                %left
                SpeedBefore.LHF=mean(SignalsdataSteps.data.JointVelocity_LHF(:,step2-10:step2-1),2);
                SpeedAfter.LHF=mean(SignalsdataSteps.data.JointVelocity_LHF(:,step2+1:step2+10),2);
                SpeedBefore.LHA=mean(SignalsdataSteps.data.JointVelocity_LHA(:,step2-10:step2-1),2);
                SpeedAfter.LHA=mean(SignalsdataSteps.data.JointVelocity_LHA(:,step2+1:step2+10),2);
                SpeedBefore.LKF=mean(SignalsdataSteps.data.JointVelocity_LKF(:,step2-10:step2-1),2);
                SpeedAfter.LKF=mean(SignalsdataSteps.data.JointVelocity_LKF(:,step2+1:step2+10),2);
                %right
                SpeedBefore.RHF=mean(SignalsdataSteps.data.JointVelocity_RHF(:,step2-10:step2-1),2);
                SpeedAfter.RHF=mean(SignalsdataSteps.data.JointVelocity_RHF(:,step2+1:step2+10),2);
                SpeedBefore.RHA=mean(SignalsdataSteps.data.JointVelocity_RHA(:,step2-10:step2-1),2);
                SpeedAfter.RHA=mean(SignalsdataSteps.data.JointVelocity_RHA(:,step2+1:step2+10),2);
                SpeedBefore.RKF=mean(SignalsdataSteps.data.JointVelocity_RKF(:,step2-10:step2-1),2);
                SpeedAfter.RKF=mean(SignalsdataSteps.data.JointVelocity_RKF(:,step2+1:step2+10),2);
                
                %Total power
                %pelvis
                PowerBefore.PX=mean(SignalsdataSteps.data.TotalPower_PX(:,step2-10:step2-1),2);
                PowerAfter.PX=mean(SignalsdataSteps.data.TotalPower_PX(:,step2+1:step2+10),2);
                PowerBefore.PZ=mean(SignalsdataSteps.data.TotalPower_PZ(:,step2-10:step2-1),2);
                PowerAfter.PZ=mean(SignalsdataSteps.data.TotalPower_PZ(:,step2+1:step2+10),2);
                %left
                PowerBefore.LHF=mean(SignalsdataSteps.data.TotalPower_LHF(:,step2-10:step2-1),2);
                PowerAfter.LHF=mean(SignalsdataSteps.data.TotalPower_LHF(:,step2+1:step2+10),2);
                PowerBefore.LHA=mean(SignalsdataSteps.data.TotalPower_LHA(:,step2-10:step2-1),2);
                PowerAfter.LHA=mean(SignalsdataSteps.data.TotalPower_LHA(:,step2+1:step2+10),2);
                PowerBefore.LKF=mean(SignalsdataSteps.data.TotalPower_LKF(:,step2-10:step2-1),2);
                PowerAfter.LKF=mean(SignalsdataSteps.data.TotalPower_LKF(:,step2+1:step2+10),2);
                %right
                PowerBefore.RHF=mean(SignalsdataSteps.data.TotalPower_RHF(:,step2-10:step2-1),2);
                PowerAfter.RHF=mean(SignalsdataSteps.data.TotalPower_RHF(:,step2+1:step2+10),2);
                PowerBefore.RHA=mean(SignalsdataSteps.data.TotalPower_RHA(:,step2-10:step2-1),2);
                PowerAfter.RHA=mean(SignalsdataSteps.data.TotalPower_RHA(:,step2+1:step2+10),2);
                PowerBefore.RKF=mean(SignalsdataSteps.data.TotalPower_RKF(:,step2-10:step2-1),2);
                PowerAfter.RKF=mean(SignalsdataSteps.data.TotalPower_RKF(:,step2+1:step2+10),2);
                
                %Work
                %pelvis
                WorkBefore.PX=mean(DataSet(trial).Results.WorkBySteps_PX(step2-10:step2-1,1));
                WorkAfter.PX=mean(DataSet(trial).Results.WorkBySteps_PX(step2+1:step2+10,1));
                WorkBefore.PZ=mean(DataSet(trial).Results.WorkBySteps_PZ(step2-10:step2-1,1));
                WorkAfter.PZ=mean(DataSet(trial).Results.WorkBySteps_PZ(step2+1:step2+10,1));
                %left
                WorkBefore.LHF=mean(DataSet(trial).Results.WorkBySteps_LHF(step2-10:step2-1,1));
                WorkAfter.LHF=mean(DataSet(trial).Results.WorkBySteps_LHF(step2+1:step2+10,1));
                WorkBefore.LHA=mean(DataSet(trial).Results.WorkBySteps_LHA(step2-10:step2-1,1));
                WorkAfter.LHA=mean(DataSet(trial).Results.WorkBySteps_LHA(step2+1:step2+10,1));
                WorkBefore.LKF=mean(DataSet(trial).Results.WorkBySteps_LKF(step2-10:step2-1,1));
                WorkAfter.LKF=mean(DataSet(trial).Results.WorkBySteps_LKF(step2+1:step2+10,1));
                %right
                WorkBefore.RHF=mean(DataSet(trial).Results.WorkBySteps_RHF(step2-10:step2-1,1));
                WorkAfter.RHF=mean(DataSet(trial).Results.WorkBySteps_RHF(step2+1:step2+10,1));
                WorkBefore.RHA=mean(DataSet(trial).Results.WorkBySteps_RHA(step2-10:step2-1,1));
                WorkAfter.RHA=mean(DataSet(trial).Results.WorkBySteps_RHA(step2+1:step2+10,1));
                WorkBefore.RKF=mean(DataSet(trial).Results.WorkBySteps_RKF(step2-10:step2-1,1));
                WorkAfter.RKF=mean(DataSet(trial).Results.WorkBySteps_RKF(step2+1:step2+10,1));
                
                
                %%%
                joints=fieldnames(VarBefore);
                xlabels={'PX';'PZ';'LHF';'LHA';'LKF';'RHF';'RHA';'RKF'};
                ylabels={'Joint','Angle','(rad)';'Joint','Stiffness','(%)';'Interaction','Forces','(N)'; 'Joint','Speed','(rad/s)';'Joint','Power','(W)'};
                %ylabelpositions=[-56.85579252017587,-0.068654704243321,-1;-57.21040216179891,15.761435624911721,-0.999999999999986;-54.01891301991525,-39.87937279159091,-1];
                p=length(xlabels);
                v=length(ylabels);
                %plot-5 rows (angle,asistance level, valocity, interaction forces, power)
                %and 8 rows (corresponding to every joint)
                figure
                [ha,pos]=tight_subplot(v,p,[.005 .005],[0.1 .01],[.01 .01]);
                for i=1:8 %for every column
                    if i==1 %add labels
                        for j=1:5 %for every row
                            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                                title({xlabels{i,1}})
                                set(gca,'XTick',[]);
                            elseif j==2 % stiffness profiles
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                set(gca,'YTick',[0, 20, 40, 60, 80,100]);
                                xlim([0 100])
                                ylim([0 100])
                                set(gca,'XTick',[]);
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                %set(mylabel,'Position',ylabelpositions(j,:));
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                %set(mylabel,'Position',ylabelpositions(j,:));
                                set(gca,'XTick',[]);
                            elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                set(gca,'XTick',[]);
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                text1=vect2str(WorkBefore.(joints{i,1}));
                                text2=vect2str(WorkAfter.(joints{i,1}));
                                axlim = get(gca, 'XLim');
                                aylim = get(gca, 'YLim');
                                text(40, (min(aylim)-5), text1, 'FontSize', 10)
                                
                            end
                        end
                    elseif i==8 && ismember(i,indexes)==1%add legends/colorbar
                        for j=1:5 %for every row
                           subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                legend('Before change','After change','Reference Step')
                                set(legend,'Position',[0.910996115017397 0.841869473090363 0.0795572927718363 0.057959402932061]);
%                                 set(gca,'YTick',[])
                                set(gca,'XTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                                %grey doted line for reference
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'YTick',[])
                                set(gca,'XTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
%                                 set(gca,'YTick',[])
                            elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
                            end
                        end
                    elseif ismember(i,indexes)==1
                        for j=1:5 %for every row
                            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                %grey doter line for reference
                                set(gca,'XTick',[]);
%                                 set(gca,'YTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'XTick',[])
                                set(gca,'YTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
%                                 set(gca,'YTick',[])
                                set(gca,'XTick',[]);
                            elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
                            end
                        end
                    elseif i==8 && ismember(i,indexes)==0
                        for j=1:5 %for every row
                           subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                legend('Before change','After change','Reference Step')
                                set(legend,'Position',[0.910996115017397 0.841869473090363 0.0795572927718363 0.057959402932061]);
                                set(gca,'XTick',[]);
%                                 set(gca,'YTick',[])
                                %grey doted line for reference
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'YTick',[])
                                set(gca,'XTick',[])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
%                                 set(gca,'YTick',[])
                             elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                            end
                        end
                    else
                        for j=1:5 %for every row
                            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                %grey doter line for reference
                                set(gca,'XTick',[]);
%                                 set(gca,'YTick',[])
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'XTick',[])
                                set(gca,'YTick',[])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
%                                 set(gca,'YTick',[])
                             elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                            end
                        end
                    end
                end

                sgtitle(t) %title over all the sublots
                [ax1,h1]=suplabel('% Gait Cycle','x'); %legend over all subplots x axis
                %%%
                clear SignalsdataSteps step2 t jointsRef VarBefore VarAfter ForcesBefore ForcesAfter StiffBefore StiffAfter

                % saving the figures into the folder with the desired name
                %saveas(h,sprintf('FIG%d.png',k)); % will create FIG1, FIG2,...
                 F=gcf;
                maxfig(F,1)
                filename=sprintf('S%dT%dGGF%dIN%dFI%d.fig',subject,trial,GGF,initalParam,finalParam);
                fullFileName=strcat(fpath,filename);
                savefig(fullFileName);
                close all
        end
    end
end

clear parameters
%% Lparetic

parameters=fieldnames(LpareticVAR);
for k=1:length(parameters)
    if strcmp(parameters{k,1},'WeightShift_GuidanceForce_pct')==1 || strcmp(parameters{k,1},'LeftStepHeight_GuidanceForce_pct')==1 || strcmp(parameters{k,1},'LeftStabilityStance_GuidanceForce_pct')==1 || strcmp(parameters{k,1},'LeftStepLength_GuidanceForce_pct')==1 ...
            || strcmp(parameters{k,1},'LeftPrepositioning_GuidanceForce_pct')==1
                %1st check that the matrix contains the values of the patients from the specific center first
        if strcmp(center,'SMK')==1
            LpareticVAR.(parameters{k,1})=sortrows(LpareticVAR.(parameters{k,1}),11);
        elseif strcmp(center,'RRD')==1
            %change order of the matrix for RRD patients (so it runs through the loop)
            LpareticVAR.(parameters{k,1})=sortrows(LpareticVAR.(parameters{k,1}),-11);
        end

        %check the trial number and select the adequate dataset to slipt into steps
        for m=1:size(LpareticVAR.(parameters{k,1}),1)
            if strcmp(center,'SMK')==1
                if LpareticVAR.(parameters{k,1})(m,11)==1
                    subject=1;
                     DataSet=DS02;
                    SignalsdataSteps=splitIntoSteps(DS02(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==2
                    subject=2;
                     DataSet=DS03;
                    SignalsdataSteps=splitIntoSteps(DS03(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==3
                    subject=3;
                     DataSet=DS04;
                    SignalsdataSteps=splitIntoSteps(DS04(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==4
                    subject=4;
                     DataSet=DS05;
                    SignalsdataSteps=splitIntoSteps(DS05(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==5
                    subject=5;
                     DataSet=DS06;
                    SignalsdataSteps=splitIntoSteps(DS06(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==6
                    subject=7;
                     DataSet=DS07;
                    SignalsdataSteps=splitIntoSteps(DS07(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==7
                    subject=7;
                     DataSet=DS08;
                    SignalsdataSteps=splitIntoSteps(DS08(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                else
                    break
                end
            elseif strcmp(center,'RRD')==1 %check the RRD patients datasets
                 if LpareticVAR.(parameters{k,1})(m,11)==8
                    subject=8;
                    DataSet=D4;
                    SignalsdataSteps=splitIntoSteps(D4(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==9
                    subject=9;
                    DataSet=D10;
                    SignalsdataSteps=splitIntoSteps(D10(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==10
                    subject=10;
                    DataSet=D22;
                    SignalsdataSteps=splitIntoSteps(D22(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==11
                    subject=11;
                    DataSet=D30;
                    SignalsdataSteps=splitIntoSteps(D30(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==12
                    subject=12;
                    DataSet=D105;
                    SignalsdataSteps=splitIntoSteps(D105(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==13
                    subject=13;
                    DataSet=D108;
                    SignalsdataSteps=splitIntoSteps(D108(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                elseif LpareticVAR.(parameters{k,1})(m,11)==14
                    subject=14;
                    DataSet=D110;
                    SignalsdataSteps=splitIntoSteps(D110(LpareticVAR.(parameters{k,1})(m,13)),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);
                 else
                    break
                end
            end
            
            step2=LpareticVAR.(parameters{k,1})(m,14); %select step form the loop
            %change the title depending on the inputs
            trial=LpareticVAR.(parameters{k,1})(m,13);
            GGF=floor(LpareticVAR.(parameters{k,1})(m,12));
            initalParam=LpareticVAR.(parameters{k,1})(m,1);
            finalParam=LpareticVAR.(parameters{k,1})(m,2);
                
            if strcmp(parameters{k,1},'WeightShift_GuidanceForce_pct')==1 
                parameter='Weight Shift GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\WeightShift\';
                indexes=[2 4 7];
            elseif strcmp(parameters{k,1},'LeftStepHeight_GuidanceForce_pct')==1
                parameter='Left Step Height GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\LeftStepHeight\';
                indexes=5;
            elseif strcmp(parameters{k,1},'LeftStabilityStance_GuidanceForce_pct')==1
                parameter='Left Stability GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\LeftStability\';
                indexes=5;
            elseif strcmp(parameters{k,1},'LeftStepLength_GuidanceForce_pct')==1
                parameter='Left Step Length GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\LeftStepLength\';
                indexes=3;
            elseif strcmp(parameters{k,1},'LeftPrepositioning_GuidanceForce_pct')==1
                parameter='Left Prepositioning GF';
                t=sprintf('Subject %d Trial %d %s',subject,trial,parameter);
                fpath='C:\Users\celia\Desktop\Work\Plots\GaitCyclePlots\LeftPrepositioning\';
                indexes=[3 5];
            end
               
                %select the reference step
                jointsRef.PX=mean(SignalsdataSteps.data.r_cont_jointAngles_PX(:,step2-10:step2+10),2);
                jointsRef.PZ=mean(SignalsdataSteps.data.r_cont_jointAngles_PZ(:,step2-10:step2+10),2);
                jointsRef.LHF=mean(SignalsdataSteps.data.r_cont_jointAngles_LHF(:,step2-10:step2+10),2);
                jointsRef.LHA=mean(SignalsdataSteps.data.r_cont_jointAngles_LHA(:,step2-10:step2+10),2);
                jointsRef.LKF=mean(SignalsdataSteps.data.r_cont_jointAngles_LKF(:,step2-10:step2+10),2);
                jointsRef.RHF=mean(SignalsdataSteps.data.r_cont_jointAngles_RHF(:,step2-10:step2+10),2);
                jointsRef.RHA=mean(SignalsdataSteps.data.r_cont_jointAngles_RHA(:,step2-10:step2+10),2);
                jointsRef.RKF=mean(SignalsdataSteps.data.r_cont_jointAngles_RKF(:,step2-10:step2+10),2);

                %calculate joint ANGLES before and after for every joint
                %pelvis
                VarBefore.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2-10:step2-1),2);
                VarAfter.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2+1:step2+10),2);
                VarBefore.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2-10:step2-1),2);
                VarAfter.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2+1:step2+10),2);
                %left
                VarBefore.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2-10:step2-1),2);
                VarAfter.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2+1:step2+10),2);
                VarBefore.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2-10:step2-1),2);
                VarAfter.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2+1:step2+10),2);
                VarBefore.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2-10:step2-1),2);
                VarAfter.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2+1:step2+10),2);
                %Right
                VarBefore.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2-10:step2-1),2);
                VarAfter.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2+1:step2+10),2);
                VarBefore.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2-10:step2-1),2);
                VarAfter.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2+1:step2+10),2);
                VarBefore.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2-10:step2-1),2);
                VarAfter.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2+1:step2+10),2);

                %interaction FORCES before and after
                %pelvis
                ForcesBefore.PX=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PX(:,step2-10:step2-1),2);
                ForcesAfter.PX=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PX(:,step2+1:step2+10),2);
                ForcesBefore.PZ=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PZ(:,step2-10:step2-1),2);
                ForcesAfter.PZ=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PZ(:,step2+1:step2+10),2);
                %left
                ForcesBefore.LHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHF(:,step2-10:step2-1),2);
                ForcesAfter.LHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHF(:,step2+1:step2+10),2);
                ForcesBefore.LHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHA(:,step2-10:step2-1),2);
                ForcesAfter.LHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHA(:,step2+1:step2+10),2);
                ForcesBefore.LKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LKF(:,step2-10:step2-1),2);
                ForcesAfter.LKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LKF(:,step2+1:step2+10),2);
                %Right
                ForcesBefore.RHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHF(:,step2-10:step2-1),2);
                ForcesAfter.RHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHF(:,step2+1:step2+10),2);
                ForcesBefore.RHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHA(:,step2-10:step2-1),2);
                ForcesAfter.RHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHA(:,step2+1:step2+10),2);
                ForcesBefore.RKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RKF(:,step2-10:step2-1),2);
                ForcesAfter.RKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RKF(:,step2+1:step2+10),2);

                %Joint Stiffness Before and After
                %pelvis
                StiffBefore.PX=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PX(:,step2-10:step2-1),2);
                StiffAfter.PX=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PX(:,step2+1:step2+10),2);
                StiffBefore.PZ=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PZ(:,step2-10:step2-1),2);
                StiffAfter.PZ=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PZ(:,step2+1:step2+10),2);
                %left
                StiffBefore.LHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHF(:,step2-10:step2-1),2);
                StiffAfter.LHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHF(:,step2+1:step2+10),2);
                StiffBefore.LHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHA(:,step2-10:step2-1),2);
                StiffAfter.LHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHA(:,step2+1:step2+10),2);
                StiffBefore.LKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LKF(:,step2-10:step2-1),2);
                StiffAfter.LKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LKF(:,step2+1:step2+10),2);
                %right
                StiffBefore.RHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHF(:,step2-10:step2-1),2);
                StiffAfter.RHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHF(:,step2+1:step2+10),2);
                StiffBefore.RHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHA(:,step2-10:step2-1),2);
                StiffAfter.RHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHA(:,step2+1:step2+10),2);
                StiffBefore.RKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RKF(:,step2-10:step2-1),2);
                StiffAfter.RKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RKF(:,step2+1:step2+10),2);
                
                 %Joint speed
                %pelvis
                SpeedBefore.PX=mean(SignalsdataSteps.data.JointVelocity_PX(:,step2-10:step2-1),2);
                SpeedAfter.PX=mean(SignalsdataSteps.data.JointVelocity_PX(:,step2+1:step2+10),2);
                SpeedBefore.PZ=mean(SignalsdataSteps.data.JointVelocity_PZ(:,step2-10:step2-1),2);
                SpeedAfter.PZ=mean(SignalsdataSteps.data.JointVelocity_PZ(:,step2+1:step2+10),2);
                %left
                SpeedBefore.LHF=mean(SignalsdataSteps.data.JointVelocity_LHF(:,step2-10:step2-1),2);
                SpeedAfter.LHF=mean(SignalsdataSteps.data.JointVelocity_LHF(:,step2+1:step2+10),2);
                SpeedBefore.LHA=mean(SignalsdataSteps.data.JointVelocity_LHA(:,step2-10:step2-1),2);
                SpeedAfter.LHA=mean(SignalsdataSteps.data.JointVelocity_LHA(:,step2+1:step2+10),2);
                SpeedBefore.LKF=mean(SignalsdataSteps.data.JointVelocity_LKF(:,step2-10:step2-1),2);
                SpeedAfter.LKF=mean(SignalsdataSteps.data.JointVelocity_LKF(:,step2+1:step2+10),2);
                %right
                SpeedBefore.RHF=mean(SignalsdataSteps.data.JointVelocity_RHF(:,step2-10:step2-1),2);
                SpeedAfter.RHF=mean(SignalsdataSteps.data.JointVelocity_RHF(:,step2+1:step2+10),2);
                SpeedBefore.RHA=mean(SignalsdataSteps.data.JointVelocity_RHA(:,step2-10:step2-1),2);
                SpeedAfter.RHA=mean(SignalsdataSteps.data.JointVelocity_RHA(:,step2+1:step2+10),2);
                SpeedBefore.RKF=mean(SignalsdataSteps.data.JointVelocity_RKF(:,step2-10:step2-1),2);
                SpeedAfter.RKF=mean(SignalsdataSteps.data.JointVelocity_RKF(:,step2+1:step2+10),2);
                
                %Total power
                %pelvis
                PowerBefore.PX=mean(SignalsdataSteps.data.TotalPower_PX(:,step2-10:step2-1),2);
                PowerAfter.PX=mean(SignalsdataSteps.data.TotalPower_PX(:,step2+1:step2+10),2);
                PowerBefore.PZ=mean(SignalsdataSteps.data.TotalPower_PZ(:,step2-10:step2-1),2);
                PowerAfter.PZ=mean(SignalsdataSteps.data.TotalPower_PZ(:,step2+1:step2+10),2);
                %left
                PowerBefore.LHF=mean(SignalsdataSteps.data.TotalPower_LHF(:,step2-10:step2-1),2);
                PowerAfter.LHF=mean(SignalsdataSteps.data.TotalPower_LHF(:,step2+1:step2+10),2);
                PowerBefore.LHA=mean(SignalsdataSteps.data.TotalPower_LHA(:,step2-10:step2-1),2);
                PowerAfter.LHA=mean(SignalsdataSteps.data.TotalPower_LHA(:,step2+1:step2+10),2);
                PowerBefore.LKF=mean(SignalsdataSteps.data.TotalPower_LKF(:,step2-10:step2-1),2);
                PowerAfter.LKF=mean(SignalsdataSteps.data.TotalPower_LKF(:,step2+1:step2+10),2);
                %right
                PowerBefore.RHF=mean(SignalsdataSteps.data.TotalPower_RHF(:,step2-10:step2-1),2);
                PowerAfter.RHF=mean(SignalsdataSteps.data.TotalPower_RHF(:,step2+1:step2+10),2);
                PowerBefore.RHA=mean(SignalsdataSteps.data.TotalPower_RHA(:,step2-10:step2-1),2);
                PowerAfter.RHA=mean(SignalsdataSteps.data.TotalPower_RHA(:,step2+1:step2+10),2);
                PowerBefore.RKF=mean(SignalsdataSteps.data.TotalPower_RKF(:,step2-10:step2-1),2);
                PowerAfter.RKF=mean(SignalsdataSteps.data.TotalPower_RKF(:,step2+1:step2+10),2);
                
                 %Work
                %pelvis
                WorkBefore.PX=mean(DataSet(trial).Results.WorkBySteps_PX(step2-10:step2-1,1));
                WorkAfter.PX=mean(DataSet(trial).Results.WorkBySteps_PX(step2+1:step2+10,1));
                WorkBefore.PZ=mean(DataSet(trial).Results.WorkBySteps_PZ(step2-10:step2-1,1));
                WorkAfter.PZ=mean(DataSet(trial).Results.WorkBySteps_PZ(step2+1:step2+10,1));
                %left
                WorkBefore.LHF=mean(DataSet(trial).Results.WorkBySteps_LHF(step2-10:step2-1,1));
                WorkAfter.LHF=mean(DataSet(trial).Results.WorkBySteps_LHF(step2+1:step2+10,1));
                WorkBefore.LHA=mean(DataSet(trial).Results.WorkBySteps_LHA(step2-10:step2-1,1));
                WorkAfter.LHA=mean(DataSet(trial).Results.WorkBySteps_LHA(step2+1:step2+10,1));
                WorkBefore.LKF=mean(DataSet(trial).Results.WorkBySteps_LKF(step2-10:step2-1,1));
                WorkAfter.LKF=mean(DataSet(trial).Results.WorkBySteps_LKF(step2+1:step2+10,1));
                %right
                WorkBefore.RHF=mean(DataSet(trial).Results.WorkBySteps_RHF(step2-10:step2-1,1));
                WorkAfter.RHF=mean(DataSet(trial).Results.WorkBySteps_RHF(step2+1:step2+10,1));
                WorkBefore.RHA=mean(DataSet(trial).Results.WorkBySteps_RHA(step2-10:step2-1,1));
                WorkAfter.RHA=mean(DataSet(trial).Results.WorkBySteps_RHA(step2+1:step2+10,1));
                WorkBefore.RKF=mean(DataSet(trial).Results.WorkBySteps_RKF(step2-10:step2-1,1));
                WorkAfter.RKF=mean(DataSet(trial).Results.WorkBySteps_RKF(step2+1:step2+10,1));
                
                %%%
                 joints=fieldnames(VarBefore);
                xlabels={'PX';'PZ';'LHF';'LHA';'LKF';'RHF';'RHA';'RKF'};
                ylabels={'Joint','Angle','(rad)';'Joint','Stiffness','(%)';'Interaction','Forces','(N)'; 'Joint','Speed','(rad/s)';'Joint','Power','(W)'};
                %ylabelpositions=[-56.85579252017587,-0.068654704243321,-1;-57.21040216179891,15.761435624911721,-0.999999999999986;-54.01891301991525,-39.87937279159091,-1];
                p=length(xlabels);
                v=length(ylabels);
                %plot-5 rows (angle,asistance level, valocity, interaction forces, power)
                %and 8 rows (corresponding to every joint)
                figure
                [ha,pos]=tight_subplot(v,p,[.005 .005],[0.1 .01],[.01 .01]);
                for i=1:8 %for every column
                    if i==1 %add labels
                        for j=1:5 %for every row
                            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                                title({xlabels{i,1}})
                                set(gca,'XTick',[]);
                            elseif j==2 % stiffness profiles
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                set(gca,'YTick',[0, 20, 40, 60, 80,100]);
                                xlim([0 100])
                                ylim([0 100])
                                set(gca,'XTick',[]);
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                %set(mylabel,'Position',ylabelpositions(j,:));
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                %set(mylabel,'Position',ylabelpositions(j,:));
                                set(gca,'XTick',[]);
                            elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                                set(gca,'XTick',[]);
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                            end
                        end
                    elseif i==8 && ismember(i,indexes)==1%add legends/colorbar
                        for j=1:5 %for every row
                           subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                legend('Before change','After change','Reference Step')
                                set(legend,'Position',[0.910996115017397 0.841869473090363 0.0795572927718363 0.057959402932061]);
%                                 set(gca,'YTick',[])
                                set(gca,'XTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                                %grey doted line for reference
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'YTick',[])
                                set(gca,'XTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
%                                 set(gca,'YTick',[])
                            elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
                            end
                        end
                    elseif ismember(i,indexes)==1
                        for j=1:5 %for every row
                            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                %grey doter line for reference
                                set(gca,'XTick',[]);
%                                 set(gca,'YTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'XTick',[])
                                set(gca,'YTick',[])
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
%                                 set(gca,'YTick',[])
                                set(gca,'XTick',[]);
                            elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                                set(gca,'Color',[0.98,0.93,0.86])
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'Color',[0.98,0.93,0.86])
                            end
                        end
                    elseif i==8 && ismember(i,indexes)==0
                        for j=1:5 %for every row
                           subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                legend('Before change','After change','Reference Step')
                                set(legend,'Position',[0.910996115017397 0.841869473090363 0.0795572927718363 0.057959402932061]);
                                set(gca,'XTick',[]);
%                                 set(gca,'YTick',[])
                                %grey doted line for reference
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'YTick',[])
                                set(gca,'XTick',[])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
%                                 set(gca,'YTick',[])
                             elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                            end
                        end
                    else
                        for j=1:5 %for every row
                            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
                            if j==1 %joint angle
                                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                                hold off
                                title({xlabels{i,1}})
                                %grey doter line for reference
                                set(gca,'XTick',[]);
%                                 set(gca,'YTick',[])
                            elseif j==2 %assistance
                                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,StiffAfter.(joints{i,1}),'LineStyle','--','Color','r') %after change
                                hold off
                                xlim([0 100])
                                ylim([0 100])
%                                 ylim([0 30])
                                set(gca,'XTick',[])
                                set(gca,'YTick',[])
                            elseif j==3 %interaction forces
                                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                                hold off
%                                 set(gca,'YTick',[])
                             elseif j==4 % joint speeds
                                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                                hold off
                                set(gca,'XTick',[]);
                            elseif j==5 %power
                                plot(0:99,PowerBefore.(joints{i,1}),'b') %before change 
                                hold on
                                plot(0:99,PowerAfter.(joints{i,1}),'r') %after change
                                hold off
                            end
                        end
                    end
                end
                
                sgtitle(t) %title over all the sublots
                [ax1,h1]=suplabel('% Gait Cycle','x'); %legend over all subplots x axis
                %%%
                clear SignalsdataSteps step2 t jointsRef VarBefore VarAfter ForcesBefore ForcesAfter StiffBefore StiffAfter

                % saving the figures into the folder with the desired name
                F=gcf;
                maxfig(F,1)
                filename=sprintf('S%dT%dGGF%dIN%dFI%d.fig',subject,trial,GGF,initalParam,finalParam);
                fullFileName=strcat(fpath,filename);
                savefig(fullFileName);
                %baseFileName = sprintf('figure_%d.fig',k);
                % Specify some particular, specific folder:
%                 imwrite(F.cdata, fullFileName, 'fig')
                %saveas(hOutFigure,fullFileName);
                close all
        end
    end
end
