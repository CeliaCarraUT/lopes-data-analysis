function[Y]=checkChangeInstancesPowerRRD(plottingmatrix1,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110)%ParamChangeInstancesD112,ParamChangeInstancesD114
%This function checks the first plotting matrix of RRD patients to
%determine which parameters are changed independently from the General
%Guidance force and stores them in a new matrix Y
%Inputs:
%plottingmatrix-matrix containing increments in parameter and variables
%ParamChangeInstances-all the different structs containing the change
%instances for all parameters and trials (one for every patient)
%Outputs:
%Y-new matrix containing only the increments for intances where the
%paramters are changed independently of the GGF.

%first check column 14(patient) then 13(trial) then 4(changeinstance)

fieldsRRD=fieldnames(plottingmatrix1);
for i=1:length(fieldsRRD)
    counter=0;
    for j=1:size((plottingmatrix1.(fieldsRRD{i,1})),1)
        if plottingmatrix1.(fieldsRRD{i,1})(j,14)==1
            n=1;
        elseif plottingmatrix1.(fieldsRRD{i,1})(j,14)==2
            n=2;
        elseif plottingmatrix1.(fieldsRRD{i,1})(j,14)==3
            n=3;
        elseif plottingmatrix1.(fieldsRRD{i,1})(j,14)==4
            n=4;
        elseif plottingmatrix1.(fieldsRRD{i,1})(j,14)==5
            n=5;
        elseif plottingmatrix1.(fieldsRRD{i,1})(j,14)==6
            n=6;
        elseif plottingmatrix1.(fieldsRRD{i,1})(j,14)==7
            n=7;
%         elseif plottingmatrix1.(fieldsRRD{i,1})(j,17)==8
%             n=8;
%         elseif plottingmatrix1.(fieldsRRD{i,1})(j,17)==9
%             n=9;
        end
        
        switch n %every 'option' of the switch loop corresponds to a different patient
            case 1
                %check trialnumb
                trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesD4.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                    Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    poscount.(fieldsRRD{i,1})=counter;
                elseif isfield(ParamChangeInstancesD4.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesD4.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                        Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                        poscount.(fieldsRRD{i,1})=counter;
                    end
                end
            case 2
                %check trialnumb
                trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesD10.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                    Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    poscount.(fieldsRRD{i,1})=counter;
                elseif isfield(ParamChangeInstancesD10.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesD10.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                        Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                        poscount.(fieldsRRD{i,1})=counter;
                    end
                end
            case 3 
                %check trialnumb
                trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesD22.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                    Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    poscount.(fieldsRRD{i,1})=counter;
                elseif isfield(ParamChangeInstancesD22.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesD22.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                        Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    end
                end
            case 4
                trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesD30.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                    Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    poscount.(fieldsRRD{i,1})=counter;
                elseif isfield(ParamChangeInstancesD30.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesD30.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                        Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                        poscount.(fieldsRRD{i,1})=counter;
                    end
                end
                case 5
                %check trialnumb
                trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesD105.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                    Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    poscount.(fieldsRRD{i,1})=counter;
                elseif isfield(ParamChangeInstancesD105.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesD105.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                        Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                        poscount.(fieldsRRD{i,1})=counter;
                    end
                end
            case 6
                %check trialnumb
                trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesD108.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                    Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    poscount.(fieldsRRD{i,1})=counter;
                elseif isfield(ParamChangeInstancesD108.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesD108.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                        Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                        poscount.(fieldsRRD{i,1})=counter;
                    end
                end
            case 7 
                %check trialnumb
                trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesD110.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                    Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                    poscount.(fieldsRRD{i,1})=counter;
                elseif isfield(ParamChangeInstancesD110.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesD110.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
                        Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+5;
                        poscount.(fieldsRRD{i,1})=counter;
                    end
                end
%              case 8 
%                 %check trialnumb
%                 trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
%                 %compare changeinstance 
%                 trials=sprintf('trial%d',trialnumb);
%                 if isfield(ParamChangeInstancesD112.(trials),'General_GuidanceForce_pct')==0
%                     counter=counter+1;
%                     Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                     Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+4;
%                     poscount.(fieldsRRD{i,1})=counter;
%                 elseif isfield(ParamChangeInstancesD112.(trials),'General_GuidanceForce_pct')==1
%                     GGFchange=ParamChangeInstancesD112.(trials).General_GuidanceForce_pct;
%                     if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
%                         counter=counter+1;
%                         Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                         Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+4;
%                         poscount.(fieldsRRD{i,1})=counter;
%                     end
%                 end
%               case 9 
%                 %check trialnumb
%                 trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,13);
%                 %compare changeinstance 
%                 trials=sprintf('trial%d',trialnumb);
%                 if isfield(ParamChangeInstancesD114.(trials),'General_GuidanceForce_pct')==0
%                     counter=counter+1;
%                     Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                     Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+4;
%                     poscount.(fieldsRRD{i,1})=counter;
%                 elseif isfield(ParamChangeInstancesD114.(trials),'General_GuidanceForce_pct')==1
%                     GGFchange=ParamChangeInstancesD114.(trials).General_GuidanceForce_pct;
%                     if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
%                         counter=counter+1;
%                         Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                         Y.(fieldsRRD{i,1})(counter,14)=plottingmatrix1.(fieldsRRD{i,1})(j,14)+4;
%                         poscount.(fieldsRRD{i,1})=counter;
%                     end
%                 end
        end
    end
end
