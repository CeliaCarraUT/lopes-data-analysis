function [Y]=CheckStoringStructs(PI,W,F,P)
%This function checks the force/power structures creating during the within-trial
%analysis are correctly storing the data
%Inputs: 
%PI-Parameter change instances struct obtained during analysis(ParamChangeInstances)
%W-Within trial parameter values structure obtained during analysis
%F-variables/force/power increment structure obtained during analysis
%P-WithinTrialParamIncrements
%Outputs:
%Y-Input variable structure checked

fieldnam1=fieldnames(F);
trialnums=regexp(fieldnam1,'\d*','Match'); %take only the number part of the fieldnam1 trials

%check that there are the same fields in the structs
for i=1:length(fieldnam1)
    fields2P=fieldnames(P.(fieldnam1{i,1}));
    fields2F=fieldnames(F.(fieldnam1{i,1}));
    for j=1:length(fields2F)
        if ismember(fields2F{j,1},fields2P)==0
            F.(fieldnam1{i,1})= rmfield(F.(fieldnam1{i,1}),(fields2F{j,1}));
        end
    end
end


 for i=1:length(fieldnam1)
     fields2F=fieldnames(F.(fieldnam1{i,1}));
   for j=1:length(fields2F)
       l=length(P.(fieldnam1{i,1}).(fields2F{j,1})); %check how many increments there are in the parameter struct)
       fields3=fieldnames(F.(fieldnam1{i,1}).(fields2F{j,1}));
       for k=1:length(fields3)
           if length(F.(fieldnam1{i,1}).(fields2F{j,1}).(fields3{k,1}))<l
               %add NANS
               m=length(F.(fieldnam1{i,1}).(fields2F{j,1}).(fields3{k,1}));
               n=l-m;
               for t=1:length(n)
                   F.(fieldnam1{i,1}).(fields2F{j,1}).(fields3{k,1})(m+n)=NaN;
               end
           end
       end
   end
 end
 
 Y=F;