function[Y]=checkChangeInstancesRRD2(VARplottingmatrix2,StepsStructD4,StepsStructD10,StepsStructD22,StepsStructD30,StepsStructD105,StepsStructD108,StepsStructD110)%ParamChangeInstancesD112,ParamChangeInstancesD114)
%This function checks the first plotting matrix of RRD patients to
%determine which parameters are changed independently from the General
%Guidance force and stores them in a new matrix Y
%Inputs:
%plottingmatrix-matrix containing increments in parameter and variables
%ParamChangeInstances-all the different structs containing the change
%instances for all parameters and trials (one for every patient)
%Outputs:
%Y-new matrix containing only the increments for intances where the
%paramters are changed independently of the GGF.

%first check column 19(patient) then 18(trial) then 4(changeinstance)

fieldsRRD=fieldnames(VARplottingmatrix2);
for i=1:length(fieldsRRD)
    counter=0;
    for j=1:size((VARplottingmatrix2.(fieldsRRD{i,1})),1)
        if VARplottingmatrix2.(fieldsRRD{i,1})(j,19)==8
            n=1;
        elseif VARplottingmatrix2.(fieldsRRD{i,1})(j,19)==9
            n=2;
        elseif VARplottingmatrix2.(fieldsRRD{i,1})(j,19)==10
            n=3;
        elseif VARplottingmatrix2.(fieldsRRD{i,1})(j,19)==11
            n=4;
        elseif VARplottingmatrix2.(fieldsRRD{i,1})(j,19)==12
            n=5;
        elseif VARplottingmatrix2.(fieldsRRD{i,1})(j,19)==13
            n=6;
        elseif VARplottingmatrix2.(fieldsRRD{i,1})(j,19)==14
            n=7;
%         elseif plottingmatrix1.(fieldsRRD{i,1})(j,19)==15
%             n=8;
%         elseif plottingmatrix1.(fieldsRRD{i,1})(j,19)==16
%             n=9;
        end
        
        switch n %every 'option' of the switch loop corresponds to a different patient
            case 1
                %check trialnumb
                trialnumb=VARplottingmatrix2.(fieldsRRD{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructD4.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructD4.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructD4.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructD4.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix2.(fieldsRRD{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsRRD{i,1})=counter;
                            removerows.(fieldsRRD{i,1})(poscount.(fieldsRRD{i,1}))=j;
                        end
                    end
                end
            case 2
                %check trialnumb
                trialnumb=VARplottingmatrix2.(fieldsRRD{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructD10.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructD10.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructD10.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructD10.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix2.(fieldsRRD{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsRRD{i,1})=counter;
                            removerows.(fieldsRRD{i,1})(poscount.(fieldsRRD{i,1}))=j;
                        end
                    end
                end
            case 3 
                %check trialnumb
                trialnumb=VARplottingmatrix2.(fieldsRRD{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructD22.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructD22.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructD22.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructD22.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix2.(fieldsRRD{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsRRD{i,1})=counter;
                            removerows.(fieldsRRD{i,1})(poscount.(fieldsRRD{i,1}))=j;
                        end
                    end
                end
            case 4
                trialnumb=VARplottingmatrix2.(fieldsRRD{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructD30.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructD30.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructD30.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructD30.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix2.(fieldsRRD{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsRRD{i,1})=counter;
                            removerows.(fieldsRRD{i,1})(poscount.(fieldsRRD{i,1}))=j;
                        end
                    end
                end
                case 5
                %check trialnumb
                trialnumb=VARplottingmatrix2.(fieldsRRD{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructD105.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructD105.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructD105.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructD105.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix2.(fieldsRRD{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsRRD{i,1})=counter;
                            removerows.(fieldsRRD{i,1})(poscount.(fieldsRRD{i,1}))=j;
                        end
                    end
                end
            case 6
                %check trialnumb
                trialnumb=VARplottingmatrix2.(fieldsRRD{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructD108.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructD108.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructD108.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructD108.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix2.(fieldsRRD{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsRRD{i,1})=counter;
                            removerows.(fieldsRRD{i,1})(poscount.(fieldsRRD{i,1}))=j;
                        end
                    end
                end
            case 7 
                %check trialnumb
                trialnumb=VARplottingmatrix2.(fieldsRRD{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructD110.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructD110.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructD110.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructD110.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix2.(fieldsRRD{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsRRD{i,1})=counter;
                            removerows.(fieldsRRD{i,1})(poscount.(fieldsRRD{i,1}))=j;
                        end
                    end
                end
%               case 8 
%                 %check trialnumb
%                 trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,18);
%                 %compare changeinstance 
%                 trials=sprintf('trial%d',trialnumb);
%                 if isfield(ParamChangeInstancesD112.(trials),'General_GuidanceForce_pct')==0
%                     counter=counter+1;
%                     Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                     Y.(fieldsRRD{i,1})(counter,19)=plottingmatrix1.(fieldsRRD{i,1})(j,19)+5;
%                     poscount.(fieldsRRD{i,1})=counter;
%                 elseif isfield(ParamChangeInstancesD112.(trials),'General_GuidanceForce_pct')==1
%                     GGFchange=ParamChangeInstancesD112.(trials).General_GuidanceForce_pct;
%                     if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
%                         counter=counter+1;
%                         Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                         Y.(fieldsRRD{i,1})(counter,19)=plottingmatrix1.(fieldsRRD{i,1})(j,19)+5;
%                         poscount.(fieldsRRD{i,1})=counter;
%                     end
%                 end
%               case 9 
%                 %check trialnumb
%                 trialnumb=plottingmatrix1.(fieldsRRD{i,1})(j,18);
%                 %compare changeinstance 
%                 trials=sprintf('trial%d',trialnumb);
%                 if isfield(ParamChangeInstancesD114.(trials),'General_GuidanceForce_pct')==0
%                     counter=counter+1;
%                     Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                     Y.(fieldsRRD{i,1})(counter,19)=plottingmatrix1.(fieldsRRD{i,1})(j,19)+5;
%                     poscount.(fieldsRRD{i,1})=counter;
%                 elseif isfield(ParamChangeInstancesD114.(trials),'General_GuidanceForce_pct')==1
%                     GGFchange=ParamChangeInstancesD114.(trials).General_GuidanceForce_pct;
%                     if any(GGFchange==plottingmatrix1.(fieldsRRD{i,1})(j,4))==0
%                         counter=counter+1;
%                         Y.(fieldsRRD{i,1})(counter,:)=plottingmatrix1.(fieldsRRD{i,1})(j,:);
%                         Y.(fieldsRRD{i,1})(counter,19)=plottingmatrix1.(fieldsRRD{i,1})(j,19)+5;
%                         poscount.(fieldsRRD{i,1})=counter;
%                     end
%                 end
        end
        clear Intervals
    end
end

if exist('removerows','var') == 1
    fieldsr=fieldnames(removerows);
    for i=1:length(fieldsr)
        VARplottingmatrix2.(fieldsr{i,1})(removerows.(fieldsr{i,1}),:)=[];
    end
end

Y=VARplottingmatrix2;

