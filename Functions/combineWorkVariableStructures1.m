function [Y]=combineWorkVariableStructures1(VarStruct,WorkStruct)
%This function combines the FINAL outputs of both the work and variables without taking into 
%account the parameter changed (all the increments together)
%Inputs:
%VarStruct-Variable structure containing fields corresponding to the
%changed parameters, and inside the variable increments for those changes
%in parameters
%WorkStruct-Work structure containing fields corresponding to the
%changed parameters, and inside the work increments for every joint for those changes
%in parameters
%Outputs:
%Y-Matrix containing the 'combined' increment information of the work per
%joint and variables. Each column corresponds to the increments in a joint
%or a variable:
%col1:WorkKF, col2:WorkHF, col3:WorkHA, col4:WorkPX, col5:WorkPZ,
%col6:stepheight col7:initial step length col8:terminal s.l. col9:lateral
%foot pos col10:prepositioning col11:weight shift

fields=fieldnames(VarStruct); %same for both var and work
for i=1:length(fields)
    tCombinedStruct=[];
    if i==1
        n=size(VarStruct.(fields{i,1}),1);
        %first add increments in work (first column)
        CombinedStruct(1:n,1)=WorkStruct.(fields{i,1})(1:n,8); %knee flexion
        CombinedStruct(1:n,2)=WorkStruct.(fields{i,1})(1:n,7); %hip flexion
        CombinedStruct(1:n,3)=WorkStruct.(fields{i,1})(1:n,6);%hip abduction
        CombinedStruct(1:n,4)=WorkStruct.(fields{i,1})(1:n,4); %px
        CombinedStruct(1:n,5)=WorkStruct.(fields{i,1})(1:n,5); %pz
        %then add increments in variables (columns 2 to 7)
        CombinedStruct=horzcat(CombinedStruct,VarStruct.(fields{i,1})(1:n,4:9));
        %add last column for colormap
        CombinedStruct=horzcat(CombinedStruct,VarStruct.(fields{i,1})(1:n,11));
    else
        n=size(VarStruct.(fields{i,1}),1);
        %first add increments in work (first column)
        tCombinedStruct(1:n,1)=WorkStruct.(fields{i,1})(1:n,8);
        tCombinedStruct(1:n,2)=WorkStruct.(fields{i,1})(1:n,7);
        tCombinedStruct(1:n,3)=WorkStruct.(fields{i,1})(1:n,6);
        tCombinedStruct(1:n,4)=WorkStruct.(fields{i,1})(1:n,4);
        tCombinedStruct(1:n,5)=WorkStruct.(fields{i,1})(1:n,5);
        %then add increments in variables (columns 2 to 7)
        tCombinedStruct=horzcat(tCombinedStruct,VarStruct.(fields{i,1})(1:n,4:9));
        %add last column for colormap
        tCombinedStruct=horzcat(tCombinedStruct,VarStruct.(fields{i,1})(1:n,11));
        %concatenate matrices vertically
        CombinedStruct=vertcat(CombinedStruct,tCombinedStruct);
    end

end

Y=CombinedStruct;