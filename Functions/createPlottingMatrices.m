function [Y1,Y2,Y3]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,DataStruct,ParamSet,DataSet,p,StepsStruct)
%(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,DataStruct,p)
%This function creates the plottingmatrices for one of the structure
%containing the variables, power or interaction forces created during the
%within trial analysis

%Inputs:
%WithinTrialParamValues-Structure containing the information on the values each tunable parameter is set to during a trial (for all trials)
%ParamChangeInstances-Structure containing the information on the instance in which each tunable parameter is modified during a trial (for all trials)
%WithinTrialParamIncrements-Structure containin
%DataStruct-Structure containing either the within trialvariables/interaction forces/ power
%p-patient number
%poscount1,2 and 3 if its not the first patient

%Outputs
%Y1-plottingmatrix1- GGF is changed and experiences within trial changes
%Y2-plottingmatrix2- GGF is changed and is kept constant during the trial
%Y3-plotttingmatrix3- GGF is not changed in the trial



fieldnam1=fieldnames(DataStruct);
trialnums=regexp(fieldnam1,'\d*','Match');

count1=0;
count2=0;
count3=0;

 %loop to check which trials have increments in parameters
    for i=1:length(trialnums)
        trials(i,1)=str2double(trialnums{i,1});
    end

%1-Distribute the info into different matrices according to how the general guidance force is modified (1.there is within trial changes in GGF; 2.GGF is constant throughout the trial; 3.GGF is notmodified in the trial)    
    for i=1:length(fieldnam1)
        fieldnam=fieldnames(DataStruct.(fieldnam1{i,1}));
            if isfield(WithinTrialParamValues.(fieldnam1{i,1}),'General_GuidanceForce_pct')==1 &&  isfield(WithinTrialParamIncrements.(fieldnam1{i,1}),'General_GuidanceForce_pct')==1  %GGF is changed and experiences within trial changes
                number=1;
                count1=count1+1;
            elseif isfield(WithinTrialParamValues.(fieldnam1{i,1}),'General_GuidanceForce_pct')==1 && isfield(WithinTrialParamIncrements.(fieldnam1{i,1}),'General_GuidanceForce_pct')==0 %GGF is changed and is kept constant during the trial
                number=2;
                count2=count2+1;
            elseif isfield(WithinTrialParamValues.(fieldnam1{i,1}),'General_GuidanceForce_pct')==0 %GGF is not changed in the trial
                number=3;
                count3=count3+1;
            end
          switch number %switch loop to distribute the different trials into the plotting matrices
                case 1
                    %GGFchangeInstances=vertcat(ParamChangeInstances.(fieldnam1{i,1}).General_GuidanceForce_pct, zeros(3,1));
                    if count1==1 %first time to create the plottingmatrix1 struct
                        for j=1:length(fieldnam)
                            fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                            for k=1:length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))
                                counter=0;
                                Y1.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k); % 1-initial value param
                                Y1.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k+1);% 2-final value param
                                Y1.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(k); %3-increment param
                                Y1.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k); %4-change instance
                                for m=1:length(fieldnam3)
                                    counter=counter+1;
                                    Y1.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(k); %5-increment in the variables
                                end
                                Y1.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                Y1.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                w=ParamSet(trials(i)).data.time;
                                vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k), 'linear', 'extrap'));
                                if vidx ~= 0
                                   result=ParamSet(trials(i)).data.time(vidx);
                                end
                                index=find(ParamSet(trials(i)).data.time==result,1);
                                Y1.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                Y1.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(k);%step of the change
                                poscount1.(fieldnam{j,1})=k;%parameter position counter for storage 
                            end
                        end
                    else %If its not the first trial, check if the parameters changed already exist in the matrix
                        for j=1:length(fieldnam)
                            fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                            if isfield(Y1,(fieldnam{j,1}))==1
                                if length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))==1 %there is only one increment for the parameter within the trial
                                    for k=(poscount1.(fieldnam{j,1})+1)
                                        counter=0;
                                        Y1.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(1); % 1-initial value param
                                        Y1.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(2);% 2-final value param
                                        Y1.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(1); %3-increment param
                                        Y1.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(1); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y1.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(1); %5-increment in the variables
                                        end
                                        Y1.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y1.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=ParamSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(1), 'linear', 'extrap'));
                                        if vidx ~= 0
                                            if vidx<length(ParamSet(trials(i)).data.time)
                                               result=ParamSet(trials(i)).data.time(vidx);
                                               index=find(ParamSet(trials(i)).data.time==result,1);
                                            else
                                                index=length(ParamSet(trials(i)).data.time);
                                            end
                                        end
                                        
                                        Y1.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y1.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(1);%step of the change
                                        poscount1.(fieldnam{j,1})=k;%parameter position counter for storage       
                                    end
                                else %there is more than one increment for the parameter within the trial
                                    v=0;
                                    for k=(poscount1.(fieldnam{j,1})+1):(poscount1.(fieldnam{j,1})+length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1})))
                                        v=v+1;
                                        counter=0;
                                        Y1.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(v); % 1-initial value param
                                        Y1.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(v+1);% 2-final value param
                                        Y1.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(v); %3-increment param
                                        Y1.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(v); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y1.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(v); %5-increment in the variables
                                        end
                                        Y1.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y1.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=ParamSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(v), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=ParamSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(ParamSet(trials(i)).data.time==result,1);
                                        Y1.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y1.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(v);%step of the change
                                        poscount1.(fieldnam{j,1})=k;%parameter position counter for storage  
                                    end
                                    clear v
                                end
                            elseif isfield(Y1,(fieldnam{j,1}))==0 %the parameter hasnt been changed in the previous trials, so we have to create a new field for it in the struct
                                    fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                                    for k=1:length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))
                                        counter=0;
                                        Y1.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k); % 1-initial value param
                                        Y1.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k+1);% 2-final value param
                                        Y1.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(k); %3-increment param
                                        Y1.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y1.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(k); %5-increment in the variables
                                        end
                                        Y1.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y1.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=ParamSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=ParamSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(ParamSet(trials(i)).data.time==result,1);
                                        Y1.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y1.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(k);%step of the change
                                        poscount1.(fieldnam{j,1})=k;%parameter position counter for storage
                                    end
                            end
                        end
                    end
              case 2
                            %GGFchangeInstances=vertcat(ParamChangeInstances.(fieldnam1{i,1}).General_GuidanceForce_pct, zeros(3,1));
                    if count2==1 %first time to create the plottingmatrix1 struct
                        for j=1:length(fieldnam)
                            fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                            for k=1:length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))
                                counter=0;
                                Y2.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k); % 1-initial value param
                                Y2.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k+1);% 2-final value param
                                Y2.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(k); %3-increment param
                                Y2.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k); %4-change instance
                                for m=1:length(fieldnam3)
                                    counter=counter+1;
                                    Y2.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(k); %5-increment in the variables
                                end
                                Y2.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                Y2.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                w=ParamSet(trials(i)).data.time;
                                vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k), 'linear', 'extrap'));
                                if vidx ~= 0
                                   result=ParamSet(trials(i)).data.time(vidx);
                                end
                                index=find(ParamSet(trials(i)).data.time==result,1);
                                Y2.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                Y2.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(k);%step of the change
                                poscount2.(fieldnam{j,1})=k;%parameter position counter for storage    
                            end
                        end
                    else %If its not the first trial, check if the parameters changed already exist in the matrix
                        for j=1:length(fieldnam)
                            fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                            if isfield(Y2,(fieldnam{j,1}))==1
                                if length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))==1 %there is only one increment for the parameter within the trial
                                    for k=(poscount2.(fieldnam{j,1})+1)
                                        counter=0;
                                        Y2.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(1); % 1-initial value param
                                        Y2.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(2);% 2-final value param
                                        Y2.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(1); %3-increment param
                                        Y2.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(1); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y2.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(1); %5-increment in the variables
                                        end
                                        Y2.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y2.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=ParamSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(1), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=ParamSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(ParamSet(trials(i)).data.time==result,1);
                                        Y2.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y2.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(1);%step of the change
                                        poscount2.(fieldnam{j,1})=k;%parameter position counter for storage       
                                    end
                                else %there is more than one increment for the parameter within the trial
                                    v=0;
                                    for k=(poscount2.(fieldnam{j,1})+1):(poscount2.(fieldnam{j,1})+length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1})))
                                        v=v+1;
                                        counter=0;
                                        Y2.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(v); % 1-initial value param
                                        Y2.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(v+1);% 2-final value param
                                        Y2.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(v); %3-increment param
                                        Y2.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(v); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y2.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(v); %5-increment in the variables
                                        end
                                        Y2.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y2.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=DataSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(v), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=DataSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(DataSet(trials(i)).data.time==result,1);
                                        Y2.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y2.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(v);%step of the change
                                        poscount2.(fieldnam{j,1})=k;%parameter position counter for storage  
                                    end
                                    clear v
                                end
                            elseif isfield(Y2,(fieldnam{j,1}))==0 %the parameter hasnt been changed in the previous trials, so we have to create a new field for it in the struct
                                    fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                                    for k=1:length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))
                                        counter=0;
                                        Y2.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k); % 1-initial value param
                                        Y2.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k+1);% 2-final value param
                                        Y2.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(k); %3-increment param
                                        Y2.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y2.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(k); %5-increment in the variables
                                        end
                                        Y2.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y2.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=DataSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=DataSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(DataSet(trials(i)).data.time==result,1);
                                        Y2.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y2.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(k);%step of the change
                                        poscount2.(fieldnam{j,1})=k;%parameter position counter for storage
                                    end
                            end
                        end
                    end
              case 3
                    %GGFchangeInstances=vertcat(ParamChangeInstances.(fieldnam1{i,1}).General_GuidanceForce_pct, zeros(3,1));
                    if count3==1 %first time to create the plottingmatrix1 struct
                        for j=1:length(fieldnam)
                            fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                            for k=1:length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))
                                counter=0;
                                Y3.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k); % 1-initial value param
                                Y3.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k+1);% 2-final value param
                                Y3.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(k); %3-increment param
                                Y3.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k); %4-change instance
                                for m=1:length(fieldnam3)
                                    counter=counter+1;
                                    Y3.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(k); %5-increment in the variables
                                end
                                Y3.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                Y3.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                w=ParamSet(trials(i)).data.time;
                                vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k), 'linear', 'extrap'));
                                if vidx ~= 0
                                   result=ParamSet(trials(i)).data.time(vidx);
                                end
                                index=find(ParamSet(trials(i)).data.time==result,1);
                                Y3.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                Y3.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(k);%step of the change
                                poscount3.(fieldnam{j,1})=k;%parameter position counter for storage    
                            end
                        end
                    else %If its not the first trial, check if the parameters changed already exist in the matrix
                        for j=1:length(fieldnam)
                            fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                            if isfield(Y3,(fieldnam{j,1}))==1
                                if length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))==1 %there is only one increment for the parameter within the trial
                                    for k=(poscount3.(fieldnam{j,1})+1)
                                        counter=0;
                                        Y3.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(1); % 1-initial value param
                                        Y3.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(2);% 2-final value param
                                        Y3.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(1); %3-increment param
                                        Y3.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(1); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y3.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(1); %5-increment in the variables
                                        end
                                        Y3.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y3.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=ParamSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(1), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=ParamSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(ParamSet(trials(i)).data.time==result,1);
                                        Y3.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y3.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(1);%step of the change
                                        poscount3.(fieldnam{j,1})=k;%parameter position counter for storage       
                                    end
                                else %there is more than one increment for the parameter within the trial
                                    v=0;
                                    for k=(poscount3.(fieldnam{j,1})+1):(poscount3.(fieldnam{j,1})+length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1})))
                                        v=v+1;
                                        counter=0;
                                        Y3.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(v); % 1-initial value param
                                        Y3.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(v+1);% 2-final value param
                                        Y3.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(v); %3-increment param
                                        Y3.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(v); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y3.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(v); %5-increment in the variables
                                        end
                                        Y3.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y3.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=DataSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(v), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=DataSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(DataSet(trials(i)).data.time==result,1);
                                        Y3.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y3.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(v);%step of the change
                                        poscount3.(fieldnam{j,1})=k;%parameter position counter for storage  
                                    end
                                    clear v
                                end
                            elseif isfield(Y3,(fieldnam{j,1}))==0 %the parameter hasnt been changed in the previous trials, so we have to create a new field for it in the struct
                                    fieldnam3=fieldnames(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}));
                                    for k=1:length(DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{1,1}))
                                        counter=0;
                                        Y3.(fieldnam{j,1})(k,1)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k); % 1-initial value param
                                        Y3.(fieldnam{j,1})(k,2)=WithinTrialParamValues.(fieldnam1{i,1}).(fieldnam{j,1})(k+1);% 2-final value param
                                        Y3.(fieldnam{j,1})(k,3)=WithinTrialParamIncrements.(fieldnam1{i,1}).(fieldnam{j,1})(k); %3-increment param
                                        Y3.(fieldnam{j,1})(k,4)=ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k); %4-change instance
                                        for m=1:length(fieldnam3)
                                            counter=counter+1;
                                            Y3.(fieldnam{j,1})(k,4+m)=DataStruct.(fieldnam1{i,1}).(fieldnam{j,1}).(fieldnam3{m,1})(k); %5-increment in the variables
                                        end
                                        Y3.(fieldnam{j,1})(k,5+counter)=trials(i);%number of the trial
                                        Y3.(fieldnam{j,1})(k,6+counter)=p;%number of the patient
                                        w=DataSet(trials(i)).data.time;
                                        vidx = floor(interp1(w, w, ParamChangeInstances.(fieldnam1{i,1}).(fieldnam{j,1})(k), 'linear', 'extrap'));
                                        if vidx ~= 0
                                           result=DataSet(trials(i)).data.time(vidx);
                                        end
                                        index=find(DataSet(trials(i)).data.time==result,1);
                                        Y3.(fieldnam{j,1})(k,7+counter)=ParamSet(trials(i)).data.General_GuidanceForce_pct(index);%GGF level
                                        Y3.(fieldnam{j,1})(k,8+counter)=StepsStruct.(fieldnam1{i,1}).(fieldnam{j,1})(k);%step of the change
                                        poscount3.(fieldnam{j,1})=k;%parameter position counter for storage
                                    end
                            end
                        end
                    end
          end
    end
    
    if count2==0
        Y2=[];
    end
    
    if count1==0
        Y1=[];
    end
    
    if count3==0
        Y3=[];
    end
%     PC1=poscount1;
%     PC2=poscount2;
end


                    