function [Y] = calculateStepLength(D)
%step length: maximal hip flexion and minimal hip extension
%This function outputs the steplength of all the steps in a trial(stored in input struct D) and
%stores them inside the original stucture (in the 'Results' field).

%Both the measured angles (field 'StepLength')  and the difference with the reference trajectory
%(field 'StepLengthDiff')are stored. Steps are also divided into right and
%left.

%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%kinematic phase:
% 1-Double stance, left in front
% 2-Right swing
% 3-Double stance, right in front
% 4-Left swing

%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
%first we check the kinematic phases present in the section to evaluate and
%select the ones we want
Rsteps=[];
Lsteps=[];
count=0;
count2=0;
for  i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==3
       count=count+1;
       Rsteps(count)=(A-1)+i;
    elseif D.data.m_cont_kinematicPhase((A-1)+i)==1
       count2=count2+1;
       Lsteps(count2)=(A-1)+i;
    end
end

if any(D.data.m_cont_kinematicPhase==1)==0
  Lsteps=[];
end

if any(D.data.m_cont_kinematicPhase==3)==0
  Rsteps=[];
end

%differentiate between different steps by making sure que take only consecutive indeces
%Right
    if isempty(Rsteps)==1
        RStepLengths=0;
        RswingInstances=0;
        RmaxHipAngles=0;
        RminHipAngles=0;
    else
        RD=diff(Rsteps);
        RswingInstances(1)=Rsteps(1);
        count=1;

        for i=1:length(RD)
            if RD(i)~=1 && i~=1
                count=count+1;
                RswingInstances(count,1)=Rsteps(i);
            end
        end
        RswingInstances(count+1,1)=Rsteps(end); %swingInstances contains the indeces at which different steps start

         %find max knee flexion angle
        for i=1:length(RswingInstances)-1
            if length(RswingInstances)==2 
                RmaxHipAngles=max(D.data.m_cont_jointAngles_RHF);
                RminHipAngles=min(D.data.m_cont_jointAngles_RHF);
            else
                if i==1
                    RmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_RHF(RswingInstances(i):RswingInstances(i+1)-1));
                    RminHipAngles(i,1)=min(D.data.m_cont_jointAngles_RHF(RswingInstances(i):RswingInstances(i+1)-1));
                else 
                    RmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_RHF(Rsteps(find(Rsteps==RswingInstances(i))+1):RswingInstances(i+1)));
                    RminHipAngles(i,1)=max(D.data.m_cont_jointAngles_RHF(Rsteps(find(Rsteps==RswingInstances(i))+1):RswingInstances(i+1)));
                end 
            end
        end
    end
    
    D.Results.StepLength.Rsteps.max=RmaxHipAngles;
    D.Results.StepLength.Rsteps.min=RminHipAngles;
   
%Left
 if isempty(Lsteps)==1
        LStepLengths=0;
        LswingInstances=0;
        LmaxHipAngles=0;
        LminHipAngles=0;
    else
        LD=diff(Lsteps);
        LswingInstances(1)=Lsteps(1);
        count2=1;

        for i=1:length(LD)
            if LD(i)~=1 && i~=1
                count2=count2+1;
                LswingInstances(count2,1)=Lsteps(i);
            end
        end
        LswingInstances(count2+1,1)=Lsteps(end); %swingInstances contains the indeces at which different steps start

    %find max knee flexion angle
        for i=1:length(LswingInstances)-1
            if length(LswingInstances)==2 
                LmaxHipAngles=max(D.data.m_cont_jointAngles_LHF);
                LminHipAngles=min(D.data.m_cont_jointAngles_LHF);
            else
                if i==1
                    LmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_LHF(LswingInstances(i):LswingInstances(i+1)-1));
                    LminHipAngles(i,1)=min(D.data.m_cont_jointAngles_LHF(LswingInstances(i):LswingInstances(i+1)-1));
                else 
                    LmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_LHF(Lsteps(find(Lsteps==LswingInstances(i))+1):LswingInstances(i+1)));
                    LminHipAngles(i,1)=min(D.data.m_cont_jointAngles_LHF(Lsteps(find(Lsteps==LswingInstances(i))+1):LswingInstances(i+1)));
                end 
            end
        end
 end
   D.Results.StepLength.Lsteps.max=LmaxHipAngles;
   D.Results.StepLength.Lsteps.min=LminHipAngles;
   
%% check the joint trajectories
%peaks for the max (left and right)
% B=find(D.data.r_cont_stepCounter==3,1);
% lastStepr=find(D.data.r_cont_stepCounter==D.data.r_cont_stepCounter(end)-4,1);
% if isempty(lastStepr)==1
%     lastStepr=lastStep;
% end
% 
% [pksm,locsm]=findpeaks(D.data.m_cont_jointAngles_LHF(A:lastStep));
% [pksr,locsr]=findpeaks(D.data.r_cont_jointAngles_LHF(B:lastStepr));
% locsm=locsm(pksm>0.1);
% pksm=pksm(pksm>0.1);
% locsr=locsr(pksr>0.1);
% pksr=pksr(pksr>0.1);
% 
% %check for 'double peaks'in the measured data
% LDM=diff(locsm);
% doublepeaks=find(LDM<92);
% doublepeaks=doublepeaks+1;
% 
%     for i=1:length(doublepeaks)
%         if i==1
%            pksm(doublepeaks(i))=[];
%            locsm(doublepeaks(i))=[];
%         else
%             pksm(doublepeaks(i)-i+1)=[];
%             locsm(doublepeaks(i)-i+1)=[];
%         end
%     end
%     
% %check for 'double peaks'in the reference data
% LDR=diff(locsr);
% doublepeaksR=find(LDR<92);
% doublepeaksR=doublepeaksR+1;
% 
%     for i=1:length(doublepeaksR)
%         if i==1
%             pksr(doublepeaksR(i))=[];
%             locsr(doublepeaksR(i))=[];
%         else
%             pksr(doublepeaksR(i)-i+1)=[];
%             locsr(doublepeaksR(i)-i+1)=[];
%         end
%     end
%     
% [Rpksm,Rlocsm]=findpeaks(D.data.m_cont_jointAngles_RHF(A:lastStep));
% [Rpksr,Rlocsr]=findpeaks(D.data.r_cont_jointAngles_RHF(B:lastStepr));
% Rlocsm=Rlocsm(Rpksm>0.1);
% Rpksm=Rpksm(Rpksm>0.1);
% Rlocsr=Rlocsr(Rpksr>0.1);
% Rpksr=Rpksr(Rpksr>0.1);
% 
% %check for 'double peaks'
% RDM=diff(Rlocsm);
% Rdoublepeaks=find(RDM<92);
% Rdoublepeaks=Rdoublepeaks+1;
% 
%     for i=1:length(Rdoublepeaks)
%         if i==1
%             Rpksm(Rdoublepeaks(i))=[];
%             Rlocsm(Rdoublepeaks(i))=[];
%         else
%             Rpksm(Rdoublepeaks(i)-i+1)=[];
%             Rlocsm(Rdoublepeaks(i)-i+1)=[];
%         end
%     end
%     
% RDR=diff(Rlocsr);
% RdoublepeaksR=find(RDR<92);
% RdoublepeaksR=RdoublepeaksR+1;
% 
%     for i=1:length(RdoublepeaksR)
%         if i==1
%             Rpksr(RdoublepeaksR(i))=[];
%             Rlocsr(RdoublepeaksR(i))=[];
%         else
%             Rpksr(RdoublepeaksR(i)-i+1)=[];
%             Rlocsr(RdoublepeaksR(i)-i+1)=[];
%         end
%     end
%     
%    
% %peaks for the min
% [pksm2,locsm2]=findpeaks(-D.data.m_cont_jointAngles_LHF(A:lastStep));
% [pksr2,locsr2]=findpeaks(-D.data.r_cont_jointAngles_LHF(B:lastStepr));
% %remove negative peaks 
%  Loutliers=find(pksr2<0);
%     for i=1:length(Loutliers)
%         if i==1
%             pksr2(Loutliers(i))=[];
%             locsr2(Loutliers(i))=[];
%         else
%             pksr2(Loutliers(i)-i+1)=[];
%             locsr2(Loutliers(i)-i+1)=[];
%         end
%     end
%     
% Loutliersm=find(pksm2<0);
%     for i=1:length(Loutliersm)
%         if i==1
%             pksm2(Loutliersm(i))=[];
%             locsm2(Loutliersm(i))=[];
%         else
%             pksm2(Loutliersm(i)-i+1)=[];
%             locsm2(Loutliersm(i)-i+1)=[];
%         end
%     end   
%     
% 
% %check double peaks-measured
% LDM2=diff(locsm2);
% doublepeaks2=find(LDM2<92);
% doublepeaks2=doublepeaks2+1;
% 
%     for i=1:length(doublepeaks2)
%         if i==1
%             pksm2(doublepeaks2(i))=[];
%             locsm2(doublepeaks2(i))=[];
%         else
%             pksm2(doublepeaks2(i)-i+1)=[];
%             locsm2(doublepeaks2(i)-i+1)=[];
%         end
%     end
%     
% %check double peaks-ref
% LDM2=diff(locsr2);
% doublepeaks2=find(LDM2<92);
% doublepeaks2=doublepeaks2+1;
% 
%     for i=1:length(doublepeaks2)
%         if i==1
%             pksr2(doublepeaks2(i))=[];
%             locsr2(doublepeaks2(i))=[];
%         else
%             pksr2(doublepeaks2(i)-i+1)=[];
%             locsr2(doublepeaks2(i)-i+1)=[];
%         end
%     end
%     
%     
%  negpeaks=find(pksm2<0);   
%    for i=1:length(negpeaks)
%         if i==1
%             pksm2(negpeaks(i))=[];
%             locsm2(negpeaks(i))=[];
%         else
%             pksm2(negpeaks(i)-i+1)=[];
%             locsm2(negpeaks(i)-i+1)=[];
%         end
%    end 
%     
% 
% %right
% [Rpksm3,Rlocsm3]=findpeaks(-D.data.m_cont_jointAngles_RHF(A:lastStep));
% [Rpksr3,Rlocsr3]=findpeaks(-D.data.r_cont_jointAngles_RHF(B:lastStepr));
% 
% Rlocsm3=Rlocsm3(Rpksm3>-0.1);
% Rpksm3=Rpksm3(Rpksm3>-0.1);
% Rlocsr3=Rlocsr3(Rpksr3>-0.1);
% Rpksr3=Rpksr3(Rpksr3>-0.1);
% 
% RDM2=diff(Rlocsm3);
% doublepeaks3=find(RDM2<67);
% % doublepeaks3=doublepeaks3+1;
%  %double peaks 
%     for i=1:length(doublepeaks3)
%         if i==1
%             Rpksm3(doublepeaks3(i))=[];
%             Rlocsm3(doublepeaks3(i))=[];
%         else
%             Rpksm3(doublepeaks3(i)-i+1)=[];
%             Rlocsm3(doublepeaks3(i)-i+1)=[];
%         end
%     end
% 
% 
%    
% %%%%check the length of the vectors
% if length(pksr)<length(pksm)
%     %check the first and last peaks
%     if abs(locsr(1)-locsm(1))>100 %first element is an extra peak
%         pksm(1)=[];
%         locsm(1)=[];
%     elseif abs(locsm(end)-locsr(end))>100
%         pksm(end)=[];
%         locsm(end)=[];
%     end
% elseif length(pksm)<length(pksr)
%         %check the first and last peaks
%     if abs(locsm(1)-locsr(1))>100 %first element is an extra peak
%         pksr(1)=[];
%         locsr(1)=[];
%     elseif abs(locsm(end)-locsr(end))>100
%         pksr(end)=[];
%         locsr(end)=[];
%     end
%     
% end
% 
% %
% if length(Rpksr)<length(Rpksm)
%     if abs(Rlocsr(1)-Rlocsm(1))>100 %first element is an extra peak
%         Rpksm(1)=[];
%         Rlocsm(1)=[];
%     elseif abs(Rlocsm(end)-Rlocsr(end))>100
%         Rpksm(end)=[];
%         Rlocsm(end)=[];
%     end
% elseif length(Rpksm)<length(Rpksr)
%         %check the first and last peaks
%     if abs(Rlocsm(1)-Rlocsr(1))>100 %first element is an extra peak
%         Rpksr(1)=[];
%         Rlocsr(1)=[];
%     elseif abs(Rlocsm(end)-Rlocsr(end))>100
%         Rpksr(end)=[];
%         Rlocsr(end)=[];
%     end
% end
% 
% %
% if length(pksr2)<length(pksm2)
%     if abs(locsr2(1)-locsm2(1))>100 %first element is an extra peak
%         pksm2(1)=[];
%         locsm2(1)=[];
%     elseif abs(locsm2(end)-locsr2(end))>100
%         pksm2(end)=[];
%         locsm2(end)=[];
%     end
% elseif length(pksm2)<length(pksr2)
%         %check the first and last peaks
%     if (locsm2(1)-locsr2(1))>100 %first element is an extra peak
%         pksr2(1)=[];
%         locsr2(1)=[];
%     elseif abs(locsm2(end)-locsr2(end))>100
%         pksr2(end)=[];
%         locsr2(end)=[];
%     end  
% end
% %
% if length(Rpksr3)<length(Rpksm3)
%     if abs(Rlocsr3(1)-Rlocsm3(1))>100 %first element is an extra peak
%         Rpksm3(1)=[];
%         Rlocsm3(1)=[];
%     elseif abs(Rlocsm3(end)-Rlocsr3(end))>100
%         Rpksm3(end)=[];
%         Rlocsm3(end)=[];
%     end
% elseif length(Rpksm3)<length(Rpksr3)
%         %check the first and last peaks
%     if abs(Rlocsm3(1)-Rlocsr3(1))>100 %first element is an extra peak
%         Rpksr3(1)=[];
%         Rlocsr3(1)=[];
%     elseif abs(Rlocsm3(end)-Rlocsr3(end))>100
%         Rpksr3(end)=[];
%         Rlocsr3(end)=[];
%     end
% end
%      
% %remove any negative peaks
% Lnegativepeaksm=find(pksm<0);
%     for i=1:length(Lnegativepeaksm)
%         if i==1
%             pksm(Lnegativepeaksm(i))=[];
%             locsm(Lnegativepeaksm(i))=[];
%         else
%             pksm(Lnegativepeaksm(i)-i+1)=[];
%             locsm(Lnegativepeaksm(i)-i+1)=[];
%         end
%     end
%     
% Lnegativepeaksr=find(pksr<0);
%     for i=1:length(Lnegativepeaksr)
%         if i==1
%             pksr(Lnegativepeaksr(i))=[];
%             locsr(Lnegativepeaksr(i))=[];
%         else
%             pksr(Lnegativepeaksr(i)-i+1)=[];
%             locsr(Lnegativepeaksr(i)-i+1)=[];
%         end
%     end 
%     
% Rnegativepeaksm=find(Rpksm<0);
%     for i=1:length(Rnegativepeaksm)
%         if i==1
%             Rpksm(Rnegativepeaksm(i))=[];
%             Rlocsm(Rnegativepeaksm(i))=[];
%         else
%             Rpksm(Rnegativepeaksm(i)-i+1)=[];
%             Rlocsm(Rnegativepeaksm(i)-i+1)=[];
%         end
%     end
%     
% Rnegativepeaksr=find(Rpksr<0);
%     for i=1:length(Rnegativepeaksr)
%         if i==1
%             Rpksr(Rnegativepeaksr(i))=[];
%             Rlocsr(Rnegativepeaksr(i))=[];
%         else
%             Rpksr(Rnegativepeaksr(i)-i+1)=[];
%             Rlocsr(Rnegativepeaksr(i)-i+1)=[];
%         end
%     end 
%     
%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% %more additional checks    
% if length(Rpksm3)~=length(Rpksr3)
%     Rlocsm3=Rlocsm3(Rpksm3>-0.017);
%     Rpksm3=Rpksm3(Rpksm3>-0.017);
% end
% 
% if length(Rpksm3)~=length(Rpksr3)
%     RDM2=diff(Rlocsm3);
%     doublepeaks3=find(RDM2<80);
%     % doublepeaks3=doublepeaks3+1;
%      %double peaks 
%         for i=1:length(doublepeaks3)
%             if i==1
%                 Rpksm3(doublepeaks3(i))=[];
%                 Rlocsm3(doublepeaks3(i))=[];
%             else
%                 Rpksm3(doublepeaks3(i)-i+1)=[];
%                 Rlocsm3(doublepeaks3(i)-i+1)=[];
%             end
%         end
% end
% 
% 
% %%%%%%repeat check first/final element
% for i=1:2
%     %check the length of the vectors
%     if length(pksr)<length(pksm)
%         %check the first and last peaks
%         if abs(locsr(1)-locsm(1))>90 %first element is an extra peak
%             pksm(1)=[];
%             locsm(1)=[];
%         elseif abs(locsm(end)-locsr(end))>90
%             pksm(end)=[];
%             locsm(end)=[];
%         end
%     elseif length(pksm)<length(pksr)
%             %check the first and last peaks
%         if abs(locsm(1)-locsr(1))>90 %first element is an extra peak
%             pksr(1)=[];
%             locsr(1)=[];
%         elseif abs(locsm(end)-locsr(end))>90
%             pksr(end)=[];
%             locsr(end)=[];
%         end
% 
%     end
% 
%     %
%     if length(Rpksr)<length(Rpksm)
%         if abs(Rlocsr(1)-Rlocsm(1))>90 %first element is an extra peak
%             Rpksm(1)=[];
%             Rlocsm(1)=[];
%         elseif abs(Rlocsm(end)-Rlocsr(end))>90
%             Rpksm(end)=[];
%             Rlocsm(end)=[];
%         end
%     elseif length(Rpksm)<length(Rpksr)
%             %check the first and last peaks
%         if abs(Rlocsm(1)-Rlocsr(1))>90 %first element is an extra peak
%             Rpksr(1)=[];
%             Rlocsr(1)=[];
%         elseif abs(Rlocsm(end)-Rlocsr(end))>90
%             Rpksr(end)=[];
%             Rlocsr(end)=[];
%         end
%     end
% 
%     %
%     if length(pksr2)<length(pksm2)
%         if abs(locsr2(1)-locsm2(1))>90 %first element is an extra peak
%             pksm2(1)=[];
%             locsm2(1)=[];
%         elseif abs(locsm2(end)-locsr2(end))>90
%             pksm2(end)=[];
%             locsm2(end)=[];
%         end
%     elseif length(pksm2)<length(pksr2)
%             %check the first and last peaks
%         if (locsm2(1)-locsr2(1))>100 %first element is an extra peak
%             pksr2(1)=[];
%             locsr2(1)=[];
%         elseif abs(locsm2(end)-locsr2(end))>90
%             pksr2(end)=[];
%             locsr2(end)=[];
%         end  
%     end
%     %
%     if length(Rpksr3)<length(Rpksm3)
%         if abs(Rlocsr3(1)-Rlocsm3(1))>90 %first element is an extra peak
%             Rpksm3(1)=[];
%             Rlocsm3(1)=[];
%         elseif abs(Rlocsm3(end)-Rlocsr3(end))>90
%             Rpksm3(end)=[];
%             Rlocsm3(end)=[];
%         end
%     elseif length(Rpksm3)<length(Rpksr3)
%             %check the first and last peaks
%         if abs(Rlocsm3(1)-Rlocsr3(1))>90 %first element is an extra peak
%             Rpksr3(1)=[];
%             Rlocsr3(1)=[];
%         elseif abs(Rlocsm3(end)-Rlocsr3(end))>90
%             Rpksr3(end)=[];
%             Rlocsr3(end)=[];
%         end
%     end
% end
% 
% if length(Rlocsm3)>length(Rlocsr3)
% RDM2=diff(Rlocsm3);
% doublepeaks3=find(RDM2<103);
% % doublepeaks3=doublepeaks3+1;
%  %double peaks 
%     for i=1:length(doublepeaks3)
%         if i==1
%             Rpksm3(doublepeaks3(i))=[];
%             Rlocsm3(doublepeaks3(i))=[];
%         else
%             Rpksm3(doublepeaks3(i)-i+1)=[];
%             Rlocsm3(doublepeaks3(i)-i+1)=[];
%         end
%     end
% end
% 
% if length(Rlocsm)>length(Rlocsr)
% RDM2=diff(Rlocsm);
% doublepeaks3=find(RDM2<140);
% % doublepeaks3=doublepeaks3+1;
%  %double peaks 
%     for i=1:length(doublepeaks3)
%         if i==1
%             Rpksm(doublepeaks3(i))=[];
%             Rlocsm(doublepeaks3(i))=[];
%         else
%             Rpksm(doublepeaks3(i)-i+1)=[];
%             Rlocsm(doublepeaks3(i)-i+1)=[];
%         end
%     end
% end
% 
% if length(locsm)>length(locsr)
%     LDM=diff(locsm);
%     doublepeaks=find(LDM<140);
%     doublepeaks=doublepeaks+1;
% 
%         for i=1:length(doublepeaks)
%             if i==1
%                pksm(doublepeaks(i))=[];
%                locsm(doublepeaks(i))=[];
%             else
%                 pksm(doublepeaks(i)-i+1)=[];
%                 locsm(doublepeaks(i)-i+1)=[];
%             end
%         end
% end
% 
% if length(locsm2)>length(locsr2)
%     LDM=diff(locsm2);
%     doublepeaks=find(LDM<100);
%         for i=1:length(doublepeaks)
%             if i==1
%                pksm2(doublepeaks(i))=[];
%                locsm2(doublepeaks(i))=[];
%             else
%                 pksm2(doublepeaks(i)-i+1)=[];
%                 locsm2(doublepeaks(i)-i+1)=[];
%             end
%         end
% end
% %Finaly, compute the difference between reference and measured   
% 
% D.Results.StepLengthDiff.Lsteps.max=pksr-pksm;
% D.Results.StepLengthDiff.Rsteps.max=Rpksr-Rpksm;
% 
% D.Results.StepLengthDiff.Lsteps.min=pksr2-pksm2;
% D.Results.StepLengthDiff.Rsteps.min=Rpksr3-Rpksm3;

%using step counter
%MEASURED
%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
%first we check the kinematic phases present in the section to evaluate and
%select the ones we want
Rsteps=[];
Lsteps=[];
count=0;
count2=0;
for  i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==3
       count=count+1;
       Rsteps(count)=(A-1)+i;
    elseif D.data.m_cont_kinematicPhase((A-1)+i)==1
       count2=count2+1;
       Lsteps(count2)=(A-1)+i;
    end
end

if any(D.data.m_cont_kinematicPhase==1)==0
  Lsteps=[];
end

if any(D.data.m_cont_kinematicPhase==3)==0
  Rsteps=[];
end

clear count count2
%differentiate between different steps by making sure que take only consecutive indeces
%Right
    if isempty(Rsteps)==1
        RStepLengths=0;
        RswingInstances=0;
        RmaxHipAngles=0;
        RminHipAngles=0;
    else
        RD=diff(Rsteps);
        RswingInstances(1)=Rsteps(1);
        count=1;

        for i=1:length(RD)
            if RD(i)~=1 && i~=1
                count=count+1;
                RswingInstances(count,1)=Rsteps(i);
            end
        end
        RswingInstances(count+1,1)=Rsteps(end); %swingInstances contains the indeces at which different steps start

         %find max knee flexion angle
        for i=1:length(RswingInstances)-1
            if length(RswingInstances)==2 
                RmaxHipAngles=max(D.data.m_cont_jointAngles_RHF);
                RminHipAngles=min(D.data.m_cont_jointAngles_RHF);
            else
                if i==1
                    RmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_RHF(RswingInstances(i):RswingInstances(i+1)-1));
                    RminHipAngles(i,1)=min(D.data.m_cont_jointAngles_RHF(RswingInstances(i):RswingInstances(i+1)-1));
                else 
                    RmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_RHF(RswingInstances(i)+1:RswingInstances(i+1)));
                    RminHipAngles(i,1)=max(D.data.m_cont_jointAngles_RHF(RswingInstances(i)+1:RswingInstances(i+1)));
                end 
            end
        end
    end
    
    RStepLengthmaxM=RmaxHipAngles;
    RStepLengthminM=RminHipAngles;
   
%Left
 if isempty(Lsteps)==1
        LStepLengths=0;
        LswingInstances=0;
        LmaxHipAngles=0;
        LminHipAngles=0;
    else
        LD=diff(Lsteps);
        LswingInstances(1)=Lsteps(1);
        count2=1;

        for i=1:length(LD)
            if LD(i)~=1 && i~=1
                count2=count2+1;
                LswingInstances(count2,1)=Lsteps(i);
            end
        end
        LswingInstances(count2+1,1)=Lsteps(end); %swingInstances contains the indeces at which different steps start

    %find max knee flexion angle
        for i=1:length(LswingInstances)-1
            if length(LswingInstances)==2 
                LmaxHipAngles=max(D.data.m_cont_jointAngles_LHF);
                LminHipAngles=min(D.data.m_cont_jointAngles_LHF);
            else
                if i==1
                    LmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_LHF(LswingInstances(i):LswingInstances(i+1)-1));
                    LminHipAngles(i,1)=min(D.data.m_cont_jointAngles_LHF(LswingInstances(i):LswingInstances(i+1)-1));
                else 
                    LmaxHipAngles(i,1)=max(D.data.m_cont_jointAngles_LHF(LswingInstances(i)+1:LswingInstances(i+1)));
                    LminHipAngles(i,1)=min(D.data.m_cont_jointAngles_LHF(LswingInstances(i)+1:LswingInstances(i+1)));
                end 
            end
        end
 end
   LStepLengthmaxM=LmaxHipAngles;
   LStepLengthminM=LminHipAngles;
   
   clear RswingInstances LswingInstances Rsteps Lsteps
%REFERENCE
%check the instance of the first step
B=find(D.data.r_cont_stepCounter==3,1);
lastStepr=find(D.data.r_cont_stepCounter==D.data.r_cont_stepCounter(end)-4,1);
%prepositioning: knee angle just before heel strike
if isempty(lastStepr)==1
    lastStepr=lastStep;
end

count=0;
count2=0;
Rsteps=[];
Lsteps=[];

%first we select the section of the data we want using the kinematic phase
for i=1:length(D.data.r_cont_kinematicPhase(B:lastStepr))
    if D.data.r_cont_kinematicPhase((B-1)+i)==3
        count=count+1;
        Rsteps(count,1)=(B-1)+i;
    elseif D.data.r_cont_kinematicPhase((B-1)+i)==1
        count2=count2+1;
        Lsteps(count2,1)=(B-1)+i;
    end
end
clear count count2

%Right
    if isempty(Rsteps)==1
        RStepLengths=0;
        RswingInstances=0;
        RmaxHipAngles=0;
        RminHipAngles=0;
    else
        RD=diff(Rsteps);
        RswingInstances(1)=Rsteps(1);
        count=1;

        for i=1:length(RD)
            if RD(i)~=1 && i~=1
                count=count+1;
                RswingInstances(count,1)=Rsteps(i);
            end
        end
        RswingInstances(count+1,1)=Rsteps(end); %swingInstances contains the indeces at which different steps start
        RswingInstances=sort(RswingInstances);
         %find max knee flexion angle
        for i=1:length(RswingInstances)-1
            if length(RswingInstances)==2 
                RmaxHipAngles=max(D.data.r_cont_jointAngles_RHF);
                RminHipAngles=min(D.data.r_cont_jointAngles_RHF);
            else
                if i==1
                    RmaxHipAngles(i,1)=max(D.data.r_cont_jointAngles_RHF(RswingInstances(i):RswingInstances(i+1)-1));
                    RminHipAngles(i,1)=min(D.data.r_cont_jointAngles_RHF(RswingInstances(i):RswingInstances(i+1)-1));
                else 
                    RmaxHipAngles(i,1)=max(D.data.r_cont_jointAngles_RHF(RswingInstances(i)+1:RswingInstances(i+1)));
                    RminHipAngles(i,1)=min(D.data.r_cont_jointAngles_RHF(RswingInstances(i)+1:RswingInstances(i+1)));
                end 
            end
        end
    end
    
    RStepLengthmaxR=RmaxHipAngles;
    RStepLengthminR=RminHipAngles;
   
%Left
 if isempty(Lsteps)==1
        LStepLengths=0;
        LswingInstances=0;
        LmaxHipAngles=0;
        LminHipAngles=0;
    else
        LD=diff(Lsteps);
        LswingInstances(1)=Lsteps(1);
        count2=1;

        for i=1:length(LD)
            if LD(i)~=1 && i~=1
                count2=count2+1;
                LswingInstances(count2,1)=Lsteps(i);
            end
        end
        LswingInstances(count2+1,1)=Lsteps(end); %swingInstances contains the indeces at which different steps start
        LswingInstances=sort(LswingInstances);
    %find max knee flexion angle
        for i=1:length(LswingInstances)-1
            if length(LswingInstances)==2 
                LmaxHipAngles=max(D.data.r_cont_jointAngles_LHF);
                LminHipAngles=min(D.data.r_cont_jointAngles_LHF);
            else
                if i==1
                    LmaxHipAngles(i,1)=max(D.data.r_cont_jointAngles_LHF(LswingInstances(i):LswingInstances(i+1)-1));
                    LminHipAngles(i,1)=min(D.data.r_cont_jointAngles_LHF(LswingInstances(i):LswingInstances(i+1)-1));
                else 
                    LmaxHipAngles(i,1)=max(D.data.r_cont_jointAngles_LHF(LswingInstances(i)+1:LswingInstances(i+1)));
                    LminHipAngles(i,1)=min(D.data.r_cont_jointAngles_LHF(LswingInstances(i)+1:LswingInstances(i+1)));
                end 
            end
        end
 end
   LStepLengthmaxR=LmaxHipAngles;
   LStepLengthminR=LminHipAngles;
   
 if length(LStepLengthmaxM)<length(LStepLengthmaxR)
     while length(LStepLengthmaxM)<length(LStepLengthmaxR)
        LStepLengthmaxR(end)=[];
     end
 end
 
 if length(RStepLengthmaxM)<length(RStepLengthmaxR)
     while length(RStepLengthmaxM)<length(RStepLengthmaxR)
      RStepLengthmaxR(end)=[];
     end
 end
 
  if length(LStepLengthminM)<length(LStepLengthminR)
      while length(LStepLengthminM)<length(LStepLengthminR)
        LStepLengthminR(end)=[];
      end
 end
 
 if length(RStepLengthminM)<length(RStepLengthminR)
     while length(RStepLengthminM)<length(RStepLengthminR)
      RStepLengthminR(end)=[];
     end
 end
  
D.Results.StepLengthDiff.Lsteps.max=LStepLengthmaxR-LStepLengthmaxM;
D.Results.StepLengthDiff.Rsteps.max=RStepLengthmaxR-RStepLengthmaxM;

D.Results.StepLengthDiff.Lsteps.min=LStepLengthminM-LStepLengthminR;
D.Results.StepLengthDiff.Rsteps.min=RStepLengthminM-RStepLengthminR;
   
Y=D;

end
   