function incrementPlotsByParameter(Rdata,Ldata)
%This function outputs two figures with three subplots each; In both the
%increments for one parameter and the 'significant'joint it affects, are
%shown. Subplots are divided by GGF level in figure 1 and by the initial
%level of assistance in figure 2

fieldsR=fieldnames(Rdata);
fieldsL=fieldnames(Ldata);

%% group by ppoints by GGFlevel
%Rdata
for i=1:length(fieldsR)
    if strcmp(fieldsR{i,1},'RightStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'RightStabilityStance_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'RightPrepositioning_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'RightStepLength_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'WeightShift_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftStabilityStance_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftPrepositioning_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftStepLength_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RGGF30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RGGF60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RGGF100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    end
end
%Ldata
for i=1:length(fieldsL)
    if strcmp(fieldsL{i,1},'RightStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'RightStabilityStance_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'RightPrepositioning_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'RightStepLength_GuidanceForce_pct')==1
       X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'WeightShift_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftStabilityStance_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftPrepositioning_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftStepLength_GuidanceForce_pct')==1
       X=Ldata.(fieldsL{i,1})(:,9);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LGGF30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LGGF60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LGGF100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    end
end

%% group by initial assitance level- check column 10-11 for weight shift
%Rdata
for i=1:length(fieldsR)
    if strcmp(fieldsR{i,1},'RightStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'RightStabilityStance_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'RightPrepositioning_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'RightStepLength_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'WeightShift_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,11);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftStabilityStance_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftPrepositioning_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    elseif strcmp(fieldsR{i,1},'LeftStepLength_GuidanceForce_pct')==1
        X=Rdata.(fieldsR{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        RASIST30.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows30,:);
        RASIST60.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows60,:);
        RASIST100.(fieldsR{i,1})=Rdata.(fieldsR{i,1})(Rows100,:);
    end
end
%Ldata
for i=1:length(fieldsL)
    if strcmp(fieldsL{i,1},'RightStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'RightStabilityStance_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'RightPrepositioning_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'RightStepLength_GuidanceForce_pct')==1
       X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'WeightShift_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,11);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftStepHeight_GuidanceForce_pct')==1
        %check column 9 of the combined structs
        X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftStabilityStance_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftPrepositioning_GuidanceForce_pct')==1
        X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    elseif strcmp(fieldsL{i,1},'LeftStepLength_GuidanceForce_pct')==1
       X=Ldata.(fieldsL{i,1})(:,10);
        Rows30=find(X<30);    
        Rows60=find(X>=30 & X<60); 
        Rows100=find(X>=60); 
        %divide into different structs 
        LASIST30.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows30,:);
        LASIST60.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows60,:);
        LASIST100.(fieldsL{i,1})=Ldata.(fieldsL{i,1})(Rows100,:);
    end
end

%% plots
Rparams={'RightStepHeight_GuidanceForce_pct';'RightStabilityStance_GuidanceForce_pct';'RightStepLength_GuidanceForce_pct';'RightPrepositioning_GuidanceForce_pct';'WeightShift_GuidanceForce_pct'};
Lparams={'LeftStepHeight_GuidanceForce_pct';'LeftStabilityStance_GuidanceForce_pct';'LeftStepLength_GuidanceForce_pct';'LeftPrepositioning_GuidanceForce_pct';'WeightShift_GuidanceForce_pct'};
ylabels={'Step Height','\Delta rad';'Stability','\Delta rad';'Step length','\Delta rad';'Prepositioning','\Delta rad';'Weight Shift','\Delta rad'};
t={'Step Height Guidance Force'; 'Stability d/Stance Guidance Force'; 'Step Length Guidance Force';'Prepositioning Guidance Force'; 'Weight Shift Guidance Force'};

% set y limits of the figures
% for i=1:p
%     if 1==1
%        for j=1:v
%            if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
%             y=vertcat(Rdata.(Rparams{i,1})(:,j+1),Ldata.(Lparams{i,1})(:,j+1));
%             yupperbounds(j)=max(y);
%             ylowerbounds(j)=min(y);
%            elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
%                 yupperbounds(j)=max(Rdata.(Rparams{i,1})(:,j+1));
%                 ylowerbounds(j)=min(Rdata.(Rparams{i,1})(:,j+1));
%            elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
%                 yupperbounds(j)=max(Ldata.(Lparams{i,1})(:,j+1));
%                 ylowerbounds(j)=min(Ldata.(Lparams{i,1})(:,j+1));
%            end
%        end
%     else 
%         for j=1:v
%             if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
%                 y=vertcat(Rdata.(Rparams{i,1})(:,j+1),Ldata.(Lparams{i,1})(:,j+1));
%                 yupperboundstemp=max(y);
%                 ylowerboundstemp=min(y);
%                 compare with the previous
%                 if yupperboundstemp>yupperbounds(j)
%                     yupperbounds(j)=yupperboundstemp;
%                 end
%                 if ylowerboundstemp<ylowerbounds(j)
%                     ylowerbounds(j)=ylowerboundstemp;
%                 end
%             elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
%                  yupperboundstemp=max(Rdata.(Rparams{i,1})(:,j+1));
%                  ylowerboundstemp=min(Rdata.(Rparams{i,1})(:,j+1));
%                  compare with the previous
%                 if yupperboundstemp>yupperbounds(j)
%                     yupperbounds(j)=yupperboundstemp;
%                 end
%                 if ylowerboundstemp<ylowerbounds(j)
%                     ylowerbounds(j)=ylowerboundstemp;
%                 end
%             elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
%                     yupperboundstemp=max(Ldata.(Lparams{i,1})(:,j+1));
%                     ylowerboundstemp=min(Ldata.(Lparams{i,1})(:,j+1));
%                     compare with the previous
%                     if yupperboundstemp>yupperbounds(j)
%                         yupperbounds(j)=yupperboundstemp;
%                     end
%                     if ylowerboundstemp<ylowerbounds(j)
%                         ylowerbounds(j)=ylowerboundstemp;
%                     end
%             end
%         end
%     end
% end
    

%% increments in variables vs increments in work
for i=1:length(Rparams)
    if i==5 %weight shift
        figure
        colormap(jet);
        [ha,pos]=tight_subplot(4,3,[.01 .03],[0.1 .01],[.01 .01]);
        %hip abduction
        subplot(4,3,1,ha(1))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,8),30,RGGF30.(Rparams{i,1})(:,11),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,8),30,LGGF30.(Lparams{i,1})(:,11),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'General Guidance Force < 30%';'Hip Abduction'})
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,8),30,RGGF30.(Rparams{i,1})(:,11),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title({'General Guidance Force < 30%';'Hip Abduction'})
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,8),30,LGGF30.(Lparams{i,1})(:,11),'d','filled')
           %set(gca,'XTick',[]);
           ylabel('\Delta Step Height (rad)','FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'General Guidance Force < 30%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,2,ha(2))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,8),30,RGGF60.(Rparams{i,1})(:,11),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,8),30,LGGF60.(Lparams{i,1})(:,11),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,8),30,RGGF60.(Rparams{i,1})(:,11),'d','filled') 
           %set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,8),30,LGGF60.(Lparams{i,1})(:,11),'d','filled')
           %set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,3,ha(3))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,8),30,RGGF100.(Rparams{i,1})(:,11),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,8),30,LGGF100.(Lparams{i,1})(:,11),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,8),30,RGGF100.(Rparams{i,1})(:,11),'d','filled') 
           %set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,8),30,LGGF100.(Lparams{i,1})(:,11),'d','filled')
           %set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %pelvis-z
        subplot(4,3,4,ha(4))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,10),RGGF30.(Rparams{i,1})(:,8),30,RGGF30.(Rparams{i,1})(:,11),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,10),LGGF30.(Lparams{i,1})(:,8),30,LGGF30.(Lparams{i,1})(:,11),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,10),RGGF30.(Rparams{i,1})(:,8),30,RGGF30.(Rparams{i,1})(:,11),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,10),LGGF30.(Lparams{i,1})(:,8),30,LGGF30.(Lparams{i,1})(:,11),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('PZ')
        end
        subplot(4,3,5,ha(5))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,10),RGGF60.(Rparams{i,1})(:,8),30,RGGF60.(Rparams{i,1})(:,11),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,10),LGGF60.(Lparams{i,1})(:,8),30,LGGF60.(Lparams{i,1})(:,11),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,10),RGGF60.(Rparams{i,1})(:,8),30,RGGF60.(Rparams{i,1})(:,11),'d','filled') 
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,10),LGGF60.(Lparams{i,1})(:,8),30,LGGF60.(Lparams{i,1})(:,11),'d','filled')
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,6,ha(6))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,10),RGGF100.(Rparams{i,1})(:,8),30,RGGF100.(Rparams{i,1})(:,11),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,10),LGGF100.(Lparams{i,1})(:,8),30,LGGF100.(Lparams{i,1})(:,11),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,10),RGGF100.(Rparams{i,1})(:,8),30,RGGF100.(Rparams{i,1})(:,11),'d','filled') 
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,10),LGGF100.(Lparams{i,1})(:,8),30,LGGF100.(Lparams{i,1})(:,11),'d','filled')
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %assistance level
        subplot(4,3,7,ha(7))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,8),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,8),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,8),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,8),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,8,ha(8))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,8),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,8),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,8),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,8),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,9,ha(9))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,8),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,8),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,8),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,8),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Hip Abduction'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %PZ
        subplot(4,3,10,ha(10))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,10),RASIST30.(Rparams{i,1})(:,8),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,10),LASIST30.(Lparams{i,1})(:,8),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,10),RASIST30.(Rparams{i,1})(:,8),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,10),LASIST30.(Lparams{i,1})(:,8),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,11,ha(11))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,10),RASIST60.(Rparams{i,1})(:,8),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,10),LASIST60.(Lparams{i,1})(:,8),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,10),RASIST60.(Rparams{i,1})(:,8),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,10),LASIST60.(Lparams{i,1})(:,8),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,12,ha(12))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,10),RASIST100.(Rparams{i,1})(:,8),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,10),LASIST100.(Lparams{i,1})(:,8),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,10),RASIST100.(Rparams{i,1})(:,8),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,10),LASIST100.(Lparams{i,1})(:,8),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        end
        sgtitle(t{i,1}) %title over all the sublots
        [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
    elseif i==3 %step length (initial final)   
        figure
        colormap(jet);
        [ha,pos]=tight_subplot(4,3,[.01 .03],[0.1 .01],[.01 .01]);
        %hip abduction
        subplot(4,3,1,ha(1))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,4),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,4),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           hold off
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'General Guidance Force < 30%';'Initial Step Length'})
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,4),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title({'General Guidance Force < 30%';'Initial Step Length'})
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,4),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           set(gca,'XTick',[]);
           ylabel('\Delta Step Height (rad)','FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'General Guidance Force < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,2,ha(2))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,4),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,4),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,4),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
           set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,4),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,3,ha(3))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,4),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,4),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,4),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
           set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,4),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %terminal sl
        subplot(4,3,4,ha(4))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,5),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,5),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,5),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,5),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        end
        subplot(4,3,5,ha(5))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,5),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,5),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,5),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,5),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,6,ha(6))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,5),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,5),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,5),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,5),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %assistance level
        subplot(4,3,7,ha(7))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,4),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,4),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,4),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,4),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,8,ha(8))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,4),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,4),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,4),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,4),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,9,ha(9))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,4),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,4),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,4),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,4),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %terminal sl
        subplot(4,3,10,ha(10))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,5),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,5),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,5),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,5),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,11,ha(11))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,5),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,5),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,5),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,5),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,12,ha(12))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,5),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,5),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,5),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,5),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        end
        sgtitle(t{i,1}) %title over all the sublots
        [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
        
    elseif i==4 %prepositioning
            figure
            colormap(jet);
            [ha,pos]=tight_subplot(2,3,[.01 .03],[0.1 .01],[.01 .01]);
            %GGF
            subplot(2,3,1,ha(1))
            if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
               scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,7),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,7),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('General Guidance Force < 30%')
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
               scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,7),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
               title('General Guidance Force < 30%')
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
               scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,7),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('General Guidance Force < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,2,ha(2))
            if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
               scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,7),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,7),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
               scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,7),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
               scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,7),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,3,ha(3))
            if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
               scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,7),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,7),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
               scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,7),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
               scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,7),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end

            %assistance level
            subplot(2,3,4,ha(4))
            if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
               scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,7),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,7),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
               scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,7),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
               scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,7),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,5,ha(5))
            if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
               scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,7),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,7),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
               scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,7),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
               scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,7),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,6,ha(6))
            if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
               scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,7),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,7),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
               scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,7),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
               scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,7),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            end
            sgtitle(t{i,1}) %title over all the sublots
            [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
    else
        figure
        colormap(jet);
        [ha,pos]=tight_subplot(2,3,[.01 .03],[0.1 .01],[.01 .01]);
        %GGF
        subplot(2,3,1,ha(1))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,i+1),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,i+1),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('General Guidance Force < 30%')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,1),RGGF30.(Rparams{i,1})(:,i+1),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title('General Guidance Force < 30%')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,1),LGGF30.(Lparams{i,1})(:,i+1),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('General Guidance Force < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,2,ha(2))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,i+1),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,i+1),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('General Guidance Force 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,1),RGGF60.(Rparams{i,1})(:,i+1),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('General Guidance Force 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,1),LGGF60.(Lparams{i,1})(:,i+1),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('General Guidance Force 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,3,ha(3))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,i+1),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,i+1),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('General Guidance Force 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,i+1),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('General Guidance Force 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,1),LGGF100.(Lparams{i,1})(:,i+1),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('General Guidance Force 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end

        %assistance level
        subplot(2,3,4,ha(4))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,i+1),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,i+1),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Initial assistance < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,1),RASIST30.(Rparams{i,1})(:,i+1),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Initial assistance < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,1),LASIST30.(Lparams{i,1})(:,i+1),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Initial assistance < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,5,ha(5))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,i+1),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,i+1),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Initial assistance 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,1),RASIST60.(Rparams{i,1})(:,i+1),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Initial assistance 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,1),LASIST60.(Lparams{i,1})(:,i+1),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Initial assistance 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,6,ha(6))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,i+1),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,i+1),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Initial assistance 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,1),RASIST100.(Rparams{i,1})(:,i+1),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Initial assistance 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,1),LASIST100.(Lparams{i,1})(:,i+1),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Initial assistance 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
        end
        sgtitle(t{i,1}) %title over all the sublots
        [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
    end
end

 %% increments in variables vs increments in asistance   
 for i=1:length(Rparams)
    if i==3 %step length (initial final)   
        figure
        colormap(jet);
        [ha,pos]=tight_subplot(4,3,[.01 .03],[0.1 .01],[.01 .01]);
        %hip abduction
        subplot(4,3,1,ha(1))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,4),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,4),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           hold off
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'General Guidance Force < 30%';'Initial Step Length'})
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,4),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title({'General Guidance Force < 30%';'Initial Step Length'})
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,4),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           set(gca,'XTick',[]);
           ylabel('\Delta Step Height (rad)','FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'General Guidance Force < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,2,ha(2))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,4),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,4),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,4),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
           set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,4),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           set(gca,'XTick',[]);
           title({'General Guidance Force 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,3,ha(3))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,11),RGGF100.(Rparams{i,1})(:,4),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,4),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,11),RGGF100.(Rparams{i,1})(:,4),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
           set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,4),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           set(gca,'XTick',[]);
           title({'General Guidance Force 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %terminal sl
        subplot(4,3,4,ha(4))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,5),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,5),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,5),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,5),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        end
        subplot(4,3,5,ha(5))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,5),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,5),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,5),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,5),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,6,ha(6))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,11),RGGF100.(Rparams{i,1})(:,5),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,5),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,1),RGGF100.(Rparams{i,1})(:,5),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,5),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %assistance level
        subplot(4,3,7,ha(7))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,4),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,4),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,4),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,4),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title({'Initial assistance < 30%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,8,ha(8))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,4),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,4),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,4),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,4),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           set(gca,'XTick',[]);
           title({'Initial assistance 30-60%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,9,ha(9))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,4),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,4),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,4),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,4),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           set(gca,'XTick',[]);
           title({'Initial assistance 60-100%';'Initial Step Length'})% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        %terminal sl
        subplot(4,3,10,ha(10))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,5),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,5),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,5),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('PZ')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,5),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,11,ha(11))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,5),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,5),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,5),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,5),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(4,3,12,ha(12))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,5),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,5),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,5),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,5),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Terminal Step Length')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.919704861241496 0.103632478170733 0.0138888888888888 0.120726497585295]);
        end
        sgtitle(t{i,1}) %title over all the sublots
        [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
        
    elseif i==4 %prepositioning
            figure
            colormap(jet);
            [ha,pos]=tight_subplot(2,3,[.01 .03],[0.1 .01],[.01 .01]);
            %GGF
            subplot(2,3,1,ha(1))
            if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
               scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,7),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,7),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('General Guidance Force < 30%')
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
               scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,7),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
               title('General Guidance Force < 30%')
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
               scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,7),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('General Guidance Force < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,2,ha(2))
            if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
               scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,7),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,7),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
               scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,7),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
               scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,7),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,3,ha(3))
            if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
               scatter(RGGF100.(Rparams{i,1})(:,11),RGGF100.(Rparams{i,1})(:,7),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,7),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
               scatter(RGGF100.(Rparams{i,1})(:,11),RGGF100.(Rparams{i,1})(:,7),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
               scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,7),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end

            %assistance level
            subplot(2,3,4,ha(4))
            if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
               scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,7),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,7),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
               scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,7),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
               scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,7),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,5,ha(5))
            if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
               scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,7),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,7),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
               scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,7),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
               scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,7),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,6,ha(6))
            if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
               scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,7),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,7),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
               scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,7),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
               scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,7),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            end
            sgtitle(t{i,1}) %title over all the sublots
            [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
        elseif i==5 %weight shift
            figure
            colormap(jet);
            [ha,pos]=tight_subplot(2,3,[.01 .03],[0.1 .01],[.01 .01]);
            %GGF
            subplot(2,3,1,ha(1))
            if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
               scatter(RGGF30.(Rparams{i,1})(:,12),RGGF30.(Rparams{i,1})(:,8),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF30.(Lparams{i,1})(:,12),LGGF30.(Lparams{i,1})(:,8),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('General Guidance Force < 30%')
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
               scatter(RGGF30.(Rparams{i,1})(:,12),RGGF30.(Rparams{i,1})(:,8),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
               title('General Guidance Force < 30%')
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
               scatter(LGGF30.(Lparams{i,1})(:,12),LGGF30.(Lparams{i,1})(:,8),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('General Guidance Force < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,2,ha(2))
            if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
               scatter(RGGF60.(Rparams{i,1})(:,12),RGGF60.(Rparams{i,1})(:,8),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF60.(Lparams{i,1})(:,12),LGGF60.(Lparams{i,1})(:,8),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
               scatter(RGGF60.(Rparams{i,1})(:,12),RGGF60.(Rparams{i,1})(:,8),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
               scatter(LGGF60.(Lparams{i,1})(:,12),LGGF60.(Lparams{i,1})(:,8),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               title('General Guidance Force 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,3,ha(3))
            if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
               scatter(RGGF100.(Rparams{i,1})(:,12),RGGF100.(Rparams{i,1})(:,8),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
               hold on
               scatter(LGGF100.(Lparams{i,1})(:,12),LGGF100.(Lparams{i,1})(:,8),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
               scatter(RGGF100.(Rparams{i,1})(:,12),RGGF100.(Rparams{i,1})(:,8),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
               scatter(LGGF100.(Lparams{i,1})(:,12),LGGF100.(Lparams{i,1})(:,8),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
               %set(gca,'XTick',[]);
               title('General Guidance Force 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end

            %assistance level
            subplot(2,3,4,ha(4))
            if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
               scatter(RASIST30.(Rparams{i,1})(:,12),RASIST30.(Rparams{i,1})(:,8),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST30.(Lparams{i,1})(:,12),LASIST30.(Lparams{i,1})(:,8),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
               scatter(RASIST30.(Rparams{i,1})(:,12),RASIST30.(Rparams{i,1})(:,8),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
               scatter(LASIST30.(Lparams{i,1})(:,12),LASIST30.(Lparams{i,1})(:,8),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
               %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
               title('Initial assistance < 30%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,5,ha(5))
            if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
               scatter(RASIST60.(Rparams{i,1})(:,12),RASIST60.(Rparams{i,1})(:,8),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST60.(Lparams{i,1})(:,12),LASIST60.(Lparams{i,1})(:,8),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
               scatter(RASIST60.(Rparams{i,1})(:,12),RASIST60.(Rparams{i,1})(:,8),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
               scatter(LASIST60.(Lparams{i,1})(:,12),LASIST60.(Lparams{i,1})(:,8),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               title('Initial assistance 30-60%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            end
            subplot(2,3,6,ha(6))
            if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
               scatter(RASIST100.(Rparams{i,1})(:,12),RASIST100.(Rparams{i,1})(:,8),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
               hold on
               scatter(LASIST100.(Lparams{i,1})(:,12),LASIST100.(Lparams{i,1})(:,8),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
               hold off
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
               scatter(RASIST100.(Rparams{i,1})(:,12),RASIST100.(Rparams{i,1})(:,8),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
               scatter(LASIST100.(Lparams{i,1})(:,12),LASIST100.(Lparams{i,1})(:,8),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
               %set(gca,'XTick',[]);
               title('Initial assistance 60-100%')% insert 'titles' of the columns
            %    xlim(interval)
            %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
            colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
            end
            sgtitle(t{i,1}) %title over all the sublots
            [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
    else
        figure
        colormap(jet);
        [ha,pos]=tight_subplot(2,3,[.01 .03],[0.1 .01],[.01 .01]);
        %GGF
        subplot(2,3,1,ha(1))
        if isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==1
           scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,i+1),30,RGGF30.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,i+1),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('General Guidance Force < 30%')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RGGF30.(Rparams{i,1})(:,11),RGGF30.(Rparams{i,1})(:,i+1),30,RGGF30.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position','VerticalAlignment','middle'); %positioning of the y labels
           title('General Guidance Force < 30%')
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF30,Rparams{i,1})==0 && isfield(LGGF30,Lparams{i,1})==1
           scatter(LGGF30.(Lparams{i,1})(:,11),LGGF30.(Lparams{i,1})(:,i+1),30,LGGF30.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('General Guidance Force < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,2,ha(2))
        if isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==1
           scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,i+1),30,RGGF60.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,i+1),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('General Guidance Force 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==1 && isfield(LGGF60,Lparams{i,1})==0
           scatter(RGGF60.(Rparams{i,1})(:,11),RGGF60.(Rparams{i,1})(:,i+1),30,RGGF60.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('General Guidance Force 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF60,Rparams{i,1})==0 && isfield(LGGF60,Lparams{i,1})==1
           scatter(LGGF60.(Lparams{i,1})(:,11),LGGF60.(Lparams{i,1})(:,i+1),30,LGGF60.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('General Guidance Force 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,3,ha(3))
        if isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==1
           scatter(RGGF100.(Rparams{i,1})(:,11),RGGF100.(Rparams{i,1})(:,i+1),30,RGGF100.(Rparams{i,1})(:,10),'d','filled')
           hold on
           scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,i+1),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('General Guidance Force 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==1 && isfield(LGGF100,Lparams{i,1})==0
           scatter(RGGF100.(Rparams{i,1})(:,11),RGGF100.(Rparams{i,1})(:,i+1),30,RGGF100.(Rparams{i,1})(:,10),'d','filled') 
           %set(gca,'XTick',[]);
           title('General Guidance Force 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RGGF100,Rparams{i,1})==0 && isfield(LGGF100,Lparams{i,1})==1
           scatter(LGGF100.(Lparams{i,1})(:,11),LGGF100.(Lparams{i,1})(:,i+1),30,LGGF100.(Lparams{i,1})(:,10),'d','filled')
           %set(gca,'XTick',[]);
           title('General Guidance Force 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end

        %assistance level
        subplot(2,3,4,ha(4))
        if isfield(RASIST30,Rparams{i,1})==1 && isfield(LASIST30,Lparams{i,1})==1
           scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,i+1),30,RASIST30.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,i+1),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Initial assistance < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==1 && isfield(LGGF30,Lparams{i,1})==0
           scatter(RASIST30.(Rparams{i,1})(:,11),RASIST30.(Rparams{i,1})(:,i+1),30,RASIST30.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Initial assistance < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST30,Rparams{i,1})==0 && isfield(LASIST30,Lparams{i,1})==1
           scatter(LASIST30.(Lparams{i,1})(:,11),LASIST30.(Lparams{i,1})(:,i+1),30,LASIST30.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           ylabel({ylabels{i,1};ylabels{i,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
           %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
           title('Initial assistance < 30%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,5,ha(5))
        if isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==1
           scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,i+1),30,RASIST60.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,i+1),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Initial assistance 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==1 && isfield(LASIST60,Lparams{i,1})==0
           scatter(RASIST60.(Rparams{i,1})(:,11),RASIST60.(Rparams{i,1})(:,i+1),30,RASIST60.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Initial assistance 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        elseif isfield(RASIST60,Rparams{i,1})==0 && isfield(LASIST60,Lparams{i,1})==1
           scatter(LASIST60.(Lparams{i,1})(:,11),LASIST60.(Lparams{i,1})(:,i+1),30,LASIST60.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Initial assistance 30-60%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        end
        subplot(2,3,6,ha(6))
        if isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==1
           scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,i+1),30,RASIST100.(Rparams{i,1})(:,9),'d','filled')
           hold on
           scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,i+1),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           hold off
           %set(gca,'XTick',[]);
           title('Initial assistance 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
        elseif isfield(RASIST100,Rparams{i,1})==1 && isfield(LASIST100,Lparams{i,1})==0
           scatter(RASIST100.(Rparams{i,1})(:,11),RASIST100.(Rparams{i,1})(:,i+1),30,RASIST100.(Rparams{i,1})(:,9),'d','filled') 
           %set(gca,'XTick',[]);
           title('Initial assistance 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
        elseif isfield(RASIST100,Rparams{i,1})==0 && isfield(LASIST100,Lparams{i,1})==1
           scatter(LASIST100.(Lparams{i,1})(:,11),LASIST100.(Lparams{i,1})(:,i+1),30,LASIST100.(Lparams{i,1})(:,9),'d','filled')
           %set(gca,'XTick',[]);
           title('Initial assistance 60-100%')% insert 'titles' of the columns
        %    xlim(interval)
        %    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
        colorbar('Position',[0.922309027870909 0.104700852460819 0.0138888888888888 0.330128209827888]);
        end
        sgtitle(t{i,1}) %title over all the sublots
        [ax1,h1]=suplabel('\Delta Work (J)','x'); %legend over all subplots x axis
    end
end
