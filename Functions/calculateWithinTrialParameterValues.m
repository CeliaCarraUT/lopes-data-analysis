function [X,Y,Z] = calculateWithinTrialParameterValues(DataSet,ParamSet,Parameter)
%This function extracts the different within trial values a parameter was
%set to (during the trial)
%The outputs are: 
%X: vector containing the values at which the parameter was set.
%Y: vector containing the change instances of the parameter.
%Z: The number of the step where the change happened.

%1-check if the parameter changed value throughout the trial
A=diff(ParamSet.data_.(Parameter)(:,1));
if isempty(A)==1 %means there is only one value for that parameter in the whole trial
    B=ParamSet.data_.(Parameter)(1,2);
    values=B;
    values2=0;
    Z=0;
else  %more than one value for the parameter
    B=find(A>20); %find the values that last for longer than 20 seconds
    for k=1:length(B) 
       values(k,1)=ParamSet.data_.(Parameter)(B(k),2); %store the values in a vector 
       values2(k,1)=ParamSet.data_.(Parameter)(B(k),1);
    end
    if (length(ParamSet.data.(Parameter)/100+DataSet.t_start)-ParamSet.data_.(Parameter)(end,1))>20 && values(k)~=ParamSet.data_.(Parameter)(end,2) % if the last value of the data set is kept for more than 20 sec
       values(k+1,1)=ParamSet.data_.(Parameter)(end,2);
       values2(k+1,1)=ParamSet.data_.(Parameter)(end,1);
    end
end


if values2(1)==0
    values2(1)=ParamSet.data.time(1);
end

%determine the step number where the parameter was changed
% for i=1:length(values2)
%      Z(i)=find(ParamSet.data.time==values2(i));
% end


for i=1:length(values2)
  v =DataSet.data.time;
  vidx = floor(interp1(v, 1:length(v), values2(i), 'linear', 'extrap'));
   if vidx ~= 0
       if vidx<length(v)
        result =DataSet.data.time(vidx);
       elseif vidx>length(v)
        result=DataSet.data.time(end);
       end
   end
  inst(i)=find(DataSet.data.time==result);
  W(i,1)=DataSet.data.m_cont_stepCounter(inst(i));
  clear result
end

X=values;
Y=values2;

Z=W; %changed step
%the change step calculate is divided by 2 bc the calculations of the variables are for each leg and not in total
%also, split into steps function divides the data into strides (2steps one
%R and one L)so its also good for selecting the appropriate stride.
Z=W./2;

for i=1:length(Z)
    Z(i)=floor(Z(i));
end

end
