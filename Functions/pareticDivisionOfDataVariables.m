function [Rparetic,Rnonparetic,Lparetic,Lnonparetic]=pareticDivisionOfDataVariables(plottingmatrix)
%This function divides the data contained in the plottingmatrix into 4
%separate structures taking into account the paretic side of the patient.
%Input:
%plottingmatrix-matrix containing the increments in parameters and increments in variables
%Outputs:
%Rparetic,Rnonparetic-structures contaning the data from patients with
%paretic R side (Rparetic contains the data of the R parameters and R subtasks and Rnonparetic the L parameters and L subtasaks).
%Lparetic,Lnonparetic-structures contaning the data from patients with paretic L side (Lparetic contains the data of the L parameters and L subtasks and Lnonparetic the R parameters and R subtasaks).

fields=fieldnames(plottingmatrix);
L=0;
R=0;
Lcounter=0;
Rcounter=0;
for i=1:length(fields)
    for j=1:size(plottingmatrix.(fields{i,1}),1)
        if plottingmatrix.(fields{i,1})(j,19)==5|| plottingmatrix.(fields{i,1})(j,19)==6 || plottingmatrix.(fields{i,1})(j,19)==7 || plottingmatrix.(fields{i,1})(j,19)==10 || plottingmatrix.(fields{i,1})(j,19)==12 || plottingmatrix.(fields{i,1})(j,19)==14%left is paretic leg 
            Lcounter=Lcounter+1;
            L=L+1;
            Lparetic.(fields{i,1})(Lcounter,1)=plottingmatrix.(fields{i,1})(j,1); %initial param value
            Lparetic.(fields{i,1})(Lcounter,2)=plottingmatrix.(fields{i,1})(j,2); %final param value
            Lparetic.(fields{i,1})(Lcounter,3)=plottingmatrix.(fields{i,1})(j,3); %increment
            Lparetic.(fields{i,1})(Lcounter,4)=plottingmatrix.(fields{i,1})(j,6); %step height
            Lparetic.(fields{i,1})(Lcounter,5)=plottingmatrix.(fields{i,1})(j,12); %stability
            Lparetic.(fields{i,1})(Lcounter,6)=plottingmatrix.(fields{i,1})(j,8); %initial step length
            Lparetic.(fields{i,1})(Lcounter,7)=plottingmatrix.(fields{i,1})(j,10); %terminal step length
            Lparetic.(fields{i,1})(Lcounter,8)=plottingmatrix.(fields{i,1})(j,14); %lateral foot pos
            Lparetic.(fields{i,1})(Lcounter,9)=plottingmatrix.(fields{i,1})(j,16); %prepositioning
            Lparetic.(fields{i,1})(Lcounter,10)=plottingmatrix.(fields{i,1})(j,17); %weightshift
            Lparetic.(fields{i,1})(Lcounter,11)=plottingmatrix.(fields{i,1})(j,19);%patient #
            Lparetic.(fields{i,1})(Lcounter,12)=plottingmatrix.(fields{i,1})(j,20);%colormap vector
            Lparetic.(fields{i,1})(Lcounter,13)=plottingmatrix.(fields{i,1})(j,18);%trial n
            Lparetic.(fields{i,1})(Lcounter,14)=plottingmatrix.(fields{i,1})(j,21);%step number
            %Lparetic right variables
            Lnonparetic.(fields{i,1})(Lcounter,1)=plottingmatrix.(fields{i,1})(j,1);
            Lnonparetic.(fields{i,1})(Lcounter,2)=plottingmatrix.(fields{i,1})(j,2);
            Lnonparetic.(fields{i,1})(Lcounter,3)=plottingmatrix.(fields{i,1})(j,3);
            Lnonparetic.(fields{i,1})(Lcounter,4)=plottingmatrix.(fields{i,1})(j,5);
            Lnonparetic.(fields{i,1})(Lcounter,5)=plottingmatrix.(fields{i,1})(j,11);%stability
            Lnonparetic.(fields{i,1})(Lcounter,6)=plottingmatrix.(fields{i,1})(j,7);
            Lnonparetic.(fields{i,1})(Lcounter,7)=plottingmatrix.(fields{i,1})(j,9);
            Lnonparetic.(fields{i,1})(Lcounter,8)=plottingmatrix.(fields{i,1})(j,13);%lfpos
            Lnonparetic.(fields{i,1})(Lcounter,9)=plottingmatrix.(fields{i,1})(j,15);
            Lnonparetic.(fields{i,1})(Lcounter,10)=plottingmatrix.(fields{i,1})(j,17);%wshift
            Lnonparetic.(fields{i,1})(Lcounter,11)=plottingmatrix.(fields{i,1})(j,19);%patient 
            Lnonparetic.(fields{i,1})(Lcounter,12)=plottingmatrix.(fields{i,1})(j,20);%colormap vector
            Lnonparetic.(fields{i,1})(Lcounter,13)=plottingmatrix.(fields{i,1})(j,18);%trial
            Lnonparetic.(fields{i,1})(Lcounter,14)=plottingmatrix.(fields{i,1})(j,21); %step number
        elseif plottingmatrix.(fields{i,1})(j,19)==1 || plottingmatrix.(fields{i,1})(j,19)==2 || plottingmatrix.(fields{i,1})(j,19)==3 || plottingmatrix.(fields{i,1})(j,19)==4 || plottingmatrix.(fields{i,1})(j,19)==8 || plottingmatrix.(fields{i,1})(j,19)==9 || plottingmatrix.(fields{i,1})(j,19)==11 ||plottingmatrix.(fields{i,1})(j,19)==13%the other way round;paretic right, non paretic left
            Rcounter=Rcounter+1;
            R=R+1;
            Rparetic.(fields{i,1})(Rcounter,1)=plottingmatrix.(fields{i,1})(j,1); %initial param value
            Rparetic.(fields{i,1})(Rcounter,2)=plottingmatrix.(fields{i,1})(j,2);%final param value
            Rparetic.(fields{i,1})(Rcounter,3)=plottingmatrix.(fields{i,1})(j,3); %increment param
            Rparetic.(fields{i,1})(Rcounter,4)=plottingmatrix.(fields{i,1})(j,5); %step height
            Rparetic.(fields{i,1})(Rcounter,5)=plottingmatrix.(fields{i,1})(j,11); %stability
            Rparetic.(fields{i,1})(Rcounter,6)=plottingmatrix.(fields{i,1})(j,7); %in. step length
            Rparetic.(fields{i,1})(Rcounter,7)=plottingmatrix.(fields{i,1})(j,9); %ter. step length
            Rparetic.(fields{i,1})(Rcounter,8)=plottingmatrix.(fields{i,1})(j,13); %lat. f.pos
            Rparetic.(fields{i,1})(Rcounter,9)=plottingmatrix.(fields{i,1})(j,15);% prep
            Rparetic.(fields{i,1})(Rcounter,10)=plottingmatrix.(fields{i,1})(j,17);%wshift
            Rparetic.(fields{i,1})(Rcounter,11)=plottingmatrix.(fields{i,1})(j,19); %patient n
            Rparetic.(fields{i,1})(Rcounter,12)=plottingmatrix.(fields{i,1})(j,20); %colormap (GGF)
            Rparetic.(fields{i,1})(Rcounter,13)=plottingmatrix.(fields{i,1})(j,18); %trial n
            Rparetic.(fields{i,1})(Rcounter,14)=plottingmatrix.(fields{i,1})(j,21);
            %Rparetic left values
            Rnonparetic.(fields{i,1})(Rcounter,1)=plottingmatrix.(fields{i,1})(j,1); 
            Rnonparetic.(fields{i,1})(Rcounter,2)=plottingmatrix.(fields{i,1})(j,2); 
            Rnonparetic.(fields{i,1})(Rcounter,3)=plottingmatrix.(fields{i,1})(j,3); 
            Rnonparetic.(fields{i,1})(Rcounter,4)=plottingmatrix.(fields{i,1})(j,6);
            Rnonparetic.(fields{i,1})(Rcounter,5)=plottingmatrix.(fields{i,1})(j,12);%stab 
            Rnonparetic.(fields{i,1})(Rcounter,6)=plottingmatrix.(fields{i,1})(j,8); 
            Rnonparetic.(fields{i,1})(Rcounter,7)=plottingmatrix.(fields{i,1})(j,10); 
            Rnonparetic.(fields{i,1})(Rcounter,8)=plottingmatrix.(fields{i,1})(j,14);
            Rnonparetic.(fields{i,1})(Rcounter,9)=plottingmatrix.(fields{i,1})(j,15);
            Rnonparetic.(fields{i,1})(Rcounter,10)=plottingmatrix.(fields{i,1})(j,17);%wshift
            Rnonparetic.(fields{i,1})(Rcounter,11)=plottingmatrix.(fields{i,1})(j,19);
            Rnonparetic.(fields{i,1})(Rcounter,12)=plottingmatrix.(fields{i,1})(j,20);
            Rnonparetic.(fields{i,1})(Rcounter,13)=plottingmatrix.(fields{i,1})(j,18);
            Rnonparetic.(fields{i,1})(Rcounter,14)=plottingmatrix.(fields{i,1})(j,21);
        end
    end
    Rcounter=0;
    Lcounter=0;
end

if R>0
fieldsRparetic=fieldnames(Rparetic);
    for i=1:length(fieldsRparetic)
      Rparetic.(fieldsRparetic{i,1})=Rparetic.(fieldsRparetic{i,1})(all(Rparetic.(fieldsRparetic{i,1}),2),:);%remove zero rows
    %    paretic.(fieldsparetic{i,1})=unique(sort(paretic.(fieldsparetic{i,1}),2),'rows'); %remove repeated rows
      Rnonparetic.(fieldsRparetic{i,1})=Rnonparetic.(fieldsRparetic{i,1})(all(Rnonparetic.(fieldsRparetic{i,1}),2),:);%remove zero rows
    %    nonparetic.(fieldsparetic{i,1})=unique(sort(nonparetic.(fieldsparetic{i,1}),2),'rows'); %remove repeated rows
    end
else
    Rparetic=0;
    Rnonparetic=0;
end


if L>0
    fieldsLparetic=fieldnames(Lparetic);
     for i=1:length(fieldsLparetic)
        Lparetic.(fieldsLparetic{i,1})=Lparetic.(fieldsLparetic{i,1})(all(Lparetic.(fieldsLparetic{i,1}),2),:);%remove zero rows
    %    Lparetic.(fieldsLparetic{i,1})=unique(sort(Lparetic.(fieldsLparetic{i,1}),2),'rows'); %remove repeated rows
        Lnonparetic.(fieldsLparetic{i,1})=Lnonparetic.(fieldsLparetic{i,1})(all(Lnonparetic.(fieldsLparetic{i,1}),2),:);%remove zero rows
    %    Lnonparetic.(fieldsLparetic{i,1})=unique(sort(Lnonparetic.(fieldsLparetic{i,1}),2),'rows'); %remove repeated rows
     end
else
    Lparetic=0;
    Lnonparetic=0;
end
