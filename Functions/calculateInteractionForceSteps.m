function Y = calculateInteractionForceSteps(D)
%This function calculates the power of every step in a trial. The mean of the
%power values within one step is calculated. This is stored inside the 'Results' field of the original structure 
%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial with the computed intercation forces by steps


%First we divide the data in steps
X=diff(D.data.m_cont_stepCounter);
Z=find(X==1);
Z=[Z; length(D.data.m_cont_stepCounter)];

%calculate mean interaction force for every step
for j=1:length(Z)-1
        D.Results.ForcesBySteps_PX(j,1)=mean(D.data.m_cont_jointForcesMeasured_PX(Z(j):Z(j+1)));
        D.Results.ForcesBySteps_PZ(j,1)=mean(D.data.m_cont_jointForcesMeasured_PZ(Z(j):Z(j+1)));
        D.Results.ForcesBySteps_LHA(j,1)=mean(D.data.m_cont_jointForcesMeasured_LHA(Z(j):Z(j+1))); %left hip abduction
        D.Results.ForcesBySteps_LHF(j,1)=mean(D.data.m_cont_jointForcesMeasured_LHF(Z(j):Z(j+1))); %left hip flexion
        D.Results.ForcesBySteps_LKF(j,1)=mean(D.data.m_cont_jointForcesMeasured_LKF(Z(j):Z(j+1)));%left knee flexion
        D.Results.ForcesBySteps_RHA(j,1)=mean(D.data.m_cont_jointForcesMeasured_RHA(Z(j):Z(j+1))); %right hip abduction
        D.Results.ForcesBySteps_RHF(j,1)=mean(D.data.m_cont_jointForcesMeasured_RHF(Z(j):Z(j+1))); %right hip flexion
        D.Results.ForcesBySteps_RKF(j,1)=mean(D.data.m_cont_jointForcesMeasured_RKF(Z(j):Z(j+1))); %right knee flexion
end

Y=D;
end