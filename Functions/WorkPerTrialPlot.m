function WorkPerTrialPlot(DataSet,Patients,n)
%Function that outputs the work per trial per joint of a patient:
%Plot with 8 subplots, each corresponding to one
%joint(PX,PZ,LHA,LHF,LKF,RHA,RHF,RKF) with the total, nea=gative and
%positive work of each trial.

trialvector=1:length(DataSet);
wjoints={'Work_PX';'Work_PZ';'Work_LHA';'Work_LHF';'Work_LKF';'Work_RHA';'Work_RHF';'Work_RKF'};
for j=1:length(wjoints)
    for i=1:length(DataSet)
         totworkvector.(wjoints{j,1})(i,1)=DataSet(i).Results.(wjoints{j,1})(1,1);
         negworkvector.(wjoints{j,1})(i,1)=DataSet(i).Results.(wjoints{j,1})(1,2);
         posworkvector.(wjoints{j,1})(i,1)=DataSet(i).Results.(wjoints{j,1})(1,3);
    end
end

figure
[ha,pos]=tight_subplot(3,3,[.01 .03],[.1 .01],[.01 .01]);
sgtitle({'Work per Joint per trial' Patients{n,1}})
subplot(3,3,1,ha(1))%LHA
scatter(trialvector,totworkvector.Work_LHA,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_LHA,30,'r','o')%negative work
hold on
scatter(trialvector,posworkvector.Work_LHA,30,'g','o')%positive work
ylabel('LHA','FontWeight', 'bold')
subplot(3,3,4,ha(4))%LHF
scatter(trialvector,totworkvector.Work_LHF,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_LHF,30,'r','o')%neg
hold on
scatter(trialvector,posworkvector.Work_LHF,30,'g','o')%pos
ylabel('LHF','FontWeight', 'bold')
subplot(3,3,7,ha(7))%LKF
scatter(trialvector,totworkvector.Work_LKF,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_LKF,30,'r','o')%negg
hold on
scatter(trialvector,posworkvector.Work_LKF,30,'g','o')%pos
ylabel('LKF','FontWeight', 'bold')
subplot(3,3,2,ha(2))%RHA
scatter(trialvector,totworkvector.Work_RHA,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_RHA,30,'r','o')%neg
hold on
scatter(trialvector,posworkvector.Work_RHA,30,'g','o')%pos
ylabel('RHA','FontWeight', 'bold')
subplot(3,3,5,ha(5))%RHF
scatter(trialvector,totworkvector.Work_RHF,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_RHF,30,'r','o')%neg
hold on
scatter(trialvector,posworkvector.Work_RHF,30,'g','o')%pos
ylabel('RHF','FontWeight', 'bold')
subplot(3,3,8,ha(8))%RKF
scatter(trialvector,totworkvector.Work_RKF,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_RKF,30,'r','o')%neg
hold on
scatter(trialvector,posworkvector.Work_RKF,30,'g','o')%pos
ylabel('RKF','FontWeight', 'bold')
legend('Total Work','Negative Work','Positive Work')
set(legend,...
    'Position',[0.636414929388298 0.252125883117525 0.0821614595005909 0.0675747877766944])
subplot(3,3,3,ha(3))%PX
scatter(trialvector,totworkvector.Work_PX,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_PX,30,'r','o')%neg
hold on
scatter(trialvector,posworkvector.Work_PX,30,'g','o')%pos
ylabel('PX','FontWeight', 'bold')
subplot(3,3,6,ha(6))%PZ
scatter(trialvector,totworkvector.Work_PZ,30,'b','d')%total work
hold on
scatter(trialvector,negworkvector.Work_PZ,30,'r','o')%neg
hold on
scatter(trialvector,posworkvector.Work_PZ,30,'g','o')%pos
ylabel('PZ','FontWeight', 'bold')
delete(ha(9))
[ax1,h1]=suplabel('Trials','x');
[ax2,h2]=suplabel('Work (J)','y');
