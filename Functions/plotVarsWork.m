function plotVarsWork(Rdatamatrix,Ldatamatrix,m,n)

labels={'Knee','Flexion';'Hip','Flexion';'Hip','Abduction';'Pelvis','x direction';'Pelvis','z direction'};

if m==1 %total
    if n==1
        t={'Changes in TOTAL Work for every joint (paretic side) vs Changes in variables of the paretic side'};
    elseif n==2
        t={'Changes in TOTAL Work for every joint (non-paretic side) vs Changes in variables of the non-paretic side'};
    end
    figure
    colormap(copper);
    %5 columns & 5 rows
    %axesHandles(i) = subplot(2,2,i);
    [ha,pos]=tight_subplot(6,5,[.01 .03],[.1 .01],[.01 .01]);
    for i=1:5
        if i==1
            subplot(6,5,1,ha(1))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
            hold off
            %knee flexion
            %lsline
            set(gca,'XTick',[]);
            ylabel({'Step';'Height'})
            caxis([1 4])
            title({labels{i,1};labels{i,2}})%'Rotation',0)
            subplot(6,5,6,ha(6))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Initial';'Step Length'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,11,ha(11))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,3),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Terminal';'Step Length'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,16,ha(16))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Lateral';'Foot Pos'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,21,ha(21))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Prepositioning'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,26,ha(26))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Weight';'Shift'})
            caxis([1 4])
            %lsline

        else
            subplot(6,5,1+(i-1),ha(1+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
            hold off
            title({labels{i,1};labels{i,2}})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,6+(i-1),ha(6+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,11+(i-1),ha(11+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,16+(i-1),ha(16+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,21+(i-1),ha(21+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,26+(i-1),ha(26+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
            hold off
            set(gca,'YTick',[]);
            caxis([1 4])
            %lsline
        end
    end
    caxis([1 4])
    cbh=colorbar;
    set(cbh,'YTickLabel',{'0','25','50','100'})
    sgtitle(t)
    [ax1,h1]=suplabel('\Delta Work (J)','x');
    [ax2,h2]=suplabel('\Delta subtasks','y');
elseif m==2 %positive
    if n==1
        t={'Changes in Positive Work for every joint (paretic side) vs Changes in variables of the paretic side'};
    elseif n==2
        t={'Changes in Positive Work for every joint (non-paretic side) vs Changes in variables of the non-paretic side'};
    end
    figure
    colormap(summer);
    %5 columns & 5 rows
    %axesHandles(i) = subplot(2,2,i);
    [ha,pos]=tight_subplot(6,5,[.01 .03],[.1 .01],[.01 .01]);
    for i=1:5
        if i==1
            subplot(6,5,1,ha(1))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
            hold off
            %knee flexion
            %lsline
            set(gca,'XTick',[]);
            ylabel({'Step';'Height'})
            caxis([1 4])
            title({labels{i,1};labels{i,2}})%'Rotation',0)
            subplot(6,5,6,ha(6))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Initial';'Step Length'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,11,ha(11))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,3),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Terminal';'Step Length'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,16,ha(16))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Lateral';'Foot Pos'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,21,ha(21))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Prepositioning'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,26,ha(26))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Weight';'Shift'})
            caxis([1 4])
            %lsline

        else
            subplot(6,5,1+(i-1),ha(1+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
            hold off
            title({labels{i,1};labels{i,2}})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,6+(i-1),ha(6+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,11+(i-1),ha(11+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,16+(i-1),ha(16+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,21+(i-1),ha(21+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,26+(i-1),ha(26+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
            hold off
            set(gca,'YTick',[]);
            caxis([1 4])
            %lsline
        end
    end
    caxis([1 4])
    cbh=colorbar;
    set(cbh,'YTickLabel',{'0','25','50','100'})
    sgtitle(t)
    [ax1,h1]=suplabel('\Delta Work (J)','x');
    [ax2,h2]=suplabel('\Delta subtasks','y');
elseif m==3 %negative
    if n==1
        t={'Changes in Negative Work for every joint (paretic side) vs Changes in variables of the paretic side'};
    elseif n==2
        t={'Changes in Negative Work for every joint (non-paretic side) vs Changes in variables of the non-paretic side'};
    end
    figure
    colormap(winter);
    %5 columns & 5 rows
    %axesHandles(i) = subplot(2,2,i);
    [ha,pos]=tight_subplot(6,5,[.01 .03],[.1 .01],[.01 .01]);
    for i=1:5
        if i==1
            subplot(6,5,1,ha(1))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
            hold off
            %knee flexion
            %lsline
            set(gca,'XTick',[]);
            ylabel({'Step';'Height'})
            caxis([1 4])
            title({labels{i,1};labels{i,2}})%'Rotation',0)
            subplot(6,5,6,ha(6))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Initial';'Step Length'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,11,ha(11))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,3),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Terminal';'Step Length'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,16,ha(16))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Lateral';'Foot Pos'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,21,ha(21))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Prepositioning'})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            subplot(6,5,26,ha(26))
            scatter(Rdatamatrix(:,1),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,1),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
            hold off
            ylabel({'Weight';'Shift'})
            caxis([1 4])
            %lsline

        else
            subplot(6,5,1+(i-1),ha(1+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
            hold off
            title({labels{i,1};labels{i,2}})
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,6+(i-1),ha(6+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,11+(i-1),ha(11+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,16+(i-1),ha(16+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,21+(i-1),ha(21+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
            hold off
            caxis([1 4])
            %lsline
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            subplot(6,5,26+(i-1),ha(26+(i-1)))
            scatter(Rdatamatrix(:,i),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
            hold on
            scatter(Ldatamatrix(:,i),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
            hold off
            set(gca,'YTick',[]);
            caxis([1 4])
            %lsline
        end
    end
    caxis([1 4])
    cbh=colorbar;
    set(cbh,'YTickLabel',{'0','25','50','100'})
    sgtitle(t)
    [ax1,h1]=suplabel('\Delta Work (J)','x');
    [ax2,h2]=suplabel('\Delta subtasks','y');
end

% figure
% colormap(copper);
% %5 columns & 5 rows
% %axesHandles(i) = subplot(2,2,i);
% [ha,pos]=tight_subplot(6,5,[.01 .03],[.1 .01],[.01 .01]);
% for i=1:5
%     if i==1
%         subplot(6,5,1,ha(1))
%         scatter(Rdatamatrix(:,1),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,1),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         %knee flexion
%         %lsline
%         set(gca,'XTick',[]);
%         ylabel({'Step';'Height'})
%         caxis([1 4])
%         title({labels{i,1};labels{i,2}})%'Rotation',0)
%         subplot(6,5,6,ha(6))
%         scatter(Rdatamatrix(:,1),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,1),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         ylabel({'Initial';'Step Length'})
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         subplot(6,5,11,ha(11))
%         scatter(Rdatamatrix(:,1),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,3),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         ylabel({'Terminal';'Step Length'})
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         subplot(6,5,16,ha(16))
%         scatter(Rdatamatrix(:,1),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,1),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         ylabel({'Lateral';'Foot Pos'})
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         subplot(6,5,21,ha(21))
%         scatter(Rdatamatrix(:,1),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,1),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         ylabel({'Prepositioning'})
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         subplot(6,5,26,ha(26))
%         scatter(Rdatamatrix(:,1),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,1),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         ylabel({'Weight';'Shift'})
%         caxis([1 4])
%         %lsline
% 
%     else
%         subplot(6,5,1+(i-1),ha(1+(i-1)))
%         scatter(Rdatamatrix(:,i),Rdatamatrix(:,6),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,i),Ldatamatrix(:,6),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         title({labels{i,1};labels{i,2}})
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         set(gca,'YTick',[]);
%         subplot(6,5,6+(i-1),ha(6+(i-1)))
%         scatter(Rdatamatrix(:,i),Rdatamatrix(:,7),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,i),Ldatamatrix(:,7),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         set(gca,'YTick',[]);
%         subplot(6,5,11+(i-1),ha(11+(i-1)))
%         scatter(Rdatamatrix(:,i),Rdatamatrix(:,8),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,i),Ldatamatrix(:,8),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         set(gca,'YTick',[]);
%         subplot(6,5,16+(i-1),ha(16+(i-1)))
%         scatter(Rdatamatrix(:,i),Rdatamatrix(:,9),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,i),Ldatamatrix(:,9),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         set(gca,'YTick',[]);
%         subplot(6,5,21+(i-1),ha(21+(i-1)))
%         scatter(Rdatamatrix(:,i),Rdatamatrix(:,10),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,i),Ldatamatrix(:,10),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         caxis([1 4])
%         %lsline
%         set(gca,'XTick',[]);
%         set(gca,'YTick',[]);
%         subplot(6,5,26+(i-1),ha(26+(i-1)))
%         scatter(Rdatamatrix(:,i),Rdatamatrix(:,11),30,Rdatamatrix(:,12),'d','filled')
%         hold on
%         scatter(Ldatamatrix(:,i),Ldatamatrix(:,11),30,Ldatamatrix(:,12),'d','filled')
%         hold off
%         set(gca,'YTick',[]);
%         caxis([1 4])
%         %lsline
%     end
% end
% caxis([1 4])
% cbh=colorbar;
% set(cbh,'YTickLabel',{'0','25','50','100'})
% sgtitle(t)
% [ax1,h1]=suplabel('\Delta Work (J)','x');
% [ax2,h2]=suplabel('\Delta subtasks','y');