function Y = calculateSignalsPower(D)
% Given 'signalsdata' struct D, this function calculates the total power of the
% different joints (PX,PZ,LHF,LHA,LKF,RHF,RHA,RKF)per trial and stores them inside
% the 'Results' field of the given struct

%%% NOTE:Run function 'addResultFieldStruct' before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%% PX
Power_PX=D.data.m_cont_jointForcesMeasured_PX.*gradient(D.data.m_cont_jointAngles_PX,0.01);
D.Results.power_PX.total=Power_PX;
PXneg=Power_PX;
for i=1:length(PXneg)
    if PXneg(i)>0
       PXneg(i)=0;
    end
 end
PXneg=PXneg(PXneg~=0);
D.Results.power_PX.negative=PXneg;
%positive-column3
PXpos=Power_PX;
for i=1:length(PXpos)
    if PXpos(i)<0
       PXpos(i)=0;
    end
 end
PXpos=PXpos(PXpos~=0); 
D.Results.power_PX.positive=PXpos;
%% PZ
Power_PZ=D.data.m_cont_jointForcesMeasured_PZ.*gradient(D.data.m_cont_jointAngles_PZ,0.01);
D.Results.power_PZ.total=Power_PZ;
PZneg=Power_PZ;
   for i=1:length(PZneg)
       if PZneg(i)>0
           PZneg(i)=0;
       end
   end
   PZneg=PZneg(PZneg~=0);
   D.Results.power_PZ.neg=PZneg;
   %positive
   PZpos=Power_PZ;
   for i=1:length(PZpos)
       if PZpos(i)<0
           PZpos(i)=0;
       end
   end
   PZpos=PZpos(PZpos~=0); 
   D.Results.power_PZ.pos=PZpos;

%% LHA
Power_LHA=D.data.m_cont_jointForcesMeasured_LHA.*gradient(D.data.m_cont_jointAngles_LHA,0.01);
D.Results.power_LHA.total=Power_LHA;
LHAneg=Power_LHA;
   for i=1:length(LHAneg)
       if LHAneg(i)>0
           LHAneg(i)=0;
       end
   end
   LHAneg=LHAneg(LHAneg~=0);
   D.Results.power_LHA.neg=LHAneg;
   %positive
   LHApos=Power_LHA;
   for i=1:length(LHApos)
       if LHApos(i)<0
           LHApos(i)=0;
       end
   end
   LHApos=LHApos(LHApos~=0); 
   D.Results.power_LHA.pos=LHApos;

%% LHF
Power_LHF=D.data.m_cont_jointForcesMeasured_LHF.*gradient(D.data.m_cont_jointAngles_LHF,0.01);
D.Results.power_LHF.total=Power_LHF;
LHFneg=Power_LHF;
   for i=1:length(LHFneg)
       if LHFneg(i)>0
           LHFneg(i)=0;
       end
   end
   LHFneg=LHFneg(LHFneg~=0);
   D.Results.power_LHF.neg=LHFneg;
   %positive
   LHFpos=Power_LHF;
   for i=1:length(LHFpos)
       if LHFpos(i)<0
           LHFpos(i)=0;
       end
   end
   LHFpos=LHFpos(LHFpos~=0); 
   D.Results.power_LHF.pos=LHFpos;

%% LKF
Power_LKF=D.data.m_cont_jointForcesMeasured_LKF.*gradient(D.data.m_cont_jointAngles_LKF,0.01);
D.Results.power_LKF.total=Power_LKF;
LKFneg=Power_LKF;
   for i=1:length(LKFneg)
       if LKFneg(i)>0
           LKFneg(i)=0;
       end
   end
   LKFneg=LKFneg(LKFneg~=0);
   D.Results.power_LKF.neg=LKFneg;
   %positive
   LKFpos=Power_LKF;
   for i=1:length(LKFpos)
       if LKFpos(i)<0
           LKFpos(i)=0;
       end
   end
   LKFpos=LKFpos(LKFpos~=0); 
   D.Results.power_LKF.pos=LKFpos;

%% RHA
Power_RHA=D.data.m_cont_jointForcesMeasured_RHA.*gradient(D.data.m_cont_jointAngles_RHA,0.01);
D.Results.power_RHA.total=Power_RHA;
%negative
   RHAneg=Power_RHA;
   for i=1:length(RHAneg)
       if RHAneg(i)>0
           RHAneg(i)=0;
       end
   end
   RHAneg=RHAneg(RHAneg~=0);
   D.Results.power_RHA.neg=RHAneg;
   %positive
   RHApos=Power_RHA;
   for i=1:length(RHApos)
       if RHApos(i)<0
           RHApos(i)=0;
       end
   end
   RHApos=RHApos(RHApos~=0); 
   D.Results.power_RHA.neg=RHApos;
   
%% RHF
Power_RHF=D.data.m_cont_jointForcesMeasured_RHF.*gradient(D.data.m_cont_jointAngles_RHF,0.01);
D.Results.power_RHF.total=Power_RHF;
RHFneg=Power_RHF;
   for i=1:length(RHFneg)
       if RHFneg(i)>0
           RHFneg(i)=0;
       end
   end
   RHFneg=RHFneg(RHFneg~=0);
   D.Results.power_RHF.neg=RHFneg;
   %positive
   RHFpos=Power_RHF;
   for i=1:length(RHFpos)
       if RHFpos(i)<0
           RHFpos(i)=0;
       end
   end
   RHFpos=RHFpos(RHFpos~=0); 
   D.Results.power_RHF.pos=RHFpos;

%% RKF
Power_RKF=D.data.m_cont_jointForcesMeasured_RKF.*gradient(D.data.m_cont_jointAngles_RKF,0.01); 
D.Results.power_RKF.total=Power_RKF;
RKFneg=Power_RKF;
   for i=1:length(RKFneg)
       if RKFneg(i)>0
           RKFneg(i)=0;
       end
   end
   RKFneg=RKFneg(RKFneg~=0);
   D.Results.power_RKF.neg=RKFneg;
   %positive
   RKFpos=Power_RKF;
   for i=1:length(RKFpos)
       if RKFpos(i)<0
           RKFpos(i)=0;
       end
   end
   RKFpos=RKFpos(RKFpos~=0); 
   D.Results.power_RKF.pos=RKFpos;  

Y=D;
end