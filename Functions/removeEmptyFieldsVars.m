function [Y]=removeEmptyFieldsVars(S)
%This function removes the empty fields from the variable storing
%structures (variables, forces and power).

%Input: S-inital storing structure 
%Output; Y-storing structure without the empty fields

f=fieldnames(S);
        for i=1:length(f)
            fieldnam=fieldnames(S.(f{i,1}));
            for j=1:length(fieldnam)
                    if all(structfun(@isempty, S.(f{i,1}).(fieldnam{j,1})))==1
                        S.(f{i,1})= rmfield(S.(f{i,1}),(fieldnam{j,1}));
                    end
            end
        end
    %  (second loop takes the 'trial'fields that are totally empty)
        for i=1:length(f)
            if all(structfun(@isempty, S.(f{i,1})))==1
               S=rmfield(S,(f{i,1}));
            end
        end
        
   Y=S;