function [Y]=combineWorkVariableStructures2(VarStruct,WorkStruct)
%This function combines %This function combines the FINAL outputs of both the work and variables
%taking into account he changed parameters.
%Inputs:
%VarStruct-Variable structure containing fields corresponding to the
%changed parameters, and inside the variable increments for those changes
%in parameters
%WorkStruct-Work structure containing fields corresponding to the
%changed parameters, and inside the work increments for every joint for those changes
%in parameters
%Outputs:
%Y-structure containing the 'combined' increment information
combinedStruct=[];
fields=fieldnames(VarStruct);
 for i=1:length(fields)
     if strcmp(fields{i,1},'RightStepHeight_GuidanceForce_pct')==1 || strcmp(fields{i,1},'RightStabilityStance_GuidanceForce_pct')==1 || strcmp(fields{i,1},'RightPrepositioning_GuidanceForce_pct')==1 || strcmp(fields{i,1},'LeftStepHeight_GuidanceForce_pct')==1 ...
            || strcmp(fields{i,1},'LeftStabilityStance_GuidanceForce_pct')==1 || strcmp(fields{i,1},'LeftPrepositioning_GuidanceForce_pct')==1
        n=size(VarStruct.(fields{i,1}),1);
        combinedStruct.(fields{i,1})(1:n,1)=WorkStruct.(fields{i,1})(1:n,8); %knee flexion
        combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,4:10)); %vars
        combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,12)); %GGF level
        combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,1)); %Initial (parameter) assistance level)
        combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,3)); %Increment in parameter
     elseif strcmp(fields{i,1},'RightStepLength_GuidanceForce_pct')==1 || strcmp(fields{i,1},'LeftStepLength_GuidanceForce_pct')==1
         n=size(VarStruct.(fields{i,1}),1);
         combinedStruct.(fields{i,1})(1:n,1)=WorkStruct.(fields{i,1})(1:n,7); %hip flexion
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,4:10)); %vars
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,12)); %GGF level
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,1)); %Initial (parameter) assistance level)
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,3)); %Increment in parameter
     elseif strcmp(fields{i,1},'WeightShift_GuidanceForce_pct')==1
         n=size(VarStruct.(fields{i,1}),1);
         combinedStruct.(fields{i,1})(1:n,1)=WorkStruct.(fields{i,1})(1:n,6); %hip abduction
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,4:10)); %vars
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,12)); %GGF level
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),WorkStruct.(fields{i,1})(1:n,5)); %pz
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,1)); %Initial (parameter) assistance level)
         combinedStruct.(fields{i,1})=horzcat(combinedStruct.(fields{i,1}),VarStruct.(fields{i,1})(1:n,3)); %Increment in parameter
     end
 end
 
 Y=combinedStruct;