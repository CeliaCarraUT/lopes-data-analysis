function Y = calculateSignalsWork(D)
% Given 'signalsdata' struct D, this function calculates the total work of the
% different joints (PX,PZ,LHF,LHA,LKF,RHF,RHA,RKF) per trial and stores them inside
% the 'Results' field of the given struct(D)
%the first column corresponds to the total work of the trial; the second to
%the negative work and the third to the positive work of the trial

%%% NOTE:Run function 'addResultFieldStruct' before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%normalize the time vector
normtime=(D.data.time-D.data.time(1)); 

%% PX - pelvis x direction
%total power-column1
Power_PX=D.data.m_cont_jointForcesMeasured_PX.*gradient(D.data.m_cont_jointAngles_PX,0.01);
D.Results.Work_PX(1,1)=trapz(normtime,Power_PX); %total
%negative-column2
PXneg=Power_PX;
for i=1:length(PXneg)
    if PXneg(i)>0
       PXneg(i)=0;
    end
end
D.Results.Work_PX(1,2)=trapz(normtime, PXneg);
%positive-column3
PXpos=Power_PX;
for i=1:length(PXpos)
    if PXpos(i)<0
       PXpos(i)=0;
    end
end
D.Results.Work_PX(1,3)=trapz(normtime, PXpos);
   
%% PZ - pelvis z direction
%total 
Power_PZ=D.data.m_cont_jointForcesMeasured_PZ.*gradient(D.data.m_cont_jointAngles_PZ,0.01);
D.Results.Work_PZ(1,1)=trapz(normtime, Power_PZ);
%negative
PZneg=Power_PZ;
   for i=1:length(PZneg)
       if PZneg(i)>0
           PZneg(i)=0;
       end
   end
D.Results.Work_PZ(1,2)=trapz(normtime, PZneg);
   %positive
PZpos=Power_PZ;
for i=1:length(PZpos)
    if PZpos(i)<0
        PZpos(i)=0;
    end
end
D.Results.Work_PZ(1,3)=trapz(normtime, PZpos);
   
%% LHA - left hip abduction
%total
Power_LHA=D.data.m_cont_jointForcesMeasured_LHA.*gradient(D.data.m_cont_jointAngles_LHA,0.01);
D.Results.Work_LHA(1,1)=trapz(normtime, Power_LHA);
%negative
   LHAneg=Power_LHA;
   for i=1:length(LHAneg)
       if LHAneg(i)>0
           LHAneg(i)=0;
       end
   end
   D.Results.Work_LHA(1,2)=trapz(normtime, LHAneg);
   %positive
   LHApos=Power_LHA;
   for i=1:length(LHApos)
       if LHApos(i)<0
           LHApos(i)=0;
       end
   end
   D.Results.Work_LHA(1,3)=trapz(normtime, LHApos);
   
   %% LHF - left hip flexion
   %total
   Power_LHF=D.data.m_cont_jointForcesMeasured_LHF.*gradient(D.data.m_cont_jointAngles_LHF,0.01);
   D.Results.Work_LHF(1,1)=trapz(normtime, Power_LHF);
   %negative
   LHFneg=Power_LHF;
   for i=1:length(LHFneg)
       if LHFneg(i)>0
           LHFneg(i)=0;
       end
   end
   D.Results.Work_LHF(1,2)=trapz(normtime, LHFneg);
   %positive
   LHFpos=Power_LHF;
   for i=1:length(LHFpos)
       if LHFpos(i)<0
           LHFpos(i)=0;
       end
   end
   D.Results.Work_LHF(1,3)=trapz(normtime, LHFpos);
   
   %% LKF - left knee flexion
   Power_LKF=D.data.m_cont_jointForcesMeasured_LKF.*gradient(D.data.m_cont_jointAngles_LKF,0.01);
   D.Results.Work_LKF(1,1)=trapz(normtime, Power_LKF);
   %negative
   LKFneg=Power_LKF;
   for i=1:length(LKFneg)
       if LKFneg(i)>0
           LKFneg(i)=0;
       end
   end
   D.Results.Work_LKF(1,2)=trapz(normtime, LKFneg);
   %positive
   LKFpos=Power_LKF;
   for i=1:length(LKFpos)
       if LKFpos(i)<0
           LKFpos(i)=0;
       end
   end
   D.Results.Work_LKF(1,3)=trapz(normtime, LKFpos);
   
   %% RHA - right hip abduction
   Power_RHA=D.data.m_cont_jointForcesMeasured_RHA.*gradient(D.data.m_cont_jointAngles_RHA,0.01);
   D.Results.Work_RHA(1,1)=trapz(normtime, Power_RHA);
   %negative
   RHAneg=Power_RHA;
   for i=1:length(RHAneg)
       if RHAneg(i)>0
           RHAneg(i)=0;
       end
   end
   D.Results.Work_RHA(1,2)=trapz(normtime, RHAneg);
   %positive
   RHApos=Power_RHA;
   for i=1:length(RHApos)
       if RHApos(i)<0
           RHApos(i)=0;
       end
   end
   D.Results.Work_RHA(1,3)=trapz(normtime, RHApos);
   
   %% RHF - right hip flexion
   Power_RHF=D.data.m_cont_jointForcesMeasured_RHF.*gradient(D.data.m_cont_jointAngles_RHF,0.01);
   D.Results.Work_RHF(1,1)=trapz(normtime, Power_RHF);
   %negative
   RHFneg=Power_RHF;
   for i=1:length(RHFneg)
       if RHFneg(i)>0
           RHFneg(i)=0;
       end
   end
   D.Results.Work_RHF(1,2)=trapz(normtime, RHFneg);
   %positive
   RHFpos=Power_RHF;
   for i=1:length(RHFpos)
       if RHFpos(i)<0
           RHFpos(i)=0;
       end
   end
   D.Results.Work_RHF(1,3)=trapz(normtime, RHFpos);
   
   %% RKF - right knee flexion
   Power_RKF=D.data.m_cont_jointForcesMeasured_RKF.*gradient(D.data.m_cont_jointAngles_RKF,0.01);
   D.Results.Work_RKF(1,1)=trapz(normtime, Power_RKF);
   %negative
   RKFneg=Power_RKF;
   for i=1:length(RKFneg)
       if RKFneg(i)>0
           RKFneg(i)=0;
       end
   end
   D.Results.Work_RKF(1,2)=trapz(normtime, RKFneg);
   %positive
   RKFpos=Power_RKF;
   for i=1:length(RKFpos)
       if RKFpos(i)<0
           RKFpos(i)=0;
       end
   end
   D.Results.Work_RKF(1,3)=trapz(normtime, RKFpos);

Y=D;
end
