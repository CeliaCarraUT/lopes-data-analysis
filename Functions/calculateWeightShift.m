function[Y]= calculateWeightShift(D)
%Obtain weight shift (as the lateral position of the pelvis; maximal and
%minimal-upper and lower bounds)
%This function outputs the weightshift of all the steps in a trial(stored in input struct D) and
%stores them inside the original stucture (in the 'Results' field).

%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%Kinematic phase LOPES:
% 1-Double stance, left in front
% 2-Right swing
% 3-Double stance, right in front
% 4-Left swing
%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
count=0;
%Rsteps/Lsteps provide us with the instances of the data where R/L steps
%happen
steps=[];

for i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==1 || D.data.m_cont_kinematicPhase((A-1)+i)==3
        count=count+1;
        steps(count,1)=(A-1)+i;
    end
end

%steps=sort(steps);

if isempty(steps)==1
    weightshift=0;
    stepInstances=0;
    weightshift=0;
else
    F=diff(steps);
    stepInstances(1)=steps(1);
    count=1;
    %step intances gives us information on when new steps are made
    for i=1:length(F)
        if F(i)~=1
            count=count+1;
            stepInstances(count,1)=steps(i+1);
        end
    end
    stepInstances(count+1,1)=steps(end);
    
    %check if last element is repeated and if so, remove
    if stepInstances(end)==stepInstances(end-1)
        stepInstances(end)=[];
    end

    for i=1:length(stepInstances)-1
        pelvisMmax(i,1)=max(D.data.m_cont_jointAngles_PZ(stepInstances(i):stepInstances(i+1)-1));%-min(D.data.m_cont_jointAngles_PZ(stepInstances(i):stepInstances(i+1)));
        pelvisMmin(i,1)=min(D.data.m_cont_jointAngles_PZ(stepInstances(i):stepInstances(i+1)-1));
        pelvisRmax(i,1)=max(D.data.r_cont_jointAngles_PZ(stepInstances(i):stepInstances(i+1)-1));%-min(D.data.r_cont_jointAngles_PZ(stepInstances(i):stepInstances(i+1)));
        pelvisRmin(i,1)=min(D.data.r_cont_jointAngles_PZ(stepInstances(i):stepInstances(i+1)-1));
        weightshift(i,1)=max(abs(pelvisRmin(i,1)-pelvisMmin(i,1)),abs(pelvisRmax(i,1)-pelvisMmax(i,1)));
    end
end

D.Results.WeightShift=weightshift;

Y=D;
end


