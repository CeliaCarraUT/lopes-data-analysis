function[Y]=addResultFieldStruct(D)
%This function adds an empty field called 'Results'(for storing the
%different computed variables)  to every 'trial' of the signals structure 
for i=1:length(D)
    if i==1
        Y(i)=D(i);
        Y(i).Results=[];
    else
        Z=D(i);
        Z.Results=[];
        Y(i)=Z;
        clear Z
    end
end