function [Y] = calculateStepHeight(D)
%Step height/knee flexion: maximal knee flexion (during swing)
%This function outputs the step height of all the steps in a trial(stored in input struct D) and
%stores them inside the original stucture (in the 'Results' field).

%Both the measured angles (field 'StepHeights')  and the difference with the reference trajectory
%are stored (field 'StepHeightDiff'). Steps are also divided into right and
%left.

%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%kinematic phase:
% 1-Double stance, left in front
% 2-Right swing
% 3-Double stance, right in front
% 4-Left swing

%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
%First we check the kinematic phases present in the section to evaluate and
%select the ones we want
count=0;
count2=0;
Rsteps=[];
Lsteps=[];
for i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==2
       count=count+1;
       Rsteps(count)=(A-1)+i;
    elseif D.data.m_cont_kinematicPhase((A-1)+i)==4
       count2=count2+1;
       Lsteps(count2)=(A-1)+i;
    end
end

if any(D.data.m_cont_kinematicPhase==4)==0
  Lsteps=[];
end

if any(D.data.m_cont_kinematicPhase==2)==0
  Rsteps=[];
end

%differentiate between different steps by making sure que take only consecutive indeces
%Right
    if isempty(Rsteps)==1
        RStepHeights=0;
        RswingInstances=0;
        RmaxKneeAngles=0;
    else
        RD=diff(Rsteps);
        RswingInstances(1)=Rsteps(1);
        count=1;

        for i=1:length(RD)
            if RD(i)~=1 && i~=1
                count=count+1;
                RswingInstances(count,1)=Rsteps(i);
            end
        end
        RswingInstances(count+1,1)=Rsteps(end); %swingInstances contains the indeces at which different steps start

         %find max knee flexion angle
        for i=1:length(RswingInstances)-1
            if length(RswingInstances)==2 
                RmaxKneeAngles=max(D.data.m_cont_jointAngles_RKF);
            else
                if i==1
                    RmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_RKF(RswingInstances(i):RswingInstances(i+1)-1));
                else 
                    RmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_RKF(RswingInstances(i)+1:RswingInstances(i+1)));
                end 
            end
        end
    end
    
    D.Results.StepHeights.Rsteps=RmaxKneeAngles;
    
%Left
 if isempty(Lsteps)==1
        LStepHeights=0;
        LswingInstances=0;
        LmaxKneeAngles=0;
    else
        LD=diff(Lsteps);
        LswingInstances(1)=Lsteps(1);
        count2=1;

        for i=1:length(LD)
            if LD(i)~=1 && i~=1
                count2=count2+1;
                LswingInstances(count2,1)=Lsteps(i);
            end
        end
        LswingInstances(count2+1,1)=Lsteps(end); %swingInstances contains the indeces at which different steps start

    %find max knee flexion angle
        for i=1:length(LswingInstances)-1
            if length(LswingInstances)==2 
                LmaxKneeAngles=max(D.data.m_cont_jointAngles_LKF);
            else
                if i==1
                    LmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_LKF(LswingInstances(i):LswingInstances(i+1)-1));
                else 
                    LmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_LKF(LswingInstances(i)+1:LswingInstances(i+1)));
                end 
            end
        end
 end
 
   D.Results.StepHeights.Lsteps=LmaxKneeAngles;
   
%% check the angle profiles and use find peaks to substract from the reference
%peaks method
% B=find(D.data.r_cont_stepCounter==3,1);
% lastStepr=find(D.data.r_cont_stepCounter==D.data.r_cont_stepCounter(end)-4,1);
% if isempty(lastStepr)==1
%     lastStepr=lastStep;
% end
% 
% [pksm,locsm]=findpeaks(D.data.m_cont_jointAngles_LKF(A:lastStep));
% locsm1=locsm(pksm>0.58);
% pksm1=pksm(pksm>0.58);
% [pksr,locsr]=findpeaks(D.data.r_cont_jointAngles_LKF(B:lastStepr));
% locsr1=locsr(pksr>0.58);
% pksr1=pksr(pksr>0.58);
% 
% [Rpksm,Rlocsm]=findpeaks(D.data.m_cont_jointAngles_RKF(A:lastStep));
% Rlocsm1=Rlocsm(Rpksm>0.58);
% Rpksm1=Rpksm(Rpksm>0.58);
% [Rpksr,Rlocsr]=findpeaks(D.data.r_cont_jointAngles_RKF(B:lastStepr));
% Rlocsr1=Rlocsr(Rpksr>0.58);
% Rpksr1=Rpksr(Rpksr>0.58);
% 
% %check double peaks
% %left
% LDM2=diff(locsm1);
% doublepeaks2=find(LDM2<120);
%     for i=1:length(doublepeaks2)
%         if i==1
%             pksm1(doublepeaks2(i))=[];
%             locsm1(doublepeaks2(i))=[];
%         else
%             pksm1(doublepeaks2(i)-i+1)=[];
%             locsm1(doublepeaks2(i)-i+1)=[];
%         end
%     end
%     
% LDM3=diff(locsr);
% doublepeaks=find(LDM3<120);
% 
%     for i=1:length(doublepeaks)
%         if i==1
%             pksr(doublepeaks(i))=[];
%             locsr(doublepeaks(i))=[];
%         else
%             pksr(doublepeaks(i)-i+1)=[];
%             locsr(doublepeaks(i)-i+1)=[];
%         end
%     end
%     
% %right
% RDM2=diff(Rlocsm1);
% doublepeaks3=find(RDM2<120);
%     for i=1:length(doublepeaks3)
%         if i==1
%             Rpksm1(doublepeaks3(i))=[];
%             Rlocsm1(doublepeaks3(i))=[];
%         else
%             Rpksm1(doublepeaks3(i)-i+1)=[];
%             Rlocsm1(doublepeaks3(i)-i+1)=[];
%         end
%     end
%     
% RDM3=diff(Rlocsr1);
% doublepeaks4=find(RDM3<120);
%     for i=1:length(doublepeaks4)
%         if i==1
%             Rpksr1(doublepeaks4(i))=[];
%             Rlocsr1(doublepeaks4(i))=[];
%         else
%             Rpksr1(doublepeaks4(i)-i+1)=[];
%             Rlocsr1(doublepeaks4(i)-i+1)=[];
%         end
%     end 
% 
%   
% %check the length of the vectors
% if length(pksr1)<length(pksm1)
%     %check the first and last peaks
%     if abs(locsr1(1)-locsm1(1))>150 %first element is an extra peak in measured
%         pksr1=[NaN;pksr1];
%     elseif abs(locsm1(end)-locsr1(end))>150 %last element is an extra peak
%         pksr1(end+1)=NaN;
%     end
% elseif length(pksm1)<length(pksr1)
%         %check the first and last peaks
%     if abs(locsm1(1)-locsr(1))>150 %first element is an extra peak
%         pksm1=[NaN;pksm1];
%     elseif abs(locsm1(end)-locsr1(end))>150
%         pksm1(end+1)=NaN;
%     end
%     
% end
% 
% if length(Rpksr1)<length(Rpksm1)
%     if abs(Rlocsr1(1)-Rlocsm1(1))>150 %first element is an extra peak
%         Rpksr1=[NaN;Rpksr1];
%     elseif abs(Rlocsm1(end)-Rlocsr1(end))>150
%         Rpksr1(end+1)=NaN;
%     end
% elseif length(Rpksm1)<length(Rpksr1)
%         %check the first and last peaks
%     if abs(Rlocsm1(1)-Rlocsr1(1))>150 %first element is an extra peak
%         Rpksm1=[NaN;Rpksm1];
%     elseif abs(Rlocsm1(end)-Rlocsr1(end))>150
%         Rpksm1(end+1)=NaN;
%     end
% end
% 
% 
% 
% 
% %check that the measured is not missing a first step; if so, add NaN value
% if length(Rpksm1)<length(Rpksr1)
%     Rpksm1=[NaN; Rpksm1];
% end
% 
% if length(pksm1)<length(pksr1)
%     pksm1=[NaN;pksm1];
% end
% 
% if length(pksr1)<length(pksm1)
%     %check the first and last peaks
%     if abs(locsr1(1)-locsm1(1))>150 %first element is an extra peak in measured
%         pksr1=[NaN;pksr1];
%     else  %last element is an extra peak
%         pksr1(end+1)=NaN;
%     end
% end
% 
% 
% D.Results.StepHeightDiff.Lsteps=pksr1-pksm1;
% D.Results.StepHeightDiff.Rsteps=Rpksr1-Rpksm1;

%using step counter
%MEASURED
%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
%prepositioning: knee angle just before heel strike

count=0;
count2=0;
Rsteps=[];
Lsteps=[];

%first we select the section of the data we want using the kinematic phase
for i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==2
        count=count+1;
        Rsteps(count,1)=(A-1)+i;
    elseif D.data.m_cont_kinematicPhase((A-1)+i)==4
        count2=count2+1;
        Lsteps(count2,1)=(A-1)+i;
    end
end
clear count count2

%Right
if isempty(Rsteps)==1
    RStepHeights=0;
    RswingInstances=0;
    RmaxKneeAngles=0;
else
    RD=diff(Rsteps);
    RswingInstances(1)=Rsteps(1);
    count=1;

    for i=1:length(RD)
        if RD(i)~=1 && i~=1
            count=count+1;
            RswingInstances(count,1)=Rsteps(i);
        end
    end
    RswingInstances(count+1,1)=Rsteps(end); %swingInstances contains the indeces at which different steps start

     %find max knee flexion angle
    for i=1:length(RswingInstances)-1
        if length(RswingInstances)==2 
            RmaxKneeAngles=max(D.data.m_cont_jointAngles_RKF);
        else
            if i==1
                RmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_RKF(RswingInstances(i):RswingInstances(i+1)-1));
            else 
                RmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_RKF(RswingInstances(i)+1:RswingInstances(i+1)));
            end 
        end
    end
end

RStepHeightsM=RmaxKneeAngles;
    
%Left
 if isempty(Lsteps)==1
        LStepHeights=0;
        LswingInstances=0;
        LmaxKneeAngles=0;
    else
        LD=diff(Lsteps);
        LswingInstances(1)=Lsteps(1);
        count2=1;

        for i=1:length(LD)
            if LD(i)~=1 && i~=1
                count2=count2+1;
                LswingInstances(count2,1)=Lsteps(i);
            end
        end
        LswingInstances(count2+1,1)=Lsteps(end); %swingInstances contains the indeces at which different steps start

    %find max knee flexion angle
        for i=1:length(LswingInstances)-1
            if length(LswingInstances)==2 
                LmaxKneeAngles=max(D.data.m_cont_jointAngles_LKF);
            else
                if i==1
                    LmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_LKF(LswingInstances(i):LswingInstances(i+1)-1));
                else 
                    LmaxKneeAngles(i,1)=max(D.data.m_cont_jointAngles_LKF(LswingInstances(i)+1:LswingInstances(i+1)));
                end 
            end
        end
 end
 
 LStepHeightsM=LmaxKneeAngles;
 clear LswingInstances RswingInstances
 
%REFERENCE
%check the instance of the first step
B=find(D.data.r_cont_stepCounter==3,1);
lastStepr=find(D.data.r_cont_stepCounter==D.data.r_cont_stepCounter(end)-4,1);
%prepositioning: knee angle just before heel strike
if isempty(lastStepr)==1
    lastStepr=lastStep;
end

count=0;
count2=0;
Rsteps=[];
Lsteps=[];

%first we select the section of the data we want using the kinematic phase
for i=1:length(D.data.r_cont_kinematicPhase(B:lastStepr))
    if D.data.r_cont_kinematicPhase((B-1)+i)==2
        count=count+1;
        Rsteps(count,1)=(B-1)+i;
    elseif D.data.r_cont_kinematicPhase((B-1)+i)==4
        count2=count2+1;
        Lsteps(count2,1)=(B-1)+i;
    end
end
clear count count2

%Right
if isempty(Rsteps)==1
    RStepHeightsR=0;
    RswingInstances=0;
    RmaxKneeAngles=0;
else
    RD=diff(Rsteps);
    RswingInstances(1)=Rsteps(1);
    count=1;

    for i=1:length(RD)
        if RD(i)~=1 && i~=1
            count=count+1;
            RswingInstances(count,1)=Rsteps(i);
        end
    end
    RswingInstances(count+1,1)=Rsteps(end); %swingInstances contains the indeces at which different steps start
    RswingInstances=sort(RswingInstances);
     %find max knee flexion angle
    for i=1:length(RswingInstances)-1
        if length(RswingInstances)==2 
            RmaxKneeAnglesR=max(D.data.r_cont_jointAngles_RKF);
        else
            if i==1
                RmaxKneeAnglesR(i,1)=max(D.data.r_cont_jointAngles_RKF(RswingInstances(i):RswingInstances(i+1)-1));
            else 
                RmaxKneeAnglesR(i,1)=max(D.data.r_cont_jointAngles_RKF(RswingInstances(i)+1:RswingInstances(i+1)));
            end 
        end
    end
end

RStepHeightsR=RmaxKneeAnglesR;
    
%Left
 if isempty(Lsteps)==1
        LStepHeightsR=0;
        LswingInstances=0;
        LmaxKneeAngles=0;
    else
        LD=diff(Lsteps);
        LswingInstances(1)=Lsteps(1);
        count2=1;

        for i=1:length(LD)
            if LD(i)~=1 && i~=1
                count2=count2+1;
                LswingInstances(count2,1)=Lsteps(i);
            end
        end
        LswingInstances(count2+1,1)=Lsteps(end); %swingInstances contains the indeces at which different steps start
        LswingInstances=sort(LswingInstances);
    %find max knee flexion angle
        for i=1:length(LswingInstances)-1
            if length(LswingInstances)==2 
                LmaxKneeAnglesR=max(D.data.r_cont_jointAngles_LKF);
            else
                if i==1
                    LmaxKneeAnglesR(i,1)=max(D.data.r_cont_jointAngles_LKF(LswingInstances(i):LswingInstances(i+1)-1));
                else 
                    LmaxKneeAnglesR(i,1)=max(D.data.r_cont_jointAngles_LKF(LswingInstances(i)+1:LswingInstances(i+1)));
                end 
            end
        end
 end
 
 LStepHeightsR=LmaxKneeAnglesR;
 
 if length(LStepHeightsM)<length(LStepHeightsR)
    while length(LStepHeightsM)<length(LStepHeightsR)
     LStepHeightsR(end)=[];
    end
 elseif length(LStepHeightsM)>length(LStepHeightsR)
    while length(LStepHeightsM)>length(LStepHeightsR)
     LStepHeightsM(end)=[];
    end
 end
 
 if length(RStepHeightsM)<length(RStepHeightsR)
    while length(RStepHeightsM)<length(RStepHeightsR)
      RStepHeightsR(end)=[];
    end
 elseif length(RStepHeightsM)>length(RStepHeightsR)
    while length(RStepHeightsM)>length(RStepHeightsR)
     RStepHeightsM(end)=[];
    end
 end
 
 
%compute the difference
D.Results.StepHeightDiff.Lsteps=LStepHeightsR-LStepHeightsM;
D.Results.StepHeightDiff.Rsteps=RStepHeightsR-RStepHeightsM;
 
Y=D;

end
