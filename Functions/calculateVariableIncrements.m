function [Y]=calculateVariableIncrements(VariableValues,vector1,vector2)
%This function calculates the increments in a variable (for one change in
%parameter)
%Inputs:
%VariableValues-vector containing the calculated variable for the whole trial
%Initial&finalSteps-margins on what sections of 'Parameter' to consider
%(steps before and after the parameter change)
%changeStep-step at which the change in parameter occurs
%Outputs:
%Y-VariableIncrement
m=find(vector2>length(VariableValues));
for i=1:length(m)
     if i==1
        vector2(m(i))=[];
     else
        vector2(m(i)-i+1)=[];
    end
end

ValuesBefore=(VariableValues(vector1));
ValuesAfter=(VariableValues(vector2));

%remove outliers
ValuesBefore=RemoveOutliersData(ValuesBefore);
ValuesAfter=RemoveOutliersData(ValuesAfter);

%do the mean
mBefore=nanmean(ValuesBefore);
mAfter=nanmean(ValuesAfter);

%compute the increment
Y=mAfter-mBefore;
                    