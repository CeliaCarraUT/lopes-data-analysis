function Y=calculatePowerSteps(D)
%This function calculates the power of every step (in a trial). The mean of the
%power values within one step is calculated and stored inside the
%'Results' field of the structure. 
%The power for every joint per step(every row corresponds to one step; column 1 is the total power, column 2 the
%negative power and column 3 the positive power) is stored

%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%First we divide the data in steps
X=diff(D.data.m_cont_stepCounter);
Z=find(X==1);
Z=[Z; length(D.data.m_cont_stepCounter)];

%check if the first step is with the right/left leg (later) for anaylisis
%of everything all together 
for j=1:length(Z)-1
    %% PX - pelvis x direction
    %total power-column1
    Power_PX=D.data.m_cont_jointForcesMeasured_PX(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_PX(Z(j):Z(j+1)),0.01);
    D.Results.PowerBySteps_PX(j,1)=mean(Power_PX);
    %negative-column2
    PXneg=Power_PX;
    for i=1:length(PXneg)
        if PXneg(i)>0
            PXneg(i)=0;
        end
    end
    PXneg=PXneg(PXneg~=0);
    D.Results.PowerBySteps_PX(j,2)=mean(PXneg);
    %positive-column3
    PXpos=Power_PX;
    for i=1:length(PXpos)
        if PXpos(i)<0
            PXpos(i)=0;
        end
    end
   PXpos=PXpos(PXpos~=0); 
   D.Results.PowerBySteps_PX(j,3)=mean(PXpos);
   
   %% PZ - pelvis z direction
   %total 
   Power_PZ=D.data.m_cont_jointForcesMeasured_PZ(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_PZ(Z(j):Z(j+1)),0.01);
   D.Results.PowerBySteps_PZ(j,1)=mean(Power_PZ);
   %negative
   PZneg=Power_PZ;
   for i=1:length(PZneg)
       if PZneg(i)>0
           PZneg(i)=0;
       end
   end
   PZneg=PZneg(PZneg~=0);
   D.Results.PowerBySteps_PZ(j,2)=mean(PZneg);
   %positive
   PZpos=Power_PZ;
   for i=1:length(PZpos)
       if PZpos(i)<0
           PZpos(i)=0;
       end
   end
   PZpos=PZpos(PZpos~=0); 
   D.Results.PowerBySteps_PZ(j,3)=mean(PZpos);
   
   %% LHA - left hip abduction
   %total
   Power_LHA=D.data.m_cont_jointForcesMeasured_LHA(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_LHA(Z(j):Z(j+1)),0.01);
   D.Results.PowerBySteps_LHA(j,1)=mean(Power_LHA);
   %negative
   LHAneg=Power_LHA;
   for i=1:length(LHAneg)
       if LHAneg(i)>0
           LHAneg(i)=0;
       end
   end
   LHAneg=LHAneg(LHAneg~=0);
   D.Results.PowerBySteps_LHA(j,2)=mean(LHAneg);
   %positive
   LHApos=Power_LHA;
   for i=1:length(LHApos)
       if LHApos(i)<0
           LHApos(i)=0;
       end
   end
   LHApos=LHApos(LHApos~=0); 
   D.Results.PowerBySteps_LHA(j,3)=mean(LHApos);
   
   %% LHF - left hip flexion
   %total
   Power_LHF=D.data.m_cont_jointForcesMeasured_LHF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_LHF(Z(j):Z(j+1)),0.01);
   D.Results.PowerBySteps_LHF(j,1)=mean(Power_LHF);
   %negative
   LHFneg=Power_LHF;
   for i=1:length(LHFneg)
       if LHFneg(i)>0
           LHFneg(i)=0;
       end
   end
   LHFneg=LHFneg(LHFneg~=0);
   D.Results.PowerBySteps_LHF(j,2)=mean(LHFneg);
   %positive
   LHFpos=Power_LHF;
   for i=1:length(LHFpos)
       if LHFpos(i)<0
           LHFpos(i)=0;
       end
   end
   LHFpos=LHFpos(LHFpos~=0); 
   D.Results.PowerBySteps_LHF(j,3)=mean(LHFpos);
   
   %% LKF - left knee flexion
   Power_LKF=D.data.m_cont_jointForcesMeasured_LKF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_LKF(Z(j):Z(j+1)),0.01);
   D.Results.PowerBySteps_LKF(j,1)=mean(Power_LKF);
   %negative
   LKFneg=Power_LKF;
   for i=1:length(LKFneg)
       if LKFneg(i)>0
           LKFneg(i)=0;
       end
   end
   LKFneg=LKFneg(LKFneg~=0);
   D.Results.PowerBySteps_LKF(j,2)=mean(LKFneg);
   %positive
   LKFpos=Power_LKF;
   for i=1:length(LKFpos)
       if LKFpos(i)<0
           LKFpos(i)=0;
       end
   end
   LKFpos=LKFpos(LKFpos~=0); 
   D.Results.PowerBySteps_LKF(j,3)=mean(LKFpos);
   
   %% RHA - right hip abduction
   Power_RHA=D.data.m_cont_jointForcesMeasured_RHA(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_RHA(Z(j):Z(j+1)),0.01);
   D.Results.PowerBySteps_RHA(j,1)=mean(Power_RHA);
   %negative
   RHAneg=Power_RHA;
   for i=1:length(RHAneg)
       if RHAneg(i)>0
           RHAneg(i)=0;
       end
   end
   RHAneg=RHAneg(RHAneg~=0);
   D.Results.PowerBySteps_RHA(j,2)=mean(RHAneg);
   %positive
   RHApos=Power_RHA;
   for i=1:length(RHApos)
       if RHApos(i)<0
           RHApos(i)=0;
       end
   end
   RHApos=RHApos(RHApos~=0); 
   D.Results.PowerBySteps_RHA(j,3)=mean(RHApos);
   
   %% RHF - right hip flexion
   Power_RHF=D.data.m_cont_jointForcesMeasured_RHF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_RHF(Z(j):Z(j+1)),0.01);
   D.Results.PowerBySteps_RHF(j,1)=mean(Power_RHF);
   %negative
   RHFneg=Power_RHF;
   for i=1:length(RHFneg)
       if RHFneg(i)>0
           RHFneg(i)=0;
       end
   end
   RHFneg=RHFneg(RHFneg~=0);
   D.Results.PowerBySteps_RHF(j,2)=mean(RHFneg);
   %positive
   RHFpos=Power_RHF;
   for i=1:length(RHFpos)
       if RHFpos(i)<0
           RHFpos(i)=0;
       end
   end
   RHFpos=RHFpos(RHFpos~=0); 
   D.Results.PowerBySteps_RHF(j,3)=mean(RHFpos);
   
   %% RKF - right knee flexion
   Power_RKF=D.data.m_cont_jointForcesMeasured_RKF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_RKF(Z(j):Z(j+1)),0.01);
   D.Results.PowerBySteps_RKF(j,1)=mean(Power_RKF);
   %negative
   RKFneg=Power_RKF;
   for i=1:length(RKFneg)
       if RKFneg(i)>0
           RKFneg(i)=0;
       end
   end
   RKFneg=RKFneg(RKFneg~=0);
   D.Results.PowerBySteps_RKF(j,2)=mean(RKFneg);
   %positive
   RKFpos=Power_RKF;
   for i=1:length(RKFpos)
       if RKFpos(i)<0
           RKFpos(i)=0;
       end
   end
   RKFpos=RKFpos(RKFpos~=0); 
   D.Results.PowerBySteps_RKF(j,3)=mean(RKFpos);
   
end

Y=D;