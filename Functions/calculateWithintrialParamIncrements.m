function [Y]=calculateWithintrialParamIncrements(WithinTrialParamValues)%VariableValues)

 fields1=fieldnames(WithinTrialParamValues);
 for i=1:length(fields1)
     fields2=fieldnames(WithinTrialParamValues.(fields1{i,1}));
     for j=1:length(fields2)
         WithinTrialParamIncrements.(fields1{i,1}).(fields2{j,1})=diff(WithinTrialParamValues.(fields1{i,1}).(fields2{j,1}));
     end
 end
 
 Y=WithinTrialParamIncrements;
%  
% for i=1:length(fields1)
%     fields2=fieldnames(VariableValues.(fields1{i,1}));
%     fields3=fieldnames(VariableValues.(fields1{i,1}).(fields2{1,1}));
%     for j=1:length(fields2)
%         for m=1:length(fields3)
%             for k=1:size(VariableValues.(fields1{i,1}).(fields2{j,1}).(fields3{1,1}),1)
%                 IncrementsVariables.(fields1{i,1}).(fields2{j,1}).(fields3{m,1})(k,1)=VariableValues.(fields1{i,1}).(fields2{j,1}).(fields3{m,1})(k,2)-VariableValues.(fields1{i,1}).(fields2{j,1}).(fields3{m,1})(k,1);
%             end
%         end
%     end
% end
%  
%   Z=IncrementsVariables;