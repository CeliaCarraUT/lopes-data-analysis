function [Y] = calculateStability(D)
% Stability: maximal knee flexion angle between 10-40% of the gait cycle 
%This function outputs the stability of all the steps in a trial(stored in input struct D) and
%stores them inside the original stucture (in the 'Results' field).

%Both the measured angles (field 'Stability')  and the difference with the reference trajectory
%are stored (field 'StabilityDiff'). Steps are also divided into right and
%left.

%%% NOTE:Run function 'addResultFieldStruct' before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%stability during stance: knee flexion peak during stance phase
%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
count=0;
count2=0;
Rsteps=[];
Lsteps=[];

for i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==3
        count=count+1;
        Rsteps(count,1)=(A-1)+i;
    elseif D.data.m_cont_kinematicPhase((A-1)+i)==1
        count2=count2+1;
        Lsteps(count2,1)=(A-1)+i;
    end
end

if any(D.data.m_cont_kinematicPhase==1)==0
    Lsteps=[];
end

if any(D.data.m_cont_kinematicPhase==3)==0
    Rsteps=[];
end

clear count count2
    %Right
    if isempty(Rsteps)==1
        Rstability=0;
    else
        RD=diff(Rsteps);
        RstandInstances(1)=Rsteps(1);
        count=1;

        for i=1:length(RD)
            if RD(i)~=1
                count=count+1;
                RstandInstances(count,1)=Rsteps(i);
            end
        end
        RstandInstances(count+1)=Rsteps(end);

        for i=1:length(RstandInstances)-1
            if i==1
                Rstability(i,1)=max(D.data.m_cont_jointAngles_RKF(RstandInstances(i):RstandInstances(i+1)));
            else 
                Rstability(i,1)=max(D.data.m_cont_jointAngles_RKF(Rsteps(find(Rsteps==RstandInstances(i))+1):RstandInstances(i+1)));
            end 
        end
    end

    %Left
    if isempty(Lsteps)==1
        Lstability=0;
    else
        LD=diff(Lsteps);
        LstandInstances(1)=Lsteps(1);
        count2=1;

        for i=1:length(LD)
            if LD(i)~=1
                count2=count2+1;
                LstandInstances(count2,1)=Lsteps(i);
            end
        end
        LstandInstances(count2+1,1)=Lsteps(end);

        for i=1:length(LstandInstances)-1
            if i==1
                Lstability(i,1)=max(D.data.m_cont_jointAngles_LKF(LstandInstances(i):LstandInstances(i+1)));
            else 
                Lstability(i,1)=max(D.data.m_cont_jointAngles_LKF(Lsteps(find(Lsteps==LstandInstances(i))+1):LstandInstances(i+1)));
            end 
        end
    end

 D.Results.Stability.Rsteps=Rstability;
 D.Results.Stability.Lsteps=Lstability;


%% check the angle profiles and use find peaks to substract from the reference
% %left
% [pksm,locsm]=findpeaks(D.data.m_cont_jointAngles_LKF);
% [pksr,locsr]=findpeaks(D.data.r_cont_jointAngles_LKF);
% locsm2=locsm(pksm<0.55);
% pksm2=pksm(pksm<0.55);
% locsr2=locsr(pksr<0.55);
% pksr2=pksr(pksr<0.55);
% 
% %remove outlier peaks (in case the walking pattern is not very good)
% outlierpeaks1=find(pksm2<0.0045);
%    for i=1:length(outlierpeaks1)
%         if i==1
%             pksm2(outlierpeaks1(i))=[];
%             locsm2(outlierpeaks1(i))=[];
%         else
%             pksm2(outlierpeaks1(i)-i+1)=[];
%             locsm2(outlierpeaks1(i)-i+1)=[];
%         end
%     end
% 
% %check for double peaks
% LDM2=diff(locsm2);
% doublepeaks2=find(LDM2<50);
% % doublepeaks2=doublepeaks2+1;
% 
%     for i=1:length(doublepeaks2)
%         if i==1
%             pksm2(doublepeaks2(i))=[];
%             locsm2(doublepeaks2(i))=[];
%         else
%             pksm2(doublepeaks2(i)-i+1)=[];
%             locsm2(doublepeaks2(i)-i+1)=[];
%         end
%     end
% 
% LDM3=diff(locsr2);
% doublepeaks=find(LDM3<50);
% % doublepeaks=doublepeaks+1;
% 
%     for i=1:length(doublepeaks)
%         if i==1
%             pksr2(doublepeaks(i))=[];
%             locsr2(doublepeaks(i))=[];
%         else
%             pksr2(doublepeaks(i)-i+1)=[];
%             locsr2(doublepeaks(i)-i+1)=[];
%         end
%     end
% 
% %make sure vectors are the same length
% Ldifvector=diff(locsm2);
% LC=find(Ldifvector<100);
% if ismember(0,pksm2)==1
%     LC=LC+1;
%     locsm2(LC)=[];
%     pksm2(LC)=[];
% else LC=LC;
%     locsm2(LC)=[];
%     pksm2(LC)=[];
% end
% 
% 
% %check the length of the vectors-L
% if length(pksr2)<length(pksm2)
%     %check the first and last peaks
%     if (locsr2(1)-locsm2(1))>100 %first element is an extra peak
%         pksm2(1)=[];
%         locsm2(1)=[];
%     elseif locsm2(end)-locsr2(end)>100
%         pksm2(end)=[];
%         locsm2(end)=[];
%     end
% elseif length(pksm2)<length(pksr2)
%         %check the first and last peaks
%     if (locsm2(1)-locsr(1))>100 %first element is an extra peak
%         pksr2(1)=[];
%         locsr2(1)=[];
%     elseif locsm(end)-locsr2(end)>100
%         pksr2(end)=[];
%         locsr2(end)=[];
%     end
%     
% end
% 
% %right
% [Rpksm,Rlocsm]=findpeaks(D.data.m_cont_jointAngles_RKF);
% [Rpksr,Rlocsr]=findpeaks(D.data.r_cont_jointAngles_RKF);
% Rlocsm2=Rlocsm(Rpksm<0.55);
% Rpksm2=Rpksm(Rpksm<0.55);
% Rlocsr2=Rlocsr(Rpksr<0.55);
% Rpksr2=Rpksr(Rpksr<0.55);
% 
% %remove outlier peaks (in case the walking pattern is not very good)
% outlierpeaks=find(Rpksm2<0.0045);
%    for i=1:length(outlierpeaks)
%         if i==1
%             Rpksm2(outlierpeaks(i))=[];
%             Rlocsm2(outlierpeaks(i))=[];
%         else
%             Rpksm2(outlierpeaks(i)-i+1)=[];
%             Rlocsm2(outlierpeaks(i)-i+1)=[];
%         end
%     end
% 
% 
% %check for double peaks
% RDM2=diff(Rlocsm2);
% doublepeaks3=find(RDM2<50);
% % doublepeaks3=doublepeaks3+1;
% 
%     for i=1:length(doublepeaks3)
%         if i==1
%             Rpksm2(doublepeaks3(i))=[];
%             Rlocsm2(doublepeaks3(i))=[];
%         else
%             Rpksm2(doublepeaks3(i)-i+1)=[];
%             Rlocsm2(doublepeaks3(i)-i+1)=[];
%         end
%     end
%     
% RDM3=diff(Rlocsr2);
% doublepeaks4=find(RDM3<50);
% % doublepeaks4=doublepeaks4+1;
% 
%     for i=1:length(doublepeaks4)
%         if i==1
%             Rpksr2(doublepeaks4(i))=[];
%             Rlocsm2(doublepeaks4(i))=[];
%         else
%             Rpksr2(doublepeaks4(i)-i+1)=[];
%             Rlocsr2(doublepeaks4(i)-i+1)=[];
%         end
%     end
% 
% 
% 
% difvector=diff(Rlocsm2);
% C=find(difvector<100);
% if ismember(0,Rpksm2)==1
%     C=C+1;
%     Rlocsm2(C)=[];
%     Rpksm2(C)=[];
% else C=C;
%     Rlocsm2(C)=[];
%     Rpksm2(C)=[];
% end
% 
% if length(Rlocsm2)>length(Rlocsr2)
%     Rlocsm2(length(Rlocsm2))=[];
%     Rpksm2(length(Rpksm2))=[];
% end
% 
% %check the length of the vectors-R
% if length(Rpksr2)<length(Rpksm2)
%     if (Rlocsr(1)-Rlocsm(1))>100 %first element is an extra peak
%         Rpksm2(1)=[];
%         Rlocsm2(1)=[];
%     elseif Rlocsm2(end)-Rlocsr2(end)>100
%         Rpksm2(end)=[];
%         Rlocsm2(end)=[];
%     end
% elseif length(Rpksm2)<length(Rpksr2)
%         %check the first and last peaks
%     if (Rlocsm2(1)-Rlocsr2(1))>100 %first element is an extra peak
%         Rpksr2(1)=[];
%         Rlocsr2(1)=[];
%     elseif Rlocsm2(end)-Rlocsr2(end)>100
%         Rpksr2(end)=[];
%         Rlocsr2(end)=[];
%     end
% end

%% use method one but also with the reference and substract like that
% B=find(D.data.r_cont_stepCounter==3,1);
% lastStepr=find(D.data.r_cont_stepCounter==D.data.r_cont_stepCounter(end)-4,1);
% 
% if isempty(lastStepr)==1
%     lastStepr=lastStep;
% end
% 
% 
% count=0;
% count2=0;
% Rstepsr=[];
% Lstepsr=[];
% 
% for i=1:length(D.data.r_cont_kinematicPhase(B:lastStepr))
%     if D.data.r_cont_kinematicPhase((B-1)+i)==3
%         count=count+1;
%         Rstepsr(count,1)=(B-1)+i;
%     elseif D.data.r_cont_kinematicPhase((B-1)+i)==1
%         count2=count2+1;
%         Lstepsr(count2,1)=(B-1)+i;
%     end
% end
% 
% if any(D.data.r_cont_kinematicPhase==1)==0
%     Lstepsr=[];
% end
% 
% if any(D.data.r_cont_kinematicPhase==3)==0
%     Rstepsr=[];
% end
% 
% clear count count2
%     %Right
%     if isempty(Rstepsr)==1
%         Rstabilityr=0;
%     else
%         RDr=diff(Rstepsr);
%         RstandInstancesr(1)=Rstepsr(1);
%         count=1;
% 
%         for i=1:length(RDr)
%             if RDr(i)~=1
%                 count=count+1;
%                 RstandInstancesr(count,1)=Rstepsr(i);
%             end
%         end
%         RstandInstancesr(count+1)=Rstepsr(end);
% 
%         for i=1:length(RstandInstancesr)-1
%             if i==1
%                 Rstabilityr(i,1)=max(D.data.r_cont_jointAngles_RKF(RstandInstancesr(i):RstandInstancesr(i+1)));
%             else 
%                 Rstabilityr(i,1)=max(D.data.r_cont_jointAngles_RKF(Rstepsr(find(Rstepsr==RstandInstancesr(i))+1):RstandInstancesr(i+1)));
%             end 
%         end
%     end
% 
%     %Left
%     if isempty(Lstepsr)==1
%         Lstabilityr=0;
%     else
%         LDr=diff(Lstepsr);
%         LstandInstancesr(1)=Lstepsr(1);
%         count2=1;
% 
%         for i=1:length(LDr)
%             if LDr(i)~=1
%                 count2=count2+1;
%                 LstandInstancesr(count2,1)=Lstepsr(i);
%             end
%         end
%         LstandInstancesr(count2+1,1)=Lstepsr(end);
% 
%         for i=1:length(LstandInstancesr)-1
%             if i==1
%                 Lstabilityr(i,1)=max(D.data.r_cont_jointAngles_LKF(LstandInstancesr(i):LstandInstancesr(i+1)));
%             else 
%                 Lstabilityr(i,1)=max(D.data.r_cont_jointAngles_LKF(Lstepsr(find(Lstepsr==LstandInstancesr(i))+1):LstandInstancesr(i+1)));
%             end 
%         end
%     end
%     
%     
% %%%check if there are the same number of steps for measured and reference and if not,insert NaN 
% %right
% if length(Rstabilityr)>length(Rstability)
%     %add ceros to the vector so they are the same length; then substract to
%     %see where is the missing step
%     addedZeros=abs(length(Rstabilityr)-length(Rstability));
%     unprocessedInst=RstandInstances;
%     unprocessedInst(numel(RstandInstancesr))=0;
%     part1=(RstandInstancesr-unprocessedInst); %check where the missing step is
%     part2=diff(part1); %check differences again 
%     final=find(abs(part2)>100);
%     %check if we have to insert consecutive NaNs 
%     part2=part2(1:(end-addedZeros-1));
%     doubleNAN=find(abs(part2)>191);
%     if isempty(doubleNAN)==0
%         for i=1:length(doubleNAN)
%             indexes(i,1)=find(final==doubleNAN(i));
%         end
%         final=final(1:end-addedZeros);
%         final=final+1;
%         counter=0;
%             for i=1:length(final)
%                 if i==1
%                     if ismember(i,indexes)==1
%                         Rstability=[Rstability(1:length(Rstability) < final(i)); NaN; NaN; Rstability(1:length(Rstability) >= final(i))];
%                         counter=counter+2;
%                     else
%                         Rstability=[Rstability(1:length(Rstability) < final(i)); NaN; Rstability(1:length(Rstability) >= final(i))];
%                         counter=counter+1;
%                     end
%                 else
%                     if ismember(i,indexes)==1
%                         counter=counter+2;
%                         Rstability=[Rstability(1:length(Rstability) < final(i)+counter); NaN; NaN;  Rstability(1:length(Rstability) >= final(i)+counter)];
%                     else
%                         counter=counter+1;
%                         Rstability=[Rstability(1:length(Rstability) < final(i)+counter); NaN; Rstability(1:length(Rstability) >= final(i)+counter)];
%                     end
%                 end
%             end
%     else
%         final=final(1:end-addedZeros);
%         final=final+1;
%         counter=0;
%         for i=1:length(final)
%             counter=counter+1;
%             if i==1
%                 Rstability=[Rstability(1:length(Rstability) < final(i)); NaN; Rstability(1:length(Rstability) >= final(i))];
%             else
%                 Rstability=[Rstability(1:length(Rstability) < final(i)+counter); NaN; Rstability(1:length(Rstability) >= final(i)+counter)];
%             end
%         end 
%     end
% elseif length(Rstabilityr)<length(Rstability)
%         addedZeros=abs(length(Rstabilityr)-length(Rstability));
%         unprocessedInst=RstandInstancesr;
%         unprocessedInst(numel(RstandInstances))=0;
%         part1=(RstandInstances-unprocessedInst); %check where the missing step is
%         part2=diff(part1); %check differences again 
%         final=find(abs(part2)>100);
%         %check if we have to insert consecutive NaNs 
%         part2=part2(1:(end-addedZeros-1));
%         doubleNAN=find(abs(part2)>191);
%         if isempty(doubleNAN)==0
%             for i=1:length(doubleNAN)
%                 indexes(i,1)=find(final==doubleNAN(i));
%             end
%             final=final(1:end-addedZeros);
%             final=final+1;
%             counter=0;
%             for i=1:length(final)
%                 if i==1
%                     if ismember(i,indexes)==1
%                         Rstabilityr=[Rstabilityr(1:length(Rstabilityr) < final(i)); NaN; NaN; Rstabilityr(1:length(Rstabilityr) >= final(i))];
%                         counter=counter+2;
%                     else
%                         Rstabilityr=[Rstabilityr(1:length(Rstabilityr) < final(i)); NaN; Rstabilityr(1:length(Rstabilityr) >= final(i))];
%                         counter=counter+1;
%                     end
%                 else
%                     if ismember(i,indexes)==1
%                         counter=counter+2;
%                         Rstabilityr=[Rstabilityr(1:length(Rstabilityr) < final(i)+counter); NaN; NaN;  Rstabilityr(1:length(Rstabilityr) >= final(i)+counter)];
%                     else
%                         counter=counter+1;
%                         Rstabilityr=[Rstabilityr(1:length(Rstabilityr) < final(i)+counter); NaN; Rstabilityr(1:length(Rstabilityr) >= final(i)+counter)];
%                     end
%                 end
%             end
%         else
%             final=final(1:end-addedZeros);
%             final=final+1;
%             counter=0;
%             for i=1:length(final)
%                 counter=counter+1;
%                 if i==1
%                     Rstabilityr=[Rstabilityr(1:length(Rstabilityr) < final(i)); NaN; Rstabilityr(1:length(Rstabilityr) >= final(i))];
%                 else
%                     Rstabilityr=[Rstabilityr(1:length(Rstabilityr) < final(i)+counter); NaN; Rstabilityr(1:length(Rstabilityr) >= final(i)+counter)];
%                 end
%             end 
%         end
% end
% 
% %left
% if length(Lstabilityr)>length(Lstability)
%     %add ceros to the vector so they are the same length; then substract to
%     %see where is the missing step
%     addedZeros=abs(length(Lstabilityr)-length(Lstability));
%     unprocessedInst=LstandInstances;
%     unprocessedInst(numel(LstandInstancesr))=0;
%     part1=(LstandInstancesr-unprocessedInst); %check where the missing step is
%     part2=diff(part1); %check differences again 
%     final=find(abs(part2)>100);
%     %check if we have to insert consecutive NaNs 
%     part2=part2(1:(end-addedZeros-1));
%     doubleNAN=find(abs(part2)>191);
%     if isempty(doubleNAN)==0
%        for i=1:length(doubleNAN)
%            indexes(i,1)=find(final==doubleNAN(i));
%        end
%        final=final(1:end-addedZeros);
%        final=final+1;
%        counter=0;
%         for i=1:length(final)
%             if i==1
%                 if ismember(i,indexes)==1
%                     Lstability=[Lstability(1:length(Lstability) < final(i)); NaN; NaN; Lstability(1:length(Lstability) >= final(i))];
%                     counter=counter+2;
%                 else
%                     Lstability=[Lstability(1:length(Lstability) < final(i)); NaN; Lstability(1:length(Lstability) >= final(i))];
%                     counter=counter+1;
%                 end
%             else
%                 if ismember(i,indexes)==1
%                     counter=counter+2;
%                     Lstability=[Lstability(1:length(Lstability) < final(i)+counter); NaN; NaN;  Lstability(1:length(Lstability) >= final(i)+counter)];
%                 else
%                     counter=counter+1;
%                     Lstability=[Lstability(1:length(Lstability) < final(i)+counter); NaN; Lstability(1:length(Lstability) >= final(i)+counter)];
%                 end
%             end
%         end
%     else
%       final=final(1:end-addedZeros);
%             final=final+1;
%             counter=0;
%             for i=1:length(final)
%                 counter=counter+1;
%                 if i==1
%                     Lstability=[Lstability(1:length(Lstability) < final(i)); NaN; Lstability(1:length(Lstability) >= final(i))];
%                 else
%                     Lstability=[Lstability(1:length(Lstability) < final(i)+counter); NaN; Lstability(1:length(Lstability) >= final(i)+counter)];
%                 end
%             end 
%      end  
% elseif length(Lstabilityr)<length(Lstability)
%         addedZeros=abs(length(Lstabilityr)-length(Lstability));
%         unprocessedInst=LstandInstancesr;
%         unprocessedInst(numel(LstandInstances))=0;
%         part1=(LstandInstances-unprocessedInst); %check where the missing step is
%         part2=diff(part1); %check differences again 
%         final=find(abs(part2)>100);
%     %check if we have to insert consecutive NaNs 
%     part2=part2(1:(end-addedZeros-1));
%     doubleNAN=find(abs(part2)>191);
%     if isempty(doubleNAN)==0
%        for i=1:length(doubleNAN)
%            indexes(i,1)=find(final==doubleNAN(i));
%        end
%        final=final(1:end-addedZeros);
%        final=final+1;
%        counter=0;
%         for i=1:length(final)
%             if i==1
%                 if ismember(i,indexes)==1
%                     Lstabilityr=[Lstabilityr(1:length(Lstabilityr) < final(i)); NaN; NaN; Lstabilityr(1:length(Lstabilityr) >= final(i))];
%                     counter=counter+2;
%                 else
%                     Lstabilityr=[Lstabilityr(1:length(Lstabilityr) < final(i)); NaN; Lstabilityr(1:length(Lstabilityr) >= final(i))];
%                     counter=counter+1;
%                 end
%             else
%                 if ismember(i,indexes)==1
%                     counter=counter+2;
%                     Lstabilityr=[Lstabilityr(1:length(Lstabilityr) < final(i)+counter); NaN; NaN;  Lstabilityr(1:length(Lstabilityr) >= final(i)+counter)];
%                 else
%                     counter=counter+1;
%                     Lstabilityr=[Lstabilityr(1:length(Lstabilityr) < final(i)+counter); NaN; Lstabilityr(1:length(Lstabilityr) >= final(i)+counter)];
%                 end
%             end
%         end
%      else
%       final=final(1:end-addedZeros);
%       final=final+1;
%       counter=0;
%             for i=1:length(final)
%                 counter=counter+1;
%                 if i==1
%                     Lstabilityr=[Lstabilityr(1:length(Lstabilityr) < final(i)); NaN; Lstabilityr(1:length(Lstabilityr) >= final(i))];
%                 else
%                     Lstabilityr=[Lstabilityr(1:length(Lstabilityr) < final(i)+counter); NaN; Lstabilityr(1:length(Lstabilityr) >= final(i)+counter)];
%                 end
%             end 
%      end  
% end
% 
% %%%%%In case the missing step is the last step (add NaN value at the end):
% if length(Lstabilityr)>length(Lstability)
%     Lstability(end+1)=NaN;
% elseif length(Lstability)>length(Lstabilityr)
%     Lstabilityr(end+1)=NaN;
% end
% 
% %%%%%%%check the legth of the vector and add NaN values if needed
% %left
% if length(Lstability)>length(Lstabilityr)
%     %check if its the first or the last step
%         %check the first and last peaks
%     if  abs(LstandInstancesr(1)-LstandInstances(1))>100%first element is an extra peak in measured
%         Lstabilityr=[NaN;Lstabilityr];
%     else  %last element is an extra peak
%         Lstabilityr(end+1)=NaN;
%     end
% end
% 
% if length(Lstabilityr)>length(Lstability)
%     %check if its the first or the last step
%         %check the first and last peaks
%     if  abs(LstandInstancesr(1)-LstandInstances(1))>100%first element is an extra peak in measured
%         Lstability=[NaN;Lstability];
%     else  %last element is an extra peak
%         Lstability(end+1)=NaN;
%     end
% end
% 
% %right
% if length(Rstability)>length(Rstabilityr)
%     %check if its the first or the last step
%         %check the first and last peaks
%     if  abs(RstandInstancesr(1)-RstandInstances(1))>100%first element is an extra peak in measured
%         Rstabilityr=[NaN;Rstabilityr];
%     else  %last element is an extra peak
%         Rstabilityr(end+1)=NaN;
%     end
% end
% 
% if length(Rstabilityr)>length(Rstability)
%     %check if its the first or the last step
%         %check the first and last peaks
%     if  abs(RstandInstancesr(1)-RstandInstances(1))>100%first element is an extra peak in measured
%         Rstability=[NaN;Rstability];
%     else  %last element is an extra peak
%         Rstability(end+1)=NaN;
%     end
% end
% 
% D.Results.StabilityDiff.Lsteps=abs(Lstabilityr-Lstability);
% D.Results.StabilityDiff.Rsteps=abs(Rstabilityr-Rstability);
 Y=D;

end