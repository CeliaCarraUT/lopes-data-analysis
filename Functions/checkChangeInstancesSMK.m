function[Y]=checkChangeInstancesSMK(plottingmatrix1SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06,ParamChangeInstancesDS07,ParamChangeInstancesDS08)
%This function checks the first plotting matrix of SMK patients to
%determine which parameters are changed independently from the General
%Guidance force and stores them in a new matrix Y
%Inputs:
%plottingmatrix-matrix containing increments in parameter and variables
%ParamChangeInstances-all the different structs containing the change
%instances for all parameters and trials (one for every patient)
%Outputs:
%Y-new matrix containing only the increments for intances where the
%paramters are changed independently of the GGF.


%first check column 19(patient) then 18(trial) then 4(changeinstance)
fieldsSMK=fieldnames(plottingmatrix1SMK);
for i=1:length(fieldsSMK)
    counter=0;
    for j=1:size((plottingmatrix1SMK.(fieldsSMK{i,1})),1)
        if plottingmatrix1SMK.(fieldsSMK{i,1})(j,19)==1
            n=1;
        elseif plottingmatrix1SMK.(fieldsSMK{i,1})(j,19)==2
            n=2;
        elseif plottingmatrix1SMK.(fieldsSMK{i,1})(j,19)==3
            n=3;
        elseif plottingmatrix1SMK.(fieldsSMK{i,1})(j,19)==4
            n=4;
        elseif plottingmatrix1SMK.(fieldsSMK{i,1})(j,19)==5
            n=5;
        elseif plottingmatrix1SMK.(fieldsSMK{i,1})(j,19)==6
            n=6;
        elseif plottingmatrix1SMK.(fieldsSMK{i,1})(j,19)==7
            n=7;
        end
        switch n %every 'option' of the switch loop corresponds to a different patient
            case 1
                %check trialnumb
                trialnumb=plottingmatrix1SMK.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesDS02.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    poscount.(fieldsSMK{i,1})=counter;
                elseif isfield(ParamChangeInstancesDS02.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesDS02.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1SMK.(fieldsSMK{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                        poscount.(fieldsSMK{i,1})=counter;
                    end
                end
            case 2
                %check trialnumb
                trialnumb=plottingmatrix1SMK.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesDS03.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    poscount.(fieldsSMK{i,1})=counter;
                elseif isfield(ParamChangeInstancesDS03.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesDS03.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1SMK.(fieldsSMK{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                        poscount.(fieldsSMK{i,1})=counter;
                    end
                end
            case 3 
                %check trialnumb
                trialnumb=plottingmatrix1SMK.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesDS04.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    poscount.(fieldsSMK{i,1})=counter;
                elseif isfield(ParamChangeInstancesDS04.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesDS04.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1SMK.(fieldsSMK{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    end
                end
            case 4
                trialnumb=plottingmatrix1SMK.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesDS05.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    poscount.(fieldsSMK{i,1})=counter;
                elseif isfield(ParamChangeInstancesDS05.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesDS05.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1SMK.(fieldsSMK{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                        poscount.(fieldsSMK{i,1})=counter;
                    end
                end
             case 5
                trialnumb=plottingmatrix1SMK.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesDS06.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    poscount.(fieldsSMK{i,1})=counter;
                elseif isfield(ParamChangeInstancesDS06.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesDS06.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1SMK.(fieldsSMK{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                        poscount.(fieldsSMK{i,1})=counter;
                    end
                end
             case 6
                trialnumb=plottingmatrix1SMK.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesDS07.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    poscount.(fieldsSMK{i,1})=counter;
                elseif isfield(ParamChangeInstancesDS07.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesDS07.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1SMK.(fieldsSMK{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                        poscount.(fieldsSMK{i,1})=counter;
                    end
                end
              case 7
                trialnumb=plottingmatrix1SMK.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(ParamChangeInstancesDS08.(trials),'General_GuidanceForce_pct')==0
                    counter=counter+1;
                    Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                    poscount.(fieldsSMK{i,1})=counter;
                elseif isfield(ParamChangeInstancesDS08.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=ParamChangeInstancesDS08.(trials).General_GuidanceForce_pct;
                    if any(GGFchange==plottingmatrix1SMK.(fieldsSMK{i,1})(j,4))==0
                        counter=counter+1;
                        Y.(fieldsSMK{i,1})(counter,:)=plottingmatrix1SMK.(fieldsSMK{i,1})(j,:);
                        poscount.(fieldsSMK{i,1})=counter;
                    end
                end
        end
    end
end
clear trials
