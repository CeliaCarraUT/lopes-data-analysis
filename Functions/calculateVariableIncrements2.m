function [Y]=calculateVariableIncrements2(WithinTrialParamValues,StepsStruct,DataSet)

fields1=fieldnames(WithinTrialParamValues);
trialnums=regexp(fields1,'\d*','Match');
 for i=1:length(fields1)
    trials(i,1)=str2double(trialnums{i,1});
 end
IncrementsVariables=[];
for i=1:length(fields1)
     fields2=fieldnames(WithinTrialParamValues.(fields1{i,1}));
     for j=1:length(fields2)
         for k=1:length(WithinTrialParamValues.(fields1{i,1}).(fields2{j,1}))-1
             %select the right steps
            changeStep=StepsStruct.(fields1{i,1}).(fields2{j,1})(k+1);
            initialstep=changeStep-11;
            finalstep=changeStep+11;
            step2=changeStep*2;
            initialstep2=step2-21;
            finalstep2=step2+21;
            vector1=(initialstep:changeStep-1);
            vector2=(changeStep+1:finalstep);
            if vector2(end)>length(DataSet(trials(i)).Results.StepHeightDiff.Lsteps) || vector2(end)>length(DataSet(trials(i)).Results.StepHeightDiff.Rsteps) 
                if length(DataSet(trials(i)).Results.StepHeightDiff.Rsteps)>length(DataSet(trials(i)).Results.StepHeightDiff.Lsteps)
                    while vector2(end)>length(DataSet(trials(i)).Results.StepHeightDiff.Lsteps) 
                        vector2(end)=[];
                    end
                elseif length(DataSet(trials(i)).Results.StepHeightDiff.Lsteps)>length(DataSet(trials(i)).Results.StepHeightDiff.Rsteps)
                    while vector2(end)>length(DataSet(trials(i)).Results.StepHeightDiff.Rsteps) 
                        vector2(end)=[];
                    end
                end
            end
            %step height
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).RStepHeights(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.StepHeightDiff.Rsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.StepHeightDiff.Rsteps(vector1)));
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).LStepHeights(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.StepHeightDiff.Lsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.StepHeightDiff.Lsteps(vector1)));
           
            %Initial step length
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).RStepLengthInitial(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Rsteps.min(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Rsteps.min(vector1)));
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).LStepLengthInitial(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Lsteps.min(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Lsteps.min(vector1)));

            %Terminal step length
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).RStepLengthFinal(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Rsteps.max(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Rsteps.max(vector1)));
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).LStepLengthFinal(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Lsteps.max(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.StepLengthDiff.Lsteps.max(vector1)));

            %Stability
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).RStability(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.Stability.Rsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.Stability.Rsteps(vector1)));
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).LStability(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.Stability.Lsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.Stability.Lsteps(vector1)));

            %Lateral Foot Position
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).RLateralFootPos(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.LateralFootPos.Rsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.LateralFootPos.Rsteps(vector1)));
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).LLateralFootPos(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.LateralFootPos.Lsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.LateralFootPos.Lsteps(vector1)));

            %Prepositioning
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).RPrepositioning(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.PrepositioningDiff.Rsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.PrepositioningDiff.Rsteps(vector1)));
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).LPrepositioning(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.PrepositioningDiff.Lsteps(vector2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.PrepositioningDiff.Lsteps(vector1)));

            %weightshift (stepx2)
            vec1=(initialstep2:step2-1);
            vec2=(step2+1:finalstep2);
            if vec2(end)>length(DataSet(trials(i)).Results.WeightShift)
                while vec2(end)>length(DataSet(trials(i)).Results.WeightShift)
                    vec2(end)=[];
                end
            end
            [IncrementsVariables.(fields1{i,1}).(fields2{j,1}).WeightShift(k,1)]=mean(RemoveOutliersData(DataSet(trials(i)).Results.WeightShift(vec2)))-mean(RemoveOutliersData(DataSet(trials(i)).Results.WeightShift(vec1)));
         end
     end
end

Y=IncrementsVariables;