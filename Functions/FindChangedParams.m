function [Y,X]=FindChangedParams(ParamStruct)
% %% Find changed parameters and give the final max value
% fields = fieldnames(ParamStruct(1).data);
% 
% %check the fields that are changed using the mean (if the mean is 0, it
% %means the parameters are unchanged
% field_means=zeros(length(fields),length(ParamStruct));
% for j=1:length(ParamStruct)
%     field_means(:,j) = structfun(@mean,ParamStruct(j).data);
% end
% %find non-zero indexes for each trial
% k = find(field_means(:,1));
% 
% changedParams=cell(length(fields),length(ParamStruct));
% for i=1:length(ParamStruct)
%     for j=1:length(fields)
%         if field_means(j,i)~=0
%             changedParams{j,i}=fields{j,1};
%         elseif field_means==0
%             changedParams{j,i}='empty';
%         end
%     end
% end
% 
% %remove time, weight and other indexes that dont refer to actual modifiable
% %parameters
% 
% for i=1:size(changedParams,2)
%     for j=1:size(changedParams,1)
%         if strcmp(changedParams{j,i},'time')==1 || strcmp(changedParams{j,i},'Patient_BodyLength_m')==1 || strcmp(changedParams{j,i},'Patient_PelvisWidth_m')==1 || strcmp(changedParams{j,i},'Patient_FootHeight_m')==1 || ...
%             strcmp(changedParams{j,i},'Patient_Mass_kg')==1 || strcmp(changedParams{j,i},'Patient_UpperLegLength_m')==1 || strcmp(changedParams{j,i},'Patient_FootLength_m')==1 || strcmp(changedParams{j,i},'Patient_LowerLegLength_m')==1 || ...
%             strcmp(changedParams{j,i},'Patient_PelvisHeight_m')==1 || strcmp(changedParams{j,i},'Patient_IsMale_bool')==1 || strcmp(changedParams{j,i},'State')==1 || strcmp(changedParams{j,i},'General_WalkingVelocity_m_s')==1 || strcmp(changedParams{j,i},'General_HipExtensionOffset_pct')==1 || ...
%             strcmp(changedParams{j,i},'WeightShift_AmplitudeRight_pct')==1 || strcmp(changedParams{j,i},'WeightShift_AmplitudeLeft_pct')==1
%          changedParams{j,i}=[];
%         else changedParams{j,i}=changedParams{j,i};
%         end
%     end
% end
% 
% 
% maxFinalValues=zeros(size(changedParams));
% for j=1:length(ParamStruct)
%     for i=1:length(changedParams)
%         if isempty(changedParams{i,j})==0
%             maxFinalValues(i,j)=ParamStruct(j).data.(changedParams{i,j})(length(ParamStruct(j).data.(changedParams{i,j})));
%         else maxFinalValues(i,j)=0;
%         end
%     end
% end
% 
% %remove empty and zero elements from ChangedParams and MaxFinalValues
% for i=1:size(changedParams,2)
%     for j=1:size(changedParams,1)
%         if maxFinalValues(j,i)==0
%             changedParams{j,i}=[];
%         else changedParams{j,i}=changedParams{j,i};
%         end
%     end
% end
% 
% % for i=1:size(changedParams,2)
% %     for j=1:size(changedParams,1)
% %        means(j,i)=mean(ParamStruct(i).data.(changedParams{j,i})); 
% %     end
% % end
% %ChangedParams(any(cellfun(@(x) any(isempty(x)),ChangedParams),2),:)=[];
% changedParams(all(cellfun('isempty',changedParams),2),:)=[];
% maxFinalValues(~any(maxFinalValues,2),:)=[]; %each row is a parameter
% 
% X=maxFinalValues;
% Y=changedParams;

%%
fields=fieldnames(ParamStruct(1).data_);

%remove the mass length etc of the patients
for i=1:length(fields)
    if strcmp(fields{i,1},'time')==1 || strcmp(fields{i,1},'Patient_BodyLength_m')==1 || strcmp(fields{i,1},'Patient_PelvisWidth_m')==1 || strcmp(fields{i,1},'Patient_FootHeight_m')==1 || ...
                strcmp(fields{i,1},'Patient_Mass_kg')==1 || strcmp(fields{i,1},'Patient_UpperLegLength_m')==1 || strcmp(fields{i,1},'Patient_FootLength_m')==1 || strcmp(fields{i,1},'Patient_LowerLegLength_m')==1 || ...
                strcmp(fields{i,1},'Patient_PelvisHeight_m')==1 || strcmp(fields{i,1},'Patient_IsMale_bool')==1 || strcmp(fields{i,1},'State')==1 || strcmp(fields{i,1},'General_WalkingVelocity_m_s')==1 || strcmp(fields{i,1},'General_HipExtensionOffset_pct')==1 || ...
                strcmp(fields{i,1},'WeightShift_AmplitudeRight_pct')==1 || strcmp(fields{i,1},'WeightShift_AmplitudeLeft_pct')==1
            fields{i,1}=[];
    end
end

%remove empty elements 
fields(all(cellfun('isempty',fields),2),:)=[];
%check the ones with length larger than 1->they are changed

for j=1:size(ParamStruct,2)
    count=0;
    for i=1:length(fields)
        if ParamStruct(j).data_.(fields{i,1})(1,2)~=0
            count=count+1;
            changedparams{count,j}=(fields{i,1});
            maxfinalvalues(count,j)=ParamStruct(j).data_.(fields{i,1})(end,2);
        end
    end
end

Y=changedparams;
X=maxfinalvalues;
