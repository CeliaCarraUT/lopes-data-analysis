function[Y]=addColorMapValues(matrix)
%This function adds values for a colormap (for final plotting) acording to
%a column of the plotting matrix (in this case, column 12 is chosen because
%it contains the information on the GGF levels of the increments)

%Inputs: matrix-plottingmatrix containing all the information to plot
%(increment variables, power, parameters...)

%Outputs: Y-same input plottingmatrix but with one column representing the
%colormap.

for j=1:size(matrix,1)
        if matrix(j,12)>75 
            matrix(j,12)=1;
        elseif matrix(j,12)>50 && matrix(j,12)<=75 
            matrix(j,12)=2;
        elseif matrix(j,12)>25 && matrix(j,12)<=50 
            matrix(j,12)=3;
        elseif matrix(j,12)<=25 
            matrix(j,12)=4;
        end
end
Y=matrix;