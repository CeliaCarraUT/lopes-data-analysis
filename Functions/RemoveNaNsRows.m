function [Y]=RemoveNaNsRows(plottingmatrix,n)
%This function removes the all NaN rows of the plotting matrices (all NaN
%rows indicate the parameter was changed but not long enough)

fieldnames=fields(plottingmatrix);
if n==1
    indexes=5:15;
elseif n==2
    indexes=5:12;
end

for i=1:length(fieldnames)
    X=plottingmatrix.(fieldnames{i,1})(:,indexes);
    removeRows=find(all(isnan(X),2));    
    plottingmatrix.(fieldnames{i,1})(removeRows,:)=[];
end

Y=plottingmatrix;
end
        