function [Y]=mergePlottingMatrices(matrix1,matrix2)
%This function merges the plotting matrices corresponding to both sets of
%patients

fieldnames=fields(matrix2);
for ii=1:length(fieldnames)
    if isfield(matrix1,fieldnames{ii,1})==1
        matrix1.(fieldnames{ii,1})=vertcat(matrix1.(fieldnames{ii,1}),matrix2.(fieldnames{ii,1}));
    else
        matrix1.(fieldnames{ii,1})=matrix2.(fieldnames{ii,1});
    end    
end

Y=matrix1;