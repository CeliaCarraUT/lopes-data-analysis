function [VectorNOutliers]=RemoveOutliersData(Vector)
%isoutlier: an outlier is a value that is more than three scaled median absolute deviations (MAD) away from the median.
outliers=isoutlier(Vector);
 indexes=find(outliers==1);
%remove outliers 
for i=1:length(indexes)
    if i==1
        Vector(indexes(i))=[];
    else
        Vector(indexes(i)-i+1)=[];
    end
end

VectorNOutliers=Vector;