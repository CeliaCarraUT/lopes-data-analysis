function [Y,Z]=removeInvalidTrials(D,P)
%This function removes the invalid trials within one patient's whole data
%and parameter set. 
%Invalid trials are defined as trials having a lower than a selected
%number of steps (in our case, 50). This threshold can be adjusted below.
%Input:
%D-Patient data set (all the trials).
%P-Patient parameter set (all the trials).
%Output:
%Y-Patient data set without the invalid trials.
%Z-Patient parameter set without the invalid trials.


for i=1:length(D)
     steps(i)=max(D(i).data.m_cont_stepCounter);
end
steps=steps';

InvalidTrials=find(steps<50); %threshold

    for i=1:length(InvalidTrials)
        if i==1
            D(InvalidTrials(i))=[];
            P(InvalidTrials(i))=[];
        else
            D(InvalidTrials(i)-i+1)=[];
            P(InvalidTrials(i)-i+1)=[];
        end
    end
    
Y=D;
Z=P;

end