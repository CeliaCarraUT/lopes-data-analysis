function[Y]=checkChangeInstancesSMK2(VARplottingmatrix1,StepsStructDS02,StepsStructDS03,StepsStructDS04,StepsStructDS05,StepsStructDS06,StepsStructDS07,StepsStructDS08)
%This function checks the first plotting matrix of SMK patients to
%determine which parameters are changed independently from the General
%Guidance force and stores them in a new matrix Y
%Inputs:
%plottingmatrix-matrix containing increments in parameter and variables
%ParamChangeInstances-all the different structs containing the change
%instances for all parameters and trials (one for every patient)
%Outputs:
%Y-new matrix containing only the increments for intances where the
%paramters are changed independently of the GGF.


%first check column 19(patient) then 18(trial) then 4(changeinstance)
fieldsSMK=fieldnames(VARplottingmatrix1);
for i=1:length(fieldsSMK)
    counter=0;
    for j=1:size((VARplottingmatrix1.(fieldsSMK{i,1})),1)
        if VARplottingmatrix1.(fieldsSMK{i,1})(j,19)==1
            n=1;
        elseif VARplottingmatrix1.(fieldsSMK{i,1})(j,19)==2
            n=2;
        elseif VARplottingmatrix1.(fieldsSMK{i,1})(j,19)==3
            n=3;
        elseif VARplottingmatrix1.(fieldsSMK{i,1})(j,19)==4
            n=4;
        elseif VARplottingmatrix1.(fieldsSMK{i,1})(j,19)==5
            n=5;
        elseif VARplottingmatrix1.(fieldsSMK{i,1})(j,19)==6
            n=6;
        elseif VARplottingmatrix1.(fieldsSMK{i,1})(j,19)==7
            n=7;
        end

        end
        switch n %every 'option' of the switch loop corresponds to a different patient
            case 1
                %check trialnumb
                trialnumb=VARplottingmatrix1.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructDS02.(trials),'General_GuidanceForce_pct')==1
                GGFchange=StepsStructDS02.(trials).General_GuidanceForce_pct;
                Intervals(:,1)=GGFchange-10;
                Intervals(:,2)=GGFchange+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix1.(fieldsSMK{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsSMK{i,1})=counter;
                            removerows.(fieldsSMK{i,1})(poscount.(fieldsSMK{i,1}))=j;
                        end
                    end
                end
            case 2
                %check trialnumb
                trialnumb=VARplottingmatrix1.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructDS03.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructDS03.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructDS03.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructDS03.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix1.(fieldsSMK{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsSMK{i,1})=counter;
                            removerows.(fieldsSMK{i,1})(poscount.(fieldsSMK{i,1}))=j;   
                        end
                    end
                end
            case 3 
                %check trialnumb
                trialnumb=VARplottingmatrix1.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructDS04.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructDS04.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructDS04.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructDS04.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix1.(fieldsSMK{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsSMK{i,1})=counter;
                            removerows.(fieldsSMK{i,1})(poscount.(fieldsSMK{i,1}))=j;
                        end
                    end
                end
            case 4
                trialnumb=VARplottingmatrix1.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructDS05.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructDS05.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructDS05.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructDS05.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix1.(fieldsSMK{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsSMK{i,1})=counter;
                            removerows.(fieldsSMK{i,1})(poscount.(fieldsSMK{i,1}))=j;
                        end
                    end
                end
             case 5
                trialnumb=VARplottingmatrix1.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructDS06.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructDS06.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructDS06.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructDS06.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix1.(fieldsSMK{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsSMK{i,1})=counter;
                            removerows.(fieldsSMK{i,1})(poscount.(fieldsSMK{i,1}))=j;
                        end
                    end
                end
             case 6
                trialnumb=VARplottingmatrix1.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructDS07.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructDS07.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructDS07.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructDS07.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix1.(fieldsSMK{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsSMK{i,1})=counter;
                            removerows.(fieldsSMK{i,1})(poscount.(fieldsSMK{i,1}))=j;
                        end
                    end
                end
              case 7
               trialnumb=VARplottingmatrix1.(fieldsSMK{i,1})(j,18);
                %compare changeinstance 
                trials=sprintf('trial%d',trialnumb);
                if isfield(StepsStructDS08.(trials),'General_GuidanceForce_pct')==1
                    GGFchange=StepsStructDS08.(trials).General_GuidanceForce_pct;
                    Intervals(:,1)=StepsStructDS08.(trials).General_GuidanceForce_pct-10;
                    Intervals(:,2)=StepsStructDS08.(trials).General_GuidanceForce_pct+10;
                    for k=1:size(Intervals,1)
                        if ismember(VARplottingmatrix1.(fieldsSMK{i,1})(j,21),(Intervals(k,1):1:Intervals(k,2)))==1
                            counter=counter+1;
                            poscount.(fieldsSMK{i,1})=counter;
                            removerows.(fieldsSMK{i,1})(poscount.(fieldsSMK{i,1}))=j;
                        end
                    end
                end
        end
        clear trials Intervals
    end

if exist('removerows','var') == 1
    fieldsr=fieldnames(removerows);
    for i=1:length(fieldsr)
        VARplottingmatrix1.(fieldsr{i,1})(removerows.(fieldsr{i,1}),:)=[];
    end
 end 
   
Y=VARplottingmatrix1;

