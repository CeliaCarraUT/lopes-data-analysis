function [Y]=removeEmptyFieldsParams(D,S)
%This function removes the empty fields inside the PARAMETER increment struct
%D-whole data set of a patient
%S-inital storing structure

%Y-storing structure without the empty fields

for j=1:length(D)
   trialchar{j,1}=sprintf('trial%d',j);
end
    

    for i=1:length(D)
        if all(structfun(@isempty, S.(trialchar{i,1})))==1
            S=rmfield(S,(trialchar{i,1}));
        elseif any(structfun(@isempty, S.(trialchar{i,1})))==1
            fieldnam=fieldnames(S.(trialchar{i,1}));
            for j=1:length(fieldnam)
                if isempty(S.(trialchar{i,1}).(fieldnam{j,1}))==1
                    S.(trialchar{i,1})= rmfield(S.(trialchar{i,1}),(fieldnam{j,1}));
                end
            end
        end
    end
Y=S;

end