function PowerPerTrialPlots(DataSet,Patients,n)

for j=1:length(DataSet)
   trials{j,1}=sprintf('trial%d',j);
end

for i=1:length(trials)
    time=(1:length(DataSet(i).Results.power_PX.total))';  
    totalpowermatrix.(trials{i,1})(:,1)=time;
    totalpowermatrix.(trials{i,1})(:,2)=DataSet(i).Results.power_PX.total;
    totalpowermatrix.(trials{i,1})(:,3)=DataSet(i).Results.power_PZ.total;
    totalpowermatrix.(trials{i,1})(:,4)=DataSet(i).Results.power_LHA.total;
    totalpowermatrix.(trials{i,1})(:,5)=DataSet(i).Results.power_LHF.total;
    totalpowermatrix.(trials{i,1})(:,6)=DataSet(i).Results.power_LKF.total;
    totalpowermatrix.(trials{i,1})(:,7)=DataSet(i).Results.power_RHA.total;
    totalpowermatrix.(trials{i,1})(:,8)=DataSet(i).Results.power_RHF.total;
    totalpowermatrix.(trials{i,1})(:,9)=DataSet(i).Results.power_RKF.total;
end

figure
[ha,pos]=tight_subplot(3,3,[.01 .03],[.1 .01],[.01 .01]);
sgtitle({'Total Power per Joint per trial' Patients{n,1}})
subplot(3,3,1,ha(1))%LHA
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,4))
    hold on
end
ylabel('LHA','FontWeight', 'bold')
subplot(3,3,4,ha(4))%LHF
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,5))
    hold on
end
ylabel('LHF','FontWeight', 'bold')
subplot(3,3,7,ha(7))%LKF
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,6))
    hold on
end
ylabel('LKF','FontWeight', 'bold')
subplot(3,3,2,ha(2))%RHA
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,7))
    hold on
end
ylabel('RHA','FontWeight', 'bold')
subplot(3,3,5,ha(5))%RHF
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,8))
    hold on
end
ylabel('RHF','FontWeight', 'bold')
subplot(3,3,8,ha(8))%RKF
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,9))
    hold on
end
ylabel('RKF','FontWeight', 'bold')
legend(trials)
set(legend,...
    'Position',[0.636414929388298 0.252125883117525 0.0821614595005909 0.0675747877766944])
subplot(3,3,3,ha(3))%PX
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,2))
    hold on
end
ylabel('PX','FontWeight', 'bold')
subplot(3,3,6,ha(6))%PZ
for i=1:length(trials)
    plot(totalpowermatrix.(trials{i,1})(:,1),totalpowermatrix.(trials{i,1})(:,3))
    hold on
end
ylabel('PZ','FontWeight', 'bold')
delete(ha(9))
[ax1,h1]=suplabel('Trials','x');
[ax2,h2]=suplabel('Total POwer (W)','y');
