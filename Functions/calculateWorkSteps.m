function Y = calculateWorkSteps(D)
%This function calculates the work of every step and stores it inside the
%'Results' field of the structure. 
%For each joint, a matrix is created where
%each row corresponds to one step. The first column is the total work,
%the second, the negative work and the thirsd the positive work.

%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%normalize the time vector
normtime=(D.data.time-D.data.time(1));
 
%First we divide the data in steps
X=diff(D.data.m_cont_stepCounter);
Z=find(X==1);
Z=[Z; length(D.data.m_cont_stepCounter)];

%calculate mean power for every step
for j=1:length(Z)-1
    %% PX - pelvis x direction
    %total power-column1
    Power_PX=D.data.m_cont_jointForcesMeasured_PX(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_PX(Z(j):Z(j+1)),0.01);
    D.Results.WorkBySteps_PX(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_PX);
    %negative-column2
    PXneg=Power_PX;
    for i=1:length(PXneg)
        if PXneg(i)>0
            PXneg(i)=0;
        end
    end
    D.Results.WorkBySteps_PX(j,2)=trapz(normtime(Z(j):Z(j+1)), PXneg);
    
    %positive-column3
    PXpos=Power_PX;
    for i=1:length(PXpos)
        if PXpos(i)<0
            PXpos(i)=0;
        end
    end
   D.Results.WorkBySteps_PX(j,3)=trapz(normtime(Z(j):Z(j+1)), PXpos);
   
   %% PZ - pelvis z direction
   %total 
   Power_PZ=D.data.m_cont_jointForcesMeasured_PZ(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_PZ(Z(j):Z(j+1)),0.01);
   D.Results.WorkBySteps_PZ(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_PZ);
   %negative
   PZneg=Power_PZ;
   for i=1:length(PZneg)
       if PZneg(i)>0
           PZneg(i)=0;
       end
   end
   D.Results.WorkBySteps_PZ(j,2)=trapz(normtime(Z(j):Z(j+1)), PZneg);
   %positive
   PZpos=Power_PZ;
   for i=1:length(PZpos)
       if PZpos(i)<0
           PZpos(i)=0;
       end
   end
   D.Results.WorkBySteps_PZ(j,3)=trapz(normtime(Z(j):Z(j+1)), PZpos);
   
   %% LHA - left hip abduction
   %total
   Power_LHA=D.data.m_cont_jointForcesMeasured_LHA(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_LHA(Z(j):Z(j+1)),0.01);
   D.Results.WorkBySteps_LHA(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_LHA);
   %negative
   LHAneg=Power_LHA;
   for i=1:length(LHAneg)
       if LHAneg(i)>0
           LHAneg(i)=0;
       end
   end
   D.Results.WorkBySteps_LHA(j,2)=trapz(normtime(Z(j):Z(j+1)), LHAneg);
   %positive
   LHApos=Power_LHA;
   for i=1:length(LHApos)
       if LHApos(i)<0
           LHApos(i)=0;
       end
   end
   D.Results.WorkBySteps_LHA(j,3)=trapz(normtime(Z(j):Z(j+1)), LHApos);
   
   %% LHF - left hip flexion
   %total
   Power_LHF=D.data.m_cont_jointForcesMeasured_LHF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_LHF(Z(j):Z(j+1)),0.01);
   D.Results.WorkBySteps_LHF(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_LHF);
   %negative
   LHFneg=Power_LHF;
   for i=1:length(LHFneg)
       if LHFneg(i)>0
           LHFneg(i)=0;
       end
   end
   D.Results.WorkBySteps_LHF(j,2)=trapz(normtime(Z(j):Z(j+1)), LHFneg);
   %positive
   LHFpos=Power_LHF;
   for i=1:length(LHFpos)
       if LHFpos(i)<0
           LHFpos(i)=0;
       end
   end
   D.Results.WorkBySteps_LHF(j,3)=trapz(normtime(Z(j):Z(j+1)), LHFpos);
   
   %% LKF - left knee flexion
   Power_LKF=D.data.m_cont_jointForcesMeasured_LKF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_LKF(Z(j):Z(j+1)),0.01);
   D.Results.WorkBySteps_LKF(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_LKF);
   %negative
   LKFneg=Power_LKF;
   for i=1:length(LKFneg)
       if LKFneg(i)>0
           LKFneg(i)=0;
       end
   end
   D.Results.WorkBySteps_LKF(j,2)=trapz(normtime(Z(j):Z(j+1)), LKFneg);
   %positive
   LKFpos=Power_LKF;
   for i=1:length(LKFpos)
       if LKFpos(i)<0
           LKFpos(i)=0;
       end
   end
   D.Results.WorkBySteps_LKF(j,3)=trapz(normtime(Z(j):Z(j+1)), LKFpos);
   
   %% RHA - right hip abduction
   Power_RHA=D.data.m_cont_jointForcesMeasured_RHA(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_RHA(Z(j):Z(j+1)),0.01);
   D.Results.WorkBySteps_RHA(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_RHA);
   %negative
   RHAneg=Power_RHA;
   for i=1:length(RHAneg)
       if RHAneg(i)>0
           RHAneg(i)=0;
       end
   end
   D.Results.WorkBySteps_RHA(j,2)=trapz(normtime(Z(j):Z(j+1)), RHAneg);
   %positive
   RHApos=Power_RHA;
   for i=1:length(RHApos)
       if RHApos(i)<0
           RHApos(i)=0;
       end
   end
   D.Results.WorkBySteps_RHA(j,3)=trapz(normtime(Z(j):Z(j+1)), RHApos);
   
   %% RHF - right hip flexion
   Power_RHF=D.data.m_cont_jointForcesMeasured_RHF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_RHF(Z(j):Z(j+1)),0.01);
   D.Results.WorkBySteps_RHF(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_RHF);
   %negative
   RHFneg=Power_RHF;
   for i=1:length(RHFneg)
       if RHFneg(i)>0
           RHFneg(i)=0;
       end
   end
   D.Results.WorkBySteps_RHF(j,2)=trapz(normtime(Z(j):Z(j+1)), RHFneg);
   %positive
   RHFpos=Power_RHF;
   for i=1:length(RHFpos)
       if RHFpos(i)<0
           RHFpos(i)=0;
       end
   end
   D.Results.WorkBySteps_RHF(j,3)=trapz(normtime(Z(j):Z(j+1)), RHFpos);
   
   %% RKF - right knee flexion
   Power_RKF=D.data.m_cont_jointForcesMeasured_RKF(Z(j):Z(j+1)).*gradient(D.data.m_cont_jointAngles_RKF(Z(j):Z(j+1)),0.01);
   D.Results.WorkBySteps_RKF(j,1)=trapz(normtime(Z(j):Z(j+1)), Power_RKF);
   %negative
   RKFneg=Power_RKF;
   for i=1:length(RKFneg)
       if RKFneg(i)>0
           RKFneg(i)=0;
       end
   end
   D.Results.WorkBySteps_RKF(j,2)=trapz(normtime(Z(j):Z(j+1)), RKFneg);
   %positive
   RKFpos=Power_RKF;
   for i=1:length(RKFpos)
       if RKFpos(i)<0
           RKFpos(i)=0;
       end
   end
   D.Results.WorkBySteps_RKF(j,3)=trapz(normtime(Z(j):Z(j+1)), RKFpos);
   
end

Y=D;