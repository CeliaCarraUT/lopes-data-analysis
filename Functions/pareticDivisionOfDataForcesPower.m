function [Rparetic,Rnonparetic,Lparetic,Lnonparetic]=pareticDivisionOfDataForcesPower(plottingmatrix)
%This function divides the data contained in the plottingmatrix into 4
%separate structures taking into account the paretic side of the patient.
%Input:
%plottingmatrix-matrix containing the increments in parameters and increments in interaction forces/power for the different joints
%Outputs:
%Rparetic,Rnonparetic-structures contaning the data from patients with paretic R side (Rparetic contains the data of the R parameters and R joints and Rnonparetic the L parameters and L joints).
%Lparetic,Lnonparetic-structures contaning the data from patients with paretic L side (Lparetic contains the data of the L parameters and L joints and Lnonparetic the R parameters and R joints).

fields=fieldnames(plottingmatrix);
L=0;
R=0;
Lcounter=0;
Rcounter=0;
for i=1:length(fields)
    for j=1:size(plottingmatrix.(fields{i,1}),1)
        if plottingmatrix.(fields{i,1})(j,14)==5 || plottingmatrix.(fields{i,1})(j,14)==8 || plottingmatrix.(fields{i,1})(j,14)==10 || plottingmatrix.(fields{i,1})(j,14)==12%left is paretic leg 
            Lcounter=Lcounter+1;
            L=L+1;
            Lparetic.(fields{i,1})(Lcounter,1)=plottingmatrix.(fields{i,1})(j,1); %initial param value
            Lparetic.(fields{i,1})(Lcounter,2)=plottingmatrix.(fields{i,1})(j,2); %final param value
            Lparetic.(fields{i,1})(Lcounter,3)=plottingmatrix.(fields{i,1})(j,3); %increment
            Lparetic.(fields{i,1})(Lcounter,4)=plottingmatrix.(fields{i,1})(j,5); %PX
            Lparetic.(fields{i,1})(Lcounter,5)=plottingmatrix.(fields{i,1})(j,6); %PZ
            Lparetic.(fields{i,1})(Lcounter,6)=plottingmatrix.(fields{i,1})(j,7); %LHA
            Lparetic.(fields{i,1})(Lcounter,7)=plottingmatrix.(fields{i,1})(j,8); %LHF
            Lparetic.(fields{i,1})(Lcounter,8)=plottingmatrix.(fields{i,1})(j,9); %LKF
            Lparetic.(fields{i,1})(Lcounter,9)=plottingmatrix.(fields{i,1})(j,14);%patient #
            Lparetic.(fields{i,1})(Lcounter,10)=plottingmatrix.(fields{i,1})(j,15);%colormap vector
            Lparetic.(fields{i,1})(Lcounter,11)=plottingmatrix.(fields{i,1})(j,13);%trial n
            %L-paretic right variables
            Lnonparetic.(fields{i,1})(Lcounter,1)=plottingmatrix.(fields{i,1})(j,1); %initial param value
            Lnonparetic.(fields{i,1})(Lcounter,2)=plottingmatrix.(fields{i,1})(j,2); %final param value
            Lnonparetic.(fields{i,1})(Lcounter,3)=plottingmatrix.(fields{i,1})(j,3);%increment
            Lnonparetic.(fields{i,1})(Lcounter,4)=plottingmatrix.(fields{i,1})(j,5); %PX
            Lnonparetic.(fields{i,1})(Lcounter,5)=plottingmatrix.(fields{i,1})(j,6); %PZ
            Lnonparetic.(fields{i,1})(Lcounter,6)=plottingmatrix.(fields{i,1})(j,10); %RHA
            Lnonparetic.(fields{i,1})(Lcounter,7)=plottingmatrix.(fields{i,1})(j,11); %RHF
            Lnonparetic.(fields{i,1})(Lcounter,8)=plottingmatrix.(fields{i,1})(j,12); %RKF
            Lnonparetic.(fields{i,1})(Lcounter,9)=plottingmatrix.(fields{i,1})(j,14);%patient #
            Lnonparetic.(fields{i,1})(Lcounter,10)=plottingmatrix.(fields{i,1})(j,15);%colormap vector
            Lnonparetic.(fields{i,1})(Lcounter,11)=plottingmatrix.(fields{i,1})(j,13);%trial n
        elseif plottingmatrix.(fields{i,1})(j,14)==1 || plottingmatrix.(fields{i,1})(j,14)==2 || plottingmatrix.(fields{i,1})(j,14)==3 || plottingmatrix.(fields{i,1})(j,14)==4 || plottingmatrix.(fields{i,1})(j,14)==6 || plottingmatrix.(fields{i,1})(j,14)==7 || plottingmatrix.(fields{i,1})(j,14)==9 ||plottingmatrix.(fields{i,1})(j,14)==11%the other way round;paretic right, non paretic left
            R=R+1;
            Rcounter=Rcounter+1;
            Rparetic.(fields{i,1})(Rcounter,1)=plottingmatrix.(fields{i,1})(j,1); %initial param value
            Rparetic.(fields{i,1})(Rcounter,2)=plottingmatrix.(fields{i,1})(j,2); %final param value
            Rparetic.(fields{i,1})(Rcounter,3)=plottingmatrix.(fields{i,1})(j,3);%increment
            Rparetic.(fields{i,1})(Rcounter,4)=plottingmatrix.(fields{i,1})(j,5); %PX
            Rparetic.(fields{i,1})(Rcounter,5)=plottingmatrix.(fields{i,1})(j,6); %PZ
            Rparetic.(fields{i,1})(Rcounter,6)=plottingmatrix.(fields{i,1})(j,10); %RHA
            Rparetic.(fields{i,1})(Rcounter,7)=plottingmatrix.(fields{i,1})(j,11); %RHF
            Rparetic.(fields{i,1})(Rcounter,8)=plottingmatrix.(fields{i,1})(j,12); %RKF
            Rparetic.(fields{i,1})(Rcounter,9)=plottingmatrix.(fields{i,1})(j,14);%patient #
            Rparetic.(fields{i,1})(Rcounter,10)=plottingmatrix.(fields{i,1})(j,15);%colormap vector
            Rparetic.(fields{i,1})(Rcounter,11)=plottingmatrix.(fields{i,1})(j,13);%trial n
            %nonparetic
            Rnonparetic.(fields{i,1})(Rcounter,1)=plottingmatrix.(fields{i,1})(j,1); %initial param value
            Rnonparetic.(fields{i,1})(Rcounter,2)=plottingmatrix.(fields{i,1})(j,2); %final param value
            Rnonparetic.(fields{i,1})(Rcounter,3)=plottingmatrix.(fields{i,1})(j,3); %increment
            Rnonparetic.(fields{i,1})(Rcounter,4)=plottingmatrix.(fields{i,1})(j,5); %PX
            Rnonparetic.(fields{i,1})(Rcounter,5)=plottingmatrix.(fields{i,1})(j,6); %PZ
            Rnonparetic.(fields{i,1})(Rcounter,6)=plottingmatrix.(fields{i,1})(j,7); %LHA
            Rnonparetic.(fields{i,1})(Rcounter,7)=plottingmatrix.(fields{i,1})(j,8); %LHF
            Rnonparetic.(fields{i,1})(Rcounter,8)=plottingmatrix.(fields{i,1})(j,9); %LKF
            Rnonparetic.(fields{i,1})(Rcounter,9)=plottingmatrix.(fields{i,1})(j,14);%patient #
            Rnonparetic.(fields{i,1})(Rcounter,10)=plottingmatrix.(fields{i,1})(j,15);%colormap vector
            Rnonparetic.(fields{i,1})(Rcounter,11)=plottingmatrix.(fields{i,1})(j,13);%trial n
        end
    end
    Rcounter=0;
    Lcounter=0;
end

if R>0
fieldsRparetic=fieldnames(Rparetic);
    for i=1:length(fieldsRparetic)
      Rparetic.(fieldsRparetic{i,1})=Rparetic.(fieldsRparetic{i,1})(all(Rparetic.(fieldsRparetic{i,1}),2),:);%remove zero rows
    %    paretic.(fieldsparetic{i,1})=unique(sort(paretic.(fieldsparetic{i,1}),2),'rows'); %remove repeated rows
      Rnonparetic.(fieldsRparetic{i,1})=Rnonparetic.(fieldsRparetic{i,1})(all(Rnonparetic.(fieldsRparetic{i,1}),2),:);%remove zero rows
    %    nonparetic.(fieldsparetic{i,1})=unique(sort(nonparetic.(fieldsparetic{i,1}),2),'rows'); %remove repeated rows
    end
else
    Rparetic=0;
    Rnonparetic=0;
end


if L>0
    fieldsLparetic=fieldnames(Lparetic);
     for i=1:length(fieldsLparetic)
        Lparetic.(fieldsLparetic{i,1})=Lparetic.(fieldsLparetic{i,1})(all(Lparetic.(fieldsLparetic{i,1}),2),:);%remove zero rows
    %    Lparetic.(fieldsLparetic{i,1})=unique(sort(Lparetic.(fieldsLparetic{i,1}),2),'rows'); %remove repeated rows
        Lnonparetic.(fieldsLparetic{i,1})=Lnonparetic.(fieldsLparetic{i,1})(all(Lnonparetic.(fieldsLparetic{i,1}),2),:);%remove zero rows
    %    Lnonparetic.(fieldsLparetic{i,1})=unique(sort(Lnonparetic.(fieldsLparetic{i,1}),2),'rows'); %remove repeated rows
     end
else
    Lparetic=0;
    Lnonparetic=0;
end


end