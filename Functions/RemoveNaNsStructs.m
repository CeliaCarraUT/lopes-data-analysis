function [Y]=RemoveNaNsStructs(DataStruct)
fields1=fieldnames(DataStruct);
fields3=fieldnames(DataStruct.(fields1{1,1}));

for i=1:length(fields1)
    fields2=fieldnames(DataStruct.(fields1{i,1}));
    fields3=fieldnames(DataStruct.(fields1{i,1}).(fields2{1,1}));
    for j=1:length(fields2)
        for k=1:length(fields3)
            if length(DataStruct.(fields1{i,1}).(fields2{j,1}).(fields3{k,1}))>1
                %remove first NaN
                DataStruct.(fields1{i,1}).(fields2{j,1}).(fields3{k,1})(1)=[];
            end
        end
    end
end
Y=DataStruct;
         
                    
