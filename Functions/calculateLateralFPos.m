function[Y]= calculateLateralFPos(D)
%Lateral foot position :hip abduction angle- angle at 100% of gait cycle
%This function outputs the lateral foot position of all the steps in a trial(stored in input struct D) and
%stores them inside the original stucture (in the 'Results' field). The lateral foot position value is calculated as the absolute value of 
%the lateral foot position of the reference joint pattern and the measured
%joint pattern.
%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%kinematic phase:
% 1-Double stance, left in front
% 2-Right swing
% 3-Double stance, right in front
% 4-Left swing

%MEASURED
%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
%prepositioning: knee angle just before heel strike

count=0;
count2=0;
Rsteps=[];
Lsteps=[];

%first we select the section of the data we want using the kinematic phase
for i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==2
        count=count+1;
        Rsteps(count,1)=(A-1)+i;
    elseif D.data.m_cont_kinematicPhase((A-1)+i)==4
        count2=count2+1;
        Lsteps(count2,1)=(A-1)+i;
    end
end
clear count count2

%Right
if isempty(Rsteps)==1
    RLFootPosM=0;
else
    RD=diff(Rsteps);
    count=0;
    RswingInstances=[];

    for i=1:length(RD)
        if RD(i)~=1
            count=count+1;
            RswingInstances(count,1)=Rsteps(i);
        end
    end
    RswingInstances(count+1,1)=Rsteps(end);
     for i=1:length(RswingInstances)
        RLFootPosM(i,1)=D.data.m_cont_jointAngles_RHA(RswingInstances(i));
     end
end

%left
if isempty(Lsteps)
    LFootPosM=0;
else
    LD=diff(Lsteps);
    count2=0;
    LswingInstances=[];

    for i=1:length(LD)
        if LD(i)~=1
            count2=count2+1;
            LswingInstances(count2,1)=Lsteps(i);
        end
    end
    LswingInstances(count2+1,1)=Lsteps(end);

    for i=1:length(LswingInstances)
        LFootPosM(i,1)=D.data.m_cont_jointAngles_LHA(LswingInstances(i));
    end
end

%REFERENCE
%check the instance of the first step
B=find(D.data.r_cont_stepCounter==3,1);
lastStepr=find(D.data.r_cont_stepCounter==D.data.r_cont_stepCounter(end)-4,1);
%prepositioning: knee angle just before heel strike
if isempty(lastStepr)==1
    lastStepr=lastStep;
end

count=0;
count2=0;
Rsteps=[];
Lsteps=[];

%first we select the section of the data we want using the kinematic phase
for i=1:length(D.data.r_cont_kinematicPhase(B:lastStepr))
    if D.data.r_cont_kinematicPhase((B-1)+i)==2
        count=count+1;
        Rsteps(count,1)=(B-1)+i;
    elseif D.data.r_cont_kinematicPhase((B-1)+i)==4
        count2=count2+1;
        Lsteps(count2,1)=(B-1)+i;
    end
end
clear count count2

%Right
if isempty(Rsteps)==1
    RLFootPosR=0;
else
    RD=diff(Rsteps);
    count=0;
    RswingInstancesr=[];

    for i=1:length(RD)
        if RD(i)~=1
            count=count+1;
            RswingInstancesr(count,1)=Rsteps(i);
        end
    end
    RswingInstancesr(count+1,1)=Rsteps(end);
     for i=1:length(RswingInstancesr)
        RLFootPosR(i,1)=D.data.r_cont_jointAngles_RHA(RswingInstancesr(i));
     end
end

%left
if isempty(Lsteps)
    LFootPosR=0;
else
    LD=diff(Lsteps);
    count2=0;
    LswingInstancesr=[];

    for i=1:length(LD)
        if LD(i)~=1
            count2=count2+1;
            LswingInstancesr(count2,1)=Lsteps(i);
        end
    end
    LswingInstancesr(count2+1,1)=Lsteps(end);

    for i=1:length(LswingInstancesr)
        LFootPosR(i,1)=D.data.r_cont_jointAngles_LHA(LswingInstancesr(i));
    end
end

%check the legth of the vector and add NaN values if needed
%left
if length(LFootPosM)>length(LFootPosR)
    %check if its the first or the last step
        %check the first and last peaks
    if  abs(LswingInstancesr(1)-LswingInstances(1))>100%first element is an extra peak in measured
        LFootPosR=[NaN;LFootPosR];
    else  %last element is an extra peak
        LFootPosR(end+1)=NaN;
    end
end

if length(LFootPosR)>length(LFootPosM)
    %check if its the first or the last step
        %check the first and last peaks
    if  abs(LswingInstancesr(1)-LswingInstances(1))>100%first element is an extra peak in measured
        LFootPosM=[NaN;LFootPosM];
    else  %last element is an extra peak
        LFootPosM(end+1)=NaN;
    end
end

%right
if length(RLFootPosM)>length(RLFootPosR)
    %check if its the first or the last step
        %check the first and last peaks
    if  abs(RswingInstancesr(1)-RswingInstances(1))>100%first element is an extra peak in measured
        RLFootPosR=[NaN;RLFootPosR];
    else  %last element is an extra peak
        RLFootPosR(end+1)=NaN;
    end
end

if length(RLFootPosR)>length(RLFootPosM)
    %check if its the first or the last step
        %check the first and last peaks
    if  abs(RswingInstancesr(1)-RswingInstances(1))>100%first element is an extra peak in measured
        RLFootPosM=[NaN;RLFootPosM];
    else  %last element is an extra peak
        RLFootPosM(end+1)=NaN;
    end
end

 
 if length(LFootPosM)<length(LFootPosR)
    while length(LFootPosM)<length(LFootPosR)
     LFootPosR(end)=[];
    end
 elseif length(LFootPosM)>length(LFootPosR)
    while length(LFootPosM)>length(LFootPosR)
     LFootPosM(end)=[];
    end
 end
 
 if length(RLFootPosM)<length(RLFootPosR)
    while length(RLFootPosM)<length(RLFootPosR)
      RLFootPosR(end)=[];
    end
 elseif length(RLFootPosM)>length(RLFootPosR)
    while length(RLFootPosM)>length(RLFootPosR)
     RLFootPosM(end)=[];
    end
 end

%compute the difference
D.Results.LateralFootPos.Lsteps=abs(LFootPosR-LFootPosM);
D.Results.LateralFootPos.Rsteps=abs(RLFootPosR-RLFootPosM);

Y=D;
