function [Y] = calculatePrepositioning(D)
%Prepositioning:knee flexion angle at 100% of gait cycle
%This function outputs the prepositioning of all the steps in a trial(stored in input struct D) and
%stores them inside the original stucture (in the 'Results' field).

%Both the measured angles (field 'Prepositioning')  and the difference with the reference trajectory
%are stored (field 'PrepositioningDiff'). Steps are also divided into right and
%left.

%%% NOTE:Run function 'addResultFieldStruct'before running this one
%Inputs:
%D-Single trial of the signals structure (ex. D(#), where # is the trial number)
%Outputs: Y-trial including the computed variable

%phase:
% 1-Double stance, left in front
% 2-Right swing
% 3-Double stance, right in front
% 4-Left swing

%check the instance of the first step
A=find(D.data.m_cont_stepCounter==3,1);
lastStep=find(D.data.m_cont_stepCounter==D.data.m_cont_stepCounter(end)-4,1);
%prepositioning: knee angle just before heel strike

count=0;
count2=0;
Rsteps=[];
Lsteps=[];

for i=1:length(D.data.m_cont_kinematicPhase(A:lastStep))
    if D.data.m_cont_kinematicPhase((A-1)+i)==2
        count=count+1;
        Rsteps(count,1)=(A-1)+i;
    elseif D.data.m_cont_kinematicPhase((A-1)+i)==4
        count2=count2+1;
        Lsteps(count2,1)=(A-1)+i;
    end
end

% if any(D.data.m_cont_kinematicPhase==4)==0
%     Lsteps=[];
% end
% 
% if any(D.data.m_cont_kinematicPhase==2)==0
%     Rsteps=[];
% end
clear count count2

%Right
if isempty(Rsteps)==1
    Rprepositioning=0;
else
    RD=diff(Rsteps);
    count=0;
    RswingInstances=[];

    for i=1:length(RD)
        if RD(i)~=1
            count=count+1;
            RswingInstances(count,1)=Rsteps(i);
        end
    end
    RswingInstances(count+1,1)=Rsteps(end);

    for i=1:length(RswingInstances)
        Rprepositioning(i,1)=D.data.m_cont_jointAngles_RKF(RswingInstances(i));
    end
end

%Left
if isempty(Lsteps)
    Lprepositioning=0;
else
    LD=diff(Lsteps);
    count2=0;
    LswingInstances=[];

    for i=1:length(LD)
        if LD(i)~=1
            count2=count2+1;
            LswingInstances(count2,1)=Lsteps(i);
        end
    end
    LswingInstances(count2+1,1)=Lsteps(end);

    for i=1:length(LswingInstances)
        Lprepositioning(i,1)=D.data.m_cont_jointAngles_LKF(LswingInstances(i));
    end
end

D.Results.Prepositioning.Rsteps=Rprepositioning;
D.Results.Prepositioning.Lsteps=Lprepositioning;


%% check the angle profiles and use find peaks to substract from the reference

% for i=1:length(D.data.r_cont_kinematicPhase(A:end))
%     if D.data.r_cont_kinematicPhase((A-1)+i)==2
%         count=count+1;
%         RstepsRef(count,1)=(A-1)+i;
%     elseif D.data.r_cont_kinematicPhase((A-1)+i)==4
%         count2=count2+1;
%         LstepsRef(count2,1)=(A-1)+i;
%     end
% end
% 
% 
% [pksr,locsr]=findpeaks(D.data.r_cont_jointAngles_LKF);
% [Lpksm3,Llocsm3]=findpeaks(-D.data.m_cont_jointAngles_LKF);
% [Lpksr3,Llocsr3]=findpeaks(-D.data.r_cont_jointAngles_LKF);
% Llocsm3=Llocsm3(Lpksm3>-0.4);
% Lpksm3=Lpksm3(Lpksm3>-0.4);
% Llocsr3=Llocsr3(Lpksr3>-0.4);
% Lpksr3=Lpksr3(Lpksr3>-0.4);
% % 
% LDM2=diff(Llocsm3);
% doublepeaks2=find(LDM2<50);
% doublepeaks2=doublepeaks2+1;
% 
%     for i=1:length(doublepeaks2)
%         if i==1
%             Lpksm3(doublepeaks2(i))=[];
%             Llocsm3(doublepeaks2(i))=[];
%         else
%             Lpksm3(doublepeaks2(i)-i+1)=[];
%             Llocsm3(doublepeaks2(i)-i+1)=[];
%         end
%     end
% 
% LDM3=diff(Llocsr3);
% doublepeaks=find(LDM3<50);
% doublepeaks=doublepeaks+1;
% 
%     for i=1:length(doublepeaks)
%         if i==1
%             Lpksr3(doublepeaks(i))=[];
%             Llocsr3(doublepeaks(i))=[];
%         else
%             Lpksr3(doublepeaks(i)-i+1)=[];
%             Llocsr3(doublepeaks(i)-i+1)=[];
%         end
%     end
% 
% 
% if pksr(1)>0.5
%     Llocsm3(1:2:end)=[];
%     Lpksm3(1:2:end)=[];
%     Llocsr3(1:2:end)=[];
%     Lpksr3(1:2:end)=[];
% else
%     Llocsm3(2:2:end)=[];
%     Lpksm3(2:2:end)=[];
%     Llocsr3(2:2:end)=[];
%     Lpksr3(2:2:end)=[];
% end
% 
% % %left-measured
% % counter=0;
% % for i=1:length(Llocsm3)
% %     if ismember(Llocsm3(i),Lsteps)==0 %if the peak found is not in the right kinematic phase, delete
% %        counter=counter+1;
% %        nopeaks(counter,1)=i;
% %     end
% % end
% %        
% % for i=1:length(nopeaks)
% %     if i==1
% %       Lpksm3(nopeaks(i))=[];
% %       Llocsm3(nopeaks(i))=[];   
% %     else
% %       Lpksm3(nopeaks(i)-i+1)=[];
% %       Llocsm3(nopeaks(i)-i+1)=[];
% %     end
% % end
% % %reference
% % counter=0;
% % for i=1:length(Llocsr3)
% %     if ismember(Llocsr3(i),LstepsRef)==0 %if the peak found is not in the right kinematic phase, delete
% %        counter=counter+1;
% %        nopeaksref(counter,1)=i;
% %     end
% % end
% %        
% % for i=1:length(nopeaksref)
% %     if i==1
% %       Lpksr3(nopeaksref(i))=[];
% %       Llocsr3(nopeaksref(i))=[];   
% %     else
% %       Lpksr3(nopeaksref(i)-i+1)=[];
% %       Llocsr3(nopeaksref(i)-i+1)=[];
% %     end
% % end
% 
% 
% 
% %right
% [Rpksr,Rlocsr]=findpeaks(D.data.r_cont_jointAngles_RKF);
% [Rpksm3,Rlocsm3]=findpeaks(-D.data.m_cont_jointAngles_RKF);
% [Rpksr3,Rlocsr3]=findpeaks(-D.data.r_cont_jointAngles_RKF);
% Rlocsm3=Rlocsm3(Rpksm3>-0.4);
% Rpksm3=Rpksm3(Rpksm3>-0.4);
% Rlocsr3=Rlocsr3(Rpksr3>-0.4);
% Rpksr3=Rpksr3(Rpksr3>-0.4);
% % 
%  %double peaks 
% RDM2=diff(Rlocsm3);
% doublepeaks3=find(RDM2<50);
% doublepeaks3=doublepeaks3+1;
% 
%     for i=1:length(doublepeaks3)
%         if i==1
%             Rpksm3(doublepeaks3(i))=[];
%             Rlocsm3(doublepeaks3(i))=[];
%         else
%             Rpksm3(doublepeaks3(i)-i+1)=[];
%             Rlocsm3(doublepeaks3(i)-i+1)=[];
%         end
%     end
%     
% RDM3=diff(Rlocsr3);
% doublepeaks4=find(RDM3<50);
% doublepeaks4=doublepeaks4+1;
% 
%     for i=1:length(doublepeaks4)
%         if i==1
%             Rpksr3(doublepeaks4(i))=[];
%             Rlocsr3(doublepeaks4(i))=[];
%         else
%             Rpksr3(doublepeaks4(i)-i+1)=[];
%             Rlocsr3(doublepeaks4(i)-i+1)=[];
%         end
%     end
% 
% 
% 
% if Rpksr(1)>0.5
%     Rlocsm3(1:2:end)=[];
%     Rpksm3(1:2:end)=[];
%     Rlocsr3(1:2:end)=[];
%     Rpksr3(1:2:end)=[];
% else
%     Rlocsm3(2:2:end)=[];
%     Rpksm3(2:2:end)=[];
%     Rlocsr3(2:2:end)=[];
%     Rpksr3(2:2:end)=[];
% end 
% 
% %right-measured
% % counter=0;
% % for i=1:length(Rlocsm3)
% %     if ismember(Rlocsm3(i),Rsteps)==0 %if the peak found is not in the right kinematic phase, delete
% %        counter=counter+1;
% %        Rnopeaks(counter,1)=i;
% %     end
% % end
% %        
% % for i=1:length(Rnopeaks)
% %     if i==1
% %       Rpksm3(Rnopeaks(i))=[];
% %       Rlocsm3(Rnopeaks(i))=[];   
% %     else
% %       Rpksm3(Rnopeaks(i)-i+1)=[];
% %       Rlocsm3(Rnopeaks(i)-i+1)=[];
% %     end
% % end
% % %reference
% % counter=0;
% % for i=1:length(Rlocsr3)
% %     if ismember(Rlocsr3(i),RstepsRef)==0 %if the peak found is not in the right kinematic phase, delete
% %        counter=counter+1;
% %        Rnopeaksref(counter,1)=i;
% %     end
% % end
% %        
% % for i=1:length(Rnopeaksref)
% %     if i==1
% %       Rpksr3(Rnopeaksref(i))=[];
% %       Rlocsr3(Rnopeaksref(i))=[];   
% %     else
% %       Rpksr3(Rnopeaksref(i)-i+1)=[];
% %       Rlocsr3(Rnopeaksref(i)-i+1)=[];
% %     end
% % end

%%  use method one but also with the reference trajectory
%check the instance of the first step
B=find(D.data.r_cont_stepCounter==3,1);
lastStepr=find(D.data.r_cont_stepCounter==D.data.r_cont_stepCounter(end)-4,1);
if isempty(lastStepr)==1
    lastStepr=lastStep;
end

count=0;
count2=0;

for i=1:length(D.data.r_cont_kinematicPhase(B:lastStepr))
    if D.data.r_cont_kinematicPhase((B-1)+i)==2
        count=count+1;
        Rstepsr(count,1)=(B-1)+i;
    elseif D.data.r_cont_kinematicPhase((B-1)+i)==4
        count2=count2+1;
        Lstepsr(count2,1)=(B-1)+i;
    end
end

if any(D.data.r_cont_kinematicPhase==4)==0
    Lstepsr=[];
end

if any(D.data.r_cont_kinematicPhase==2)==0
    Rstepsr=[];
end
clear count count2

%Right
if isempty(Rstepsr)==1
    Rprepositioningr=0;
else
    RDr=diff(Rstepsr);
    count=0;
    RswingInstancesr=[];

    for i=1:length(RDr)
        if RDr(i)~=1
            count=count+1;
            RswingInstancesr(count,1)=Rstepsr(i);
        end
    end
    RswingInstancesr(count+1,1)=Rstepsr(end);

    for i=1:length(RswingInstancesr)
        Rprepositioningr(i,1)=D.data.r_cont_jointAngles_RKF(RswingInstancesr(i));
    end
end

%Left
if isempty(Lstepsr)
    Lprepositioningr=0;
else
    LDr=diff(Lstepsr);
    count2=0;
    LswingInstancesr=[];

    for i=1:length(LDr)
        if LDr(i)~=1
            count2=count2+1;
            LswingInstancesr(count2,1)=Lstepsr(i);
        end
    end
    LswingInstancesr(count2+1,1)=Lstepsr(end);

    for i=1:length(LswingInstancesr)
        Lprepositioningr(i,1)=D.data.r_cont_jointAngles_LKF(LswingInstancesr(i));
    end
end

%check length of vectors
%right
if length(Rprepositioningr)>length(Rprepositioning)
    %check first and last instances
%   RswingInstancesr(end)-RswingInstances(end)>100
    Rprepositioningr(end)=[];
elseif length(Rprepositioning)>length(Rprepositioningr)
    Rprepositioning(end)=[];
end

%left
if length(Lprepositioningr)>length(Lprepositioning)
    %check first and last instances
%   LswingInstancesr(end)-LswingInstances(end)>100
    Lprepositioningr(end)=[];
elseif length(Lprepositioning)>length(Lprepositioningr)
    Lprepositioning(end)=[];
end

 
 if length(Lprepositioning)<length(Lprepositioningr)
    while length(Lprepositioning)<length(Lprepositioningr)
     Lprepositioningr(end)=[];
    end
 elseif length(Lprepositioning)>length(Lprepositioningr)
    while length(Lprepositioning)>length(Lprepositioningr)
     Lprepositioning(end)=[];
    end
 end
 
 if length(Rprepositioning)<length(Rprepositioningr)
    while length(Rprepositioning)<length(Rprepositioningr)
      Rprepositioningr(end)=[];
    end
 elseif length(Rprepositioning)>length(Rprepositioningr)
    while length(Rprepositioning)>length(Rprepositioningr)
     Rprepositioning(end)=[];
    end
 end

D.Results.PrepositioningDiff.Lsteps=Lprepositioningr-Lprepositioning;
D.Results.PrepositioningDiff.Rsteps=Rprepositioningr-Rprepositioning;

Y=D;
end