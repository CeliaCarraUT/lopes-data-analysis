function [Y]=findZerosMatrices(plottingmatrix)
%This function finds the zero instances in the plotting matrices and
%replaces them by NaN values (for easier removal of the empty content rows
%of the matrices-rows corresponding to the changes in parameters that did
%not last long enough)and removes empty fields (if any)
%Input: plottingmatrix-(struct containing the increments in parameters and
%corresponding increments in variables)
%Output: Y-plottingmatrix without zeros

%1-remove empty fields
fields=fieldnames(plottingmatrix);
 for i=1:length(fields)
     if isempty(plottingmatrix.(fields{i,1}))==1
         plottingmatrix=rmfield(plottingmatrix,(fields{i,1}));
     end
 end

fields=fieldnames(plottingmatrix);
indexes=5:15;
%2-change zeros
for i=1:length(fields)
    X=plottingmatrix.(fields{i,1})(:,indexes);
    [row,col]=find(X==0);
    for j=1:length (row)
        X(row(j),col(j))=NaN;
    end
    plottingmatrix.(fields{i,1})(:,indexes)=X;
end
Y= plottingmatrix;

