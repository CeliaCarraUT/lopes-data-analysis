function [Y]=calculateForceIncrements(ForceValues,vector1, vector2)
%This function calculates the increments in interaction forces (for one change in
%parameter)
%Inputs:
%ForceValues-vector containing the calculated variable for the whole trial
%Initial&finalSteps-margins on what sections of 'Parameter' to consider
%(steps before and after the parameter change)
%changeStep-step at which the change in parameter occurs
%Outputs:
%Y-VariableIncrement


ValuesBefore=(ForceValues(vector1));
ValuesAfter=(ForceValues(vector2));

%remove outliers
ValuesBefore=RemoveOutliersData(ValuesBefore);
ValuesAfter=RemoveOutliersData(ValuesAfter);

%do the RMS
mBefore=rms(ValuesBefore);
mAfter=rms(ValuesAfter);

%take into account steps of the adequate leg (check kinematic phase)

%compute the increment
Y=mAfter-mBefore;