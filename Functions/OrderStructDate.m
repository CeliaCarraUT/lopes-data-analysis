function [OrderedStruct]=OrderStructDate(Struct)

fields=fieldnames(Struct);
cell=struct2cell(Struct);
sz=size(cell);  
% Convert to a matrix
cell=reshape(cell,sz(1),[]);
% Make each field a column
cell=cell';  
% Sort by first field "name"
cell = sortrows(cell, 1);

% Put back into original cell array format
cell=reshape(cell',sz);
% Convert to Struct
OrderedStruct =cell2struct(cell,fields, 1);


