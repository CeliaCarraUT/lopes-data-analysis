function incrementVarParamPlot(Rdata,Ldata,variables,Rparams,Lparams,t)

p=length(Rparams); %parameters
v=length(variables); %variables

xlabels={'StepHeight','GuidanceForce';'StabilityStance','GuidanceForce';'Prepositioning','GuidanceForce';'StepLength','GuidanceForce';'WeightShift','GuidanceForce'};
ylabels={'Step Height','\Delta rad','blank';'Stability','\Delta rad','blank';'Initial','Step length','\Delta rad';'Terminal','Step length','\Delta rad';'Lateral','Foot positioning','\Delta rad';'Prepositioning','\Delta rad','blank';'Weight Shift','\Delta rad','blank'};

%matrix containing the positions of the y labels
% ylabelpositions=[-34.8293,-0.006296,-0.999;-34.8293,-0.006296,-0.999;-35.237465,-0.003962,-0.999;-35.23746530612245,-0.00276380952381,-0.999000012874617;-36.25787346938776,0.00019680952381,-0.999000012874617;-36.053791836734696,-0.003977152941177,-0.999000012874603;-35.645628571428574,-0.005287670588235,-0.999000012874617];

%t=input('Insert the title of the figure:','s');

%set y limits of the figures
for i=1:p
    if 1==1
       for j=1:v
           if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
            y=vertcat(Rdata.(Rparams{i,1})(:,j+1),Ldata.(Lparams{i,1})(:,j+1));
            yupperbounds(j)=max(y);
            ylowerbounds(j)=min(y);
           elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                yupperbounds(j)=max(Rdata.(Rparams{i,1})(:,j+1));
                ylowerbounds(j)=min(Rdata.(Rparams{i,1})(:,j+1));
           elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                yupperbounds(j)=max(Ldata.(Lparams{i,1})(:,j+1));
                ylowerbounds(j)=min(Ldata.(Lparams{i,1})(:,j+1));
           end
       end
    else 
        for j=1:v
            if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                y=vertcat(Rdata.(Rparams{i,1})(:,j+1),Ldata.(Lparams{i,1})(:,j+1));
                yupperboundstemp=max(y);
                ylowerboundstemp=min(y);
                %compare with the previous
                if yupperboundstemp>yupperbounds(j)
                    yupperbounds(j)=yupperboundstemp;
                end
                if ylowerboundstemp<ylowerbounds(j)
                    ylowerbounds(j)=ylowerboundstemp;
                end
            elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                 yupperboundstemp=max(Rdata.(Rparams{i,1})(:,j+1));
                 ylowerboundstemp=min(Rdata.(Rparams{i,1})(:,j+1));
                 %compare with the previous
                if yupperboundstemp>yupperbounds(j)
                    yupperbounds(j)=yupperboundstemp;
                end
                if ylowerboundstemp<ylowerbounds(j)
                    ylowerbounds(j)=ylowerboundstemp;
                end
            elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    yupperboundstemp=max(Ldata.(Lparams{i,1})(:,j+1));
                    ylowerboundstemp=min(Ldata.(Lparams{i,1})(:,j+1));
                    %compare with the previous
                    if yupperboundstemp>yupperbounds(j)
                        yupperbounds(j)=yupperboundstemp;
                    end
                    if ylowerboundstemp<ylowerbounds(j)
                        ylowerbounds(j)=ylowerboundstemp;
                    end
            end
        end
    end
end
    

% figure
colormap(copper);
[ha,pos]=tight_subplot(v,p,[.01 .03],[0.1 .01],[.01 .01]);
%we fill in the 'plots matrix' by columns; aka by parameter changed
for i=1:p %for the number of columns
    %set xlimits for the column
%     if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
%         y=vertcat(Rdata.(Rparams{i,1})(:,1),Ldata.(Lparams{i,1})(:,1));
%         upperbound=max(y);
%         lowerbounds=min(y);
%         if abs(upperbound)<10 && abs(lowerbounds)<10
%             interval=[lowerbounds-1,upperbound+1];
%         else 
%             interval=[lowerbounds-5,upperbound+5];
%         end
%     elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
%         upperbound=max(Rdata.(Rparams{i,1})(:,1));
%         lowerbounds=min(Rdata.(Rparams{i,1})(:,1));
%         if abs(upperbound)<10 && abs(lowerbounds)<10
%             interval=[lowerbounds-1,upperbound+1];
%         else 
%             interval=[lowerbounds-5,upperbound+5];
%         end
%     elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
%         upperbound=max(Ldata.(Lparams{i,1})(:,1));
%         lowerbounds=min(Ldata.(Lparams{i,1})(:,1));
%         if abs(upperbound)<10 && abs(lowerbounds)<10
%             interval=[lowerbounds-1,upperbound+1];
%         else 
%             interval=[lowerbounds-5,upperbound+5];
%         end
%     end
    if i==1 %first column of the matrix-- we need to add labels for every row
        for j=1:v %for the number of rows
            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
            if j==1  %first plot of the column-- we need to add title
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                end
            elseif  j==2 %middle plots (no titles, no x ticks)
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels 
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([-0.05 0.05])
                end
            elseif  j==3 %middle plots (no titles, no x ticks)
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
                elseif  j==4 %middle plots (no titles, no x ticks)
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            elseif  j==5 %middle plots (no titles, no x ticks)
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            elseif  j==6 %middle plots (no titles, no x ticks)
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            elseif  j==v %last plot (no titles, x ticks)
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0); %insert y labels 
                    %set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            end
        end
    elseif i~=1 && i~=p %in between columns-no y labels
        for j=1:v
            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
            if j==1 %title on top, no x,yticks
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                end
            elseif j==v %no title, x ticks only
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            else %no title, no ticks
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            end
        end
    elseif i==p %last coulmn-add the colormap
        for j=1:v
            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
            if j==1
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    title({xlabels{i,1};xlabels{i,2}})% insert 'titles' of the columns
                    %xlim(interval)
                    ylim([-0.05 0.05])
                end
            elseif j==v 
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                    colorbar('Position',[0.917105168620934 0.105769230076709 0.00640625007751316 0.0929487193337618])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'YTick',[]);
                    colorbar('Position',[0.917105168620934 0.105769230076709 0.00640625007751316 0.0929487193337618])
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'YTick',[]);
                    colorbar('Position',[0.917105168620934 0.105769230076709 0.00640625007751316 0.0929487193337618])
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            else
                if isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled')
                    hold on
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    hold off
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                elseif isfield(Rdata,Rparams{i,1})==1 && isfield(Ldata,Lparams{i,1})==0
                    scatter(Rdata.(Rparams{i,1})(:,3),Rdata.(Rparams{i,1})(:,j+3),30,Rdata.(Rparams{i,1})(:,12),'d','filled') 
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    %xlim(interval)
                elseif isfield(Rdata,Rparams{i,1})==0 && isfield(Ldata,Lparams{i,1})==1
                    scatter(Ldata.(Lparams{i,1})(:,3),Ldata.(Lparams{i,1})(:,j+3),30,Ldata.(Lparams{i,1})(:,12),'d','filled')
                    set(gca,'XTick',[]);
                    set(gca,'YTick',[]);
                    %xlim(interval)
                    ylim([ylowerbounds(j)-0.01 yupperbounds(j)+0.01])
                end
            end
        end
    end
end     

sgtitle(t) %title over all the sublots
[ax1,h1]=suplabel('\Delta Parameters (J)','x'); %legend over all subplots x axis
%[ax2,h2]=suplabel('\Delta Variables','y');

