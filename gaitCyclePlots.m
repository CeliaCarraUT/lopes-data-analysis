function gaitCyclePlots(DataSet,parameter,trial) %varagin
%this function outputs a figure containing the information 
%Inputs: 
%patient--string containing the name of the patient
%trial--trial number of which we want to see the power/joint 
%step--number of the step to plot
%joint--string containing the joint of the variable to plot.List of joints:

%calculate power and velocities and add (joint speed, power) to the raw data of the
%original data structure:

SignalsdataSteps=splitIntoSteps(DataSet(trial),'phaseSignal','m_cont_phase','phaseDetectionMode','100pct','maxStrideDuration',4,'nPointsPerStride',100,'includeEndpoint',0);

step2=723;
t={'Subject 1 Trial 1 Weight Shift GF'};

jointsRef.PX=SignalsdataSteps.data.r_cont_jointAngles_PX(:,step2);
jointsRef.PZ=SignalsdataSteps.data.r_cont_jointAngles_PZ(:,step2);
jointsRef.LHF=SignalsdataSteps.data.r_cont_jointAngles_LHF(:,step2);
jointsRef.LHA=SignalsdataSteps.data.r_cont_jointAngles_LHA(:,step2);
jointsRef.LKF=SignalsdataSteps.data.r_cont_jointAngles_LKF(:,step2);
jointsRef.RHF=SignalsdataSteps.data.r_cont_jointAngles_RHF(:,step2);
jointsRef.RHA=SignalsdataSteps.data.r_cont_jointAngles_RHA(:,step2);
jointsRef.RKF=SignalsdataSteps.data.r_cont_jointAngles_RKF(:,step2);

%calculate joint ANGLES before and after for every joint
%pelvis
VarBefore.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2-10:step2-1),2);
VarAfter.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2+1:step2+10),2);
VarBefore.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2-10:step2-1),2);
VarAfter.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2+1:step2+10),2);
%left
VarBefore.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2-10:step2-1),2);
VarAfter.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2+1:step2+10),2);
VarBefore.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2-10:step2-1),2);
VarAfter.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2+1:step2+10),2);
VarBefore.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2-10:step2-1),2);
VarAfter.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2+1:step2+10),2);
%Right
VarBefore.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2-10:step2-1),2);
VarAfter.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2+1:step2+10),2);
VarBefore.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2-10:step2-1),2);
VarAfter.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2+1:step2+10),2);
VarBefore.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2-10:step2-1),2);
VarAfter.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2+1:step2+10),2);

%calculate POWER before and after for every joint
%pelvis
PowBefore.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2-10:step2-1),2);
PowAfter.PX=mean(SignalsdataSteps.data.m_cont_jointAngles_PX(:,step2+1:step2+10),2);
PowBefore.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2-10:step2-1),2);
PowAfter.PZ=mean(SignalsdataSteps.data.m_cont_jointAngles_PZ(:,step2+1:step2+10),2);
%left
PowBefore.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2-10:step2-1),2);
PowAfter.LHF=mean(SignalsdataSteps.data.m_cont_jointAngles_LHF(:,step2+1:step2+10),2);
PowBefore.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2-10:step2-1),2);
PowAfter.LHA=mean(SignalsdataSteps.data.m_cont_jointAngles_LHA(:,step2+1:step2+10),2);
PowBefore.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2-10:step2-1),2);
PowAfter.LKF=mean(SignalsdataSteps.data.m_cont_jointAngles_LKF(:,step2+1:step2+10),2);
%Right
PowBefore.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2-10:step2-1),2);
PowAfter.RHF=mean(SignalsdataSteps.data.m_cont_jointAngles_RHF(:,step2+1:step2+10),2);
PowBefore.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2-10:step2-1),2);
PowAfter.RHA=mean(SignalsdataSteps.data.m_cont_jointAngles_RHA(:,step2+1:step2+10),2);
PowBefore.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2-10:step2-1),2);
PowAfter.RKF=mean(SignalsdataSteps.data.m_cont_jointAngles_RKF(:,step2+1:step2+10),2);

%joint VELOCITY before and after
SpeedBefore.PX=mean(SignalsdataSteps.data.JointVelocity_PX(:,step2-10:step2-1),2);
SpeedAfter.PX=mean(SignalsdataSteps.data.JointVelocity__PX(:,step2+1:step2+10),2);
SpeedBefore.PZ=mean(SignalsdataSteps.data.JointVelocity_PZ(:,step2-10:step2-1),2);
SpeedAfter.PZ=mean(SignalsdataSteps.data.JointVelocity_PZ(:,step2+1:step2+10),2);
%left
SpeedBefore.LHF=mean(SignalsdataSteps.data.JointVelocity_LHF(:,step2-10:step2-1),2);
SpeedAfter.LHF=mean(SignalsdataSteps.data.JointVelocity_LHF(:,step2+1:step2+10),2);
SpeedBefore.LHA=mean(SignalsdataSteps.data.JointVelocity_LHA(:,step2-10:step2-1),2);
Speedfter.LHA=mean(SignalsdataSteps.data.JointVelocity_LHA(:,step2+1:step2+10),2);
SpeedBefore.LKF=mean(SignalsdataSteps.data.JointVelocity_LKF(:,step2-10:step2-1),2);
SpeedAfter.LKF=mean(SignalsdataSteps.data.JointVelocity_LKF(:,step2+1:step2+10),2);
%Right
SpeedBefore.RHF=mean(SignalsdataSteps.data.JointVelocity_RHF(:,step2-10:step2-1),2);
SpeedAfter.RHF=mean(SignalsdataSteps.data.JointVelocity_RHF(:,step2+1:step2+10),2);
SpeedBefore.RHA=mean(SignalsdataSteps.data.JointVelocity_RHA(:,step2-10:step2-1),2);
SpeedAfter.RHA=mean(SignalsdataSteps.data.JointVelocity_RHA(:,step2+1:step2+10),2);
SpeedBefore.RKF=mean(SignalsdataSteps.data.JointVelocity_RKF(:,step2-10:step2-1),2);
SpeedAfter.RKF=mean(SignalsdataSteps.data.JointVelocity_RKF(:,step2+1:step2+10),2);

%interaction FORCES before and after
%pelvis
ForcesBefore.PX=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PX(:,step2-10:step2-1),2);
ForcesAfter.PX=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PX(:,step2+1:step2+10),2);
ForcesBefore.PZ=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PZ(:,step2-10:step2-1),2);
ForcesAfter.PZ=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_PZ(:,step2+1:step2+10),2);
%left
ForcesBefore.LHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHF(:,step2-10:step2-1),2);
ForcesAfter.LHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHF(:,step2+1:step2+10),2);
ForcesBefore.LHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHA(:,step2-10:step2-1),2);
ForcesAfter.LHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LHA(:,step2+1:step2+10),2);
ForcesBefore.LKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LKF(:,step2-10:step2-1),2);
ForcesAfter.LKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_LKF(:,step2+1:step2+10),2);
%Right
ForcesBefore.RHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHF(:,step2-10:step2-1),2);
ForcesAfter.RHF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHF(:,step2+1:step2+10),2);
ForcesBefore.RHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHA(:,step2-10:step2-1),2);
ForcesAfter.RHA=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RHA(:,step2+1:step2+10),2);
ForcesBefore.RKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RKF(:,step2-10:step2-1),2);
ForcesAfter.RKF=mean(SignalsdataSteps.data.m_cont_jointForcesMeasured_RKF(:,step2+1:step2+10),2);

%Joint Stiffness Before and After
%pelvis
StiffBefore.PX=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PX(:,step2-10:step2-1),2);
StiffAfter.PX=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PX(:,step2+1:step2+10),2);
StiffBefore.PZ=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PZ(:,step2-10:step2-1),2);
StiffAfter.PZ=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_PZ(:,step2+1:step2+10),2);
%left
StiffBefore.LHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHF(:,step2-10:step2-1),2);
StiffAfter.LHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHF(:,step2+1:step2+10),2);
StiffBefore.LHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHA(:,step2-10:step2-1),2);
StiffAfter.LHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LHA(:,step2+1:step2+10),2);
StiffBefore.LKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LKF(:,step2-10:step2-1),2);
StiffAfter.LKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_LKF(:,step2+1:step2+10),2);
%right
StiffBefore.RHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHF(:,step2-10:step2-1),2);
StiffAfter.RHF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHF(:,step2+1:step2+10),2);
StiffBefore.RHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHA(:,step2-10:step2-1),2);
StiffAfter.RHA=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RHA(:,step2+1:step2+10),2);
StiffBefore.RKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RKF(:,step2-10:step2-1),2);
StiffAfter.RKF=mean(SignalsdataSteps.data.r_cont_jointStiffnesses_pct_RKF(:,step2+1:step2+10),2);

joints=fieldnames(VarBefore);
xlabels={'PX';'PZ';'LHF';'LHA';'LKF';'RHF';'RHA';'RKF'};
ylabels={'Joint','Angle','(rad)';'Parameter','Assistance','(%)';'Joint','Velocity','(rad/s)';'Interaction','Forces','(N)';'Power','(W)','blank'};
ylabelpositions=[-56.85579252017587,-0.068654704243321,-1;-57.21040216179891,15.761435624911721,-0.999999999999986;-54.01891301991525,-39.87937279159091,-1];
p=length(xlabels);
v=length(ylabels);

%plot-5 rows (angle,asistance level, valocity, interaction forces, power)
%and 8 rows (corresponding to every joint)
figure
[ha,pos]=tight_subplot(v,p,[.01 .03],[0.1 .01],[.01 .01]);
subplot
for i=1:8 %for every column
    if i==1 %add labels
        for j=1:5 %for every row
            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
            if j==1 %joint angle
                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                hold off
                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                set(mylabel,'Position',ylabelpositions(j,:)); %positioning of the y labels
                title({xlabels{i,1}})
                set(gca,'XTick',[]);
                %grey doter line for reference
            elseif j==2 %joint stiffnesss
                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,StiffAfter.(joints{i,1}),'r') %after change
                hold off
                set(gca,'YTick',[0, 20, 30]);
                xlim([0 100])
                ylim([0 30])
                set(gca,'XTick',[]);
                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                set(mylabel,'Position',ylabelpositions(j,:));
            elseif j==3 %joint velocity
                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                hold off
                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                set(mylabel,'Position',ylabelpositions(j,:));
                set(gca,'XTick',[]);
            elseif j==4 %interaction forces
                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                hold off
                mylabel=ylabel({ylabels{j,1};ylabels{j,2};ylabels{j,3}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                set(mylabel,'Position',ylabelpositions(j,:));
                set(gca,'XTick',[]);
            elseif j==5 %power
                plot(0:99,PowBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,PowAfter.(joints{i,1}),'r') %after change
                hold off
                mylabel=ylabel({ylabels{j,1};ylabels{j,2}},'FontSize',10,'VerticalAlignment','middle','Rotation',0);
                set(mylabel,'Position',ylabelpositions(j,:));
            end
        end
    elseif i==8 %add legends/colorbar
        for j=1:5 %for every row
           subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
            if j==1 %joint angle
                plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                hold off
                title({xlabels{i,1}})
                legend('Before change','After change','Reference Step')
                set(legend,'Position',[0.910996115017397 0.841869473090363 0.0795572927718363 0.057959402932061]);
                set(gca,'XTick',[]);
            elseif j==2 %stiffness
                 plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,StiffAfter.(joints{i,1}),'r') %after change
                hold off
                xlim([0 100])
                ylim([0 30])
                set(gca,'YTick',[])
                set(gca,'XTick',[])
            elseif j==3 %joint velocity
                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                hold off
                set(gca,'YTick',[])
                set(gca,'XTick',[])
            elseif j==4 %interaction forces
                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                hold off
                set(gca,'YTick',[])
                set(gca,'XTick',[])
            elseif j==5 %power
                plot(0:99,PowBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,PowAfter.(joints{i,1}),'r') %after change
                hold off
            end
        end
    else
        for j=1:5 %for every row
            subplot(v,p,i+(j-1)*p,ha(i+(j-1)*p))
            if j==1 %joint angle
               plot(0:99,VarBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,VarAfter.(joints{i,1}),'r') %after change
                plot(0:99,jointsRef.(joints{i,1}),'LineStyle','--','Color',[0.4 0.4 0.4])
                hold off
                title({xlabels{i,1}})
                set(gca,'XTick',[]);
            elseif j==2 %assistance
                plot(0:99,StiffBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,StiffAfter.(joints{i,1}),'r') %after change
                hold off
                xlim([0 100])
                ylim([0 30])
                set(gca,'XTick',[])
                set(gca,'YTick',[])
            elseif j==3 %joint velocity
                plot(0:99,SpeedBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,SpeedAfter.(joints{i,1}),'r') %after change
                hold off
                set(gca,'XTick',[])
                set(gca,'YTick',[])
            elseif j==4 %interaction forces
                plot(0:99,ForcesBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,ForcesAfter.(joints{i,1}),'r') %after change
                hold off
                set(gca,'XTick',[])
                set(gca,'YTick',[])
            elseif j==5 %power
                plot(0:99,PowBefore.(joints{i,1}),'b') %before change 
                hold on
                plot(0:99,PowAfter.(joints{i,1}),'r') %after change
                hold off
            end
        end
    end
end

sgtitle(t) %title over all the sublots
[ax1,h1]=suplabel('% Gait Cycle)','x'); %legend over all subplots x axis

