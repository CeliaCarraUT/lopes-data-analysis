%% Power Analysis Part 2
%This script performs the second (and final) part of the power analysis
%with the output from 'WhitinTrialIncrementAnalysis2' (after
%running the script for both sets of patients) we join the  matrices for
%all the patients and select the points corresponding to changes in
%parameters independent of changes in the GGF

%After this, data is divided according to the paretic side of the patient
%and finally plotted (some manual modifications must be applied to the
%graphs to get the same as in the paper- i.e.highlight the background of
%the significant ones and plot the trend lines)

%% 1-Checking change instnaces and merging matrices
%%power
load('RRDpower.mat')
load('SMKpower(wDS04).mat')

%power total
[POWtotplottingmatrix1]=checkChangeInstancesPowerSMK(plottingmatrixPOWtot1_SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06);%,ParamChangeInstancesDS07,ParamChangeInstancesDS08);
[POWtotplottingmatrix2]=checkChangeInstancesPowerRRD(plottingmatrixPOWtot1_RRD,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110);%,ParamChangeInstancesD112,ParamChangeInstancesD114);

%merge the plotting matrices obtained above and also add the points from
%plotting matrices number 2
[POWtotplottingmatrix1]=mergePlottingMatrices(POWtotplottingmatrix1,plottingmatrixPOWtot2_SMK);
[POWtotplottingmatrix2]=mergePlottingMatrices(POWtotplottingmatrix2,plottingmatrixPOWtot2_RRD);
[POWtotplottingmatrix]=mergePlottingMatrices(POWtotplottingmatrix1,POWtotplottingmatrix2);

%add column for setting the colour scheme of the plots
% fields=fieldnames(POWtotplottingmatrix);
% 
% for i=1:length(fields)
%     for j=1:size(POWtotplottingmatrix.(fields{i,1}),1)
%         if POWtotplottingmatrix.(fields{i,1})(j,1)>75 && POWtotplottingmatrix.(fields{i,1})(j,1)<=100 
%             POWtotplottingmatrix.(fields{i,1})(j,15)=1;
%         elseif POWtotplottingmatrix.(fields{i,1})(j,1)>50 && POWtotplottingmatrix.(fields{i,1})(j,1)<=75 
%             POWtotplottingmatrix.(fields{i,1})(j,15)=2;
%         elseif POWtotplottingmatrix.(fields{i,1})(j,1)>25 && POWtotplottingmatrix.(fields{i,1})(j,1)<=50 
%             POWtotplottingmatrix.(fields{i,1})(j,15)=3;
%         elseif POWtotplottingmatrix.(fields{i,1})(j,1)>=0 && POWtotplottingmatrix.(fields{i,1})(j,1)<=25 
%             POWtotplottingmatrix.(fields{i,1})(j,15)=4;
%         end
%     end
% end



%power-positive
[POWPOSplottingmatrix1]=checkChangeInstancesPowerSMK(plottingmatrixPOWpos1_SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06);%,ParamChangeInstancesDS07,ParamChangeInstancesDS08);
[POWPOSplottingmatrix2]=checkChangeInstancesPowerRRD(plottingmatrixPOWpos1_RRD,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110);%,ParamChangeInstancesD112,ParamChangeInstancesD114);

%merge the plotting matrices obtained above and also add the points from
%plotting matrices number 2
[POWPOSplottingmatrix1]=mergePlottingMatrices(POWPOSplottingmatrix1,plottingmatrixPOWpos2_SMK);
[POWPOSplottingmatrix2]=mergePlottingMatrices(POWPOSplottingmatrix2,plottingmatrixPOWpos2_RRD);
[POWPOSplottingmatrix]=mergePlottingMatrices(POWPOSplottingmatrix1,POWPOSplottingmatrix2);

%add column for setting the colour scheme of the plots
% fields=fieldnames(POWPOSplottingmatrix);
% 
% for i=1:length(fields)
%     for j=1:size(POWPOSplottingmatrix.(fields{i,1}),1)
%         if POWPOSplottingmatrix.(fields{i,1})(j,1)>75 && POWPOSplottingmatrix.(fields{i,1})(j,1)<=100 
%             POWPOSplottingmatrix.(fields{i,1})(j,15)=1;
%         elseif POWPOSplottingmatrix.(fields{i,1})(j,1)>50 && POWPOSplottingmatrix.(fields{i,1})(j,1)<=75 
%             POWPOSplottingmatrix.(fields{i,1})(j,15)=2;
%         elseif POWPOSplottingmatrix.(fields{i,1})(j,1)>25 && POWPOSplottingmatrix.(fields{i,1})(j,1)<=50 
%             POWPOSplottingmatrix.(fields{i,1})(j,15)=3;
%         elseif POWPOSplottingmatrix.(fields{i,1})(j,1)>=0 && POWPOSplottingmatrix.(fields{i,1})(j,1)<=25 
%             POWPOSplottingmatrix.(fields{i,1})(j,15)=4;
%         end
%     end
% end



%power-negative
[POWNEGplottingmatrix1]=checkChangeInstancesPowerSMK(plottingmatrixPOWneg1_SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06);%,ParamChangeInstancesDS07,ParamChangeInstancesDS08);
[POWNEGplottingmatrix2]=checkChangeInstancesPowerRRD(plottingmatrixPOWneg1_RRD,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110);%,ParamChangeInstancesD112,ParamChangeInstancesD114);

%merge the plotting matrices obtained above and also add the points from
%plotting matrices number 2
[POWNEGplottingmatrix1]=mergePlottingMatrices(POWNEGplottingmatrix1,plottingmatrixPOWneg2_SMK);
[POWNEGplottingmatrix2]=mergePlottingMatrices(POWNEGplottingmatrix2,plottingmatrixPOWneg2_RRD);
[POWNEGplottingmatrix]=mergePlottingMatrices(POWNEGplottingmatrix1,POWNEGplottingmatrix2);

%add column for setting the colour scheme of the plots
% fields=fieldnames(POWNEGplottingmatrix);

% for i=1:length(fields)
%     for j=1:size(POWNEGplottingmatrix.(fields{i,1}),1)
%         if POWNEGplottingmatrix.(fields{i,1})(j,1)>75 && POWNEGplottingmatrix.(fields{i,1})(j,1)<=100 
%             POWNEGplottingmatrix.(fields{i,1})(j,15)=1;
%         elseif POWNEGplottingmatrix.(fields{i,1})(j,1)>50 && POWNEGplottingmatrix.(fields{i,1})(j,1)<=75 
%             POWNEGplottingmatrix.(fields{i,1})(j,15)=2;
%         elseif POWNEGplottingmatrix.(fields{i,1})(j,1)>25 && POWNEGplottingmatrix.(fields{i,1})(j,1)<=50 
%             POWNEGplottingmatrix.(fields{i,1})(j,15)=3;
%         elseif POWNEGplottingmatrix.(fields{i,1})(j,1)>=0 && POWNEGplottingmatrix.(fields{i,1})(j,1)<=25 
%             POWNEGplottingmatrix.(fields{i,1})(j,15)=4;
%         end
%     end
% end


%%work
load('RRDwork.mat')
load('SMKwork(wDS04).mat')

%work total
[WORtotplottingmatrix1]=checkChangeInstancesPowerSMK(plottingmatrixWORtot1_SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06);%,ParamChangeInstancesDS07,ParamChangeInstancesDS08);
[WORtotplottingmatrix2]=checkChangeInstancesPowerRRD(plottingmatrixWORtot1_RRD,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110);%,ParamChangeInstancesD112,ParamChangeInstancesD114);

%merge the plotting matrices obtained above and also add the points from
%plotting matrices number 2
[WORtotplottingmatrix1]=mergePlottingMatrices(WORtotplottingmatrix1,plottingmatrixWORtot2_SMK);
[WORtotplottingmatrix2]=mergePlottingMatrices(WORtotplottingmatrix2,plottingmatrixWORtot2_RRD);
[WORtotplottingmatrix]=mergePlottingMatrices(WORtotplottingmatrix1,WORtotplottingmatrix2);

%add column for setting the colour scheme of the plots (FIRST COLOR SCHEME)
% fields=fieldnames(WORtotplottingmatrix);
% 
% for i=1:length(fields)
%     for j=1:size(WORtotplottingmatrix.(fields{i,1}),1)
%         if WORtotplottingmatrix.(fields{i,1})(j,1)>75 && WORtotplottingmatrix.(fields{i,1})(j,1)<=100 
%             WORtotplottingmatrix.(fields{i,1})(j,15)=1;
%         elseif WORtotplottingmatrix.(fields{i,1})(j,1)>50 && WORtotplottingmatrix.(fields{i,1})(j,1)<=75 
%             WORtotplottingmatrix.(fields{i,1})(j,15)=2;
%         elseif WORtotplottingmatrix.(fields{i,1})(j,1)>25 && WORtotplottingmatrix.(fields{i,1})(j,1)<=50 
%             WORtotplottingmatrix.(fields{i,1})(j,15)=3;
%         elseif WORtotplottingmatrix.(fields{i,1})(j,1)>=0 && WORtotplottingmatrix.(fields{i,1})(j,1)<=25 
%             WORtotplottingmatrix.(fields{i,1})(j,15)=4;
%         end
%     end
% end



%work-positive
[WORPOSplottingmatrix1]=checkChangeInstancesPowerSMK(plottingmatrixWORpos1_SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06);%,ParamChangeInstancesDS07,ParamChangeInstancesDS08);
[WORPOSplottingmatrix2]=checkChangeInstancesPowerRRD(plottingmatrixWORpos1_RRD,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110);%,ParamChangeInstancesD112,ParamChangeInstancesD114);

%merge the plotting matrices obtained above and also add the points from
%plotting matrices number 2
[WORPOSplottingmatrix1]=mergePlottingMatrices(WORPOSplottingmatrix1,plottingmatrixWORpos2_SMK);
[WORPOSplottingmatrix2]=mergePlottingMatrices(WORPOSplottingmatrix2,plottingmatrixWORpos2_RRD);
[WORPOSplottingmatrix]=mergePlottingMatrices(WORPOSplottingmatrix1,WORPOSplottingmatrix2);

%add column for setting the colour scheme of the plots
fields=fieldnames(WORPOSplottingmatrix);

% for i=1:length(fields)
%     for j=1:size(WORPOSplottingmatrix.(fields{i,1}),1)
%         if WORPOSplottingmatrix.(fields{i,1})(j,1)>75 && WORPOSplottingmatrix.(fields{i,1})(j,1)<=100 
%             WORPOSplottingmatrix.(fields{i,1})(j,15)=1;
%         elseif WORPOSplottingmatrix.(fields{i,1})(j,1)>50 && WORPOSplottingmatrix.(fields{i,1})(j,1)<=75 
%             WORPOSplottingmatrix.(fields{i,1})(j,15)=2;
%         elseif WORPOSplottingmatrix.(fields{i,1})(j,1)>25 && WORPOSplottingmatrix.(fields{i,1})(j,1)<=50 
%             WORPOSplottingmatrix.(fields{i,1})(j,15)=3;
%         elseif WORPOSplottingmatrix.(fields{i,1})(j,1)>=0 && WORPOSplottingmatrix.(fields{i,1})(j,1)<=25 
%             WORPOSplottingmatrix.(fields{i,1})(j,15)=4;
%         end
%     end
% end



%work-negative
[WORNEGplottingmatrix1]=checkChangeInstancesPowerSMK(plottingmatrixWORneg1_SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06);%,ParamChangeInstancesDS07,ParamChangeInstancesDS08);
[WORNEGplottingmatrix2]=checkChangeInstancesPowerRRD(plottingmatrixWORneg1_RRD,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110);%,ParamChangeInstancesD112,ParamChangeInstancesD114);

%merge the plotting matrices obtained above and also add the points from
%plotting matrices number 2
[WORNEGplottingmatrix1]=mergePlottingMatrices(WORNEGplottingmatrix1,plottingmatrixWORneg2_SMK);
[WORNEGplottingmatrix2]=mergePlottingMatrices(WORNEGplottingmatrix2,plottingmatrixWORneg2_RRD);
[WORNEGplottingmatrix]=mergePlottingMatrices(WORNEGplottingmatrix1,WORNEGplottingmatrix2);

%add column for setting the colour scheme of the plots
fields=fieldnames(WORNEGplottingmatrix);

% for i=1:length(fields)
%     for j=1:size(WORNEGplottingmatrix.(fields{i,1}),1)
%         if WORNEGplottingmatrix.(fields{i,1})(j,1)>75 && WORNEGplottingmatrix.(fields{i,1})(j,1)<=100 
%             WORNEGplottingmatrix.(fields{i,1})(j,15)=1;
%         elseif WORNEGplottingmatrix.(fields{i,1})(j,1)>50 && WORNEGplottingmatrix.(fields{i,1})(j,1)<=75 
%             WORNEGplottingmatrix.(fields{i,1})(j,15)=2;
%         elseif WORNEGplottingmatrix.(fields{i,1})(j,1)>25 && WORNEGplottingmatrix.(fields{i,1})(j,1)<=50 
%             WORNEGplottingmatrix.(fields{i,1})(j,15)=3;
%         elseif WORNEGplottingmatrix.(fields{i,1})(j,1)>=0 && WORNEGplottingmatrix.(fields{i,1})(j,1)<=25 
%             WORNEGplottingmatrix.(fields{i,1})(j,15)=4;
%         end
%     end
% end


%% 2-paretic-nonparetic division of data 
%power total
[RpareticPOWtot,RnonpareticPOWtot,LpareticPOWtot,LnonpareticPOWtot]=pareticDivisionOfDataForcesPower(POWtotplottingmatrix);
%power positive
[RpareticPOWpos,RnonpareticPOWpos,LpareticPOWpos,LnonpareticPOWpos]=pareticDivisionOfDataForcesPower(POWPOSplottingmatrix);
%power negative
[RpareticPOWneg,RnonpareticPOWneg,LpareticPOWneg,LnonpareticPOWneg]=pareticDivisionOfDataForcesPower(POWNEGplottingmatrix);

%work
[RpareticWORtot,RnonpareticWORtot,LpareticWORtot,LnonpareticWORtot]=pareticDivisionOfDataForcesPower(WORtotplottingmatrix);
%power positive
[RpareticWORpos,RnonpareticWORpos,LpareticWORpos,LnonpareticWORpos]=pareticDivisionOfDataForcesPower(WORPOSplottingmatrix);
%power negative
[RpareticWORneg,RnonpareticWORneg,LpareticWORneg,LnonpareticWORneg]=pareticDivisionOfDataForcesPower(WORNEGplottingmatrix);

%% FINAL PLOTS
% 
% rightparams={'RightStepHeight_GuidanceForce_pct';'RightStabilityStance_GuidanceForce_pct';'RightPrepositioning_GuidanceForce_pct';'RightStepLength_GuidanceForce_pct';'WeightShift_GuidanceForce_pct';'RightStepHeight_KneeFlexion_pct'};
% labels={'StepHeight','GuidanceForce';'StabilityStance','GuidanceForce';'Prepositioning','GuidanceForce';'StepLength','GuidanceForce';'WeightShift','GuidanceForce';'StepHeight','KneeFlexion'};
% leftparams={'LeftStepHeight_GuidanceForce_pct';'LeftStabilityStance_GuidanceForce_pct';'LeftPrepositioning_GuidanceForce_pct';'LeftStepLength_GuidanceForce_pct';'WeightShift_GuidanceForce_pct';'LeftStepHeight_KneeFlexion_pct'};
% 
% 
% %power positive
% myplot(datamatrix, variables, parameters)
% %power negative
% myplot(datamatrix, variables, parameters)