%% FINAL plots combined variable and work analysis
%Increment plots work vs variables (taking into account the parameters
%changed)
load('VarWorkPowerDataPlots.mat')
load('FINALpareticRLstructs.mat')

%% 1-Combine the output matices of work and variable analysis
%total work
CombinedRpareticTotal=combineWorkVariableStructures2(RpareticVAR,RpareticWORtot);
CombinedRnonpareticTotal=combineWorkVariableStructures2(RnonpareticVAR,RnonpareticWORtot);

CombinedLpareticTotal=combineWorkVariableStructures2(LpareticVAR,LpareticWORtot);
CombinedLnonpareticTotal=combineWorkVariableStructures2(LnonpareticVAR,LnonpareticWORtot);

%positive work
CombinedRpareticPos=combineWorkVariableStructures2(RpareticVAR,RpareticWORpos);
CombinedRnonpareticPos=combineWorkVariableStructures2(RnonpareticVAR,RnonpareticWORpos);

CombinedLpareticPos=combineWorkVariableStructures2(LpareticVAR,LpareticWORpos);
CombinedLnonpareticPos=combineWorkVariableStructures2(LnonpareticVAR,LnonpareticWORpos);

%negative work
CombinedRpareticNeg=combineWorkVariableStructures2(RpareticVAR,RpareticWORneg);
CombinedRnonpareticNeg=combineWorkVariableStructures2(RnonpareticVAR,RnonpareticWORneg);

CombinedLpareticNeg=combineWorkVariableStructures2(LpareticVAR,LpareticWORneg);
CombinedLnonpareticNeg=combineWorkVariableStructures2(LnonpareticVAR,LnonpareticWORneg);

%% 2-Plots variables vs work
Rparams={'RightStepHeight_GuidanceForce_pct';'RightStabilityStance_GuidanceForce_pct';'RightPrepositioning_GuidanceForce_pct';'RightStepLength_GuidanceForce_pct';'WeightShift_GuidanceForce_pct';'WeightShift_GuidanceForce_pct'};
variables={'Step Height','\Delta rad','blank';'Stability','\Delta rad','blank';'Initial','Step length','\Delta rad';'Terminal','Step length','\Delta rad';'Lateral','Foot positioning','\Delta rad';'Prepositioning','\Delta rad','blank';'Weight Shift','\Delta rad','blank'};
Lparams={'LeftStepHeight_GuidanceForce_pct';'LeftStabilityStance_GuidanceForce_pct';'LeftPrepositioning_GuidanceForce_pct';'LeftStepLength_GuidanceForce_pct';'WeightShift_GuidanceForce_pct';'WeightShift_GuidanceForce_pct'};

%total work
t={'Changes in TOTAL Work for every joint of the paretic leg vs Changes in variables of the paretic leg'};
figure(1)
incrementWorkVarPlot(CombinedRpareticTotal,CombinedLpareticTotal,variables,Rparams,Lparams,t)

t={'Changes in TOTAL Work for every joint of the non-paretic leg vs Changes in variables of the non-paretic leg'};
figure(2)
incrementWorkVarPlot(CombinedRnonpareticTotal,CombinedLnonpareticTotal,variables,Lparams,Rparams,t)

t={'Changes in TOTAL Work for every joint of the paretic leg vs Changes in variables of the non-paretic leg'};
figure(3)
incrementWorkVarPlot(CombinedRnonpareticTotal,CombinedLnonpareticTotal,variables,Rparams,Lparams,t)

t={'Changes in TOTAL Work for every joint of the non-paretic leg vs Changes in variables of the paretic leg'};
figure(4)
incrementWorkVarPlot(CombinedRpareticTotal,CombinedLpareticTotal,variables,Lparams,Rparams,t)


%positive work
t={'Changes in Positive Work for every joint of the paretic leg vs Changes in variables of the paretic leg'};
figure(5)
incrementWorkVarPlot(CombinedRpareticPos,CombinedLpareticPos,variables,Rparams,Lparams,t)

t={'Changes in Positive Work for every joint of the non-paretic leg vs Changes in variables of the non-paretic leg'};
figure(6)
incrementWorkVarPlot(CombinedRnonpareticPos,CombinedLnonpareticPos,variables,Lparams,Rparams,t)

t={'Changes in Positive Work for every joint of the paretic leg vs Changes in variables of the non-paretic leg'};
figure(7)
incrementWorkVarPlot(CombinedRnonpareticPos,CombinedLnonpareticPos,variables,Rparams,Lparams,t)

t={'Changes in Positive Work for every joint of the non-paretic leg vs Changes in variables of the paretic leg'};
figure(8)
incrementWorkVarPlot(CombinedRpareticPos,CombinedLpareticPos,variables,Lparams,Rparams,t)


%negative work
t={'Changes in Negative Work for every joint of the paretic leg vs Changes in variables of the paretic leg'};
figure(9)
incrementWorkVarPlot(CombinedRpareticNeg,CombinedLpareticNeg,variables,Rparams,Lparams,t)

t={'Changes in Negative Work for every joint of the non-paretic leg vs Changes in variables of the non-paretic leg'};
figure(10)
incrementWorkVarPlot(CombinedRnonpareticNeg,CombinedLnonpareticNeg,variables,Lparams,Rparams,t)

t={'Changes in Negative Work for every joint of the paretic leg vs Changes in variables of the non-paretic leg'};
figure(11)
incrementWorkVarPlot(CombinedRnonpareticNeg,CombinedLnonpareticNeg,variables,Rparams,Lparams,t)

t={'Changes in Negative Work for every joint of the non-paretic leg vs Changes in variables of the paretic leg'};
figure(12)
incrementWorkVarPlot(CombinedRpareticNeg,CombinedLpareticNeg,variables,Lparams,Rparams,t)

%% 3-Plots variables vs assistance
Rparams={'RightStepHeight_GuidanceForce_pct';'RightStabilityStance_GuidanceForce_pct';'RightPrepositioning_GuidanceForce_pct';'RightStepLength_GuidanceForce_pct';'WeightShift_GuidanceForce_pct'};
variables={'Step Height','\Delta rad','blank';'Stability','\Delta rad','blank';'Initial','Step length','\Delta rad';'Terminal','Step length','\Delta rad';'Lateral','Foot positioning','\Delta rad';'Prepositioning','\Delta rad','blank';'Weight Shift','\Delta rad','blank'};
Lparams={'LeftStepHeight_GuidanceForce_pct';'LeftStabilityStance_GuidanceForce_pct';'LeftPrepositioning_GuidanceForce_pct';'LeftStepLength_GuidanceForce_pct';'WeightShift_GuidanceForce_pct'};

t={'Changes in assistance of the parameters of paretic leg vs Changes in variables of the paretic leg'};
figure(1)
incrementVarParamPlot(RpareticVAR,LpareticVAR,variables,Rparams,Lparams,t)
%statistics
%test for correlation kendall corr coeff

% for i=1:5
%     if  i==2 
%         for j=1:7
%             X=RpareticVAR.(Rparams{i,1})(:,3);
%             Y=RpareticVAR.(Rparams{i,1})(:,j+3);
%            [Tau(j,i),pval(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     elseif i== 1 || i==5 || i==3 || i==4
%         for j=1:6
%             X=vertcat(RpareticVAR.(Rparams{i,1})(:,3),LpareticVAR.(Lparams{i,1})(:,3));
%             Y=vertcat(RpareticVAR.(Rparams{i,1})(:,j+3),LpareticVAR.(Lparams{i,1})(:,j+3));
%            [Tau(j,i),pval(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     end
% end

t={'Changes in assistance of the parameters of non-paretic leg vs Changes in variables of the non-paretic leg'};
figure(2)
incrementVarParamPlot(RnonpareticVAR,LnonpareticVAR,variables,Rparams,Lparams,t)
%statistics
% for i=1:5
%     if  i==2 
%         for j=1:7
%             X=RnonpareticVAR.(Rparams{i,1})(:,3);
%             Y=RnonpareticVAR.(Rparams{i,1})(:,j+3);
%            [Tau2(j,i),pval2(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     elseif i== 1 || i==5 || i==3 || i==4
%         for j=1:6
%             X=vertcat(RnonpareticVAR.(Rparams{i,1})(:,3),LnonpareticVAR.(Lparams{i,1})(:,3));
%             Y=vertcat(RnonpareticVAR.(Rparams{i,1})(:,j+3),LnonpareticVAR.(Lparams{i,1})(:,j+3));
%            [Tau2(j,i),pval2(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     end
% end


t={'Changes in assistance of the parameters of paretic leg vs Changes in variables of the non-paretic leg'};
figure(3)
incrementVarParamPlot(RpareticVAR,LpareticVAR,variables,Lparams,Rparams,t)
%statistics
% for i=1:5
%     if  i==2 
%         for j=1:7
%             X=RpareticVAR.(Lparams{i,1})(:,3);
%             Y=RpareticVAR.(Lparams{i,1})(:,j+3);
%            [Tau3(j,i),pval3(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     elseif i== 1 || i==5 || i==3 || i==4
%         for j=1:6
%             X=vertcat(RpareticVAR.(Lparams{i,1})(:,3),LpareticVAR.(Rparams{i,1})(:,3));
%             Y=vertcat(RpareticVAR.(Lparams{i,1})(:,j+3),LpareticVAR.(Rparams{i,1})(:,j+3));
%            [Tau3(j,i),pval3(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     end
% end


t={'Changes in assistance of the parameters of non-paretic leg vs Changes in variables of the paretic leg'};
figure(4)
incrementVarParamPlot(RnonpareticVAR,LnonpareticVAR,variables,Lparams,Rparams,t)
%statistics
% for i=1:5
%     if  i==2 
%         for j=1:7
%             X=RnonpareticVAR.(Lparams{i,1})(:,3);
%             Y=RnonpareticVAR.(Lparams{i,1})(:,j+3);
%            [Tau4(j,i),pval4(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     elseif i== 1 || i==5 || i==3 || i==4
%         for j=1:6
%             X=vertcat(RnonpareticVAR.(Lparams{i,1})(:,3),LnonpareticVAR.(Rparams{i,1})(:,3));
%             Y=vertcat(RnonpareticVAR.(Lparams{i,1})(:,j+3),LnonpareticVAR.(Rparams{i,1})(:,j+3));
%            [Tau4(j,i),pval4(j,i)]=corr(X,Y,'Type','Kendall','Rows','complete');
%             clear X Y
%         end
%     end
% end

%% 4-Plots divided by parameters
%total
incrementPlotsByParameter(CombinedRpareticTotal,CombinedLpareticTotal)

%positive
incrementPlotsByParameter(CombinedRpareticPos,CombinedLpareticPos)

%negative
incrementPlotsByParameter(CombinedRpareticNeg,CombinedLpareticNeg)