%% Plots combining increments in variables and increments in Work
load('VarWorkPowerDataPlots.mat')
%1-Combine the output matices of work and variable analysis

%total work
RpareticRvarsWTOT=combineWorkVariableStructures(RpareticVAR,RpareticWORtot);
RpareticRvarsWTOT=addColorMapValues(RpareticRvarsWTOT);
RpareticLvarsWTOT=combineWorkVariableStructures(RnonpareticVAR,RnonpareticWORtot);
RpareticLvarsWTOT=addColorMapValues(RpareticLvarsWTOT);
LpareticLvarsWTOT=combineWorkVariableStructures(LpareticVAR,LpareticWORtot);
LpareticLvarsWTOT=addColorMapValues(LpareticLvarsWTOT);
LpareticRvarsWTOT=combineWorkVariableStructures(LnonpareticVAR,LnonpareticWORtot);
LpareticRvarsWTOT=addColorMapValues(LpareticRvarsWTOT);

%positive work
RpareticRvarsWPOS=combineWorkVariableStructures(RpareticVAR,RpareticWORpos);
RpareticRvarsWPOS=addColorMapValues(RpareticRvarsWPOS);
RpareticLvarsWPOS=combineWorkVariableStructures(RnonpareticVAR,RnonpareticWORpos);
RpareticLvarsWPOS=addColorMapValues(RpareticLvarsWPOS);
LpareticLvarsWPOS=combineWorkVariableStructures(LpareticVAR,LpareticWORpos);
LpareticLvarsWPOS=addColorMapValues(LpareticLvarsWPOS);
LpareticRvarsWPOS=combineWorkVariableStructures(LnonpareticVAR,LnonpareticWORpos);
LpareticRvarsWPOS=addColorMapValues(LpareticRvarsWPOS);

%negative work
RpareticRvarsWNEG=combineWorkVariableStructures(RpareticVAR,RpareticWORneg);
RpareticRvarsWNEG=addColorMapValues(RpareticRvarsWNEG);
RpareticLvarsWNEG=combineWorkVariableStructures(RnonpareticVAR,RnonpareticWORneg);
RpareticLvarsWNEG=addColorMapValues(RpareticLvarsWNEG);
LpareticLvarsWNEG=combineWorkVariableStructures(LpareticVAR,LpareticWORneg);
LpareticLvarsWNEG=addColorMapValues(LpareticLvarsWNEG);
LpareticRvarsWNEG=combineWorkVariableStructures(LnonpareticVAR,LnonpareticWORneg);
LpareticRvarsWNEG=addColorMapValues(LpareticRvarsWNEG);

%% Total work
plotVarsWork(RpareticRvarsWTOT,LpareticLvarsWTOT,1,1) %paretic side
plotVarsWork(RpareticLvarsWTOT,LpareticRvarsWTOT,1,2) %non-paretic side

%% Positive work
plotVarsWork(RpareticRvarsWPOS,LpareticLvarsWPOS,2,1) %paretic side
plotVarsWork(RpareticLvarsWPOS,LpareticRvarsWPOS,2,2) %non-paretic side

%% Negative work
plotVarsWork(RpareticRvarsWNEG,LpareticLvarsWNEG,3,1) %paretic side
plotVarsWork(RpareticLvarsWNEG,LpareticRvarsWNEG,3,2) %non-paretic side

