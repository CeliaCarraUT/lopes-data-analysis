%% Within trial analysis -Part 2
%This script performs the second (and final) part of the within trial increment analysis
%with the output from 'WhitinTrialIncrementAnalysis2' (after
%running the script for both sets of patients) we join the  matrices for
%all the patients and select the points corresponding to changes in
%parameters independent of changes in the GGF

%After this, data is divided according to the paretic side of the patient
%and finally plotted (some manual modifications must be applied to the
%graphs to get the same as in the paper- i.e.highlight the background of
%the significant ones and plot the trend lines)

%% All patients (instances where no GGF changes were made)
%check the params where the change instances are different from the guidance force ones (saved structs)and combine the data from both patient sets
% Patients={'DS02','PS02';'DS03','PS03';'DS05','PS05';'DS06','PS06'};
% Patients2={'D4','P4';'D10','P10';'D22','P22';'D30','P30';'D105','P105';'D108','P108'};

%load('RRDvariables(wStability).mat')
load('RRDvariables(FINAL).mat')
%load('SMKvariables(wStability).mat')
load('SMKvariables(FINAL).mat')


%variables
[VARplottingmatrix1]=checkChangeInstancesSMK(plottingmatrixVAR1_SMK,ParamChangeInstancesDS02,ParamChangeInstancesDS03,ParamChangeInstancesDS04,ParamChangeInstancesDS05,ParamChangeInstancesDS06,ParamChangeInstancesDS07,ParamChangeInstancesDS08);
[VARplottingmatrix2]=checkChangeInstancesRRD(plottingmatrixVAR1_RRD,ParamChangeInstancesD4,ParamChangeInstancesD10,ParamChangeInstancesD22,ParamChangeInstancesD30,ParamChangeInstancesD105,ParamChangeInstancesD108,ParamChangeInstancesD110);

%second round of checking chance instances
[VARplottingmatrix1]=checkChangeInstancesSMK2(VARplottingmatrix1,StepsStructDS02,StepsStructDS03,StepsStructDS04,StepsStructDS05,StepsStructDS06,StepsStructDS07,StepsStructDS08);
[VARplottingmatrix2]=checkChangeInstancesRRD2(VARplottingmatrix2,StepsStructD4,StepsStructD10,StepsStructD22,StepsStructD30,StepsStructD105,StepsStructD108,StepsStructD110);

%merge the plotting matrices obtained above and also add the points from
%plotting matrices number 2
[VARplottingmatrix1]=mergePlottingMatrices(VARplottingmatrix1,plottingmatrixVAR2_SMK);
[VARplottingmatrix2]=mergePlottingMatrices(VARplottingmatrix2,plottingmatrixVAR2_RRD);
[VARplottingmatrix]=mergePlottingMatrices(VARplottingmatrix1,VARplottingmatrix2);

%% paretic-nonparetic division of data
%two subplots one for paretic and another for non paretic - create two new plotting matrices based on the paretic leg of the patients

%the 'paretic' structs contain patient w paresis on the RIGHT side
%paretic-struct contains the data from the increments in parameters and
%increments in variables of the right side (RIGHT PARETIC PATIENTS)
%nonparetic-struct contains the data from the increments in parameters and
%increments in variables of the left side (RIGHT PARETIC PATIENTS)

%the 'Lparetic' structs contain patient w paresis on the RIGHT side
%Lparetic-struct contains the data from the increments in parameters and
%increments in variables of the left side (LEFT PARETIC PATIENTS)
%Lnonparetic-struct contains the data from the increments in parameters and
%increments in variables of the right side (LEFT PARETIC PATIENTS)

%variables
[RpareticVAR,RnonpareticVAR,LpareticVAR,LnonpareticVAR]=pareticDivisionOfDataVariables(VARplottingmatrix);
