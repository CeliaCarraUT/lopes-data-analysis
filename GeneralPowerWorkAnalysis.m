%Work/power graphs for all patients 
%% First of all, you must select wich patient set to analyze
%Matlab cannot store all the information of all the patients at the same time
%select one of the two sets of patients
Patients={'DS02','PS02';'DS03','PS03';'DS04','PS04';'DS05','PS05';'DS06','PS06';'DS07','PS07';'DS08','PS08'}; %SMK
Patients2={'D4','P4';'D10','P10';'D22','P22';'D30','P30';'D105','P105';'D108','P108';'D110','P110';'D112','P112';'D114','P114'}; %RRD

for p=1:length(Patients) %change taking into account patient set
    DataSet=eval(Patients{p,1});  %change taking into account patient set
    ParamSet=eval(Patients{p,2}); %change taking into account patient set

    %% Remove useless trials
    [DataSet,ParamSet]=removeInvalidTrials(DataSet,ParamSet);
    
    %% Add empty field for storing variables
    DataSet=addResultFieldStruct(DataSet);

    %% Check Changed paameters for every trial
    [ChangedParams,MaxFinalValues]=FindChangedParams(ParamSet);
    %Check if the genreral guidance force is changed and if so when and
    %check if the other parameters change accordingly
    %% Calculate increments within trials (both for parameters and variables)
     for i=1:length(DataSet)%for all of the trials of each subject
         DataSet(i)=calculatePowerSteps(DataSet(i));
         DataSet(i)=calculateWorkSteps(DataSet(i));
         DataSet(i)=calculateSignalsPower(DataSet(i));
         DataSet(i)=calculateSignalsWork(DataSet(i));
     end
     
     WorkPerTrialPlot(DataSet,Patients,p)
     PowerPerTrialPlots(DataSet,Patients,p)
     
end