%% Additional patients RRD
%dir *.signals*
%dir *.params*
%% P112
p112sigfilenames=['ARTS LOPES - Copy\112\recordings\20170523_151046.signals';  'ARTS LOPES - Copy\112\recordings\20170619_133901.signals';  'ARTS LOPES - Copy\112\recordings\20170629_110754.signals';  'ARTS LOPES - Copy\112\recordings\20170717_133813.signals';  
'ARTS LOPES - Copy\112\recordings\20170523_151426.signals';  'ARTS LOPES - Copy\112\recordings\20170619_135108.signals';  'ARTS LOPES - Copy\112\recordings\20170629_112134.signals';  'ARTS LOPES - Copy\112\recordings\20170717_134855.signals';  
'ARTS LOPES - Copy\112\recordings\20170523_152203.signals';  'ARTS LOPES - Copy\112\recordings\20170622_154135.signals';  'ARTS LOPES - Copy\112\recordings\20170703_134306.signals';  'ARTS LOPES - Copy\112\recordings\20170718_133305.signals';  
'ARTS LOPES - Copy\112\recordings\20170608_160700.signals';  'ARTS LOPES - Copy\112\recordings\20170622_154532.signals';  'ARTS LOPES - Copy\112\recordings\20170703_135522.signals';  'ARTS LOPES - Copy\112\recordings\20170718_134528.signals';  
'ARTS LOPES - Copy\112\recordings\20170608_161542.signals';  'ARTS LOPES - Copy\112\recordings\20170622_155531.signals';  'ARTS LOPES - Copy\112\recordings\20170704_111354.signals';  'ARTS LOPES - Copy\112\recordings\20170720_111449.signals';  
'ARTS LOPES - Copy\112\recordings\20170612_141055.signals';  'ARTS LOPES - Copy\112\recordings\20170626_133629.signals';  'ARTS LOPES - Copy\112\recordings\20170704_133805.signals';  'ARTS LOPES - Copy\112\recordings\20170720_112513.signals';  
'ARTS LOPES - Copy\112\recordings\20170612_142150.signals';  'ARTS LOPES - Copy\112\recordings\20170626_134734.signals';  'ARTS LOPES - Copy\112\recordings\20170704_135205.signals';  
'ARTS LOPES - Copy\112\recordings\20170613_134214.signals';  'ARTS LOPES - Copy\112\recordings\20170627_133759.signals';  'ARTS LOPES - Copy\112\recordings\20170711_133750.signals';  
'ARTS LOPES - Copy\112\recordings\20170613_135312.signals';  'ARTS LOPES - Copy\112\recordings\20170627_134234.signals';  'ARTS LOPES - Copy\112\recordings\20170711_134633.signals';  
'ARTS LOPES - Copy\112\recordings\20170619_133541.signals';  'ARTS LOPES - Copy\112\recordings\20170627_135218.signals';  'ARTS LOPES - Copy\112\recordings\20170717_133644.signals'];  
p112sigfilenames = cellstr(p112sigfilenames);  

p112paramfilenames=['ARTS LOPES - Copy\112\recordings\20170523_151046.params';  'ARTS LOPES - Copy\112\recordings\20170619_133901.params';  'ARTS LOPES - Copy\112\recordings\20170629_110754.params';  'ARTS LOPES - Copy\112\recordings\20170717_133813.params';  
'ARTS LOPES - Copy\112\recordings\20170523_151426.params';  'ARTS LOPES - Copy\112\recordings\20170619_135108.params';  'ARTS LOPES - Copy\112\recordings\20170629_112134.params';  'ARTS LOPES - Copy\112\recordings\20170717_134855.params';  
'ARTS LOPES - Copy\112\recordings\20170523_152203.params';  'ARTS LOPES - Copy\112\recordings\20170622_154135.params';  'ARTS LOPES - Copy\112\recordings\20170703_134306.params';  'ARTS LOPES - Copy\112\recordings\20170718_133305.params';  
'ARTS LOPES - Copy\112\recordings\20170608_160700.params';  'ARTS LOPES - Copy\112\recordings\20170622_154532.params';  'ARTS LOPES - Copy\112\recordings\20170703_135522.params';  'ARTS LOPES - Copy\112\recordings\20170718_134528.params';  
'ARTS LOPES - Copy\112\recordings\20170608_161542.params';  'ARTS LOPES - Copy\112\recordings\20170622_155531.params';  'ARTS LOPES - Copy\112\recordings\20170704_111354.params';  'ARTS LOPES - Copy\112\recordings\20170720_111449.params';  
'ARTS LOPES - Copy\112\recordings\20170612_141055.params';  'ARTS LOPES - Copy\112\recordings\20170626_133629.params';  'ARTS LOPES - Copy\112\recordings\20170704_133805.params';  'ARTS LOPES - Copy\112\recordings\20170720_112513.params';  
'ARTS LOPES - Copy\112\recordings\20170612_142150.params';  'ARTS LOPES - Copy\112\recordings\20170626_134734.params';  'ARTS LOPES - Copy\112\recordings\20170704_135205.params';  
'ARTS LOPES - Copy\112\recordings\20170613_134214.params';  'ARTS LOPES - Copy\112\recordings\20170627_133759.params';  'ARTS LOPES - Copy\112\recordings\20170711_133750.params';  
'ARTS LOPES - Copy\112\recordings\20170613_135312.params';  'ARTS LOPES - Copy\112\recordings\20170627_134234.params';  'ARTS LOPES - Copy\112\recordings\20170711_134633.params';  
'ARTS LOPES - Copy\112\recordings\20170619_133541.params';  'ARTS LOPES - Copy\112\recordings\20170627_135218.params';  'ARTS LOPES - Copy\112\recordings\20170717_133644.params']; 
p112paramfilenames = cellstr(p112paramfilenames); 

for i=1:length(p112sigfilenames)
    [D112(i),P112(i)]=readSignalsFile(p112sigfilenames{i,1},p112paramfilenames{i,1});
end

D112=OrderStructDate(D112);
P112=OrderStructDate(P112);

%% P114
p114sigfilenames=['ARTS LOPES - Copy\114\recordings\20180522_093846.signals';  'ARTS LOPES - Copy\114\recordings\20180522_094903.signals';  'ARTS LOPES - Copy\114\recordings\20180529_150941.signals';  'ARTS LOPES - Copy\114\recordings\20180530_082809.signals';  
'ARTS LOPES - Copy\114\recordings\20180522_094149.signals';  'ARTS LOPES - Copy\114\recordings\20180522_095044.signals';  'ARTS LOPES - Copy\114\recordings\20180529_151307.signals';  'ARTS LOPES - Copy\114\recordings\20180530_083236.signals';  
'ARTS LOPES - Copy\114\recordings\20180522_094419.signals';  'ARTS LOPES - Copy\114\recordings\20180529_150837.signals';  'ARTS LOPES - Copy\114\recordings\20180529_151800.signals'];  
p114sigfilenames = cellstr(p114sigfilenames);  

p114paramfilenames=['ARTS LOPES - Copy\114\recordings\20180522_093846.params';  'ARTS LOPES - Copy\114\recordings\20180522_094903.params';  'ARTS LOPES - Copy\114\recordings\20180529_150941.params';  'ARTS LOPES - Copy\114\recordings\20180530_082809.params';  
'ARTS LOPES - Copy\114\recordings\20180522_094149.params';  'ARTS LOPES - Copy\114\recordings\20180522_095044.params';  'ARTS LOPES - Copy\114\recordings\20180529_151307.params';  'ARTS LOPES - Copy\114\recordings\20180530_083236.params';  
'ARTS LOPES - Copy\114\recordings\20180522_094419.params';  'ARTS LOPES - Copy\114\recordings\20180529_150837.params';  'ARTS LOPES - Copy\114\recordings\20180529_151800.params']; 
p114paramfilenames = cellstr(p114paramfilenames); 

for i=1:length(p114sigfilenames)
    [D114(i),P114(i)]=readSignalsFile(p114sigfilenames{i,1},p114paramfilenames{i,1});
end

D114=OrderStructDate(D114);
P114=OrderStructDate(P114);
