%% Forces & Power analysis Part 1
%% First of all, you must select wich patient set to analyze
%Matlab cannot store all the information of all the patients at the same time
%select one of the two sets of patients
Patients={'DS02','PS02';'DS03','PS03';'DS04','PS04';'DS05','PS05';'DS06','PS06'};%'DS07','PS07';'DS08','PS08'}; %SMK
Patients2={'D4','P4';'D10','P10';'D22','P22';'D30','P30';'D105','P105';'D108','P108';'D110','P110'};%'D112','P112';'D114','P114'}; %RRD

    
for p=1:length(Patients) %change taking into account patient set
    DataSet=eval(Patients{p,1});  %change taking into account patient set
    ParamSet=eval(Patients{p,2}); %change taking into account patient set

    %% Remove useless trials
    [DataSet,ParamSet]=removeInvalidTrials(DataSet,ParamSet);
    
    %% Add empty field for storing variables
    DataSet=addResultFieldStruct(DataSet);

    %% Check Changed paameters for every trial
    [ChangedParams,MaxFinalValues]=FindChangedParams(ParamSet);
    %Check if the genreral guidance force is changed and if so when and
    %check if the other parameters change accordingly
    %% Calculate increments within trials (both for parameters and variables)
    for j=1:length(ParamSet)
        trialchar{j,1}=sprintf('trial%d',j);
    end

    count=0;
     for i=1:length(DataSet)%for all of the trials of each subject
         DataSet(i)=calculatePowerSteps(DataSet(i));
         DataSet(i)=calculateWorkSteps(DataSet(i));
         DataSet(i)=calculateInteractionForceSteps(DataSet(i));
         %check if the first step of the trial is left or right
        A=find(DataSet(i).data.m_cont_stepCounter==1,1);
        if DataSet(i).data.m_cont_kinematicPhase(A)==4 || DataSet(i).data.m_cont_kinematicPhase(A)==3
           firstStepLeft=1; 
        else
           firstStepLeft=0; 
        end
        params={};
        count2=0;
            for j=1:size(ChangedParams,1) %for every tunable parameter
  
                if isempty(ChangedParams{j,i}) %check if the parameter is changed-the parameter is not changed
                    count=count;
                else  %check if the parameter is changed-the parameter is changed
                   count2=count2+1;
                   params{count2,1}=ChangedParams{j,i};
                   [values,changeInstances,changeSteps]=calculateWithinTrialParameterValues(DataSet(i),ParamSet(i),ChangedParams{j,i});
                   %store this information in structures
                   WithinTrialParamValues.(trialchar{i,1}).(params{count2,1})=values;
                   ParamChangeInstances.(trialchar{i,1}).(params{count2,1})=changeInstances;
                   Steps.(trialchar{i,1}).(params{count2,1})=changeSteps;
                   WithinTrialParamIncrements.(trialchar{i,1}).(params{count2,1})=diff(WithinTrialParamValues.(trialchar{i,1}).(params{count2,1})); %structure that contains the increments in the parameters (for every trial)
                end

            %select the adecuate variable sections, remove outliers,compute mean, before and after change and store in a structure.
            
%             for e=1:size(params,1)
                for k=1:length(changeSteps) %for every value the parameter is set to during a trial
                    if length(changeSteps)==1 %the parameter is kept constant (only one value)
                    %interaction forces
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PX=[];
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PZ=[];
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHA=[];
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHF=[];
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LKF=[];
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHA=[];
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHF=[];
%                         IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RKF=[];
                        %power TOTAL
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PX=[];
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PZ=[];
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHA=[];
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHF=[];
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LKF=[];
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHA=[];
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHF=[];
                        IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RKF=[];
                        %power NEGATIVE
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PX=[];
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PZ=[];
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHA=[];
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHF=[];
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LKF=[];
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHA=[];
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHF=[];
                        IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RKF=[];
                        %POWER POSITIVE
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PX=[];
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PZ=[];
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHA=[];
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHF=[];
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LKF=[];
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHA=[];
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHF=[];
                        IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RKF=[];
                        
                        %WORK total
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PX=[];
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PZ=[];
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHA=[];
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHF=[];
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LKF=[];
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHA=[];
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHF=[];
                        IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RKF=[];
                        %WORK negative
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PX=[];
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PZ=[];
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHA=[];
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHF=[];
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LKF=[];
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHA=[];
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHF=[];
                        IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RKF=[];
                        %WORK positive
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PX=[];
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PZ=[];
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHA=[];
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHF=[];
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LKF=[];
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHA=[];
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHF=[];
                        IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RKF=[];
                    else
                        initialstep=changeSteps(k)-10;
                        finalstep=changeSteps(k)+10;
                        step2=changeSteps(k)*2;
                        initialstep2=step2-20;
                        finalstep2=step2+20;
                            
                        if initialstep<0 || finalstep> DataSet(i).data.m_cont_stepCounter(end) || finalstep2> DataSet(i).data.m_cont_stepCounter(end)%there is less than 20 steps before/after the change 
                             %interaction forces
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PX(k,1)=0;
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PZ(k,1)=0;
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHA(k,1)=0;
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHF(k,1)=0;
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LKF(k,1)=0;
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHA(k,1)=0;
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHF(k,1)=0;
%                             IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RKF(k,1)=0;
                            %power TOTAL
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PX(k,1)=NaN;
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PZ(k,1)=NaN;
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHA(k,1)=NaN;
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHF(k,1)=NaN;
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LKF(k,1)=NaN;
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHA(k,1)=NaN;
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHF(k,1)=NaN;
                            IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RKF(k,1)=NaN;
                            %power NEGATIVE
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PX(k,1)=NaN;
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PZ(k,1)=NaN;
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHA(k,1)=NaN;
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHF(k,1)=NaN;
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LKF(k,1)=NaN;
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHA(k,1)=NaN;
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHF(k,1)=NaN;
                            IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RKF(k,1)=NaN;
                            %POWER POSITIVE
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PX(k,1)=NaN;
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PZ(k,1)=NaN;
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHA(k,1)=NaN;
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHF(k,1)=NaN;
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LKF(k,1)=NaN;
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHA(k,1)=NaN;
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHF(k,1)=NaN;
                            IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RKF(k,1)=NaN;
                            
                            %WORK total
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PX(k,1)=NaN;
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PZ(k,1)=NaN;
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHA(k,1)=NaN;
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHF(k,1)=NaN;
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LKF(k,1)=NaN;
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHA(k,1)=NaN;
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHF(k,1)=NaN;
                            IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RKF(k,1)=NaN;
                            %WORK negative
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PX(k,1)=NaN;
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PZ(k,1)=NaN;
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHA(k,1)=NaN;
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHF(k,1)=NaN;
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LKF(k,1)=NaN;
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHA(k,1)=NaN;
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHF(k,1)=NaN;
                            IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RKF(k,1)=NaN;
                            %WORK positive
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PX(k,1)=NaN;
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PZ(k,1)=NaN;
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHA(k,1)=NaN;
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHF(k,1)=NaN;
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LKF(k,1)=NaN;
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHA(k,1)=NaN;
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHF(k,1)=NaN;
                            IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RKF(k,1)=NaN;
                        else
                            vector1=(initialstep:changeSteps(k)-1);
                            vector2=(changeSteps(k)+1:finalstep);
                        
                            vec1=(initialstep2:step2-1);
                            vec2=(step2+1:finalstep2);
                            
                            if  firstStepLeft==1 %odd instances are the left ones
                                Lvector1=vec1(1:2:end);
                                Lvector2=vec2(1:2:end);
                                Rvector1=vec1(2:2:end);
                                Rvector2=vec2(2:2:end);
                  
                               %power TOTAL
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PX(:,1),vec1,vec2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PZ(:,1),vec1,vec2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHA(:,1),Lvector1,Lvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHF(:,1),Lvector1,Lvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LKF(:,1),Lvector1,Lvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHA(:,1),Rvector1,Rvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHF(:,1),Rvector1,Rvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RKF(:,1),Rvector1,Rvector2);
                               %power NEGATIVE
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PX(:,2),vec1,vec2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PZ(:,2),vec1,vec2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHA(:,2),Lvector1,Lvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHF(:,2),Lvector1,Lvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LKF(:,2),Lvector1,Lvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHA(:,2),Rvector1,Rvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHF(:,2),Rvector1,Rvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RKF(:,2),Rvector1,Rvector2);
                               %power POSITIVE
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PX(:,3),vec1,vec2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PZ(:,3),vec1,vec2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHA(:,3),Lvector1,Lvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHF(:,3),Lvector1,Lvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LKF(:,3),Lvector1,Lvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHA(:,3),Rvector1,Rvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHF(:,3),Rvector1,Rvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RKF(:,3),Rvector1,Rvector2);
                               
                               %%work TOTAL
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PX(:,1),vec1,vec2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PZ(:,1),vec1,vec2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHA(:,1),Lvector1,Lvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHF(:,1),Lvector1,Lvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LKF(:,1),Lvector1,Lvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHA(:,1),Rvector1,Rvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHF(:,1),Rvector1,Rvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RKF(:,1),Rvector1,Rvector2);
                               %%work NEGATIVE
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PX(:,2),vec1,vec2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PZ(:,2),vec1,vec2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHA(:,2),Lvector1,Lvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHF(:,2),Lvector1,Lvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LKF(:,2),Lvector1,Lvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHA(:,2),Rvector1,Rvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHF(:,2),Rvector1,Rvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RKF(:,2),Rvector1,Rvector2);
                               %%work POSITIVE
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PX(:,3),vec1,vec2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PZ(:,3),vec1,vec2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHA(:,3),Lvector1,Lvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHF(:,3),Lvector1,Lvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LKF(:,3),Lvector1,Lvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHA(:,3),Rvector1,Rvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHF(:,3),Rvector1,Rvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RKF(:,3),Rvector1,Rvector2);

%                                %interaction forces (take RMS) (stepx2) 
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_PX,vec1,vec2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_PZ,vec1,vec2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_LHA,Lvector1,Lvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_LHF,Lvector1,Lvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_LKF,Lvector1,Lvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_RHA,Rvector1,Rvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_RHF,Rvector1,Rvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_RKF,Rvector1,Rvector2);
                           else
                                Lvector1=vec1(2:2:end);
                                Lvector2=vec2(2:2:end);
                                Rvector1=vec1(1:2:end);
                                Rvector2=vec2(1:2:end);
                               %power TOTAL
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PX(:,1),vec1,vec2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PZ(:,1),vec1,vec2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHA(:,1),Lvector1,Lvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHF(:,1),Lvector1,Lvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LKF(:,1),Lvector1,Lvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHA(:,1),Rvector1,Rvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHF(:,1),Rvector1,Rvector2);
                               [IncrementsPowerTotal.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RKF(:,1),Rvector1,Rvector2);
                               %power NEGATIVE
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PX(:,2),vec1,vec2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PZ(:,2),vec1,vec2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHA(:,2),Lvector1,Lvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHF(:,2),Lvector1,Lvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LKF(:,2),Lvector1,Lvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHA(:,2),Rvector1,Rvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHF(:,2),Rvector1,Rvector2);
                               [IncrementsPowerNeg.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RKF(:,2),Rvector1,Rvector2);
                               %power POSITIVE
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PX(:,3),vec1,vec2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_PZ(:,3),vec1,vec2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHA(:,3),Lvector1,Lvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LHF(:,3),Lvector1,Lvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_LKF(:,3),Lvector1,Lvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHA(:,3),Rvector1,Rvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RHF(:,3),Rvector1,Rvector2);
                               [IncrementsPowerPos.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.PowerBySteps_RKF(:,3),Rvector1,Rvector2);
                               
                               %%work TOTAL
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PX(:,1),vec1,vec2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PZ(:,1),vec1,vec2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHA(:,1),Lvector1,Lvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHF(:,1),Lvector1,Lvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LKF(:,1),Lvector1,Lvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHA(:,1),Rvector1,Rvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHF(:,1),Rvector1,Rvector2);
                               [IncrementsWorkTotal.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RKF(:,1),Rvector1,Rvector2);
                               %%work NEGATIVE
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PX(:,2),vec1,vec2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PZ(:,2),vec1,vec2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHA(:,2),Lvector1,Lvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHF(:,2),Lvector1,Lvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LKF(:,2),Lvector1,Lvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHA(:,2),Rvector1,Rvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHF(:,2),Rvector1,Rvector2);
                               [IncrementsWorkNeg.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RKF(:,2),Rvector1,Rvector2);
                               %%work POSITIVE
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PX(:,3),vec1,vec2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_PZ(:,3),vec1,vec2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHA(:,3),Lvector1,Lvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LHF(:,3),Lvector1,Lvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_LKF(:,3),Lvector1,Lvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHA(:,3),Rvector1,Rvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RHF(:,3),Rvector1,Rvector2);
                               [IncrementsWorkPos.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateVariableIncrements(DataSet(i).Results.WorkBySteps_RKF(:,3),Rvector1,Rvector2);
                               
                               StepsStruct.(trialchar{i,1}).(params{count2,1})(k,1)=changeSteps(k);
%                                %interaction forces (take RMS) (stepx2) 
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PX(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_PX,vec1,vec2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).PZ(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_PZ,vec1,vec2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHA(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_LHA,Lvector1,Lvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LHF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_LHF,Lvector1,Lvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).LKF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_LKF,Lvector1,Lvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHA(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_RHA,Rvector1,Rvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RHF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_RHF,Rvector1,Rvector2);
%                                [IncrementsIForces.(trialchar{i,1}).(params{count2,1}).RKF(k,1)]=calculateForceIncrements(DataSet(i).data.ForcesBySteps_RKF,Rvector1,Rvector2);
                           end
                        end
                    end
                end
                %clear initialstep finalstep changeSteps initialstep2finalstep2 step2 values changeInstances changeSteps
            end
     end
     
     %% Remove empty fields in the paramIncrements and variable increments 
     %first remove empty fields of the PARAMETER increments
     WithinTrialParamIncrements=removeEmptyFieldsParams(DataSet,WithinTrialParamIncrements);
     WithinTrialParamValues=removeEmptyFieldsParams(DataSet,WithinTrialParamValues);
     ParamChangeInstances=removeEmptyFieldsParams(DataSet,ParamChangeInstances);
     
     %Remove empty fields of the POWER increments (first loop takes the empty parameter fields inside the 'trial' fields)
     IncrementsPowerTotal=removeEmptyFieldsVars(IncrementsPowerTotal);
     IncrementsPowerNeg=removeEmptyFieldsVars(IncrementsPowerNeg);
     IncrementsPowerPos=removeEmptyFieldsVars(IncrementsPowerPos);
     %Remove empty fields of the WORK increments (first loop takes the empty parameter fields inside the 'trial' fields)
     IncrementsWorkTotal=removeEmptyFieldsVars(IncrementsWorkTotal);
     IncrementsWorkNeg=removeEmptyFieldsVars(IncrementsWorkNeg);
     IncrementsWorkPos=removeEmptyFieldsVars(IncrementsWorkPos);
     
     %Remove empty fields of the FORCE increments (first loop takes the empty parameter fields inside the 'trial' fields)
%    IncrementsIForces=removeEmptyFieldsVars(IncrementsIForces);
   
        trials=fieldnames(StepsStruct);
        for i=1:length(trials)
            params=fieldnames(StepsStruct.(trials{i,1}));
            for j=1:length(params)
                StepsStruct.(trials{i,1}).(params{j,1})(1)=[];
            end
        end
    
    %% Check everything is stored correctly     
    %remove extra NaNs
    IncrementsPowerTotal=RemoveNaNsStructs(IncrementsPowerTotal);
    IncrementsPowerNeg=RemoveNaNsStructs(IncrementsPowerNeg);
    IncrementsPowerPos=RemoveNaNsStructs(IncrementsPowerPos);
    %IncrementsIForces=RemoveNaNsStructs(IncrementsIForces);
    IncrementsWorkTotal=RemoveNaNsStructs(IncrementsWorkTotal);
    IncrementsWorkNeg=RemoveNaNsStructs(IncrementsWorkNeg);
    IncrementsWorkPos=RemoveNaNsStructs(IncrementsWorkPos);
     
   %% Finally create the output matrices   
   if p==1
       %power
       [plottingmatrixPOWtot1,plottingmatrixPOWtot2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerTotal,ParamSet,p);
       [plottingmatrixPOWneg1,plottingmatrixPOWneg2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerNeg,ParamSet,p);
       [plottingmatrixPOWpos1,plottingmatrixPOWpos2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerPos,ParamSet,p);
      %[plottingmatrixFOR1,plottingmatrixFOR2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsIForces,p);
       %work
       [plottingmatrixWORtot1,plottingmatrixWORtot2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsWorkTotal,ParamSet,p);
       [plottingmatrixWORneg1,plottingmatrixWORneg2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsWorkNeg,ParamSet,p);
       [plottingmatrixWORpos1,plottingmatrixWORpos2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsWorkPos,ParamSet,p);
   else
       %power
       [TEMPplottingmatrixPOWtot1,TEMPplottingmatrixPOWtot2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerTotal,ParamSet,p);
       [TEMPplottingmatrixPOWneg1,TEMPplottingmatrixPOWneg2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerNeg,ParamSet,p);
       [TEMPplottingmatrixPOWpos1,TEMPplottingmatrixPOWpos2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerPos,ParamSet,p);
       %merge the plotting matrices and increase the counter of the pos.count structures
       plottingmatrixPOWtot1=mergePlottingMatrices(plottingmatrixPOWtot1,TEMPplottingmatrixPOWtot1);
       if isempty(plottingmatrixPOWtot2)==1
           plottingmatrixPOWtot2=TEMPplottingmatrixPOWtot2;
       elseif isempty(plottingmatrixPOWtot2)==0 && isempty(TEMPplottingmatrixPOWtot2)==0
           plottingmatrixPOWtot2=mergePlottingMatrices(plottingmatrixPOWtot2,TEMPplottingmatrixPOWtot2);
       end
       plottingmatrixPOWneg1=mergePlottingMatrices(plottingmatrixPOWneg1,TEMPplottingmatrixPOWneg1);
       if isempty(plottingmatrixPOWneg2)==1
           plottingmatrixPOWneg2=TEMPplottingmatrixPOWneg2;
       elseif isempty(plottingmatrixPOWneg2)==0 && isempty(TEMPplottingmatrixPOWneg2)==0
           plottingmatrixPOWneg2=mergePlottingMatrices(plottingmatrixPOWneg2,TEMPplottingmatrixPOWneg2);
       end
       plottingmatrixPOWpos1=mergePlottingMatrices(plottingmatrixPOWpos1,TEMPplottingmatrixPOWpos1);
       if isempty(plottingmatrixPOWpos2)==1
           plottingmatrixPOWpos2=TEMPplottingmatrixPOWpos2;
       elseif isempty(plottingmatrixPOWpos2)==0 && isempty(TEMPplottingmatrixPOWpos2)==0
           plottingmatrixPOWpos2=mergePlottingMatrices(plottingmatrixPOWpos2,TEMPplottingmatrixPOWpos2);
       end
       
       %work
       [TEMPplottingmatrixWORtot1,TEMPplottingmatrixWORtot2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerTotal,ParamSet,p);
       [TEMPplottingmatrixWORneg1,TEMPplottingmatrixWORneg2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerNeg,ParamSet,p);
       [TEMPplottingmatrixWORpos1,TEMPplottingmatrixWORpos2]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsPowerPos,ParamSet,p);
       %merge the plotting matrices and increase the counter of the pos.count structures
       plottingmatrixWORtot1=mergePlottingMatrices(plottingmatrixWORtot1,TEMPplottingmatrixWORtot1);
       if isempty(plottingmatrixWORtot2)==1
           plottingmatrixWORtot2=TEMPplottingmatrixWORtot2;
       elseif isempty(plottingmatrixWORtot2)==0 && isempty(TEMPplottingmatrixWORtot2)==0
           plottingmatrixWORtot2=mergePlottingMatrices(plottingmatrixWORtot2,TEMPplottingmatrixWORtot2);
       end
       plottingmatrixWORneg1=mergePlottingMatrices(plottingmatrixWORneg1,TEMPplottingmatrixWORneg1);
       if isempty(plottingmatrixWORneg2)==1
           plottingmatrixWORneg2=TEMPplottingmatrixWORneg2;
       elseif isempty(plottingmatrixWORneg2)==0 && isempty(TEMPplottingmatrixWORneg2)==0
           plottingmatrixWORneg2=mergePlottingMatrices(plottingmatrixWORneg2,TEMPplottingmatrixWORneg2);
       end
       plottingmatrixWORpos1=mergePlottingMatrices(plottingmatrixWORpos1,TEMPplottingmatrixWORpos1);
       if isempty(plottingmatrixWORpos2)==1
           plottingmatrixWORpos2=TEMPplottingmatrixWORpos2;
       elseif isempty(plottingmatrixWORpos2)==0 && isempty(TEMPplottingmatrixWORpos2)==0
           plottingmatrixWORpos2=mergePlottingMatrices(plottingmatrixWORpos2,TEMPplottingmatrixWORpos2);
       end
   end
   
   %% save the WithinTrialParamValues and ParamChangeInstances for every patient
%  assignin('base',strcat('WithinTrialParamValues',Patients{p,1}),WithinTrialParamValues); 
   assignin('base',strcat('ParamChangeInstances',Patients{p,1}),ParamChangeInstances); %change taking into account patient set
   
  % Clear all the variables for next patient (except for plottingmatrix and poscount)
    clear ChangedParams count fieldnam i b IncrementsVariables Invalidtrials j k l m finalstep changeSteps step2 initalstep2 finalstep2  fieldnam3 counter1 counter2 counter3 vector1 vector2 vec1 vec2 IncrementsPowerTotal IncrementsPowerNeg IncrementsPowerPos...
        method n initialstep trialchar values WhitinTrialVariableValues WithinTrialParamIncrements WithinTrialParamValues InvalidTrials DataSet ParamSet fieldnam1 number trialnums trials ParamChangeInstances v  counterNonValid numb numb2 numb3 params2 Lvector1 Lvector2 Rvector1...
        Rvector2 params MaxFinalValues count1 count2 count3 A changeInstances firstStepLeft initialstep2 MaxFinalValues TEMPplottingmatrixPOWneg1 TEMPplottingmatrixPOWneg2 TEMPplottingmatrixPOWpos1 TEMPplottingmatrixPOWpos2 TEMPplottingmatrixPOWtot1 TEMPplottingmatrixPOWtot2...
        TEMPplottingmatrixWORneg1 TEMPplottingmatrixWORneg2 TEMPplottingmatrixWORpos1 TEMPplottingmatrixWORpos2 TEMPplottingmatrixWORtot1 TEMPplottingmatrixWORtot2 Steps IncrementsWorkNeg IncrementsWorkPos IncrementsWorkTotal p
    
end
   %% Remove the 'all NaN' rows of the plotting matrices for following analysis
   %power
   [plottingmatrixPOWtot1]=RemoveNaNsRows(plottingmatrixPOWtot1,2);
   [plottingmatrixPOWtot2]=RemoveNaNsRows(plottingmatrixPOWtot2,2);
   [plottingmatrixPOWneg1]=RemoveNaNsRows(plottingmatrixPOWneg1,2);
   [plottingmatrixPOWneg2]=RemoveNaNsRows(plottingmatrixPOWneg2,2);
   [plottingmatrixPOWpos1]=RemoveNaNsRows(plottingmatrixPOWpos1,2);
   [plottingmatrixPOWpos2]=RemoveNaNsRows(plottingmatrixPOWpos2,2);
   %work
   [plottingmatrixWORtot1]=RemoveNaNsRows(plottingmatrixWORtot1,2);
   [plottingmatrixWORtot2]=RemoveNaNsRows(plottingmatrixWORtot2,2);
   [plottingmatrixWORneg1]=RemoveNaNsRows(plottingmatrixWORneg1,2);
   [plottingmatrixWORneg2]=RemoveNaNsRows(plottingmatrixWORneg2,2);
   [plottingmatrixWORpos1]=RemoveNaNsRows(plottingmatrixWORpos1,2);
   [plottingmatrixWORpos2]=RemoveNaNsRows(plottingmatrixWORpos2,2);
