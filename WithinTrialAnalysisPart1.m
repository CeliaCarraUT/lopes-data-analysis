%% Within trial analysis -Part 1
%This script performs the first part of the within trial increment analysis
%inputs are the patient structs with all the information
%outputs are:
%plottingmatrices 1,2,3 - matrices containing the parameter increment,
%parameter initial anf final values, variable increments, patient number
%and trial number
%paramchangeindeces - structs containing the instance when the parameters are changes for every patient, trial and parameter 

%This code must be run twice to obtain the data from all patients (from the
%two datasets, RRD and SMK)

%% First of all, you must select wich patient set to analyze
%Matlab cannot store all the information of all the patients at the same time
%select one of the two sets of patients
Patients={'DS02','PS02';'DS03','PS03';'DS04','PS04';'DS05','PS05';'DS06','PS06';'DS07','PS07';'DS08','PS08'}; %SMK
Patients2={'D4','P4';'D10','P10';'D22','P22';'D30','P30';'D105','P105';'D108','P108';'D110','P110'}; %RRD
count1=0;
count2=0;
count3=0;
counterNonValid=0;
    
for p=1:length(Patients) %change taking into account patient set
    DataSet=eval(Patients{p,1});  %change taking into account patient set
    ParamSet=eval(Patients{p,2}); %change taking into account patient set

    %% Remove useless trials
    [DataSet,ParamSet]=removeInvalidTrials(DataSet,ParamSet);
    
    %% Add empty field for storing variables
    DataSet=addResultFieldStruct(DataSet);

    %% Check Changed parameters for every trial
    [ChangedParams,MaxFinalValues]=FindChangedParams(ParamSet);
    %Check if the general guidance force is changed and if so when and
    %check if the other parameters change accordingly
    
    %% Calculate increments within trials (both for parameters and variables)
    for j=1:length(ParamSet)
        trialchar{j,1}=sprintf('trial%d',j);
    end

    count=0;
    for i=1:length(DataSet)%for all of the trials of each subject
        %calculate all the variables
        DataSet(i)=calculateStepHeight(DataSet(i));
        DataSet(i)=calculateStepLength(DataSet(i));
        DataSet(i)=calculateStability(DataSet(i));
        DataSet(i)=calculateLateralFPos(DataSet(i));
        DataSet(i)=calculatePrepositioning(DataSet(i));
        DataSet(i)=calculateWeightShift(DataSet(i));
        %calculate power, work and interaction forces per step

        %check if the first step of the trial is left or right
        A=find(DataSet(i).data.m_cont_stepCounter==1,1);
        if DataSet(i).data.m_cont_kinematicPhase(A)==4 || DataSet(i).data.m_cont_kinematicPhase(A)==3
           firstStepLeft=1; 
        else
           firstStepLeft=0; 
        end
        params={};
        count2=0;
        for j=1:size(ChangedParams,1) %for every tunable parameter
            counter3=0;
            if isempty(ChangedParams{j,i}) %check if the parameter is changed-the parameter is not changed
                count=count;
            else  %check if the parameter is changed-the parameter is changed
               count2=count2+1;
               params{count2,1}=ChangedParams{j,i};
               [values,changeInstances,changeSteps]=calculateWithinTrialParameterValues(DataSet(i),ParamSet(i),ChangedParams{j,i});
               %store this information in structures
               AllWithinTrialParamValues.(trialchar{i,1}).(params{count2,1})=values;
               AllParamChangeInstances.(trialchar{i,1}).(params{count2,1})=changeInstances;
               AllSteps.(trialchar{i,1}).(params{count2,1})=changeSteps;
               %WithinTrialParamIncrements.(trialchar{i,1}).(params{count2,1})=diff(WithinTrialParamValues.(trialchar{i,1}).(params{count2,1})); %structure that contains the increments in the parameters (for every trial)
            end
     
            %select the adecuate variable sections, remove outliers,compute mean, before and after change and store in a structure.
            
%             for e=1:size(params,1)
                for k=1:length(changeSteps) %for every value the parameter is set to during a trial
                    if length(changeSteps)==1 %the parameter is kept constant (only one value)
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStepHeights=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStepHeights=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStepLengthInitial=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStepLengthInitial=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStepLengthFinal=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStepLengthFinal=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStability=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStability=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RLateralFootPos=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LLateralFootPos=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RPrepositioning=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LPrepositioning=[];
                        IncrementsVariables.(trialchar{i,1}).(params{count2,1}).WeightShift=[];
                        StepsStruct.(trialchar{i,1}).(params{count2,1})=changeSteps(k);
                        ParamChangeInstances.(trialchar{i,1}).(params{count2,1})=changeInstances(k);
                        WithinTrialParamValues.(trialchar{i,1}).(params{count2,1})=values(k);

                    else
                        if k==1 && changeSteps(k)==0 && changeSteps(k+1)>20
                            initialstep=changeSteps(k+1)-1-20;
                            finalstep=changeSteps(k+1)-1;
                            initialstep2=initialstep*2;
                            finalstep2=finalstep*2;
                        elseif  k==1 && changeSteps(k)==0 && changeSteps(k+1)<20
                            initialstep=-1;
                            finalstep=0;
                            initialstep2=0;
                            finalstep2=0;
                        else
                            initialstep=changeSteps(k)-10;
                            finalstep=changeSteps(k)+10;
                            step2=changeSteps(k)*2;
                            initialstep2=step2-20;
                            finalstep2=step2+20;
                        end
                        
                        if initialstep<0 || finalstep> DataSet(i).data.m_cont_stepCounter(end) || finalstep2> DataSet(i).data.m_cont_stepCounter(end) %there is less than 20 steps before/after the change
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStepHeights(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStepHeights(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStepLengthInitial(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStepLengthInitial(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStepLengthFinal(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStepLengthFinal(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RStability(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LStability(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RLateralFootPos=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LLateralFootPos=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).RPrepositioning(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).LPrepositioning(k,1)=NaN;
%                             IncrementsVariables.(trialchar{i,1}).(params{count2,1}).WeightShift(k,1)=NaN;
%                             StepsStruct.(trialchar{i,1}).(params{count2,1})(k,1)=changeSteps(k);
%                             ParamChangeInstances.(trialchar{i,1}).(params{count2,1})(k,1)=changeInstances(k);
                        else
                            counter3=counter3+1;
%                             vector1=(initialstep:changeSteps(k)-1);
%                             vector2=(changeSteps(k)+1:finalstep);
%                             %step height
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStepHeights(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.StepHeightDiff.Rsteps(vector1)));%before change 
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStepHeights(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.StepHeightDiff.Rsteps(vector2)));%after change 
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStepHeights(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.StepHeightDiff.Lsteps(vector1)));%before change
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStepHeights(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.StepHeightDiff.Lsteps(vector2)));%after
% 
%                             %Initial step length
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStepLengthInitial(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Rsteps.min(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStepLengthInitial(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Rsteps.min(vector2)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStepLengthInitial(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Lsteps.min(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStepLengthInitial(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Lsteps.min(vector2)));
% 
%                             %Terminal step length1
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStepLengthFinal(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Rsteps.max(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStepLengthFinal(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Rsteps.max(vector2)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStepLengthFinal(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Lsteps.max(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStepLengthFinal(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.StepLengthDiff.Lsteps.max(vector2)));
% 
%                             %Stability
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStability(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.Stability.Rsteps(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RStability(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.Stability.Rsteps(vector2)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStability(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.Stability.Lsteps(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LStability(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.Stability.Lsteps(vector2)));
% 
%                             %Lateral Foot Position
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RLateralFootPos(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.LateralFootPos.Rsteps(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RLateralFootPos(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.LateralFootPos.Rsteps(vector2)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LLateralFootPos(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.LateralFootPos.Lsteps(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LLateralFootPos(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.LateralFootPos.Lsteps(vector2)));
% 
%                             %Prepositioning
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RPrepositioning(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.PrepositioningDiff.Rsteps(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).RPrepositioning(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.PrepositioningDiff.Rsteps(vector2)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LPrepositioning(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.PrepositioningDiff.Lsteps(vector1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).LPrepositioning(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.PrepositioningDiff.Lsteps(vector2)));
% 
%                             %weightshift (stepx2)
%                             vec1=(initialstep2:step2-1);
%                             vec2=(step2+1:finalstep2);
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).WeightShift(counter3,1)]=mean(RemoveOutliersData(DataSet(i).Results.WeightShift(vec1)));
%                             [VariableValues.(trialchar{i,1}).(params{count2,1}).WeightShift(counter3,2)]=mean(RemoveOutliersData(DataSet(i).Results.WeightShift(vec2)));
                            StepsStruct.(trialchar{i,1}).(params{count2,1})(counter3,1)=changeSteps(k);
                            ParamChangeInstances.(trialchar{i,1}).(params{count2,1})(counter3,1)=changeInstances(k);
                            WithinTrialParamValues.(trialchar{i,1}).(params{count2,1})(counter3,1)=values(k);
                        end
                    end
                end
                %clear initialstep finalstep changeSteps initialstep2finalstep2 step2 values changeInstances changeSteps
            %end
        end
    end
   %calculate withintrial param and variable increments
   [WithinTrialParamIncrements]=calculateWithintrialParamIncrements(WithinTrialParamValues);
   
   %calculate variable increments
   IncrementsVariables=calculateVariableIncrements2(WithinTrialParamValues,StepsStruct,DataSet);

   if isempty(IncrementsVariables)==1
   %save the WithinTrialParamValues and ParamChangeInstances for every patient
%  assignin('base',strcat('WithinTrialParamValues',Patients{p,1}),WithinTrialParamValues); 
   assignin('base',strcat('ParamChangeInstances',Patients{p,1}),ParamChangeInstances); %change taking into account patient set
   assignin('base',strcat('StepsStruct',Patients{p,1}),StepsStruct); %change taking into account patient set
   % Clear all the variables for next patient(except for plottingmatrix and poscount)
    clear ChangedParams count fieldnam i b IncrementsVariables Invalidtrials j k l m finalstep changeSteps step2 initalstep2 finalstep2  fieldnam3 counter1 counter2 counter3 vector1 vector2 vec1 vec2 IncrementsPowerTotal IncrementsPowerNeg IncrementsPowerPos...
    method n initialstep trialchar values WhitinTrialVariableValues WithinTrialParamIncrements WithinTrialParamValues InvalidTrials DataSet ParamSet fieldnam1 number trialnums trials ParamChangeInstances v  counterNonValid numb numb2 numb3 params2 Lvector1 Lvector2 Rvector1...
    Rvector2 params MaxFinalValues count1 count2 count3 A changeInstances firstStepLeft initialstep2 MaxFinalValues TEMPplottingmatrixVAR1 TEMPplottingmatrixVAR2 p StepsStruct TEMPplottingmatrixVAR3
   else
   %% Finally create the output matrices   
   if p==1
    [plottingmatrixVAR1,plottingmatrixVAR2,plottingmatrixVAR3]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsVariables,ParamSet,DataSet,p,StepsStruct);
   else
    [TEMPplottingmatrixVAR1,TEMPplottingmatrixVAR2,TEMPplottingmatrixVAR3]=createPlottingMatrices(WithinTrialParamValues,ParamChangeInstances,WithinTrialParamIncrements,IncrementsVariables,ParamSet,DataSet,p,StepsStruct);
    %merge the plotting matrices and increase the counter of the pos.count structures
       plottingmatrixVAR1=mergePlottingMatrices(plottingmatrixVAR1,TEMPplottingmatrixVAR1);
       if isempty(plottingmatrixVAR2)==1
           plottingmatrixVAR2=TEMPplottingmatrixVAR2;
       elseif isempty(plottingmatrixVAR2)==0 && isempty(TEMPplottingmatrixVAR2)==0
          plottingmatrixVAR2=mergePlottingMatrices(plottingmatrixVAR2,TEMPplottingmatrixVAR2);
       end
       
       if isempty(plottingmatrixVAR3)==1
           plottingmatrixVAR3=TEMPplottingmatrixVAR3;
       elseif isempty(plottingmatrixVAR3)==0 && isempty(TEMPplottingmatrixVAR3)==0
          plottingmatrixVAR3=mergePlottingMatrices(plottingmatrixVAR3,TEMPplottingmatrixVAR3);
       end
   end
   
   %% save the WithinTrialParamValues and ParamChangeInstances for every patient
%  assignin('base',strcat('WithinTrialParamValues',Patients{p,1}),WithinTrialParamValues); 
   assignin('base',strcat('ParamChangeInstances',Patients{p,1}),ParamChangeInstances); %change taking into account patient set
   assignin('base',strcat('StepsStruct',Patients{p,1}),StepsStruct); %change taking into account patient set
   %% Clear all the variables for next patient(except for plottingmatrix and poscount)
    clear ChangedParams count fieldnam i b IncrementsVariables Invalidtrials j k l m finalstep changeSteps step2 initalstep2 finalstep2  fieldnam3 counter1 counter2 counter3 vector1 vector2 vec1 vec2 IncrementsPowerTotal IncrementsPowerNeg IncrementsPowerPos...
        method n initialstep trialchar values WhitinTrialVariableValues WithinTrialParamIncrements WithinTrialParamValues InvalidTrials DataSet ParamSet fieldnam1 number trialnums trials ParamChangeInstances v  counterNonValid numb numb2 numb3 params2 Lvector1 Lvector2 Rvector1...
        Rvector2 params MaxFinalValues count1 count2 count3 A changeInstances firstStepLeft initialstep2 MaxFinalValues TEMPplottingmatrixVAR1 TEMPplottingmatrixVAR2 p StepsStruct TEMPplottingmatrixVAR3
    
   end
end
%% save the output of the script-uncomment depending on the patient set
 assignin('base',strcat('plottingmatrixVAR1','_SMK',plottingmatrixVAR1));
% save('SMKwithin.mat','plottingmatrixVAR1','plottingmatrixVAR2','plottingmatrixPOWtot1','plottingmatrixPOWtot2','plottingmatrixPOWneg1','plottingmatrixPOWneg2','plottingmatrixPOWpos1','plottingmatrixPOWpos2','plottingmatrixFOR1','plottingmatrixFOR2','ParamChangeInstancesDS02','ParamChangeInstancesDS03','ParamChangeInstancesDS05','ParamChangeInstancesDS06');
% save('RRDwithin.mat','plottingmatrixVAR1','plottingmatrixVAR2','plottingmatrixPOWtot1','plottingmatrixPOWtot2','plottingmatrixPOWneg1','plottingmatrixPOWneg2','plottingmatrixPOWpos1','plottingmatrixPOWpos2','plottingmatrixFOR1','plottingmatrixFOR2','ParamChangeInstancesD10','ParamChangeInstancesD105','ParamChangeInstancesD108','ParamChangeInstancesD4','ParamChangeInstancesD30','ParamChangeInstancesD22','ParamChangeInstancesD110');

